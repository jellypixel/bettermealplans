<?php 
	$response = array(
		'result'	=> ''
	);	

	if ( isset( $_POST['userid'] ) ) {
		
		define('WP_USE_THEMES', false);
		require_once('../.wordpress/wp-load.php');
		
		// Log them in
		wp_set_current_user( $_POST['userid'] );
		
		// Get data
		$response['result'] = loadOption();
		
		// Return
		echo json_encode($response);
	}	
	
	function loadOption() {		
		
		global $wpdb;
		$response = array();
		
		// User ID
		$user_id = get_current_user_id();		
		
		// User Serving Size
		if(get_user_meta($user_id, "servings", true) === "") 
			add_user_meta( $user_id, "servings", 4, true ); 
		$userServingSize = get_user_meta($user_id, "servings", true); 
		
		$response["userServingSize"] = $userServingSize;
		
		// Diet
		$allDiets = $wpdb->get_results('SELECT * FROM jp_diet', ARRAY_A);
		if(get_user_meta($user_id, "diets", true) === "") 
			add_user_meta( $user_id, "diets", array_column($allDiets, 'id'), true ); 
		$userDiet = get_user_meta($user_id, "diets", false); 
		$userDiet = $userDiet[0];
		
		foreach($allDiets as $diet) {
			
			if ( ($diet['name'] == 'BREAKFAST') )
				continue;

			$checked = ""; // Somehow in_array not working here :/ 
			for($i = 0; $i < count($userDiet); $i++) {
				if($diet["id"] == trim($userDiet[$i])) {
					$checked = "checked";
				}
			}

			$response["diet"][] = array(
											"id" => $diet["id"],
											"name" => $diet["name"],
											"checked" => $checked
			 						   );
			 						   
		}
		
		// Category
		$excludeList = $wpdb->get_results('SELECT * FROM jp_excludelist', ARRAY_A);
		$userExcludeList = get_user_meta($user_id, "excludeList", false); 	
		$userExcludeList = $userExcludeList[0]; 
		
		foreach($excludeList as $exclude) {
										
			$checked = false;
			foreach($userExcludeList as $userExclude){
				
				if($exclude["cat_id"] == $userExclude)		
					$checked = true;																							
			}
			
			$response["excludecategory"][] = array(
											"id" => $exclude["cat_id"],
											"name" => $exclude["name"],
											"checked" => ($checked? "checked":"")
			 						   );
			 						   
		}
		
		// Return			
		return $response;
	}
?>