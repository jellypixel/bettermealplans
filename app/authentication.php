<?php 
	require_once('../.wordpress/wp-load.php');
	 
	$response = array(
		'data'		=> array(),
		'msg'		=> 'Invalid email or password',
		'status'	=> false
	);
	 
	/* Sanitize all received posts */
	foreach($_POST as $k => $value){
		$_POST[$k] = sanitize_text_field($value);
	}
	
	/**
	 * Login Method
	 *
	 */
	if( isset( $_POST['type'] ) &&  $_POST['type'] == 'login' )
	{
	 
		/* Get user data */
		$user = get_user_by( 'email', $_POST['email'] );

		if(!$user)
			$user = get_user_by( 'login', $_POST['email'] ); 
		
		if ( $user )
		{
			$password_check = wp_check_password( $_POST['password'], $user->user_pass, $user->ID );
		
			if ( $password_check )
			{
				/* Generate a unique auth token */
				$token = md5( $_POST['email'] . $_POST['password'] . rand() );
	 
				/* Store / Update auth token in the database */
				if( update_user_meta( $user->ID, 'auth_token', $token ) )
				{					
					/* Return generated token and user ID*/
					$response['status'] = 1;
					$response['data'] = array(
						'auth_token' 	=>	$token,
						'user_id'		=>	$user->ID,
						'user_login'	=>	$user->user_login
					);
					$response['msg'] = 'Successfully Authenticated';
					
					echo json_encode($response);	
				}
			} 
			else 
			{
			
				$response['status'] = 2;
				$response['msg'] = 'Wrong Password';
				
				echo json_encode($response);	
			}
		} 
		else 
		{			
			$response['status'] = 3;
			$response['msg'] = 'Wrong User';
			
			echo json_encode($response);	
		}
	} 
	else 
	{			
		$response['status'] = 4;
		$response['msg'] = 'Empty User / Password';
		
		echo json_encode( $response );	
	}
	
	exit();
?>