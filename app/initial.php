<?php
	$response = array();	

	$recipe1El = array();
	$recipe2El = array();
	$recipe3El = array();
	
	$recipe1El["name"] = 'Apple';
	$recipe2El["name"] = 'Banana';
	$recipe3El["name"] = 'Cherry';
	
	$response["recipe"][] = $recipe1El;
	$response["recipe"][] = $recipe2El;
	$response["recipe"][] = $recipe3El;

	echo json_encode($response);	
?>