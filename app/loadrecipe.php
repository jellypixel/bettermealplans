<?php
	$response = array(
		'result'	=> ''
	);	

	if(!isset( $_POST['cat'] )) {
		$_POST['cat'] = -1;
	}

	if ( isset( $_POST['searchTerm'] ) && isset( $_POST['pageNum'] ) && isset( $_POST['totalRecipes'] ) && isset( $_POST['first'] ) && isset( $_POST['cat'] ) && isset( $_POST['infinitescroll'] ) && isset( $_POST['userid'] ) ) {

		define('WP_USE_THEMES', false);
		require_once('../.wordpress/wp-load.php');
		require_once( 'previewrecipe.php' );
		
		// Log them in
		wp_set_current_user( $_POST['userid'] );
		
		$ajaxResult = loadRecipe($_POST['searchTerm'], $_POST['pageNum'], $_POST['totalRecipes'], $_POST['first'], $_POST['cat'], $_POST['infinitescroll']);
		
		// Main Return // echo $ajaxResult;
		$response['result'] = $ajaxResult;
		echo json_encode($response);
		
	} 		
	
	function title_filter( $where, &$wp_query )
	{
		global $wpdb;
		
		$current_user = wp_get_current_user();
		$userDiet = get_user_meta($current_user->ID, "diets", false);
		$userDiet = $wpdb->get_results("SELECT name FROM jp_diet WHERE ID in(".implode(",", $userDiet[0]).")", ARRAY_A);
		$userDiet = array_map('array_pop', $userDiet);	
		
		$where .= " AND (wtt.taxonomy IN ('category') AND wt.slug IN ("."'".implode("','", str_replace(" ", "-", $userDiet)). "'".") )";
		if ( $search_term = $wp_query->get( 'search_recipe_title' ) ) {
			$where .= ' AND (' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
			$where .= ' OR ' . $wpdb->postmeta . '.meta_value LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\')';
		}
		
		return $where;
	}
	
	function ingredient_filter( $where, &$wp_query )
	{
		global $wpdb;
		
		$current_user = wp_get_current_user();
		$exingredients = get_user_meta($current_user->ID, "exingredients", false); 
		$exingredients = $exingredients[0];		
		
		if( !empty($exingredients) ) {
		
		    /*
			$where .= " AND wp_posts.ID NOT IN (SELECT DISTINCT a.meta_value as 'ID' from wp_postmeta a, wp_postmeta b where a.post_id = b.post_id AND a.meta_key= 'wprm_parent_post_id' AND (";
			
			foreach($exingredients as $exingredient) 
					$where .= " ( b.meta_key = 'wprm_ingredients' AND CAST(b.meta_value AS CHAR) LIKE '%" . $exingredient . "%' ) OR ";			
			$where = rtrim($where, 'OR ');
			$where .= ")) ";
			*/
			
			$where .= " AND wp_posts.ID NOT IN (";
			
			$excludedPostSQL = "SELECT DISTINCT a.meta_value as 'ID' from wp_postmeta a, wp_postmeta b where a.post_id = b.post_id AND a.meta_key= 'wprm_parent_post_id' AND (";
			
			foreach($exingredients as $exingredient) 
					$excludedPostSQL .= " ( b.meta_key = 'wprm_ingredients' AND CAST(b.meta_value AS CHAR) LIKE '%" . $exingredient . "%' ) OR ";			
			$excludedPostSQL = rtrim($excludedPostSQL, 'OR ');
			$excludedPostSQL .= ") ";
			
			$excludedPosts = $wpdb->get_results($excludedPostSQL);
			
			foreach($excludedPosts as $excludedPost) 
				$where .= $excludedPost->ID . ", ";
			$where = rtrim($where, ', ');	
			$where .= ")"; 
		}
		
		return $where;
	}		
	
	function custom_posts_join($join){
		global $wpdb;
		
		$join .= " INNER JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id ";
		$join .= " INNER JOIN $wpdb->term_relationships as wtr ON ($wpdb->posts.ID = wtr.object_id) ";
		$join .= " INNER JOIN $wpdb->term_taxonomy as wtt ON (wtr.term_taxonomy_id = wtt.term_taxonomy_id) ";
		$join .= " INNER JOIN $wpdb->terms as wt ON(wtt.term_id = wt.term_id) ";

		return $join;
	}
	
	function search_distinct() {
		return "DISTINCT";
	}	
	
	function loadRecipe($search, $pageNum, $totalRecipes, $first, $cat, $infinitescroll) {		
		global $wp_query;
		global $wpdb;
		global $post;
		global $paged;
		
		$current_user = wp_get_current_user();			
		$user_id = $current_user->ID;
				
		// Find every fav only on first load
		$everyFav = array();		
		if ( $first == 'first' ) {
			$sql = $wpdb->prepare( 'SELECT a.ID, b.*
									FROM wp_posts a
									INNER JOIN wp_usermeta b
									ON CONCAT("favorite-", a.ID) = b.meta_key
									WHERE b.meta_value = "yes" AND
										b.user_id = %d AND
										a.post_type = "post" AND
										a.post_status = "publish"
									ORDER BY a.post_title asc
									', $user_id ); 
		
			$result = $wpdb->get_results( $sql );
			
			if ( $result ) {			
				foreach ( $result as $key ){
					$everyFav[] = $key->ID;
				}			
			}
		}
		else
		{
			// Even if it's not fav list request, let's check it anyway
			$sqlfav = $wpdb->prepare( 'SELECT a.ID, b.*
									FROM wp_posts a
									INNER JOIN wp_usermeta b
									ON CONCAT("favorite-", a.ID) = b.meta_key
									WHERE b.meta_value = "yes" AND
										b.user_id = %d AND
										a.post_type = "post" AND
										a.post_status = "publish"
									ORDER BY a.post_title asc
									', $user_id ); 
		
			$resultfav = $wpdb->get_results( $sqlfav );
			
			if ( $resultfav ) {			
				foreach ( $resultfav as $key ){
					$everyFav[] = $key->ID;
				}			
			}
		}
		
		// Paging
		if ( get_query_var('paged') ) { 
			$paged = get_query_var('paged');
		}
		if ( get_query_var('page') ) {
			$paged = get_query_var('page');
		}
		
		if ( empty( $paged ) ) {
			$paged = 1;	
		}
		
		if ( !empty( $paged ) ) {
			$paged = $pageNum;	
		}
		
		if ( empty( $totalRecipes ) ) {
			$totalRecipes = 16;	
		}
		
		global $randommaxpost;			
		$randommaxpost = $totalRecipes;
		
		$userExcludeList = get_user_meta($current_user->ID, "excludeList", false); 	
		$userExcludeList = $userExcludeList[0];	
		
		// Excluded Specials
		$userDiet = get_user_meta($current_user->ID, "diets", false);
		$userDiet = $wpdb->get_results("SELECT name FROM jp_diet WHERE ID in(".implode(",", $userDiet[0]).")", ARRAY_A);
		$userDiet = array_map('array_pop', $userDiet);
		
		$args = array (					
			'post_type'		 			=> 'post',
			'posts_per_page' 			=> $totalRecipes,
			'search_recipe_title'	=> $search,
			'paged'						=> $paged,
			'category__not_in' 		=> $userExcludeList
		);
		
		// Fav conditional, first load only
		if ( $first == 'first' ) {
			
			if(!empty( $everyFav)) {
				$args['post__in'] = $everyFav;	
				$args['orderby'] = 'title';	
				$args['order'] = 'asc';	
			}
			else{
					echo '<div class="searchrecipe-empty">No recipes based on your settings. :(</div>';	
					return;
				}
		}
		// If not first, check category combobox
		else {
			if ( $cat != '-1' ) {
				
				$arr_cat = explode(",", $cat);
				
				//$args['cat'] = $cat;
				$args['category__and'] = $arr_cat;
			}
		}
		
		add_filter( 'posts_join' , 'custom_posts_join');
		add_filter( 'posts_where', 'title_filter', 10, 2 );
		add_filter( 'posts_where', 'ingredient_filter', 10, 2 );
		add_filter( 'posts_distinct', 'search_distinct');
		
		// First Query - All Post ID		
		$argsFirstQuery = $args;
		$argsFirstQuery['fields'] = 'ids';
		$argsFirstQuery['posts_per_page'] = -1;	
		$first_ids = get_posts( $argsFirstQuery );
		
		// Second Query - 10 Random Post
		$argsSecondQuery = $args;
		$argsSecondQuery['fields'] = 'ids';
		$argsSecondQuery['orderby'] = 'date';
		$argsSecondQuery['posts_per_page'] = $totalRecipes;		
		
		$second_ids = get_posts( $argsSecondQuery );
		
		// Create WP Query Merge if not searching / by category
		if ( !( $cat != '-1' || !empty( $search ) ) )
		{
			$post_ids = array_unique( array_merge( $second_ids, $first_ids ) );
			$args['post__in'] = $post_ids;
			$args['orderby'] = 'post__in';
		}
		
		$wp_query = new WP_Query( $args );

		// Max Page Num		
		$max = $wp_query->max_num_pages;
		
		/* TWI - UNUSED IN APP
		$requestedRecipes = '';
		if ( $infinitescroll == '' )
		{
			$requestedRecipes = '<div class="searchrecipe-stage" max_page="' . $max . '">';	
		}
		*/
		$totalRecipes = 0;
		$allRecipesArr = array();
		
		if ( $wp_query->have_posts() ) 
		{
			while ( $wp_query->have_posts() ) 
			{
				$wp_query->the_post();

				// Get Blobmeta
				$recipe = new TwentyDishes_Recipe(get_the_ID());
				
				$title = $recipe->title; 
				$yield = $recipe->yield;
				$readytime = $recipe->readytime; 
				$cooktime = $recipe->cooktime; 
				$diff = $recipe->difficulty; 
				$thumb = $recipe->thumbmedium;
				$ingredients = $recipe->ingredients;
				$instructions = $recipe->instructions;
				$cats = $recipe->cats;
				$nutritional = reverseTransform($recipe->nutritional);
				$star = $recipe->getFavClass();

				// Cut The Title
				$titlecount = 10; $resultTitle = $title;
				if ( str_word_count($title, 0) > $titlecount ) {
					$words = str_word_count( $title, 2 );
					$pos = array_keys( $words );
					$resultTitle = substr( $title, 0, $pos[$titlecount] ) . '...';
				}
				
				$deblob = '@^@';
				$blob = $title . $deblob . $yield . $deblob . $readytime . $deblob . $thumb . $deblob . $ingredients . $deblob . $instructions . $deblob . $cats . $deblob . $isFav;
				$blob = htmlspecialchars($blob, ENT_QUOTES, 'UTF-8');
				
				/* TWI - UNUSED IN APP
				$requestedRecipes .= '<div class="searchrecipe" style="background-image:url(' . $thumb . ');" " recipeid="' . $post->ID . '">
						<div class="searchrecipe-overlay">
						' . $resultTitle . '
						</div>				
						<div class="searchrecipe-addbutton" recipeid="' . $post->ID . '" blobmeta="' . $blob . '">
							Add
						</div>	
						<div class="searchrecipe-fav">
							<div class="fav'. $recipe->getFavClass() .'" recipeid="' . $post->ID . '"></div>
						</div>						
					</div>';										 
				*/
				
				$totalRecipes++;
				
				$recipeEl = array();
				$recipeEl["id"] = $post->ID;
				$recipeEl["name"] = $resultTitle;
				$recipeEl["thumb"] = $recipe->thumbmedium;
				$recipeEl["fav"] = $recipe->fav;
				$recipeEl["cats"] = $cats[0]->name;
				$recipeEl["detail"] = previewrecipe( $post->ID );	
				
				$allRecipesArr["recipe"][] = $recipeEl;	
			}																
		}
		
		/* TWI - UNUSED IN APP
		$requestedRecipes .= 	''; // Remnant of closing div
		if ( $infinitescroll == '' )
			$requestedRecipes .= '</div>';	
		*/
		
		remove_filter( 'posts_join' , 'custom_posts_join');
		remove_filter( 'posts_where', 'title_filter', 10, 2 );
		remove_filter( 'posts_where', 'ingredient_filter', 10, 2 );
		remove_filter( 'posts_distinct', 'search_distinct');
		
		wp_reset_query();	
		
		if ( $totalRecipes > 0 ) 		
			//echo $requestedRecipes;
			return $allRecipesArr;
		else 
			/* TWI - UNUSED IN APP
			echo '<div class="searchrecipe-empty">No recipes based on your settings. :(</div>';	
			*/
			return "-1";

	}
?>