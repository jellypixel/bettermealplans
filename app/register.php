<?php 
    /*  
        TODO:
        1. Detect payment from this user
        2. Add tags
	*/
	require_once('../.wordpress/wp-load.php');
	 
	$response = array(
		'data'		=> array(),
		'msg'		=> 'Unknown error',
		'status'	=> false
	);

	/* Sanitize all received posts */
	foreach($_POST as $k => $value){
		$_POST[$k] = sanitize_text_field($value);
	}
	
	/**
	 * Register Method
	 *
	 */
	 
	 /* Get user data */
	if( isset( $_POST['firstname'] ) &&  && isset( $_POST['email'] ) &&  isset( $_POST['password'] ))
	{
	 
		/* Get user data */
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$response['status'] = 5;
			$response['msg'] = 'Invalid Email';
			
			echo json_encode( $response );
		}
		
		$user = get_user_by( 'email', $email );

		if ( !$user )
		{
		    
		    $addtoCRM = AccessAllyMembershipUtilities::add_contact($firstname, $lastname, $email, null, null);
			
			if (!is_array($addtoCRM)) {
				$response['status'] = 2;
				$response['msg'] = 'Unknown error';
				
				echo json_encode($response);	
			} else {
			    
			    try {
			       AccessAlly::add_user_and_password($addtoCRM["Id"], true, false);
    			    AccessAllyMembershipUtilities::set_contact_data($addtoCRM["Id"], array($password_field_id => $password));
    			    // AccessAllyMembershipUtilities::add_contact_tags($addtoCRM["Id"], $tags_ids_to_add);
    			    
    			   $response['status'] = 1;
    				$response['msg'] = 'Success';
    				
    				/* Return generated token and user ID*/
    				$user = get_user_by( 'email', $email );
    				$token = md5( $email . $password . rand() );
    				
    				$response['status'] = 1;
    				$response['data'] = array(
    					'auth_token' 	=>	$token,
    					'user_id'		=>	$user->ID,
    					'user_login'	=>	$user->user_login
    				);
    				
    				$response['msg'] = 'Successfully Registered and Authenticated';
    				
    				echo json_encode($response);
			        
			    } catch (Exception $ex) {
			        $response['status'] = 2;
    				$response['msg'] = 'Unknown error';
    				
    				echo json_encode($response);
			        
			    }
			    	
			}
		} 
		else 
		{			
			$response['status'] = 3;
			$response['msg'] = 'User Registered';
			
			echo json_encode($response);	
		}
	} 
	else 
	{			
		$response['status'] = 4;
		$response['msg'] = 'Empty Fields';
		
		echo json_encode( $response );	
	}
	
	exit();
?>