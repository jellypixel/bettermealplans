<?php
	/*
		Template Name: Webinar Sales
	*/
	
	get_header();	
?>

    <style>
		.navbar, .logo, .page-bg, .nc_socialPanel, .general-wrapper, .footer {
			display: none;
		}
		
		.wssection1 {
			margin-top: 0px;
		}
	</style>

	<div class="beginpage webinarsalespage">
    	<div class="wssection1">
        	<div class="wssection1-background" style="background-image:url(<?php the_field( 'section1_image' ); ?>);">
            	<div class="wssection1-overlay">
                    <div class="wssection1-inner">
                    	<div class="container">
                            <div class="wssection1-line1">
                                <?php the_field( 'section1_line_1' ); ?>
                            </div>
                            <div class="wssection1-line2">
                                <?php the_field( 'section1_line_2' ); ?>
                            </div>
                            
                            <!--
                            <div class="wssection1-counter">
                                <?php get_field( 'section2_counter' ); ?>
                            </div>
                            -->
                            
                            <div>
                                <?php echo do_shortcode( '[ec id="1"]' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="wssection2">
        	<div class="container">
            	<div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">
                        <div class="wssection2-title">
                            <?php
                                the_field( 'section2_title' );
                            ?>
                        </div>
                        <div class="wssection2-content">
                            <?php
                                the_field( 'section2_content' );
                            ?>
                        </div>
                        <div class="wssection2-quote">
                            <?php
                                the_field( 'section2_quote' );
                            ?>
                        </div>
                	</div>
                </div>
            </div>
        </div>
        
        <div class="wssection3">
        	<div class="container">
            	<div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">
                        <div class="wssection3-content">
                            <?php
                                the_field( 'section3_content' );
                            ?>
                        </div>
                        <div class="wssection3-list">
                            <?php
                                if( have_rows( 'section3_list' ) )
                                {
                                    ?>
                                        <ul>
                                            <?php
                                                while( have_rows( 'section3_list') ) 
                                                {
                                                    the_row(); 
                                                    
                                                    echo '<li>' . get_sub_field( 'list' ) . '</li>';
                                                }
                                            ?>
                                        </ul>
                                    <?php
                                }
                            ?>
                        </div>
                	</div>
                </div>
            </div>
        </div>
        
        <div class="wssection4">
        	<div class="container">
            	<div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">
                        <div class="wssection4-join">
                            <?php
                                the_field( 'section4_join' );
                            ?>
                        </div>
                        <div class="wssection4-normal-fee">
                            <?php
                                the_field( 'section4_normal_fee' );
                            ?>
                        </div>
                        <div class="wssection4-content">
                            <?php
                                the_field( 'section4_content' );
                            ?>
                        </div>
                        <!--
                        <div class="wssection4-join2">
                        -->
                        <div class="section1-tagline-wrapper">
                        	<div class="section1-tagline">
								<?php
                                    the_field( 'section4_join_2' );
                                ?>
                            </div>
                        </div>
                        <div class="wssection4-arrow">
                            <img src="<?php the_field( 'section4_arrow' ); ?>" alt="Arrow" />
                        </div>
                        <div class="wssection4-footer">
                            <?php the_field( 'form_shortcode' ); ?>
                        </div>
                	</div>
                </div>
            </div>
        </div>
        
    </div>   
    
    <!--
    <script>
		var $j = jQuery.noConflict();
		
		$j(function(){
			var clock;
		
			var currentDate = new Date();
			var futureDate  = new Date('<?php echo get_field( 'section2_counter' ); ?>');
			var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;
		
			clock = $j('.wssection1-counter').FlipClock(diff, {
				clockFace: 'DailyCounter',
				countdown: true
			});
		});
	</script>
	-->
            
<?php
	get_footer();
?>