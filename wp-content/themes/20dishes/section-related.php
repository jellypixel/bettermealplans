<?php
	$currentPost = $post;  
	global $post;  	
	$arrColumn = null;
	$displayPost = 3;
	
	$tags = wp_get_post_tags($post->ID);  		
	
	if ( $tags ) {  
		$arrTags = array();  
		$arrSmallHeading = array();
		
		foreach( $tags as $tag ) 
		{ 
			$arrTags[] = $tag->term_id; 
		}
		
		
		$args = array(  			
			'tag__in'			=> $arrTags,  
			'post__not_in'		=> array( $post->ID ),				
			'caller_get_posts'	=> 1
		);  
	  
		$wp_query = new WP_Query( $args );  
		
		if ( $wp_query->have_posts() ) 
		{	
			
		
			$relatedCount = $wp_query->post_count;
			
			if ( $relatedCount >= $displayPost ) {
				$relatedCount = $displayPost;	
			}
			
			// Column Size
			/*if ( $relatedCount == 1 ) {
				$arrColumn = array( '6 smaller' );
			}
			else if ( $relatedCount == 2 ) {
				$arrColumn = array( '6 smaller', '6 smaller' );	
			}
			else if ( $relatedCount == 3 ) {
				if ( $sidebarPosition == 'none' ) 
				{
					$arrColumn = array( 7, '5 smaller', '5 smaller' );	
				}
				else 
				{
					$arrColumn = array( 7, '5 smaller no-meta', '5 smaller no-meta' );		
				}
			}			
			else if ( $relatedCount == 4 ) {
				if ( $sidebarPosition == 'none' ) {
					$arrColumn = array( '3 smaller no-meta', '3 smaller no-meta', '3 smaller no-meta', '3 smaller no-meta' );	
				}
				else 
				{
					$arrColumn = array( '6 smaller', '6 smaller', '6 smaller', '6 smaller' );		
				}
			}
			else if ( $relatedCount == 5 ) {
				$arrColumn = array( 6, '3 smaller no-meta', '3 smaller no-meta', '3 smaller no-meta', '3 smaller no-meta' );	
			}
			else {
				$arrColumn = array( 6, '3 smaller no-meta', '3 smaller no-meta', '3 smaller no-meta', '3 smaller no-meta' );	
			}*/
			
			if ( $relatedCount == 1 ) {
				$arrColumn = array( 12 );	
			}
			else if ( $relatedCount == 2 ) {
				$arrColumn = array( 6, 6 );	
			}
			else if ( $relatedCount >= 3 ) {
				$arrColumn = array( 3, 3, 3 );	
			}
			
			// Column Height
			/*if ( $sidebarPosition == 'none' ) {
				$heightClass = 'wider';
			}
			else {
				$heightClass = '';	
			}*/
			
			// Section
			?>
				<h1 class="post-related">
					<?php echo __( 'Related Post', 'jellypixel' ); ?>
				</h1>
				
				<nav class="row row-thin nav-related">	
				<?php
					$i = 0;
				
					while( $wp_query->have_posts() && $i < $displayPost ) 
					{  
						$wp_query->the_post();  
						
						// Title
						$titlecount = 5;
						$getTitle = get_the_title();						
						
						if ( $titlecount != 'all' && str_word_count($getTitle, 0) > $titlecount ) {
							$words = str_word_count( $getTitle, 2 );
							$pos = array_keys( $words );
							$resultTitle = substr( $getTitle, 0, $pos[$titlecount] ) . '...';
						}
						else {
							$resultTitle = $getTitle;
						}					
						
						/*$arrSmallHeading = array(
							'3 smaller',
							'3 smaller no-meta',
							'5 smaller',
							'5 smaller no-meta',
							'6 smaller'						
						);

						if ( in_array( ( string )$arrColumn[$i], $arrSmallHeading ) )
						{
							$titleEl = '<h6 class="post-title sans">' . $resultTitle . '</h6>';
						}
						else 
						{													
							$titleEl = '<h4 class="post-title sans">' . $resultTitle . '</h4>';							
						}*/						
						
						// Thumbnail												
						$thumbUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'gallery-thumb' );
						$thumbUrl = $thumbUrl[0];
						
						if ( empty( $thumbUrl ) ) 
						{
							$thumbUrl = '';
						}						
						
						// Compromise for extra space
						/*if ( $arrColumn[$i] === '3 smaller' ) 
						{
							$columnClass = 'col-lg-3 col-md-3 col-sm-3 col-xs-12 smaller ' . $heightClass;							
						}
						else if ( $arrColumn[$i] === '5 smaller' ) 
						{
							$columnClass = 'col-lg-5 col-md-5 col-sm-5 col-xs-12 smaller ' . $heightClass;							
						}
						else if ( $arrColumn[$i] === '5 smaller no-meta' ) 
						{
							$columnClass = 'col-lg-5 col-md-5 col-sm-5 col-xs-12 smaller no-meta ' . $heightClass;							
						}
						else if ( $arrColumn[$i] === '6 smaller' ) 
						{
							$columnClass = 'col-lg-6 col-md-6 col-sm-6 col-xs-12 smaller ' . $heightClass;							
						}						
						else 
						{							
							$columnClass = 'col-lg-' . $arrColumn[$i] . ' col-md-' . $arrColumn[$i] . ' col-sm-' . $arrColumn[$i] . ' col-xs-12 ' . $heightClass;
						}*/
												
						$columnClass = 'col-lg-' . $arrColumn[$i] . ' col-md-' . $arrColumn[$i] . ' col-sm-' . $arrColumn[$i] . ' col-xs-12';
												
						?> 
							<div class="post-related-wrapper <?php echo $columnClass; ?>">  
                            	<div class="post-related-inner-wrapper">
                                    <a href="<?php the_permalink(); ?>" title="<?php echo $getTitle; ?>">
                                        <div class="post-related-thumb" style="background-image:url(<?php echo $thumbUrl; ?>);">                                            
                                        </div>
                                    </a>                                    
                                    <div class="post-related-bottom">
                                    	<div class="post-related-title">
                                            <a href="<?php the_permalink(); ?>" title="<?php echo $getTitle; ?>">
                                                <?php echo $resultTitle; ?>
                                            </a>
                                        </div>
                                    </div>	                                                                     
                                </div>	
							</div>  				  
						<?php
						 
						$i++;
					}  
				?>
				</nav>
			<?php
		}
	}  
	
	$post = $currentPost;  
	wp_reset_query();  
?>
