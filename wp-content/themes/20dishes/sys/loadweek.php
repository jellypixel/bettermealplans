<?php
	require_once 'globalfunction.php';

	if ( isset( $_POST['targetDate'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
		
		$ajaxResult = loadweek($_POST['targetDate']);
	
		echo $ajaxResult;
	}	
	
	function loadWeek($targetDate, $userpassed = 0) {		
		global $wpdb;
		        
		$current_user = checkCorporateAccount();
		$user_id = $current_user;

		if($userpassed != 0)
			$current_user = $user_id = $userpassed;
		
		// Master / Slave
		$master = false;	
		if ( $current_user != get_current_user_id() )
		{
			$master = true;
		}
		
		// Elements
		$addrecipeElSunday = 
			'<div class="addcard" data-step="5" data-intro="Click here to choose another recipe from the database">
				Add Recipe
			</div>';
			
		$addrecipeEl = 
			'<div class="addcard">
				Add Recipe
			</div>';
			
		$addnoteEl = 
			'<div class="addcard addnote">
				Add Note
			</div>';
			
		$removerecipeEl =
			'<div class="recipe-remove" title="Remove">
				<div class="icon tdicon-thin-close"></div>										
			</div>';
			
		$removerecipeClass = '';
			
		// Slave Only Restriction				
		if ( !$master )
		{
			$addrecipe = get_user_meta( $user_id, 'restrict-addrecipe', true );
			$addnote = get_user_meta( $user_id, 'restrict-addnote', true );
			$removerecipe = get_user_meta( $user_id, 'restrict-removerecipe', true );
			
			if ( $addrecipe == 'true' )
			{
				$addrecipeElSunday = $addrecipeEl = '';
			}
			
			if ( $addnote == 'true' )
			{
				$addnoteEl = '';
			}
			
			if ( $removerecipe == 'true' )
			{
				$removerecipeEl = '';
				$removerecipeClass = 'noremove';
			}
		}
		
		// Query Diets 
		$queryDiets = '';
		$arrDiet = get_user_meta( $current_user, "diets", false);
		$arrDiet = $arrDiet[0];
		
		if(empty($arrDiet))
			return "<div class='error-warning'>No diet selected. Please select at least one mode of diet on the options tab.</div>";
		
		foreach ($arrDiet as $value) {
			if ( $queryDiets == '' ) {
				$queryDiets = $value;		
			}
			else {
				$queryDiets .= ',' . $value;	
			}			
		}
		
		if ( !empty( $queryDiets ) ) {
			$queryDiets = 'WHERE z.id IN (' . $queryDiets . ')';	
		}
		
		// Target Date and Date Formatting
		if ( empty( $targetDate ) ) {
			$targetDateQuery = 'CURDATE()';	
			$targetDate = date( 'Y-m-d' );
		}
		else {
			$targetDateQuery = '"' . $targetDate . '"';
		}
					
		$targetDateFormatted = date( 'm/d/Y', strtotime( $targetDate ) );
		$targetDay = date( 'w', strtotime( $targetDate ) );
		
		$targetSunday = date( 'Y-m-d', strtotime( $targetDateFormatted . '-' . $targetDay . ' days' ) );
		$targetMonday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 1 - $targetDay ) . ' days') );
		$targetTuesday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 2 - $targetDay ) . ' days') );
		$targetWednesday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 3 - $targetDay ) . ' days') );
		$targetThursday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 4 - $targetDay ) . ' days') );
		$targetFriday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 5 - $targetDay ) . ' days') );
		$targetSaturday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 6 - $targetDay ) . ' days') );
		
		$targetSimpleSunday = date( 'F j', strtotime( $targetDateFormatted . '-' . $targetDay . ' days' ) );
		$targetSimpleMonday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 1 - $targetDay ) . ' days') );
		$targetSimpleTuesday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 2 - $targetDay ) . ' days') );
		$targetSimpleWednesday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 3 - $targetDay ) . ' days') );
		$targetSimpleThursday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 4 - $targetDay ) . ' days') );
		$targetSimpleFriday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 5 - $targetDay ) . ' days') );
		$targetSimpleSaturday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 6 - $targetDay ) . ' days') );
		
		$sundayDay = 'Day 1'; //'Sunday';
		$sundayDate = $targetSimpleSunday;
		
		$mondayDay = 'Day 2'; //'Monday';
		$mondayDate = $targetSimpleMonday;
		
		$tuesdayDay = 'Day 3'; //'Tuesday';
		$tuesdayDate = $targetSimpleTuesday;
		
		$wednesdayDay = 'Day 4'; //'Wednesday';
		$wednesdayDate = $targetSimpleWednesday;
		
		$thursdayDay = 'Day 5'; //'Thursday';
		$thursdayDate = $targetSimpleThursday;
		
		$fridayDay = 'Day 6'; //'Friday';
		$fridayDate = $targetSimpleFriday;
		
		$saturdayDay = 'Day 7'; //'Saturday';
		$saturdayDate = $targetSimpleSaturday;
		
		// Get the word exclusion from database
		//$current_user = wp_get_current_user();
		$exingredients = get_user_meta($current_user, "exingredients", false); 
		$exingredients = $exingredients[0];				
		
		$exWhere = " ";

		if( is_array( $exingredients ) )
		{
			if(count($exingredients) >= 1) {
				
				$exWhere .= " WHERE a.recipe_id NOT IN (SELECT DISTINCT a.meta_value as 'ID' from wp_postmeta a, wp_postmeta b where a.post_id = b.post_id AND a.meta_key= 'wprm_parent_post_id' AND (";
				foreach($exingredients as $exingredient) 
					$exWhere .= " ( b.meta_key = 'wprm_ingredients' AND CAST(b.meta_value AS CHAR) LIKE '%" . $exingredient . "%' ) OR ";			
				
				$exWhere = rtrim($exWhere, 'OR ');
				$exWhere .= ")) ";
				
			}							
		}
		// Execution
		$sql = ( "
				SELECT * FROM (
					SELECT * FROM (
						SELECT x.*, z.name AS menu_name, z.id AS diet_id, z.colordark as diet_colordark, z.colorlight as diet_colorlight
						FROM
						(
							SELECT user_id, recipe_id, menu_id, tanggal, MIN(orders) as 'orders', notes, DAYOFWEEK(tanggal) AS date_order
							FROM jp_userschedule 
							WHERE 
								user_id = " . $user_id . " AND
								YEARWEEK(tanggal, 0) = YEARWEEK(" . $targetDateQuery . ", 0)	
							GROUP BY user_id, recipe_id, menu_id, tanggal, notes	
						) x
						LEFT JOIN jp_menu y
						ON x.menu_id = y.id
						LEFT JOIN jp_diet z
						ON y.diet_id = z.id
						" . $queryDiets . "
					) a
					UNION
					SELECT * FROM (
						SELECT x.*, z.name AS menu_name, z.id AS diet_id, z.colordark as diet_colordark, z.colorlight as diet_colorlight
						FROM
						(
							SELECT user_id, recipe_id, menu_id, tanggal, MIN(orders) as 'orders', notes, DAYOFWEEK(tanggal) AS date_order
							FROM jp_userschedule 
							WHERE 
								user_id = " . $user_id . " AND
								YEARWEEK(tanggal, 0) = YEARWEEK(" . $targetDateQuery . ", 0) 	
							GROUP BY user_id, recipe_id, menu_id, tanggal, notes							
						) x
						LEFT JOIN jp_menu y
						ON x.menu_id = y.id
						LEFT JOIN jp_diet z
						ON y.diet_id = z.id
						WHERE x.menu_id IN (0, -1)
					) b
				) a
				" . $exWhere . " 
				 ORDER BY a.tanggal, a.orders ASC
				"); 
				
		$result = $wpdb->get_results( $sql );
		
		/*Get color based on diet*/
		$allDiets = $wpdb->get_results('SELECT name, colordark, colorlight FROM jp_diet', ARRAY_A);
		$allDietsName = array_column($allDiets, 'name');		
		
		/*Get exclude list*/
		$userExcludeList = get_user_meta( $current_user, "excludeList", false); 	
		$userExcludeList = $userExcludeList[0];		
		
		if ( $result ) {			
			
			$sundayEl = $mondayEl = $tuesdayEl = $wednesdayEl = $thursdayEl = $fridayEl = $saturdayEl = '';
			$sundayNotes = $mondayNotes = $tuesdayNotes = $wednesdayNotes = $thursdayNotes = $fridayNotes = $saturdayNotes = '';

			$previousRecipe = -99;
			
			$day1 = array();
			$day2 = array();
			$day3 = array();
			$day4 = array();
			$day5 = array();
			$day6 = array();
			$day7 = array();
			
			foreach ( $result as $key ){
				$post_categories = wp_get_post_categories( $key->recipe_id );
				
				$excluded = false;
				foreach($post_categories as $c) {
					
					foreach($userExcludeList as $d) {
						
						if($d == $c)
							$excluded = true;
					}
				}
							
				if(!$excluded) {
					
					$recipe = ''; $titleRecipe = 'Notes'; $thumbnailUrl = '';
					if($key->recipe_id != 0) {
						$recipe = new TwentyDishes_Recipe($key->recipe_id);
						if($recipe->title == "recipe deleted")
					 		continue;
					 
						$titleRecipe = $recipe->title;
						$thumbnailUrl = $recipe->thumbsmall;
					} 
							
					$tanggalDateOnly = date_create( $key->tanggal );							
					$tanggalRecipe = date_format( $tanggalDateOnly, 'F j' );
					$tanggalRecipemd = date_format( $tanggalDateOnly, 'm.d' );
					$tanggalRecipeD = date_format( $tanggalDateOnly, 'D' );
					
					$dayRecipe = date_format( $tanggalDateOnly, 'l' );					
					$dateorderRecipe = $key->date_order;
					$ordersRecipe = $key->orders;
					
					$notes = $key->notes;
					
					$recipeId = $key->recipe_id;
					$menuId = $key->menu_id;
					$tanggal = $key->tanggal;
	
					$cardColor = 'background-color:#ffffff; color:#444444; border:1px solid #e8e8e8;';
					$buttonColor = 'color:#555555;';			
					
					$notesEl = '';
					$recipeEl = '';
					
					if ( !empty( $notes ) ) {
						// ganti $notesEl klo mo
						$recipeEl = '<li class="notes">
										<div class="notes-handle">
										</div>
										<div class="notes-content">
											' . $notes . '
										</div>
										<div class="notes-remove">
											<div class="tdicon-thin-close"></div>
										</div>
									</li>';
					}
					else {
						$recipeEl = '<li class="recipe" style="' . $cardColor . '" recipeid="' . $recipeId . '" menuid="' . $menuId . '">
										<div class="recipe-header">
											<div class="recipe-thumbnail" style="background-image:url(' . $thumbnailUrl . ');">												
											</div>
										</div>
										<div class="recipe-content-wrapper ' . $removerecipeClass . '">	
											<div class="recipe-diet recipe-content">' . (empty($key->menu_name)? 'Custom' : $key->menu_name) . '</div>										
											<div class="recipe-content">                                            	
												' . $titleRecipe . '
											</div>
											' . $removerecipeEl . '
											<div class="recipe-hover" title="See Recipe">											
												<div class="icon tdicon-notes" style="' . $buttonColor . '"></div>										
												<div class="recipe-insight">												
												</div>
											</div>
											<div class="clear"></div>
										</div>
									</li>';
									
									//' . (empty($key->menu_name)? 'Custom' : $key->menu_name) . ' -> nama category
									//<div class="glyphicon glyphicon-eye-close"></div>	-> icon mata
					}
					   
					if ( $dateorderRecipe == 1 ) {
						
						if( in_array($recipeId, $day1) )
							continue;
						else
							$day1[] = $recipeId;
						
						$sundayEl .= $recipeEl;
						$sundayNotes .= $notesEl;
						$sundayDate = $tanggalRecipe;
						/*$sundayDay = 'Day 1';*/
						$sundayDay = '<span class="bold">' . $tanggalRecipemd . '</span> ' . $tanggalRecipeD;	
					}
					else if ( $dateorderRecipe == 2 ) {
						
						if( in_array($recipeId, $day2) )
							continue;
						else
							$day2[] = $recipeId;
							
						$mondayEl .= $recipeEl;
						$mondayNotes .= $notesEl;
						$mondayDate = $tanggalRecipe;
						/*$mondayDay = 'Day 2';*/
						$mondayDay = '<span class="bold">' . $tanggalRecipemd . '</span> ' . $tanggalRecipeD;										
					}
					else if ( $dateorderRecipe == 3 ) {
						
						if( in_array($recipeId, $day3) )
							continue;
						else
							$day3[] = $recipeId;
							
						$tuesdayEl .= $recipeEl;
						$tuesdayNotes .= $notesEl;
						$tuesdayDate = $tanggalRecipe;
						/*$tuesdayDay = 'Day 3';*/
						$tuesdayDay = '<span class="bold">' . $tanggalRecipemd . '</span> ' . $tanggalRecipeD;	
					}
					else if ( $dateorderRecipe == 4 ) {
						
						if( in_array($recipeId, $day4) )
							continue;
						else
							$day4[] = $recipeId;
							
						$wednesdayEl .= $recipeEl;
						$wednesdayNotes .= $notesEl;
						$wednesdayDate = $tanggalRecipe;
						/*$wednesdayDay = 'Day 4';*/
						$wednesdayDay = '<span class="bold">' . $tanggalRecipemd . '</span> ' . $tanggalRecipeD;	
					}
					else if ( $dateorderRecipe == 5 ) {
						
						if( in_array($recipeId, $day5) )
							continue;
						else
							$day5[] = $recipeId;
							
						$thursdayEl .= $recipeEl;
						$thursdayNotes .= $notesEl;
						$thursdayDate = $tanggalRecipe;
						/*$thursdayDay = 'Day 5';*/
						$thursdayDay = '<span class="bold">' . $tanggalRecipemd . '</span> ' . $tanggalRecipeD;	
					}
					else if ( $dateorderRecipe == 6 ) {
						
						if( in_array($recipeId, $day6) )
							continue;
						else
							$day6[] = $recipeId;
							
						$fridayEl .= $recipeEl;
						$fridayNotes .= $notesEl;
						$fridayDate = $tanggalRecipe;
						/*$fridayDay = 'Day 6';*/
						$fridayDay = '<span class="bold">' . $tanggalRecipemd . '</span> ' . $tanggalRecipeD;	
					}
					else if ( $dateorderRecipe == 7 ) {
						
						if( in_array($recipeId, $day7) )
							continue;
						else
							$day7[] = $recipeId;
							
						$saturdayEl .= $recipeEl;
						$saturdayNotes .= $notesEl;
						$saturdayDate = $tanggalRecipe;
						/*$saturdayDay = 'Day 7';*/
						$saturdayDay = '<span class="bold">' . $tanggalRecipemd . '</span> ' . $tanggalRecipeD;	
					}						
				}		
																								
			}
		}	
		
		$tips4path = "<img src='" . get_template_directory_uri() . "/img/tips4.jpg'>";
		$tips5path = "<img src='" . get_template_directory_uri() . "/img/tips5.jpg'>";
		
		
		// Execute
		$alltheweek = 			
			'<div id="mymenu-sunday" class="mymenu-day" targetdate="' . $targetSunday . '" data-step="4" data-intro="' . $tips4path . ' Click paper icon above to view the recipe ingredients and instructions <br><br> ' . $tips5path . 'Click eye icon above to hide the recipe">
				<div class="mymenu-date">' . $sundayDay . '</div>                                				                                 
				<ul class="mymenu-stage">' 
					. $sundayEl . $separatorEl . $sundayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						' . $addrecipeElSunday . $addnoteEl . '						
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .		

			'<div id="mymenu-monday" class="mymenu-day" targetdate="' . $targetMonday . '">
				<div class="mymenu-date">' . $mondayDay . '</div>                                				                                 
				<ul class="mymenu-stage">' 
					. $mondayEl . $separatorEl . $mondayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						' . $addrecipeEl . $addnoteEl . '
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .
			
			'<div id="mymenu-tuesday" class="mymenu-day" targetdate="' . $targetTuesday . '">
				<div class="mymenu-date">' . $tuesdayDay . '</div>				
				<ul class="mymenu-stage">' . $tuesdayEl . $separatorEl . $tuesdayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						' . $addrecipeEl . $addnoteEl . '
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .
			
			'<div id="mymenu-wednesday" class="mymenu-day" targetdate="' . $targetWednesday . '">
				<div class="mymenu-date">' .$wednesdayDay . '</div>                                				
				<ul class="mymenu-stage">' . $wednesdayEl . $separatorEl . $wednesdayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						' . $addrecipeEl . $addnoteEl . '
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .
			
			'<div id="mymenu-thursday" class="mymenu-day" targetdate="' . $targetThursday . '">
				<div class="mymenu-date">' . $thursdayDay . '</div>                                				
				<ul class="mymenu-stage">' . $thursdayEl . $separatorEl . $thursdayNotes .
					'<div class="mymenu-addrecipe-wrapper">
						' . $addrecipeEl . $addnoteEl . '
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .
			
			'<div id="mymenu-friday" class="mymenu-day" targetdate="' . $targetFriday . '">
				<div class="mymenu-date">' . $fridayDay . '</div>                                				
				<ul class="mymenu-stage">' . $fridayEl . $separatorEl . $fridayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						' . $addrecipeEl . $addnoteEl . '
						<div class="dailymacros">
							Macro
						</div>
					</div>' .                             
				'</ul>
			</div>' .
			
			'<div id="mymenu-saturday" class="mymenu-day" targetdate="' . $targetSaturday . '">
				<div class="mymenu-date">' . $saturdayDay . '</div>				
				<ul class="mymenu-stage">' . $saturdayEl . $separatorEl . $saturdayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						' . $addrecipeEl . $addnoteEl . '
						<div class="dailymacros">
							Macro
						</div>
					</div>' .                               
				'</ul>
				
			</div>' .
			 
			'<input id="currentdate" type="hidden" value="' . $targetDate . '">
			<input id="currentweek" type="hidden" value="">
			<input id="currentcustommenuid" type="hidden" value="none">';
			
		// Return			
		return $alltheweek;
	}
?>