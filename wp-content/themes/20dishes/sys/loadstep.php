<?php
	require_once 'globalfunction.php';

	if ( isset( $_POST['postTitle'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
				
		$ajaxResult = "";
		
		$current_user = checkCorporateAccount();
			
		$userServingSize = get_user_meta( $current_user, "servings", true); 
		$ajaxResult .= loadstepbyrecipewithingredients($_POST['recipes'], $userServingSize);			

		echo $ajaxResult;		
	}	
	
	function loadstepbyrecipewithingredients($recipes, $userServingSize) {		
		global $wpdb;
		
		$arrDetail = array_filter(explode("^)^@", $recipes));
			
		// Get recipe step by step
		$getRecipeID = implode(",", $arrDetail);
		$menuToUpdate = $wpdb->get_results( "	
												SELECT DISTINCT GROUP_CONCAT(d.recipe) as 'recipe', e.todo FROM jp_stepbystep_recipe d, jp_stepbystep e
												WHERE d.stepbystep = e.id AND d.recipe IN ($getRecipeID) AND e.id >= 122
												GROUP BY e.todo
												ORDER BY e.priority asc
											" );
		$view = "";				
		$view .= "<table class='wp-list-table widefat fixed bordered dataTable'>";
		$view .= "<thead><tr><th>Instructions</th><th>Recipes & Ingredients</th><th>Done</th></tr></thead>";
		
		$ingredientSummary = array(); $i = 1; $allRecipe = "";
		foreach ($menuToUpdate as $row ){
			$view .= "<tr>";
			$view .= "<td>". str_replace(array('[',']'),'',$row->todo) ."</td>";	
			
			// Search todo for any occurences of ingredients
			preg_match_all('/\[([^\]]*)\]/', $row->todo, $out);
			
			// Search all recipes with this todo
			$recipeWithThisTodo = explode(",", $row->recipe);			
			
			// Iterate through each ingredient
			$ingredientsAndRecipes = "";
			foreach($out[1] as $foundIngredient) {
				
				// Iterate through each recipes and find the number of ingredients.
				foreach($recipeWithThisTodo as $foundRecipe){
					
					$recipe = new TwentyDishes_Recipe($foundRecipe);
					if($recipe->title == "recipe deleted")
						continue;
					 
					$recipeName = $recipe->title;
					$recipeServing = $recipe->yield;
					$recipeIngredient = $recipe->ingredients;
		
					for($i = 0; $i < count($recipeIngredient); $i++) {
						
						$foundIngredient_var1 = $foundIngredient . "s";
						$foundIngredient_var2 = $foundIngredient . "(s)";
						$foundIngredient_var3 = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $foundIngredient);
						$foundIngredient_var4 = rtrim($foundIngredient,"s");
						$foundIngredient_var5 = $foundIngredient_var4 . "(s)";
						
						if(
						     ($recipeIngredient[$i]["name"] == $foundIngredient) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var1) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var2) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var3) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var4) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var5)  
						) {
							
							// Add to list of recipe that got ingredient preparation
							if(strpos($allRecipe, $recipeName) === false) {
								if($allRecipe == "")
									$allRecipe .= $recipeName;
								else
									$allRecipe .= ', ' . $recipeName;
							}
								
							//Continue our business
							$singleServe = $recipeIngredient[$i]["amount"] / $recipeServing;
							$multiServe = float2rat(ceil(($singleServe * $userServingSize) * 4) / 4);
							
							if($ingredientsAndRecipes != "")
								$ingredientsAndRecipes .= ",<br>";
							
							$ingredientsAndRecipes .= $recipeName . " (" . $multiServe . " " . $recipeIngredient[$i]["unit"] . " $foundIngredient)";
							
							if( array_key_exists($foundIngredient, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;
								
							else if( array_key_exists($foundIngredient_var1, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;
								
							else if( array_key_exists($foundIngredient_var2, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;
								
							else if( array_key_exists($foundIngredient_var3, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;
								
							else if( array_key_exists($foundIngredient_var4, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;	
								
							else if( array_key_exists($foundIngredient_var5, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;	
								
							else 
								$ingredientSummary[$foundIngredient][0] = $multiServe;
								
							$ingredientSummary[$foundIngredient][1] = $recipeIngredient[$i]["unit"];

							$term = get_term_by( "name", $recipeIngredient[$i]["name"], "ingredient" );
							if(!$term)
								$term = get_term_by( "slug", str_replace(' ', '-', $recipeIngredient[$i]["name"]), "ingredient" );
							
							if($term)
								$ingredientSummary[$foundIngredient][2] = get_term( $term->parent, "ingredient" )->name;	
							else
								$ingredientSummary[$foundIngredient][2] = "";
						}
					}	
				}
			}
			
			// No ingredients. Just get the recipe name.
			if($ingredientsAndRecipes == ""){
				
				// Iterate through each recipes and find the number of ingredients.
				foreach($recipeWithThisTodo as $foundRecipe){
					
					$recipe = new TwentyDishes_Recipe($foundRecipe);
					if($recipe->title == "recipe deleted")
						continue;
						 
					$recipeName = $recipe->title;
					
					if($ingredientsAndRecipes != "")
						$ingredientsAndRecipes .= ",<br>";
							
					$ingredientsAndRecipes .= $recipeName;
					
				}				
			}
			
			$view .= "<td style='width:30%;'>$ingredientsAndRecipes</td>";	
			$view .= "<td class='step-done'><input type='checkbox' class='step-done-check' /></td>";					
			$view .= "</tr>";
			$i++;
		}
		$view .= "</table>";
		
		$ingredientview = "<div id='miseenplace'>Prepare your Mise en Place and Get the following ingredients out:<br><br>";
		foreach($ingredientSummary as $ingredientname=>$value) {
			
			/*
				($ingredientname == "avocado oil") ||
				($ingredientname == "bacon grease") ||
				($ingredientname == "coconut oil") ||
				($ingredientname == "olive oil") ||
				($ingredientname == "buttermilk") ||
				($ingredientname == "heavy cream") ||
				($ingredientname == "kefir") ||
				($ingredientname == "whole milk") ||
				($ingredientname == "almond milk") ||
				($ingredientname == "apple cider juice") ||
				($ingredientname == "apple cider vinegar") ||
				($ingredientname == "apple juice") ||
				($ingredientname == "avocado oil") ||
				($ingredientname == "balsamic vinegar") ||
				($ingredientname == "barbecue sauce") ||
				($ingredientname == "beef broth") ||
				($ingredientname == "brewed coffee") ||
				($ingredientname == "champagne vinegar") ||
				($ingredientname == "chicken broth") ||
				($ingredientname == "chili oil") ||
				($ingredientname == "coconut aminos or tamari") ||
				($ingredientname == "coconut cream") ||
				($ingredientname == "coconut milk") ||
				($ingredientname == "coconut oil") ||
				($ingredientname == "coconut water") ||
				($ingredientname == "cooking sherry") ||
				($ingredientname == "cranberry juice") ||
				($ingredientname == "dry vermouth") ||
				($ingredientname == "enchilada sauce") ||
				($ingredientname == "evaporated milk") ||
				($ingredientname == "fish sauce") ||
				($ingredientname == "flax milk") ||
				($ingredientname == "honey") ||
				($ingredientname == "hot sauce") ||
				($ingredientname == "kombucha") ||
				($ingredientname == "maple syrup") ||
				($ingredientname == "marinara sauce") ||
				($ingredientname == "marsala wine") ||
				($ingredientname == "molasses") ||
				($ingredientname == "olive oil") ||
				($ingredientname == "orange juice") ||
				($ingredientname == "pickle juice") ||
				($ingredientname == "pineapple juice") ||
				($ingredientname == "red wine") ||
				($ingredientname == "red wine vinegar") ||
				($ingredientname == "rice vinegar") ||
				($ingredientname == "salsa") ||
				($ingredientname == "salsa verde") ||
				($ingredientname == "sesame oil") ||
				($ingredientname == "sherry vinegar") ||
				($ingredientname == "sweet and sour sauce") ||
				($ingredientname == "toasted sesame oil") ||
				($ingredientname == "tomato paste") ||
				($ingredientname == "tomato sauce") ||
				($ingredientname == "vegetable broth") ||
				($ingredientname == "vinegar") ||			
				($ingredientname == "white balsamic vinegar") ||
				($ingredientname == "white wine") ||
				($ingredientname == "white wine vinegar") ||
				($ingredientname == "worcestershire sauce")
			*/
			
			if(
				!(
					($ingredientname == "water")
				) &&
				($value[2] != 'Spices')
			  ) {
			  	$ingredientview .= $value[0] . ' ' . $value[1] . ' ' . $ingredientname . ', ';
			  } else {
				$ingredientview .= $ingredientname . ', ';
			  }
		}
		$ingredientview = rtrim($ingredientview, ', ');
		$ingredientview .= "<br><br>Label your storage bags and/or jars with these recipes:<br><br>";
		$ingredientview .= $allRecipe . ".</div>";
		
		return $ingredientview . $recipeview . $view;
	}		
?>