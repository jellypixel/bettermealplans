<?php
	if ( isset( $_POST['recipeid'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = previewrecipe($_POST['recipeid']);
		echo $ajaxResult;
	}	
	
	function previewrecipe($recipeid) {		
		
		// Get the Recipe Details
		$recipe = new TwentyDishes_Recipe($recipeid);
		
		$title = $recipe->title; 
		$yield = $recipe->yield;
		$readytime = $recipe->readytime; 
		$cooktime = $recipe->cooktime; 
		$difficulty = $recipe->difficulty; 
		$thumb = $recipe->thumbmedium;
		$ingredients = $recipe->ingredients;
		$instructions = $recipe->instructions;
		$cats = $recipe->cats;
		$nutritional = $recipe->nutritional;
		$star = $recipe->getFavClass();
		$summary = $recipe->summary;
		
		$de = '^%$^';			
		return 	$title . $de . 
					$yield . $de . 
					$readytime . $de . 
					$thumb . $de . 
					$recipe->printIngredients() . $de .  
					$recipe->printInstructions() . $de . 
					$cats[ count($cats) -1 ]->name . $de . 
					$cooktime . $de . 
					$difficulty . $de . 
					$star . $de . 
					$recipeid . $de . 
					"<div class='addrecipe-longcontent'>" . $nutritional . "</div>" . $de . 
					$summary;
	}
?>