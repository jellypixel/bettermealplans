<?php
	if ( isset( $_POST['menuid'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = editMenu($_POST['menuid']);
		echo $ajaxResult;
	}	
	
	function editMenu($menuid) {		
		global $wpdb;
		
		// Query
		$sql = $wpdb->prepare( 'SELECT a.notes, a.recipe_id, a.day as hari, a.part, a.orders, b.name as menu_name, b.user_id, b.schedule, c.id as diet_id, c.name as diet_name, b.shared, c.colordark as diet_colordark, c.colorlight as diet_colorlight
								FROM jp_menu_recipe a, jp_menu b, jp_diet c
								WHERE a.menu_id = %d 
								AND a.day in (1,2,3,4,5,6,7)
								AND a.menu_id = b.id
								AND b.diet_id = c.id
								', $menuid );
	
		$result = $wpdb->get_results( $sql );
		
		/*Get color based on diet*/
		$allDiets = $wpdb->get_results('SELECT name, colordark, colorlight FROM jp_diet', ARRAY_A);
		$allDietsName = array_column($allDiets, 'name');			
		
		if ( $result ) {	
			$sundayEl = $mondayEl = $tuesdayEl = $wednesdayEl = $thursdayEl = $fridayEl = $saturdayEl = '';
		
			foreach ( $result as $key ){
				
				$recipeId = $key->recipe_id;

				$recipe = new TwentyDishes_Recipe($recipeId);
				if($recipe->title == "recipe deleted")
					 continue;
					 
				$titleRecipe = $recipe->title;
				$thumbnailUrl = $recipe->thumb;
				
				/* Look at recipe categories and determine color */
				$categories = $wpdb->get_results( "SELECT wt.Name		
									FROM $wpdb->posts, $wpdb->term_relationships as wtr, $wpdb->term_taxonomy as wtt, $wpdb->terms as wt
									WHERE $wpdb->posts.ID = wtr.object_id AND $wpdb->posts.id = $recipeId AND wtr.term_taxonomy_id = wtt.term_taxonomy_id AND wtt.term_id = wt.term_id
											AND wtt.taxonomy IN ('category') AND wt.slug IN ("."'".implode("','", $allDietsName). "'".")", ARRAY_A
								);
				$categories = array_column($categories, 'Name');	
				
				$cardColor = 'background-color:#ffffff; color:#444444; border:1px solid #cccccc;';
				$buttonColor = 'color:#555555;';						
				
				$shared = $key->shared;
				
				$menuName = $key->menu_name;
				$menuSchedule = $key->schedule;
				$diet = $key->diet_id;
				$owner = $key->user_id;
				$notes = $key->notes;
				
				$recipeEl = '';	
				if ( !empty( $notes ) ) {
					$recipeEl = '<li class="notes">
									<div class="notes-handle">
									</div>
									<div class="notes-content">
										' . $notes . '
									</div>
									<div class="notes-remove">
										<div data-icon="&#xde;" class="icon" style="color:#777777;"></div>
									</div>
								</li>';	
				}
				else {
					$recipeEl = '<li class="recipe" style="' . $cardColor . '" recipeid="' . $recipeId . '" menuid="' . $menuId . '">
										<div class="recipe-header">
											<div class="recipe-thumbnail" style="background-image:url(' . $thumbnailUrl . ');">												
											</div>
										</div>
										<div class="recipe-content-wrapper">											
											<div class="recipe-content">                                            	
												' . $titleRecipe . '
											</div>
											<div class="recipe-remove" title="Remove">
												<div class="icon tdicon-thin-close"></div>										
											</div>
											<div class="recipe-hover" title="Notes">											
												<div class="icon tdicon-notes" style="' . $buttonColor . '"></div>										
												<div class="recipe-insight">												
												</div>
											</div>
											<div class="clear"></div>
										</div>
									</li>';
				}
			
				if ( $key->hari == 1 ) {
					$sundayEl .= $recipeEl;
				}
				else if ( $key->hari == 2 ) {
					$mondayEl .= $recipeEl;
				}
				else if ( $key->hari == 3 ) {
					$tuesdayEl .= $recipeEl;
				}
				else if ( $key->hari == 4 ) {
					$wednesdayEl .= $recipeEl;
				}
				else if ( $key->hari == 5 ) {
					$thursdayEl .= $recipeEl;
				}
				else if ( $key->hari == 6 ) {
					$fridayEl .= $recipeEl;
				}
				else if ( $key->hari == 7 ) {
					$saturdayEl .= $recipeEl;
				}
			}			
			
			// Return			
			return $menuName . '^$$^' . $menuSchedule . '^$$^' . $diet . '^$$^' . $shared . '^$$^' . $owner . '^$$^' . $sundayEl . '|' . $mondayEl . '|' . $tuesdayEl . '|' . $wednesdayEl . '|' . $thursdayEl . '|' . $fridayEl . '|' . $saturdayEl ;
		}	
		else {
			$sqlEmpty = $wpdb->prepare( 'SELECT * FROM jp_menu WHERE id = %d', $menuid ); 
			$resultEmpty = $wpdb->get_results( $sqlEmpty );

			if ( $resultEmpty ) {
				foreach ( $resultEmpty as $empty ) {
					$menuName = $empty->name;
					$menuSchedule = $empty->schedule;
					$diet = $empty->diet_id;
					$shared = $empty->shared;
					$owner = $empty->user_id;
				}	
			}
			
			// Return			
			return $menuName . '^$$^' . $menuSchedule . '^$$^' . $diet . '^$$^' . $shared . '^$$^' . $owner . '^$$^';
		}
			
		
	}
?>