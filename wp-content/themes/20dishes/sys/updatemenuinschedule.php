<?php
	require_once 'globalfunction.php'; 

	if ( isset( $_POST['targetDate'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';

		
		$userID = checkCorporateAccount();
		
		$targetDateFormatted = date( 'm/d/Y', strtotime( $_POST['targetDate'] ) );
		$targetDay = date( 'w', strtotime( $_POST['targetDate'] ) );
		$targetSunday = date( 'Y-m-d', strtotime( $targetDateFormatted . '-' . ($targetDay + 7) . ' days' ) );
		$targetSaturday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 6 - $targetDay + 7) . ' days') );
		
		$ajaxResult = updatemenuinschedule($targetSunday, $targetSaturday, $userID);
		echo $ajaxResult;
	}	
	
	function updatemenuinschedule($fromDate, $toDate, $userID) {
		global $wpdb;
		
		$menuToUpdate = $wpdb->get_results( "	SELECT DISTINCT a.id FROM jp_menu a, jp_userschedule b
												WHERE a.id = b.menu_id AND a.updates != b.updates AND a.Shared = 'Y' AND b.user_id = ".$userID."
												AND a.schedule >= '$fromDate' AND a.schedule <= '$toDate' AND b.tanggal >= '$fromDate' AND b.tanggal <= '$toDate'" );
												
		$array = array();
		foreach ($menuToUpdate as $res){
			$array[] = $res->id;
		}										
		$array = implode (", ", $array);
									
		$menuDelete = $wpdb->get_results( "	DELETE FROM jp_userschedule 
											WHERE user_id = ".$userID." AND tanggal >= '$fromDate' AND tanggal <= '$toDate' AND menu_id IN(".$array.")"); 
											
		return 1;
	}
?>