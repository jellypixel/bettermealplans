<?php
	if ( isset( $_POST['targetDate'] ) && isset( $_POST['userid'] ) && isset( $_POST['blob'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = saveweek($_POST['userid'], $_POST['targetDate'], $_POST['blob'] );
		echo $ajaxResult;
	}	
	
	function saveweek($userid, $targetDate, $blob) {	
		global $wpdb;		
		$responseErr = '';
		
		$wpdb->query( 'SET autocommit = 0;' );
		$wpdb->query( 'START TRANSACTION' );
		
		//DELETE
		$targetDateFormatted = date( 'm/d/Y', strtotime( $targetDate ) );
		$targetDay = date( 'w', strtotime( $targetDate ) );
		
		$arrTargetDate = array();
		
		$arrTargetDate[0] = date( 'Y-m-d', strtotime( $targetDateFormatted . '-' . $targetDay . ' days' ) );
		$arrTargetDate[1] = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 1 - $targetDay ) . ' days') );
		$arrTargetDate[2] = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 2 - $targetDay ) . ' days') );
		$arrTargetDate[3] = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 3 - $targetDay ) . ' days') );
		$arrTargetDate[4] = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 4 - $targetDay ) . ' days') );
		$arrTargetDate[5] = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 5 - $targetDay ) . ' days') );
		$arrTargetDate[6] = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 6 - $targetDay ) . ' days') );		

		for ( $i = 0; $i < 7; $i++ ) {		
			if ( $wpdb->delete(
							"jp_userschedule",
							array(
								'user_id' => $userid,
								'tanggal' => $arrTargetDate[$i]
							),
							array(
								'%d',
								'%s'
				)
			) === false ) {
				$responseErr .= 'error';
				$wpdb->query('ROLLBACK');
			}
			else {
				$wpdb->query( 'COMMIT' );
			}
		}
		
		//INSERT
		if($blob != '') {
			$arrBlob = explode("^@%^", $blob);		
					
			foreach($arrBlob as $singleblob) {
				$arrDetail = explode("^)^@", $singleblob);
				
				$recipeid = $arrDetail[0];
				$menuid = $arrDetail[1];
				$tanggal = $arrDetail[2];
				$orders = $arrDetail[3];
				$notes = $arrDetail[4];
				
				if ( $wpdb->insert(
								"jp_userschedule",
								array(
									'user_id' => $userid,
									'recipe_id' => $recipeid,
									'menu_id' => $menuid,
									'tanggal' => $tanggal,
									'orders' => $orders,
									'notes' => $notes
								),
								array(
									'%d',
									'%d',
									'%d',
									'%s',
									'%s',
									'%s'
								)
				) === false ) {
					$responseErr .= 'error';
					$wpdb->query('ROLLBACK');
				}
				else {
					$wpdb->query( 'COMMIT' );
				}
			}				
		}
		
		$wpdb->query( 'SET autocommit = 1;' );
				
		if ( $responseErr == '' ) {	
			return true;	
		}
		else {
			return false;	
		}
	}
?>