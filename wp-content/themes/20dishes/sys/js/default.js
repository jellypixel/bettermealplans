var currentTab = "plan";
var tabPassing = 0;
var recipesChanged = 1;
var stepChanged = 1;
var shoppingChanged = 1;

function HCF(u, v) { 
    var U = u, V = v
    while (true) {
        if (!(U%=V)) return V
        if (!(V%=U)) return U 
    } 
}

var entityMap = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;',
  '/': '&#x2F;',
  '`': '&#x60;',
  '=': '&#x3D;'
};

function escapeHtml (string) {
  return String(string).replace(/[&<>"'`=\/]/g, function (s) {
    return entityMap[s];
  });
}

function fraction(decimal){

    if(!decimal){
        decimal=this;
    }
    whole = String(decimal).split('.')[0];
    decimal = parseFloat("."+String(decimal).split('.')[1]);
    num = "1";
    for(z=0; z<String(decimal).length-2; z++){
        num += "0";
    }
    decimal = decimal*num;
    num = parseInt(num);
    for(z=2; z<decimal+1; z++){
        if(decimal%z==0 && num%z==0){
            decimal = decimal/z;
            num = num/z;
            z=2;
        }
    }
    //if format of fraction is xx/xxx
    if (decimal.toString().length == 2 && 
            num.toString().length == 3) {
                //reduce by removing trailing 0's
        decimal = Math.round(Math.round(decimal)/10);
        num = Math.round(Math.round(num)/10);
    }
    //if format of fraction is xx/xx
    else if (decimal.toString().length == 2 && 
            num.toString().length == 2) {
        decimal = Math.round(decimal/10);
        num = Math.round(num/10);
    }
    //get highest common factor to simplify
    var t = HCF(decimal, num);
	
	//For 3/10 change it into 1/3
	if((decimal + '/' + num) == '3/10') {
		decimal = 1;
		num = 3;
	}	
	
	//For 7/10 change it into 2/3
	if((decimal + '/' + num) == '7/10') {
		decimal = 2;
		num = 3;
	}	
	
	//For 33/100 change it into 1/3
	if((decimal + '/' + num) == '33/100') {
		decimal = 1;
		num = 3;
	}		
	
    //return the fraction after simplifying it
	if(isNaN(decimal)) {
		return whole;
	} else
		return ((whole==0)?"" : whole+" ")+decimal/t+"/"+num/t;	
}

jQuery(document).ready(function(){  
	
	// Lightbox Height
	var windowHeight = jQuery(window).height();
    var windowWidth = jQuery(window).width();
    
    if(windowWidth < 768) {
        jQuery("#addrecipe-stage").css('height',windowHeight - 80);
        jQuery("#searchrecipe-stage").css('height',windowHeight - 280);
   }
   
   	// Popup timer for free user
   	if ( freeUser ) {
   		window.setTimeout(function () {
	    	callLightbox3();
		}, 900000);
	}
	
	// Lightbox	
		var lightboxEl = jQuery(".lightbox-overlay");		
	
		jQuery(".calllightbox").click(function() {
			var thisId = jQuery(this).attr('id');
	
			lightboxEl.find(".lightbox").show();
			lightboxEl.find(".lightbox2").hide();
			
			if ( thisId == 'paleo' ) {
				lightboxEl.find(".lightbox-photo").removeClass("aip fodmap vege quick plan classic").addClass("paleo");
				lightboxEl.find(".lightbox-title").html("Paleo Diet");
				lightboxEl.find(".lightbox-subtitle").html("20 Dishes knows the structure and rules of the Paleo Diet inside and out.");
				lightboxEl.find(".lightbox-inner-text").html("<p>This meal plan focuses on the base principles of the diet, which includes fresh vegetables, quality proteins, and nuts/seeds. The recipes are specifically chosen for their ease of preparation, as well as their deliciousness.  The Paleo Meal Plan serves up a mix of family favorites, slow cooker meals, tasty sides, and much more.</p>");
				lightboxEl.find(".lightbox-inner-text").append('<div class="exampleButton green"><a style="color:#fff" href="http://20dishes.com/wp-content/uploads/2015/05/paleo-recipe-sample.mp4" class="popup" data-height="410" data-width="855" data-scrollbars="0">Take a peek</a></div>');
			}
			else if ( thisId == 'aip' ) {
				lightboxEl.find(".lightbox-photo").removeClass("paleo fodmap vege quick plan classic").addClass("aip");
				lightboxEl.find(".lightbox-title").html("Paleo Autoimmune Protocol Diet");
				lightboxEl.find(".lightbox-subtitle").html("");
				lightboxEl.find(".lightbox-inner-text").html("<p>The Paleo Autoimmune Protocol (AIP) is, in essence, an elimination diet.  By removing foods that cause inflammation in the body you are allowing your body to heal the gut and immune system. We help you navigate the waters of this diet by providing delicious and simple recipes that will help take  the stress of the autoimmune protocol.</p>");
				lightboxEl.find(".lightbox-inner-text").append('<div class="exampleButton green"><a style="color:#fff" href="http://20dishes.com/wp-content/uploads/2015/05/AIP-Recipe-Sample.mp4" class="popup" data-height="410" data-width="855" data-scrollbars="0">Take a peek</a></div>');
			}
			else if ( thisId == 'lowfodmap' ) {
				lightboxEl.find(".lightbox-photo").removeClass("aip paleo vege quick plan classic").addClass("fodmap");
				lightboxEl.find(".lightbox-title").html("Gluten Free Diet");
				lightboxEl.find(".lightbox-subtitle").html("");
				lightboxEl.find(".lightbox-inner-text").html("<p>The Gluten-Free  Diet is suggested as a healing tool for those who suffer from Celiac Disease. Eating a gluten-free diet helps people with Celiac disease control their signs and symptoms and prevent complications.. Our meal plan is centered around nourishing foods and quick preparation that won't have you missing the gluten.</p>");
				lightboxEl.find(".lightbox-inner-text").append('<div class="exampleButton green"><a style="color:#fff" href="http://20dishes.com/wp-content/uploads/2015/05/GLUTENFREE-recipes-Sample.mp4" class="popup" data-height="410" data-width="855" data-scrollbars="0">Take a peek</a></div>');
			} 
			else if ( thisId == 'vegetarian' ) {
				lightboxEl.find(".lightbox-photo").removeClass("aip paleo fodmap quick plan classic").addClass("vege");
				lightboxEl.find(".lightbox-title").html("Vegetarian Diet");
				lightboxEl.find(".lightbox-subtitle").html("");
				lightboxEl.find(".lightbox-inner-text").html("<p>Our Lacto-Ovo Vegetarian meal plan focuses on gluten-free and soy-free recipes filled with flavor.  Reap all the benefits of a plant-based diet with these healthy, easy recipes.</p>");
			} 
			else if ( thisId == 'classic' ) {
				lightboxEl.find(".lightbox-photo").removeClass("aip paleo fodmap quick plan vege").addClass("classic");
				lightboxEl.find(".lightbox-title").html("Classic Diet");
				lightboxEl.find(".lightbox-subtitle").html("");
				lightboxEl.find(".lightbox-inner-text").html("<p>Our Classic meal plans provide you with proven, kitchen-tested recipes that include grains and legumes for those on non-restricted diets. Our delicious meals will remind you of old family favorites turned healthy (and easy) using our proven meal prep and planning system.<br><br>Meal planning is the best way to ensure your family is eating wholesome, nutritious food every day. Plus, it’s also an excellent way to stretch those hard-earned pennies!.</p>");
			} 
	
			lightboxEl.fadeIn(400);
		});
		
		jQuery(".calllightboximage").click(function() {
			var thisId = jQuery(this).attr('id');
			
			lightboxEl.find(".lightbox").hide();
			lightboxEl.find(".lightbox2").show();
			
			if ( thisId == 'plan' ) {
				lightboxEl.find(".lightbox-photo").removeClass("aip fodmap paleo quick").addClass("plan");
			}
			else if ( thisId == 'quick' ) {
				lightboxEl.find(".lightbox-photo").removeClass("aip fodmap paleo plan").addClass("quick");
			}		
	
			lightboxEl.fadeIn(400);
		});
		
		jQuery(".lightbox-sales-close").click(function() {
			lightboxEl.fadeOut(400);
		});
		
		
		jQuery('input').on('ifChecked', function(event){
			var finalEl = jQuery('.final');
			if ( jQuery(this).val() == '1month') {
				finalEl.attr({ 'href' : 'https://ov208.infusionsoft.com/app/orderForms/20-Dishes-M' });
			}
			else if ( jQuery(this).val() == '3months') {
				finalEl.attr({ 'href' : 'https://ov208.infusionsoft.com/app/orderForms/20-Dishes-Q' });
			}
			else if ( jQuery(this).val() == '1year') {
				finalEl.attr({ 'href' : 'https://ov208.infusionsoft.com/app/orderForms/20year' });
			}
		});
	
	// Pop Up Login
		jQuery("#kitch_log").click( function(){
			loadPopup();
		});
	
		jQuery("div.close").hover(
			function() {
				jQuery('span.ecs_tooltip').show();
			},
			function () {
				jQuery('span.ecs_tooltip').hide();
			}
		);
		
		jQuery("div.close").click(function() {
			disablePopup();  // function close pop up
		});
		
		jQuery(this).keyup(function(event) {
			if (event.which == 27) { // 27 is 'Ecs' in the keyboard
				disablePopup();  // function close pop up
			}  	
		});
		
		jQuery("div#backgroundPopup").click(function() {
			disablePopup();  // function close pop up
		});
		
		jQuery('a.livebox').click(function() {
			//alert('Hello World!');
			return false;
		});				
	
		 /************** start: functions. **************/
		function loading() {
			jQuery("div.loader").show();  
		}
		function closeloading() {
			jQuery("div.loader").fadeOut('normal');  
		}
		
		var popupStatus = 0; // set value
		
		function loadPopup() { 
			if(popupStatus == 0) { // if value is 0, show popup
				closeloading(); // fadeout loading
				jQuery("#toPopup").fadeIn(0500); // fadein popup div
				jQuery("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
				jQuery("#backgroundPopup").fadeIn(0001); 
				popupStatus = 1; // and set value to 1
			}	
		}
			
		function disablePopup() {
			if(popupStatus == 1) { // if value is 1, close popup
				jQuery("#toPopup").fadeOut("normal");  
				jQuery("#backgroundPopup").fadeOut("normal");  
				popupStatus = 0;  // and set value to 0
			}
		}
});
		
jQuery(document).ready(function(){   
		
	//Sortable		
		jQuery( ".mymenu-stage" ).sortable({
			//revert: true
			handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
			placeholder: "recipe-placeholder",
			connectWith: ".mymenu-stage",
			zIndex: 10000,
			/*drag: function(event, ui) { 
				var offset = jQuery(this).offset();
				var yPos = offset.top; 
				ui.helper.css('margin-top', jQuery(window).scrollTop() + 'px');
			}*/
			/*start: function(e, ui){
				ui.placeholder.height(ui.item.height());
			}*/
		}).disableSelection();
		
		
		/*jQuery( '.nav-tabs' ).click(function() {
			
		});*/
	//Print
		jQuery("#print").click(function() {			
			// Refresh thing first
			showLoadingAnimation();
			tabPassing = 1;
            stepChanged = 1;
            shoppingChanged = 1;
				jQuery("#shoppinglistTab").click();
				jQuery("#stepTab").click();
				jQuery("#recipesdetailTab").click();
			tabPassing = 0;
			
			// Checkbox Check - Yo dawg
			var flagPrintCheck = 0;
			var flagPlan = 0;
			var flagStep = 0;
			var flagShop = 0;
			var flagRecipe = 0;
			
			jQuery("#stepbystep > img").removeClass("pagebreak");
			jQuery("#shoppinglist > img").removeClass("pagebreak");
			jQuery("#recipesdetail > img").removeClass("pagebreak");
			
			if ( jQuery("#recipePrintPlan").is(":checked") ) {
				jQuery("#plan").removeClass("notprinted");
				jQuery(".logoprint").removeClass("notprinted");				
				flagPlan = 1;
				flagPrintCheck++;
			}
			else {
				flagPlan = 0;
				jQuery("#plan").addClass("notprinted");
				jQuery(".logoprint").addClass("notprinted");
			}			
			
			if ( jQuery("#recipePrintStep").is(":checked") ) {
				jQuery("#stepbystep").removeClass("notprinted");
				flagStep = 1;
				flagPrintCheck++;
			}
			else {
				flagStep = 0;
				jQuery("#stepbystep").addClass("notprinted");
			}			
			
			if ( jQuery("#recipePrintShopping").is(":checked") ){
				jQuery("#shoppinglist").removeClass("notprinted");
				flagShop = 1;
				flagPrintCheck++;
			}
			else {
				flagShop = 0;
				jQuery("#shoppinglist").addClass("notprinted");
			}
			
			if ( jQuery("#recipePrintRecipe").is(":checked") ){
				jQuery("#recipesdetail").removeClass("notprintedparent");
				jQuery("#recipesdetail-print").removeClass("notprinted");
				flagRecipe = 1;
				flagPrintCheck++;
			}
			else {
				flagRecipe = 0;
				jQuery("#recipesdetail").addClass("notprintedparent");
				jQuery("#recipesdetail-print").addClass("notprinted");
			}
			
			// Page Break Check
			if ( flagStep == 1 && flagPlan == 1 ) {
				jQuery("#stepbystep > img").addClass("pagebreak");
			}
			if ( ( flagShop == 1 && flagPlan == 1 && flagStep == 0 ) || ( flagShop == 1 && flagStep == 1 ) ) {
				jQuery("#shoppinglist > img").addClass("pagebreak");
			}
			if (( flagRecipe == 1 && flagPlan == 1 && flagStep == 1 && flagShop == 0 ) ||
				( flagRecipe == 1 && flagPlan == 1 && flagStep == 0 && flagShop == 1 ) ||
				( flagRecipe == 1 && flagPlan == 0 && flagStep == 1 && flagShop == 1 ) ||
				( flagRecipe == 1 && flagPlan == 1 && flagStep == 0 && flagShop == 0 ) ||
				( flagRecipe == 1 && flagPlan == 0 && flagStep == 1 && flagShop == 0 ) ||
				( flagRecipe == 1 && flagPlan == 0 && flagStep == 0 && flagShop == 1 ) ||
				( flagRecipe == 1 && flagPlan == 1 && flagStep == 1 && flagShop == 1 ) ) {
				jQuery("#recipesdetail > img").addClass("pagebreak");
			}
			
			// Tidy up
			jQuery("#recipesdetail-print").empty();
			jQuery(".textareaadditemprint").html( jQuery(".textareaadditem").html() );
			jQuery(".recipe-check").each(function( index ) {
				if(jQuery(this).is(":checked")) {
					
					if(checkedPrint == "yes")
						jQuery("#recipesdetail-print").append(jQuery(this).closest(".accordion-group").find(".accordion-inner .addrecipe-rest").addClass("pagebreakafter").parent().html());
					else
						jQuery("#recipesdetail-print").append(jQuery(this).closest(".accordion-group").find(".accordion-inner").parent().html());
				} 
			});
			
			// Final Check
			if ( flagPrintCheck == 0 ) {
				alert("There is nothing to print :(");	
			}
			else {
				jQuery(".col_amount").each(function( index ) {
					jQuery(this).siblings(".col_amount_print").html( jQuery(this).val() );
				});
				/*
				jQuery("body").attr('class', '');
				jQuery("body").attr('style', '');
				*/
				// Call print
				setTimeout(function(){	window.print(); }, 8000);				
				
				// Call Xportability CSS to PDF				
				/*setTimeout( function() {
					var originalStageEl = jQuery( '#mymenu-stage-wrapper' );
					var previewStageEl = jQuery( '#pdf-preview' );				
					
					previewStageEl.empty();	// Empty				
					originalStageEl.clone().appendTo( previewStageEl ); // Clone
					
					previewStageEl.find( '.tdicon-gear-full' ).removeAttr( 'data-intro' );
					previewStageEl.find( '#selectedWeek' ).removeAttr( 'data-intro' );
					previewStageEl.find( '.glyphicon-trash' ).removeAttr( 'data-intro' );
					previewStageEl.find( '#mymenu-sunday' ).removeAttr( 'data-intro' );				
					previewStageEl.find( '#mymenu-sunday .tdicon-plus' ).removeAttr( 'data-intro' );
					previewStageEl.find( '#saveweek' ).removeAttr( 'data-intro' );
					previewStageEl.find( '#saveasnewmenu' ).removeAttr( 'data-intro' );
					previewStageEl.find( '#shoppinglistTab' ).removeAttr( 'data-intro' );				
					previewStageEl.find( '#stepTab' ).removeAttr( 'data-intro' );
					previewStageEl.find( '.tdicon-print' ).removeAttr( 'data-intro' );				
					
					previewStageEl.find( '#sys-tabpanel > .nav-tabs' ).remove();
					previewStageEl.find( '.dp-numberPicker-input' ).remove();
					previewStageEl.find( '.recipe' ).removeAttr( 'blobmeta' );
					
					previewStageEl.find( '.savewrapper' ).remove();
					previewStageEl.find( '#options' ).remove();
					previewStageEl.find( '#recipesdetail .insidePane #recipesdetail-stage' ).remove();
					previewStageEl.find( '#recipesdetail .insidePane > input' ).remove();
					previewStageEl.find( '#recipesdetail .insidePane > label' ).remove();
					previewStageEl.find( '.mymenu-day' ).removeAttr( 'targetdate' );
					previewStageEl.find( '#planpanel > input' ).remove();
					
					setTimeout(function(){ return xepOnline.Formatter.Format('pdf-preview',{ embedLocalImages:'true' }); }, 1000);
				}, 2000);*/
				
	
				
				
			}
		});
		
		/*jQuery( '.footer-top' ).click(function() {
			setTimeout(function(){ return xepOnline.Formatter.Format('pdf-preview',{ embedLocalImages:'true' }); }, 1000);
		});*/
		
	//Datepicker	
		jQuery("#selectDate").datepicker({
			buttonImage: template_url + '/sys/img/tabicons.png',
			buttonImageOnly: true,
			changeMonth: true,
			changeYear: true,
			showOn: 'both',
			onSelect: function() {
				var currDate = jQuery.datepicker.formatDate("yy-mm-dd", jQuery(this).datepicker('getDate'));

				//jQuery(this).datepicker('setDate', jQuery(this).datepicker('getDate') );
			
				if(currentTab == "plan") {
					showLoadingAnimation();
				}
				
				/* Get Member Press ID & CA */
				var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
				var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );				
						
				jQuery.ajax({	
					url: template_url + "/sys/loadweek.php",				
					type: "POST",
					data: {'id': memberpressID , 'ca': memberpressCA, 'targetDate': currDate},
					success: function(data)
					{
						if ( data ) {
							jQuery("#planpanel").html(data);	
							
							var tmpTitle = jQuery("#selectedWeek").html().split('<br>');
							jQuery("#custommenu-title").html(tmpTitle[1]);
							
							jQuery( ".mymenu-stage" ).sortable({
								//revert: true
								handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
								placeholder: "recipe-placeholder",
								connectWith: ".mymenu-stage",
								zIndex: 10000,
							}).disableSelection(); 
							
							recipesChanged = 1;
							stepChanged = 1;
							shoppingChanged = 1;
							
							if(currentTab == "plan") {
								closeLightbox("generic");
							}														
							if(currentTab == "shopping") {
								jQuery("#shoppinglistTab").click();
							}
							if(currentTab == "step") {
								jQuery("#stepTab").click();
							}	
							if (currentTab == "recipes") {
								jQuery("#recipesdetailTab").click();	
							}
							
							checkRecipe();
						}							
					}
				});		
			}
		});
		
	// Create Menu Datepicker
		jQuery("#createmenu_selectDate").datepicker({
			buttonImage: template_url + '/sys/img/tabicons2.png',
			buttonImageOnly: true,
			changeMonth: true,
			changeYear: true,
			showOn: 'both',
			dateFormat: "M d, yy"
		});
		
	// Options UI
		jQuery( "#excludedIngredientAdd" ).autocomplete({
			source: template_url + '/sys/loadingredients.php',
			minLength: 3,
			select: function( event, ui ) {				
				jQuery( "<div class='excludedIngredient'>" ).text( ui.item.value ).prependTo( "#excludedIngredientList" );
				jQuery( "#excludedIngredientList" ).scrollTop( 0 );
			
			}
		});
		
		jQuery( "#excludedIngredientAddFirst" ).autocomplete({
			source: template_url + '/sys/loadingredients.php',
			appendTo: "#wizard4",
			minLength: 3,
			select: function( event, ui ) {
				jQuery( "<div class='excludedIngredient'>" ).text( ui.item.value ).prependTo( "#excludedIngredientListFirst" );
				jQuery( "#excludedIngredientListFirst" ).scrollTop( 0 );
			
			}
		});
		
		jQuery('.excludedIngredient').live('click', function() 
		{
			jQuery(this).remove();
		});		
		
	//Save Options
		jQuery("#saveoptions").click(function() {			
				
			var currDate = jQuery("#options #currentdate").val();	
			var serveSize = jQuery("#options #number-picker .dp-numberPicker-input").val();
			var fontPrintSize = jQuery("#options #fontprint-picker .dp-numberPicker-input").val();
			
			var dietPlan = JSON.stringify(jQuery("#options #dietOptions input:checkbox:checked").map(function(){
								return jQuery(this).val();
							}).get());
			var excludeList = JSON.stringify(jQuery("#options #excludedCategoriesOptions input:checkbox:checked").map(function(){
								return jQuery(this).val();
							}).get());							
			var exIngredient = 	"";
		
			if(jQuery("#options #checkPrint").is(':checked'))
				checkedPrint = "yes";
			else	
				checkedPrint = "no";
 			
			jQuery('#options .excludedIngredient').each(function() {
				exIngredient += jQuery(this).html() + ",";
			});		
				
			if(dietPlan == "[]") {				
				modalPls( "error", '', 'Please select at least one mode of diet.' );
				return;
			} 	
					
			showLoadingAnimation();		
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );			
			
			jQuery.ajax({	
				url: template_url + "/sys/saveoptions.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, servingSize: serveSize, dietPlans: dietPlan, exIngredient: exIngredient, fontPrintSize: fontPrintSize, excludeList: excludeList, checkedPrint:checkedPrint},
				success: function(data)
				{
					data = data.trim();
					
					if ( data == "1" ) {
	
						var arrTargetDate = jQuery("#currentdate").val().split("-");
						var currDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date(arrTargetDate[0], arrTargetDate[1] - 1, arrTargetDate[2]) );
						
						if (arrTargetDate.length == '1') {
							
							menuid = jQuery("#currentcustommenuid").val();
							
							if (typeof menuid != 'undefined')
								currDate = menuid + "-01-01";
							else
								currDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date());
						}
						
						/* Get Member Press ID & CA */
						var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
						var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );						
						
						jQuery.ajax({	
							url: template_url + "/sys/loadweek.php",				
							type: "POST",
							data: {'id': memberpressID , 'ca': memberpressCA, 'targetDate': currDate},
							success: function(data)
							{
								if ( data ) {
									jQuery("#planpanel").html(data);	
									
									var tmpTitle = jQuery("#selectedWeek").html().split('<br>');
									jQuery("#custommenu-title").html(tmpTitle[1]);
									
									jQuery( ".mymenu-stage" ).sortable({
										//revert: true
										handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
										placeholder: "recipe-placeholder",
										connectWith: ".mymenu-stage",
										zIndex: 10000,										
									}).disableSelection(); 
									
									recipesChanged = 1;
									stepChanged = 1;
									shoppingChanged = 1;
									
									if(currentTab == "plan") {
										closeLightbox("generic");
									}														
									if(currentTab == "shopping") {
										jQuery("#shoppinglistTab").click();
									}
									if(currentTab == "step") {
										jQuery("#stepTab").click();
									}	
									if (currentTab == "recipes") {
										jQuery("#recipesdetailTab").click();	
									}	

									checkRecipe();
								}							
							}
						});							

						closeLightbox("continued");
						modalPls('success', '', 'Preferences has been saved.');					
						
					} else {
						
						closeLightbox("continued");
						modalPls('error', '', 'Error occured, please try again or contact administrator.');		
					}						
				}
			});
		});
		
	// Save Options - First Time	
		jQuery("#wizard-save").click(function() {			
				
			var currDate = jQuery("#currentdate").val();	
			var serveSize = jQuery("#wizard-lightbox #number-picker .dp-numberPicker-input").val();
			var fontPrintSize = jQuery("#wizard-lightbox #fontprint-picker .dp-numberPicker-input").val();
			
			var dietPlan = JSON.stringify(jQuery("#wizard-lightbox #dietOptions input:checkbox:checked").map(function(){
								return jQuery(this).val();
							}).get());
			var excludeList = JSON.stringify(jQuery("#wizard-lightbox #excludedCategoriesOptions input:checkbox:checked").map(function(){
								return jQuery(this).val();
							}).get());							
			var exIngredient = 	"";
		
			if(jQuery("#wizard-lightbox #checkPrint").is(':checked'))
				checkedPrint = "yes";
			else	
				checkedPrint = "no";
 			
			jQuery('#wizard-lightbox .excludedIngredient').each(function() {
				if ( exIngredient == "" )
				{
					exIngredient = jQuery(this).html();
				}
				else
				{
					exIngredient += "," + jQuery(this).html();	
				}				
			});		
				
			/*if(dietPlan == "[]") {				// Already checked in lightbox version
				modalPls( "error", '', 'Please select at least one mode of diet.' );
				return;
			} 	*/
			closeLightbox("wizard");
			showLoadingAnimation();		
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );			
			
			jQuery.ajax({	
				url: template_url + "/sys/saveoptions.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, servingSize: serveSize, dietPlans: dietPlan, exIngredient: exIngredient, fontPrintSize: fontPrintSize, excludeList: excludeList, checkedPrint:checkedPrint},
				success: function(data)
				{
					data = data.trim();
					if ( data == "1" ) {
						
						/* Get Member Press ID & CA */
						var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
						var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );						
	
						jQuery.ajax({	
							url: template_url + "/sys/loadweek.php",				
							type: "POST",
							data: {'id': memberpressID , 'ca': memberpressCA, 'targetDate': currDate},
							success: function(data)
							{
								if ( data ) {
									jQuery("#planpanel").html(data);	
									
									var tmpTitle = jQuery("#selectedWeek").html().split('<br>');
									jQuery("#custommenu-title").html(tmpTitle[1]);
									
									jQuery( ".mymenu-stage" ).sortable({
										//revert: true
										handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
										placeholder: "recipe-placeholder",
										connectWith: ".mymenu-stage",
										zIndex: 10000,
									}).disableSelection(); 
									
									recipesChanged = 1;
									stepChanged = 1;
									shoppingChanged = 1;
									
									if(currentTab == "plan") {
										closeLightbox("generic");
									}														
									if(currentTab == "shopping") {
										jQuery("#shoppinglistTab").click();
									}
									if(currentTab == "step") {
										jQuery("#stepTab").click();
									}	
									if (currentTab == "recipes") {
										jQuery("#recipesdetailTab").click();	
									}	

									checkRecipe();
									
									closeLightbox("continued");						
									overlayEl.removeClass('firsttime-overlay');
									//modalPls('success', '', 'Preferences has been saved.');	
									/*
									if ( firstentry ) {
										setTimeout( function() {
											introJs().start();
										}, 400 );
									} TWI */
								}							
							}
						});							
					} 
					else 
					{						
						closeLightbox("continued");						
						modalPls('error', '', 'Error occured, please try again or contact administrator.');		
					}						
				}
			});
		});
		
	// Save User Settings
		jQuery("#saveusersettings").click(function() 
		{					
			var addrecipe = jQuery( '#checkRestrict-AddRecipe' ).iCheck('update')[0].checked;
			var addnote = jQuery( '#checkRestrict-AddNote' ).iCheck('update')[0].checked;
			var removerecipe = jQuery( '#checkRestrict-RemoveRecipe' ).iCheck('update')[0].checked;
			
			var startfromscratch = jQuery( '#checkRestrict-StartFromScratch' ).iCheck('update')[0].checked;
			var portion = jQuery( '#checkRestrict-Portion' ).iCheck('update')[0].checked;
			var diet = jQuery( '#checkRestrict-Diet' ).iCheck('update')[0].checked;
			var avoidfood = jQuery( '#checkRestrict-AvoidFood' ).iCheck('update')[0].checked;
			var excludeadditionalingredients = jQuery( '#checkRestrict-ExcludeAdditionalIngredients' ).iCheck('update')[0].checked;
							
			var restrictcalories = jQuery("#usersettings #restrictcalories-picker .dp-numberPicker-input").val();
			var restrictfat = jQuery("#usersettings #restrictfat-picker .dp-numberPicker-input").val();
			var restrictcarbs = jQuery("#usersettings #restrictcarbs-picker .dp-numberPicker-input").val();
			
			showLoadingAnimation();		
			
			jQuery.ajax({	
				url: template_url + "/sys/saveusersettings.php",				
				type: "POST",
				data: {'userid': userid, 'addrecipe' : addrecipe, 'addnote' : addnote, 'removerecipe' : removerecipe, 'startfromscratch' : startfromscratch, 'portion' : portion, 'diet' : diet, 'avoidfood' : avoidfood, 'excludeadditionalingredients' : excludeadditionalingredients, 'restrictcalories' : restrictcalories, 'restrictfat' : restrictfat, 'restrictcarbs' : restrictcarbs			
				},
				success: function(data)
				{
					data = data.trim();
					
					if ( data == "1" ) {	
						closeLightbox("continued");
						modalPls('success', '', 'User settings have been saved.');					
						
					} else {
						
						closeLightbox("continued");
						modalPls('error', '', 'Error occured, please try again or contact administrator.');		
					}						
				}
			});
			
		});

	//Tab Status
		jQuery("#shoppinglistTab").click(function() {
			
			currentTab = "shopping";
			
			//Only refresh if there is a change in the planning system
			if(shoppingChanged == 1) { 
				generateShoppingData(2);
				shoppingChanged = 0;
			}
		});

		jQuery("#planTab").click(function() {
			
			currentTab = "plan";
		});

		jQuery("#stepTab").click(function() {
			
			currentTab = "step";
			
			//Only refresh if there is a change in the planning system
			if(stepChanged == 1) { 			
				generateStepData();
				stepChanged = 0;
			}	
		});
		
		jQuery("#recipesdetailTab").click(function() {
			currentTab = "recipes";
			
			jQuery("#recipesdetail-stage > .icheckbox_minimal, #recipesdetail-stage > label").hide();
			
			//Only refresh if there is a change in the planning system
			if(recipesChanged == 1) { 				
				generateRecipesDetail();
				recipesChanged = 0;
			}	
			else {
				jQuery("#recipesdetail-stage > .icheckbox_minimal, #recipesdetail-stage > label").show();
			}

			// Show ads if exist
			jQuery(".centerbar").fadeIn(400);
		});

		jQuery("#optionsTab").click(function() {
			
			currentTab = "option";

			// Show ads if exist
			jQuery(".centerbar").fadeIn(400);
		});		
		
		function generateStepData() {
			
			if(tabPassing != 1)
				showLoadingAnimation();
			
			var delimiterDetail = '^)^@';								
			
			// For data based on menu
			var blob = '';			
			jQuery(".mymenu-day").each(function() {
		
				jQuery(this).find(".mymenu-stage").find("li").find(".recipe-header").each(function() {
					thisEl = jQuery(this);						
					blob += thisEl.html() + delimiterDetail;
				});
			});		

			// For data based step by step
			var currDate = jQuery("#currentdate").val();	

			// For data based on recipe
			var blob2 = '';			
			jQuery(".mymenu-day").each(function() {
		
				jQuery(this).find(".mymenu-stage").find("li").each(function() {
					thisEl = jQuery(this);						
					recipeid = '';
					 
					if ( thisEl.hasClass("recipe") ) {							
						recipeid = thisEl.attr('recipeid');
						blob2 += recipeid + delimiterDetail;
					}
				});
			});			
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );
						
			jQuery.ajax({	
				url: template_url + "/sys/loadstep.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, postTitle: blob, recipes: blob2, 'targetDate': currDate},
				success: function(data)
				{
					jQuery("#step_list").empty().html(data);
					closeLightbox("generic");
				}
			});					
			
		}
		
		function generateRecipesDetail() {
			if (tabPassing != 1) {
				showLoadingAnimation();	
			}
			
			var arrRecipeId = [];
			var found = 0;
			
			jQuery("#plan").find("li.recipe").each(function() {
				thisId = jQuery(this).attr('recipeid');

				found = jQuery.inArray( thisId, arrRecipeId );

				if ( found == -1 ) {
					arrRecipeId.push(thisId);
				}				
			});		
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );			
			
			jQuery.ajax({
				url: template_url + "/sys/loadrecipesdetail.php",
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, arrRecipeId: arrRecipeId},
				success: function(data) 
				{
					if (data) {
						jQuery("#recipesdetail-accordion").css({ 'height' : '', 'padding-top' : '40px' }).html(data);	
						closeLightbox("generic");
						
						jQuery("#recipesdetail-stage > .icheckbox_minimal, #recipesdetail-stage > label").show();
						jQuery("#recipeSelectAll").iCheck("indeterminate");
						
						jQuery('#recipesdetail-accordion input').iCheck({
							checkboxClass: 'icheckbox_minimal',
							radioClass: 'iradio_minimal',
						});
					}					
					else {
						jQuery("#recipesdetail-accordion").css({ 'height' : '400px', 'padding-top' : '40px' }).html('You have no plan yet.');
						closeLightbox("generic");
					}
				}
			});
		}
	//Shopping List Opened
		function generateShoppingData(mode) {
			
			var loading = showLoadingAnimation();
			var blob = '';			
			var delimiterDetail = '^)^@';								
							
			jQuery(".mymenu-day").each(function() {
		
				jQuery(this).find(".mymenu-stage").find("li").each(function() {
					thisEl = jQuery(this);						
					recipeid = '';
					 
					if ( thisEl.hasClass("recipe") ) {							
						recipeid = thisEl.attr('recipeid');
						blob += recipeid + delimiterDetail;
					}
				});
			});			

			var arrTargetDate = jQuery("#currentdate").val().split("-");
		
			targetDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date(arrTargetDate[0], arrTargetDate[1] - 1, arrTargetDate[2]) );
			
			if (arrTargetDate.length == '1') {
				
				menuid = jQuery("#currentcustommenuid").val();
				
				if (typeof menuid != 'undefined')
					targetDate = menuid + "-01-01";
				else
					targetDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date());
			}		
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );

			jQuery.ajax({	
				url: template_url + "/sys/loadshopping.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, 'userid': userid , 'targetDate': targetDate, 'recipes' : blob, 'mode' : mode},
				success: function(data)
				{
					jQuery("#ingredient_list").empty().html(data);
					
					jQuery(".col_amount.pure").each(function( index ) {
						jQuery(this).val(fraction(jQuery(this).val()));
						jQuery(this).next().html( jQuery(this).val() );
					});			

					jQuery(".col_default").each(function( index ) {
						jQuery(this).val(fraction(jQuery(this).val()));
						jQuery(this).next().html( jQuery(this).val() );
					});								

					jQuery(".col_ingredient").click(function() {
						jQuery(this).parent().children().toggleClass('stroked');
						jQuery(this).parent().toggleClass('stroked');
					});		   						

					jQuery( "#ingredient_list input[value='0']" ).parent().remove();
					
					if(loading == 1)
						closeLightbox("generic");
				}
			});		
			
		}
		
	//Lightbox	
		var overlayEl = jQuery(".overlay");
		var overlay2El = jQuery(".overlay-2");
		var overlay3El = jQuery(".overlay-3");
		var addcardLightbox = jQuery("#addcard-lightbox");
		var dailymacrosLightbox = jQuery("#dailymacros-lightbox");
		var addmenuLightbox = jQuery("#addmenu-lightbox");
		var wizardLightbox = jQuery("#wizard-lightbox");
		var loadcustommenuLightbox = jQuery("#loadcustommenu-lightbox");
		var addcardStage = jQuery("#searchrecipe-stage");
		var favoriterecipesStage = jQuery("#favoriterecipes-stage");
		var addnoteStage = jQuery("#addnotes-textarea");
		var addmenuStage = jQuery("#addmenu-stage");
		var editmenuStage = jQuery("#editmenu-stage");
		var loadcustommenuStage =jQuery(".customMenu-stage");
		var addrecipeLightbox = jQuery("#addrecipe-lightbox");
		var addrecipeStage = jQuery("#addrecipe-stage");
		var addrecipeTitle = jQuery(".lightbox-2-title");
		var addrecipeButton = jQuery("#reallyaddrecipe");
		
		var freeuserLightbox = jQuery("#freeuser-lightbox");
		
	// Generic Loading Animation
		function showLoadingAnimation() {
			
			if( !overlayEl.hasClass( "generic-wait-background" ) ) {
				overlayEl.fadeIn(300);
				overlayEl.append('<div class="generic-wait"></div>');
				overlayEl.addClass("generic-wait-background");

				return 1; // Success
			}
			else
				return 0; // Existing loading is in affect
		}			
		
	// Lightbox Exit
		jQuery(".lightbox-close").click(function() {
			var parentId = jQuery(this).parents(".lightbox").attr('id');
			if ( parentId =="addcard-lightbox" ) {				
				closeLightbox("addcard");	
			}
			else if ( parentId =="dailymacros-lightbox" ) {				
				closeLightbox("dailymacros");	
			}
			else if ( parentId == "addmenu-lightbox" ) {		

				closeLightbox("addmenu");	
			}
			else if ( parentId == "loadcustommenu-lightbox" ) {		
				closeLightbox("loadcustommenu");	
			}
			else if ( parentId == "wizard-lightbox" ) {		
				closeLightbox("wizard");	
			}
		});
		
		jQuery(".inner-lightbox-close").click(function() {
			closeLightbox2();
		});
		
		jQuery(".lightbox-3-close").click(function() {
			closeLightbox3();
		});
		
		
	
	// Overlay Exit
		jQuery(".overlay").click(function(event) {			
			if (jQuery(event.target).is('.overlay')){
				if ( jQuery( this ).find( '#wizard-lightbox' ).css( 'display' ) == 'block' )
				{
					// do nothing	
				}
				else
				{				
					closeLightbox("all");
					closeModal("all");
				}
			}
		});
		
		jQuery(".overlay-2").click(function(event) {
			if (jQuery(event.target).is('.overlay-2')){
				closeLightbox2();
				/*closeModal("all");*/
			}
		});
		
		jQuery(".overlay-3").click(function(event) {
			if (jQuery(event.target).is('.overlay-3')){
				closeLightbox3();
			}
		});
		
	// Call Lightbox
		function callLightbox( lightboxType, callerId ) {
			addcardLightbox.hide();
			dailymacrosLightbox.hide();
			addmenuLightbox.hide();
			loadcustommenuLightbox.hide();
			overlayEl.fadeIn(300);
			
			if ( lightboxType == 'addcard' ) {
				addcardStage.empty();
				favoriterecipesStage.empty();
				addnoteStage.val('');
				
				addcardLightbox.find( '.nav-tabs' ).children( 'li' ).removeClass( 'active' ).eq(0).addClass( 'active' );
				addcardLightbox.find( '.tab-content' ).children( 'div' ).removeClass( 'active' ).eq(0).addClass( 'active' );
				
				addcardLightbox.fadeIn(300, function() {							
					categoryclickonprocess = true;
					var getactivecats = getActiveCats();
				
					callRecipe(jQuery("#searchrecipe-input").val(),'', '', getactivecats );					
				
					//callRecipe(jQuery("#searchrecipe-input").val(), '', '', jQuery("#searchrecipe-cat").val());
					callFavoriteRecipes('', '', 'first', '');								
				});						
				
				addcardLightbox.attr({ 'callfrom' : callerId });
			}
			else if ( lightboxType == 'addnote' )
			{
				addcardStage.empty();
				favoriterecipesStage.empty();
				addnoteStage.val('');
				
				addcardLightbox.find( '.nav-tabs' ).children( 'li' ).removeClass( 'active' ).eq(2).addClass( 'active' );
				addcardLightbox.find( '.tab-content' ).children( 'div' ).removeClass( 'active' ).eq(2).addClass( 'active' );
				
				addcardLightbox.addClass( 'addnote-lightbox' );
				addcardLightbox.fadeIn(300, function() {							
					categoryclickonprocess = true;
					var getactivecats = getActiveCats();
				
					callRecipe(jQuery("#searchrecipe-input").val(),'', '', getactivecats );					
				
					callFavoriteRecipes('', '', 'first', '');								
				});						
				
				addcardLightbox.attr({ 'callfrom' : callerId });
			}
			else if ( lightboxType == 'dailymacros' ) {
				var dailymacrosLightboxTitle = dailymacrosLightbox.find("#dailymacros-title");
				var dailymacrosLightboxContent = dailymacrosLightbox.find("#dailymacros-content");
				dailymacrosLightboxTitle.empty();
				dailymacrosLightboxContent.empty();
				
				// Title
				dailymacrosLightboxTitle.html( jQuery( '#' + callerId ).find( '.mymenu-date').html() + ' Macros' );
				
				// Blob
				var dailymacrosBlob = '';
				
				jQuery( '#' + callerId ).find( '.recipe' ).each( function() {
					if ( dailymacrosBlob == '' )
					{
						dailymacrosBlob = jQuery( this ).attr( 'recipeid' );
					}
					else
					{
						dailymacrosBlob = dailymacrosBlob + '^)^@' + jQuery( this ).attr( 'recipeid' );
					}
				});
				
				jQuery.ajax({	
					url: template_url + "/sys/loaddailymacros.php",				
					type: "POST",
					data: {'recipes' : dailymacrosBlob },
					success: function(data)
					{
						dailymacrosLightboxContent.html(data);
						
						dailymacrosLightbox.fadeIn(300);		
					}
				});	
				
			}
			else if ( lightboxType == 'addmenu' ) {
				addmenuStage.empty();
				addmenuLightbox.fadeIn(300, function() {
					callMenu(jQuery("#addmenu-input").val(), '');	
				});		
			}
			else if ( lightboxType == 'editmenu' ) {
				editmenuStage.empty();
				addmenuLightbox.fadeIn(300, function() {
					callEditMenu(jQuery("#editmenu-input").val(), '');	
				});		
			}
			else if ( lightboxType == 'loadcustommenu' ) {
				loadcustommenuStage.empty();
				loadcustommenuLightbox.fadeIn(300, function() {
					callCustomMenu();
				});		
			}
			else if ( lightboxType == 'addcardfavorite' ) {
				addcardStage.empty();
				favoriterecipesStage.empty();
				addnoteStage.val('');
				
				addcardLightbox.fadeIn(300, function() {							
					categoryclickonprocess = true;
					var getactivecats = getActiveCats();
					
					callRecipe(jQuery("#searchrecipe-input").val(),'', '', getactivecats );	
					//callRecipe(jQuery("#searchrecipe-input").val(), '', '', jQuery("#searchrecipe-cat").val());
					callFavoriteRecipes('', '', 'first', '');		
					
					// Force to Second Tab upon load
					jQuery( '#addcard-tabs > .nav-tabs > li:eq(1) > a' ).click();
				});						
				
				addcardLightbox.attr({ 'callfrom' : callerId });	
			}
			else if ( lightboxType == 'wizard' ) {
				overlayEl.addClass('firsttime-overlay');
				wizardLightbox.removeClass( 'firstsetup' );
				
				if ( callerId == 'firstsetup' )
				{
					wizardLightbox.addClass( 'firstsetup' );
				}
				
				wizardLightbox.fadeIn(300, function() {							
					
				});						
			}
		}
		
	// Call Lightbox
		function callLightbox2( recipeId, callfrom ) {
			addrecipeLightbox.hide();	
			overlay2El.fadeIn(300);
			
			addrecipeStage.empty();			
			addrecipeLightbox.attr('callfrom', callfrom);
			
			jQuery.ajax({	
				url: template_url + "/sys/previewrecipe.php",				
				type: "POST",
				data: {'recipeid': recipeId},
				success: function(data)
				{
					if ( data ) {						
						var metarecipe = data.split('^%$^');
						
						var title = metarecipe[0];
						var yield = 'Serves ' + metarecipe[1];
						var readytime = 'Preparation time: ' + metarecipe[2] + ', Cook time: ' + metarecipe[7] + '';
						var thumb = metarecipe[3];
						var ingredients = metarecipe[4];
						var instructions = metarecipe[5];
						var cats = metarecipe[6];
						var diff = metarecipe[8];
						var star = metarecipe[9];
						var recipeid = metarecipe[10];
						var longcontent = metarecipe[11];
						var shortcontent = metarecipe[12];
						
						addrecipeTitle.html(title);
						
						var blobmeta = '';
						var deblob = '@&^';
						blobmeta = title + deblob + yield + deblob + readytime + deblob + thumb + deblob + ingredients + deblob + instructions + deblob + cats;
						
						blobmeta = escapeHtml(blobmeta);

						var el = 
							'<div class="addrecipe-belt clearfix">' +
								'<div class="fav ' + star + '" recipeid="' + recipeid + '"></div>' +
								'<div class="searchrecipe-addbutton" recipeid="' + recipeId + '" blobmeta="' + blobmeta + '">Add</div>' +
							'</div>' +
							'<div class="addrecipe-header clearfix">' +
								'<div class="addrecipe-thumbnail" style="background-image:url(' + thumb + ')"></div>' +
								'<div class="addrecipe-topmeta">' +
									'<div class="addrecipe-cat">' + cats + '</div>' +
									yield + '<br>' + readytime + '<br>'+
									shortcontent + 
									'<!--Difficulty: ' + diff +
								'--></div>' +								
							'</div>' +
							'<div class="addrecipe-rest">' +
								longcontent + 
								'Ingredients:' + '<br>' + ingredients + '<br>' + 'Instructions:' + '<br>' + instructions +
							'</div>';
							
						
						
						addrecipeButton.attr({
							'recipeid' : recipeId,
							'blobmeta' : blobmeta
						});
						
						jQuery("#addrecipe-stage").html(el);								
					}							
				}
			});
			
			addrecipeLightbox.fadeIn(300);
		}
		
	// Call Lightbox 3
		function callLightbox3() {
			overlay3El.fadeIn(300);
			
			freeuserLightbox.fadeIn(300);
		}
				
	// Close Lightbox
		function closeLightbox( lightboxType ) {
			
			if ( lightboxType == "continued" ) {
				
				overlayEl.find(".generic-wait").remove();
				overlayEl.removeClass("generic-wait-background");						
			} else{
				
				if ( lightboxType == 'addcard' ) {
					addcardLightbox.removeClass( 'addnote-lightbox' );
					addcardLightbox.fadeOut(300);		
				}				
				else if ( lightboxType == 'dailymacros' ) {
					dailymacrosLightbox.fadeOut(300);		
				}
				else if ( lightboxType == 'addmenu' ) {
					addmenuLightbox.fadeOut(300);		
				}
				else if ( lightboxType == 'loadcustommenu' ) {
					loadcustommenuLightbox.fadeOut(300);		
				}
				else if ( lightboxType == 'wizard' ) {
					wizardLightbox.fadeOut(300);		
				}
				else if ( lightboxType == 'all' ) {
					addcardLightbox.fadeOut(300);
					addmenuLightbox.fadeOut(300);	
					loadcustommenuLightbox.fadeOut(300);		
				}
				else if ( lightboxType == 'generic' ) {
					overlayEl.find(".generic-wait").remove();
					overlayEl.removeClass("generic-wait-background");		
				}							
				overlayEl.fadeOut(300);				
			}

		}	
		
		function closeLightbox2() {
			addrecipeLightbox.fadeOut(300);
			yesnoModal2.fadeOut(300);
			overlay2El.fadeOut(300);	
		}
		
		function closeLightbox3() {
			freeuserLightbox.fadeOut(300);
			overlay3El.fadeOut(300);	
		}
		
	//Add Recipe Paging
		jQuery("body").on("click", "#searchrecipe-stage .nav-numbered", function() {
			var thisEl = jQuery(this);
			if ( thisEl.hasClass("static") || thisEl.hasClass("disabled") ) {
				
			}
			else {
				/* DOGE */
				categoryclickonprocess = true;
				var getactivecats = getActiveCats();
				
				callRecipe( thisEl.parent( '.paging-wrapper' ).attr( 'searchterm' ), thisEl.attr( 'target' ), '', getactivecats );
			}
		});	
		
	//Favorite Paging
		jQuery("body").on("click", "#favoriterecipes-stage .nav-numbered", function() {
			var thisEl = jQuery(this);
			if ( thisEl.hasClass("static") || thisEl.hasClass("disabled") ) {
				
			}
			else {
				/* DOGE */
				callFavoriteRecipes( '', thisEl.attr( 'target' ), 'first', '' );
			}
		});	
		
	// Call Recipe
		var addrecipecount = 1; 		
		var maxpagerecipecount = 1;		
		var onAJAXProcess = false;
	
		function callRecipe( searchTerm, pageNum, first, searchCat, infinitescroll ) {
			if ( typeof(infinitescroll)  === "undefined" )
			{
				// First page, clear all
				infinitescroll = '';
				addcardStage.empty();
				jQuery("#searchrecipe-message").empty();
				addcardStage.html('<div class="searchrecipe-wait">Please Wait</div>');				
				addrecipecount = 1;
			}
			else
			{
				// Second page and so on, no need to clear	
				addcardStage.find( '.searchrecipe-stage' ).append('<div class="searchrecipe-infinite-wait"></div>');	
			}
			
			var totalRecipesCount = 16;
			var widthEl = jQuery(window).width();
			
			if ( widthEl < 997 ) {
				//totalRecipesCount = 4; //8
			}			
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );			

			jQuery.ajax({	
				url: template_url + "/sys/loadrecipe.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, 'searchTerm': searchTerm, 'pageNum': pageNum, 'totalRecipes': totalRecipesCount, 'first' : first, 'cat' : searchCat, 'infinitescroll' : infinitescroll},
				success: function(data)
				{
					if ( data ) {	
						// Breakfast special
						if((jQuery("#searchrecipe-cat").val() == 87) && (data == '<div class="searchrecipe-empty">No recipes based on your settings. :(</div>'))
						{
							jQuery("#searchrecipe-stage").html("<div class='promotiontitle'>Purchase Over 200 Breakfast Recipes Now.<br><a href='http://20dishes.com/products/2213-monthly-breakfast.aspx' target=_blank><div class='promotionbutton'>Yes! I want breakfast!</div></a></div>");	
						} 
						else				
						{
							if ( infinitescroll == '' )
							{
								// First page
								
								// Find max page
								var string_maxpagerecipecount = jQuery( '<div>' + data + '</div>' ).find( '.searchrecipe-stage' ).attr( 'max_page' );
								if ( string_maxpagerecipecount != '' )
								{
									maxpagerecipecount = string_maxpagerecipecount;
								}

								jQuery("#searchrecipe-stage").html(jQuery(".sidebar").html() + data);								
							}
							else
							{
								// Second page and so on
								jQuery("#searchrecipe-stage .searchrecipe-stage").append(data);	
								addcardStage.find( '.searchrecipe-infinite-wait' ).remove();
							}
							
							
						}
						
						/*if ( first == 'first' ) {
							jQuery("#searchrecipe-message").html('Your favorite recipes:');	
						}
						else {
							jQuery("#searchrecipe-message").html('');	
						}*/
						
						onAJAXProcess = false;
						//jQuery( '#add-recipe' ).css({ 'background-color' : '#fff' });
						categoryclickonprocess = false;
					}							
				}
			});
		}	
		
		// Call Recipe - Infinite Scroll 
		
		
		jQuery("#searchrecipe-stage").scroll(function(){
			if ( onAJAXProcess == false )
			{			
				if(jQuery(this).scrollTop() + jQuery(this).innerHeight() >= jQuery(this)[0].scrollHeight - 200) {				
					//alert( addrecipecount + "|" + maxpagerecipecount );
					
					//jQuery( '#add-recipe' ).css({ 'background-color' : '#f00 !important' });
					
					onAJAXProcess = true;
					if ( ( addrecipecount + 1 ) > maxpagerecipecount )
					{
						addcardStage.find( '.searchrecipe-stage' ).append('<div class="searchrecipe-infinite-wait theend">No more recipes found.</div>');
						return false;	
					}
					else
					{
						categoryclickonprocess = true;
						var getactivecats = getActiveCats();
						
						callRecipe(jQuery("#searchrecipe-input").val(), ( addrecipecount + 1 ), '', getactivecats, 'infinitescrollpaging');					
					}
					addrecipecount++;
					
					
				}
			}
		});
		
	// Call Favorite Recipe - Call Recipe but favorite only
		var favoriterecipecount = 1;
		var maxpagefavoriterecipecount = 1;
	
		function callFavoriteRecipes( searchTerm, pageNum, first, searchCat, infinitescroll ) {
			if ( typeof(infinitescroll)  === "undefined" )
			{
				// First page, clear all
				infinitescroll = '';
				jQuery("#favoriterecipes-message").html('');
				favoriterecipesStage.empty();
				favoriterecipesStage.html('<div class="favoriterecipes-wait">Please Wait</div>');
				favoriterecipecount = 1;
			}
			else
			{
				// Second page and so on, no need to clear	
				favoriterecipesStage.find( '.searchrecipe-stage' ).append('<div class="searchrecipe-infinite-wait"></div>');	
			}
			
			var totalRecipesCount = 16;
			var widthEl = jQuery(window).width();
			
			if ( widthEl < 997 ) {
				//totalRecipesCount = 4; //8
			}			
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );
		
			jQuery.ajax({	
				url: template_url + "/sys/loadrecipe.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, 'searchTerm': searchTerm, 'pageNum': pageNum, 'totalRecipes': totalRecipesCount, 'first' : first, 'cat' : searchCat, 'infinitescroll' : infinitescroll},
				success: function(data)
				{
					if ( data ) {								
						/*jQuery("#favoriterecipes-stage").html(data);								
							*/
						
						if ( infinitescroll == '' )
						{
							// First page
							
							// Find max page
							var string_maxpagefavoriterecipecount = jQuery( '<div>' + data + '</div>' ).find( '.searchrecipe-stage' ).attr( 'max_page' );
							if ( string_maxpagefavoriterecipecount != '' )
							{
								maxpagefavoriterecipecount = string_maxpagefavoriterecipecount;
							}
							
							jQuery("#favoriterecipes-stage").html(data);								
						}
						else
						{
							// Second page and so on
							jQuery("#favoriterecipes-stage .searchrecipe-stage").append(data);	
							favoriterecipesStage.find( '.searchrecipe-infinite-wait' ).remove();
						}
						
						jQuery("#favoriterecipes-message").html('');
					}							
				}
			});
		}	
		
		// Call Favorite Recipe - Infinite Scroll 
		jQuery("#favoriterecipes-stage").scroll(function(){
			if(jQuery(this).scrollTop() + jQuery(this).innerHeight() >= jQuery(this)[0].scrollHeight) {				
				if ( ( favoriterecipecount + 1 ) > maxpagefavoriterecipecount )
				{
					favoriterecipesStage.find(".searchrecipe-stage").append('<div class="searchrecipe-infinite-wait theend">No more favorite recipe found.</div>');
					return false;	
				}
				else
				{		
					callFavoriteRecipes('', ( favoriterecipecount + 1 ), 'first', '', 'infinitescrollpaging');	
				}
				favoriterecipecount++;		
				
			}
		});
		
	// Prevent Scroll on lightbox affecting body scroll
		jQuery( '#searchrecipe-stage' ).scrollstop();		
		jQuery( '#favoriterecipes-stage' ).scrollstop();
		
	// Call Custom Menu
		function callCustomMenu() {
			loadcustommenuStage.empty();			
			//loadcustommenuStage.html('<div class="loadcustommenu-wait">Please Wait</div>');
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );			

			jQuery.ajax({	
				url: template_url + "/sys/loadcustommenu.php",				
				type: "POST",
				data: { 'id': memberpressID , 'ca': memberpressCA, user_id: jQuery('#user_id').val() },
				success: function(data)
				{
					if ( data ) {								
						loadcustommenuStage.html('<div class="custommenu-wrapper"><div class="newcustommenu custommenu">New custom menu...</div></div>' + data);								
					}							
				}
			});
		}	
		
	// Call Menu
		function callMenu( searchTerm, pageNum ) {
			addmenuStage.empty();
			addmenuStage.html('<div class="addmenu-wait">Please Wait</div>');
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );	
			
			jQuery.ajax({	
				url: template_url + "/sys/loadmenu.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, 'searchTerm': searchTerm, 'pageNum': pageNum},
				success: function(data)
				{
					if ( data ) {								
						jQuery("#addmenu-stage").html(data);								
					}							
				}
			});
		}
		
	// Call Edit Menu
		function callEditMenu( searchTerm, pageNum ) {
			editmenuStage.empty();
			editmenuStage.html('<div class="editmenu-wait">Please Wait</div>');
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );			
			
			jQuery.ajax({	
				url: template_url + "/sys/createmenu_loadmenu.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, 'searchTerm': searchTerm, 'pageNum': pageNum},
				success: function(data)
				{
					if ( data ) {								
						jQuery("#editmenu-stage").html(data);								
					}							
				}
			});
		}
		
	// Call Add Recipe Lightbox in My Menu
		jQuery("#mymenu-stage-wrapper").on("click", ".addcard", function() {
			callLightbox( 'addcard', jQuery(this).parents('.mymenu-day').attr('id') );
		});
		
	// Call Add Note Lightbox in My Menu
		jQuery("#mymenu-stage-wrapper").on("click", ".addnote", function() {
			callLightbox( 'addnote', jQuery(this).parents('.mymenu-day').attr('id') );
		});
		
	// Call Add Recipe Lightbox in Create Menu
		jQuery("#createmenu-stage-wrapper").on("click", ".addcard", function() {
			callLightbox( 'addcard', jQuery(this).parents('.mymenu-day').attr('id') );
		});
		
	// Call Add Note Lightbox in Create Menu
		jQuery("#createmenu-stage-wrapper").on("click", ".addnote", function() {	callLightbox( 'addnote', jQuery(this).parents('.mymenu-day').attr('id') );
		});	
		
	// Call Daily Macros Lightbox in Create Menu
		jQuery("#mymenu-stage-wrapper").on("click", ".dailymacros", function() {			
			callLightbox( 'dailymacros', jQuery(this).parents('.mymenu-day').attr('id') );
		});
		
	// Call Add Menu Lightbox
		jQuery("#mymenu-stage-wrapper").on("click", "#addmenu", function() {
			callLightbox( 'addmenu', '' );
		});
		
	// Add Recipe Card Click
		jQuery("body").on("click", ".searchrecipe", function() {										
			callLightbox2(jQuery(this).attr('recipeid'), jQuery(this).parents('#addcard-lightbox').attr('callfrom'));
		});
		
	// Add Recipe Click
		jQuery("body").on("click", ".searchrecipe-addbutton", function(e) {		
			e.stopPropagation();
			//addRecipe( jQuery(this).attr('recipeid'), jQuery(this).parents('#addcard-lightbox').attr('callfrom') );
			
			//callLightbox2(jQuery(this).attr('recipeid'), jQuery(this).parents('#addcard-lightbox').attr('callfrom'));
			
			if ( freeUser )
			{
				callLightbox3();	
			}
			else
			{
				if ( jQuery( this ).parent( '.addrecipe-belt' ).length )
				{
					// Add From Recipe Detail Lightbox
					addRecipeFromRecipeDetail( jQuery(this).attr('recipeid'), jQuery(this).parents('#addrecipe-lightbox').attr('callfrom'), jQuery(this).attr('blobmeta'), jQuery( this ) );
				}
				else
				{
					// Add From Add Recipe Lightbox
					addRecipe( jQuery(this).attr('recipeid'), jQuery(this).parents('#addcard-lightbox').attr('callfrom'), jQuery(this).attr('blobmeta'), jQuery( this ) );					
				}
			}
			
			
		});
	
	
	// DEFUNCT: Add Recipe from Recipe Detail - Elementnya jg di hidden, tp masi bisa diunlock
		jQuery("body").on("click", "#reallyaddrecipe", function() {
			addRecipeFromRecipeDetail( jQuery(this).attr('recipeid'), jQuery(this).parents('#addrecipe-lightbox').attr('callfrom'), jQuery(this).attr('blobmeta') );
		});
		
	// Add Menu Click
		jQuery("body").on("click", "#addmenu-stage .searchmenu", function() {
			addMenu( jQuery(this).attr('menuid') );
		});

	// Edit Menu Click
		jQuery("body").on("click", "#editmenu-stage .searchmenu", function() {
			editMenu( jQuery(this).attr('menuid') );
		});
		
	// Search with Category
		var categoryclickonprocess = false;
	
		jQuery(".searchrecipe-catbox").click(function() {
			if ( categoryclickonprocess == false )
			{			
				categoryclickonprocess = true;
				
				if ( jQuery( this ).hasClass( 'active' ) )
				{
					jQuery( this ).removeClass( 'active' );	
				}
				else 
				{
					jQuery( this ).addClass( 'active' );			
				}
				
				var getactivecats = getActiveCats();
				
				callRecipe(jQuery("#searchrecipe-input").val(),'', '', getactivecats );								
			}
		});
		
		function getActiveCats() {
			var allactivecats = '-1';
				
			jQuery( '#searchrecipe-catbox-wrapper' ).children( '.searchrecipe-catbox' ).each( function() {
				if ( jQuery( this ).hasClass( 'active' ) )
				{
					if ( allactivecats == '-1' )
					{
						allactivecats = jQuery( this ).attr( 'value' );
					}
					else
					{
						allactivecats += ',' + jQuery( this ).attr( 'value' );
					}
				}
			});	
			
			return allactivecats;
		}

	// Search Recipe				
		jQuery( "#searchrecipe-formitself" ).submit(function( event ) {
			event.preventDefault();
			
			categoryclickonprocess = true;			
			var getactivecats = getActiveCats();
				
			callRecipe(jQuery("#searchrecipe-input").val(),'', '', getactivecats );		
			//callRecipe(jQuery("#searchrecipe-input").val(),'', '',jQuery("#searchrecipe-cat").val());
		});
		
		// Search Menu				
		jQuery( "#addmenu-formitself" ).submit(function( event ) {
			event.preventDefault();
			callMenu(jQuery("#addmenu-input").val(),'');
		});
		
		jQuery( "#editmenu-formitself" ).submit(function( event ) {
			event.preventDefault();
			callEditMenu(jQuery("#editmenu-input").val(),'');
		});
		
	// Add Recipe
		function addRecipe(recipeId, callerId, blobmeta, addButton) {
			addButton.addClass( 'onadd onspin' ).html( 'Adding...' );
			
			// Check All Blob
			var dailymacrosBlob = '';
			
			jQuery( '#' + callerId ).find( '.recipe' ).each( function() {
				if ( dailymacrosBlob == '' )
				{
					dailymacrosBlob = jQuery( this ).attr( 'recipeid' );
				}
				else
				{
					dailymacrosBlob = dailymacrosBlob + '^)^@' + jQuery( this ).attr( 'recipeid' );
				}
			});			
			dailymacrosBlob = dailymacrosBlob + '^)^@' + recipeId;
			
			jQuery.ajax({	
				url: template_url + "/sys/addrecipe.php",				
				type: "POST",
				data: {'recipeId': recipeId, 'blobmeta' : blobmeta, 'currentdayrecipes' : dailymacrosBlob },
				success: function(data)
				{
					if ( data ) {						
						var arrData = jQuery.parseJSON(data);
						var flagExceed = flagCalories = flagFat = flagCarbs = 0;
						var warningCalories = warningFat = warningCarbs = '';

						if (typeof userRestrictCalories == 'undefined')
							userRestrictCalories = 200000;

						if (typeof userRestrictFat == 'undefined')
							userRestrictFat = 200000;	
						
						if (typeof userRestrictCarbs == 'undefined')
							userRestrictCarbs = 200000;		

						// Check Warning
						if ( arrData['calories'] > userRestrictCalories ) {
							flagCalories++; flagExceed++; warningCalories = 'Calories is exceeding ' + userRestrictCalories + '<br>';
						}
						
						if ( arrData['fat'] > userRestrictFat ) {
							flagFat++; flagExceed++; warningFat = 'Fat is exceeding ' + userRestrictFat + '<br>';
						}
						
						if ( arrData['carbohydrate'] > userRestrictCarbs ) {
							flagCarbs++; flagExceed++; warningCarbs = 'Carbohydrate is exceeding ' + userRestrictCarbs + '<br>';
						}
						
						jQuery('#' + callerId)
							.removeClass( 'active-warning' )
							.find( '.stage-warning' ).remove();
						
						if ( flagExceed > 0 )
						{
							jQuery('#' + callerId)
								.addClass( 'active-warning' )
								.find( '.mymenu-addrecipe-wrapper' )
								.append( '<div class="stage-warning">' + warningCalories + warningFat + warningCarbs + '</div>' );
						}

						// Add Element
						jQuery('#' + callerId).find(".mymenu-stage .mymenu-addrecipe-wrapper").before( arrData['newrecipeelement'] );	
						recipesChanged = 1;
						stepChanged = 1;
						shoppingChanged = 1;
						
						addButton.removeClass( 'onspin' ).html( 'Added!' );
						
						// From Add Recipe Lightbox
						setTimeout( function() {
							addButton.removeClass( 'onadd' ).html( 'Add' );
						}, 2000);
					}							
				}
			});
		}
		
	// Add Recipe from Recipe Detail
		function addRecipeFromRecipeDetail(recipeId, callerId, blobmeta, addButton) {
			//jQuery("#reallyaddrecipe").addClass("loaded");
			addButton.addClass( 'onadd onspin' ).html( 'Adding...' );
			
			// Check All Blob
			var dailymacrosBlob = '';
			
			jQuery( '#' + callerId ).find( '.recipe' ).each( function() {
				if ( dailymacrosBlob == '' )
				{
					dailymacrosBlob = jQuery( this ).attr( 'recipeid' );
				}
				else
				{
					dailymacrosBlob = dailymacrosBlob + '^)^@' + jQuery( this ).attr( 'recipeid' );
				}
			});			
			dailymacrosBlob = dailymacrosBlob + '^)^@' + recipeId;
			
			jQuery.ajax({	
				url: template_url + "/sys/addrecipe.php",				
				type: "POST",
				data: {'recipeId': recipeId, 'blobmeta' : blobmeta, 'currentdayrecipes' : dailymacrosBlob},
				success: function(data)
				{
					if ( data ) {
						var arrData = jQuery.parseJSON(data);
						var flagExceed = flagCalories = flagFat = flagCarbs = 0;
						var warningCalories = warningFat = warningCarbs = '';

						if (typeof userRestrictCalories == 'undefined')
							userRestrictCalories = 200000;

						if (typeof userRestrictFat == 'undefined')
							userRestrictFat = 200000;	
						
						if (typeof userRestrictCarbs == 'undefined')
							userRestrictCarbs = 200000;	

						// Check Warning
						if ( arrData['calories'] > userRestrictCalories ) {
							flagCalories++; flagExceed++; warningCalories = 'Calories is exceeding ' + userRestrictCalories + '<br>';
						}
						
						if ( arrData['fat'] > userRestrictFat ) {
							flagFat++; flagExceed++; warningFat = 'Fat is exceeding ' + userRestrictFat + '<br>';
						}
						
						if ( arrData['carbohydrate'] > userRestrictCarbs ) {
							flagCarbs++; flagExceed++; warningCarbs = 'Carbohydrate is exceeding ' + userRestrictCarbs + '<br>';
						}
						
						jQuery('#' + callerId)
							.removeClass( 'active-warning' )
							.find( '.stage-warning' ).remove();
						
						if ( flagExceed > 0 )
						{
							jQuery('#' + callerId)
								.addClass( 'active-warning' )
								.find( '.mymenu-addrecipe-wrapper' )
								.append( '<div class="stage-warning">' + warningCalories + warningFat + warningCarbs + '</div>' );
						}
						
						// Add Element
						jQuery('#' + callerId).find(".mymenu-stage .mymenu-addrecipe-wrapper").before( arrData['newrecipeelement'] );	
						recipesChanged = 1;
						stepChanged = 1;
						shoppingChanged = 1;
						
						addButton.removeClass( 'onspin' ).html( 'Added!' );
						
						// From Recipe Detail Lightbox	
						setTimeout( function() {
							addButton.removeClass( 'onadd' ).html( 'Add' );
							closeLightbox2();
							
						}, 200);
					}							
					
					//jQuery("#reallyaddrecipe").removeClass("loaded");
				}
			});
		}
		
	// Add Notes
		jQuery("body").on("click", "#addnotes-button", function() {										
			var notescontentEl = jQuery("#addnotes-textarea").val();					
			
			if ( notescontentEl != '' ) {
				var callerId = jQuery(this).parents('#addcard-lightbox').attr('callfrom');						
				var notescard = '';
				
				notescard = '<li class="notes">' +
								'<div class="notes-handle">' +
								'</div>' +
								'<div class="notes-content">' +
									notescontentEl +
								'</div>' +
								'<div class="notes-remove">' +
									'<div class="tdicon-thin-close"></div>' +
								'</div>' +
							'</li>';						
				
				closeLightbox('addcard');
				//jQuery('#' + callerId).find(".mymenu-stage").append( notescard );	
				jQuery('#' + callerId).find(".mymenu-stage .mymenu-addrecipe-wrapper").before(notescard);				
			}
		});
		
	// Add Menu
		function addMenu(menuid) {
			jQuery.ajax({	
				url: template_url + "/sys/addmenu.php",				
				type: "POST",
				data: {'menuid': menuid},
				success: function(data)
				{
					if ( data ) {
						closeLightbox('addmenu');								
						var arrData = data.split("|");														
						
						jQuery("#mymenu-sunday").find(".mymenu-stage").append(arrData[0]);
						jQuery("#mymenu-sunday").find(".mymenu-stage").append(x);
						jQuery("#mymenu-monday").find(".mymenu-stage").append(arrData[1]);
						jQuery("#mymenu-tuesday").find(".mymenu-stage").append(arrData[2]);
						jQuery("#mymenu-wednesday").find(".mymenu-stage").append(arrData[3]);
						jQuery("#mymenu-thursday").find(".mymenu-stage").append(arrData[4]);
						jQuery("#mymenu-friday").find(".mymenu-stage").append(arrData[5]);
						jQuery("#mymenu-saturday").find(".mymenu-stage").append(arrData[6]);
						
						recipesChanged = 1;
						stepChanged = 1;
						shoppingChanged = 1;
						
						if(currentTab == "step") {
							jQuery("#stepTab").click();
						}
						if(currentTab == "shopping") {
							jQuery("#shoppinglistTab").click();
						}
						if (currentTab == "recipes") {
							jQuery("#recipesdetailTab").click();	
						}						
					}							
				}
			});
		}
		
	// Add Menu
		function editMenu(menuid) {
			jQuery.ajax({	
				url: template_url + "/sys/editmenu.php",				
				type: "POST",
				data: {'menuid': menuid},
				success: function(data)
				{
					if ( data ) {
						closeLightbox('addmenu');								
						var arrData = data.split("^$$^");								
						var menuname = arrData[0];
						var schedule = arrData[1];
						var diet = arrData[2];
						var shared = arrData[3];
						var owner = arrData[4]
						
						var arrDataDetail = arrData[5].split('|');

						jQuery("#menuname-input").val(menuname);
						jQuery("#diet-input").val(diet);
						jQuery("#createmenu_selectDate").datepicker('setDate', new Date(schedule) );
						jQuery("#ownership").val(owner);
						jQuery("#currentmenuid").val(menuid);
						
						if ( shared == 'Y' ) {
							//jQuery("#checkShare").prop( 'checked', true );							
							jQuery('#checkShare').iCheck('check');
						}
						else {
							//jQuery("#checkShare").prop( 'checked', false );
							jQuery('#checkShare').iCheck('uncheck');
						}
						
					
						var buttonElements = '<div class="mymenu-addrecipe-wrapper">' +
							'<div class="addcard" data-step="5" data-intro="Click here to choose another recipe from the database">' +
								'Add Recipe' +
							'</div>' +
							'<div class="addcard addnote">' +
								'Add Note' +
							'</div>' +
						'</div>';
								
								
											
						jQuery("#mymenu-sunday").find(".mymenu-stage").empty().append(arrDataDetail[0]).append( buttonElements );
						jQuery("#mymenu-monday").find(".mymenu-stage").empty().append(arrDataDetail[1]).append( buttonElements );
						jQuery("#mymenu-tuesday").find(".mymenu-stage").empty().append(arrDataDetail[2]).append( buttonElements );
						jQuery("#mymenu-wednesday").find(".mymenu-stage").empty().append(arrDataDetail[3]).append( buttonElements );
						jQuery("#mymenu-thursday").find(".mymenu-stage").empty().append(arrDataDetail[4]).append( buttonElements );
						jQuery("#mymenu-friday").find(".mymenu-stage").empty().append(arrDataDetail[5]).append( buttonElements );
						jQuery("#mymenu-saturday").find(".mymenu-stage").empty().append(arrDataDetail[6]).append( buttonElements );
						
						if (owner == userid) {						
							jQuery("#deletemenu").show();
						}
					}							
				}
			});
		}

		
	// Remove recipe or note
		var yesnoModal = jQuery("#yesnoModal");
		var yesnoModal2 = jQuery("#yesnoModal2");
		var staticModal = jQuery("#static-modal");
		var textinputModal = jQuery("#textinputModal");
		
		window.modalPls = function( modalType, source, customContent ) {
			yesnoModal.hide();
			yesnoModal2.hide();
			staticModal.hide();
			textinputModal.hide();
			
			if ( modalType != 'custommenuremove' )
			{
				overlayEl.fadeIn(100);
				overlayEl.addClass("generic-wait-background");
			}
			
			if ( modalType == "removerecipe" ) {
				if ( customContent != '' ) {
					yesnoModal.find(".modal-scrutiny").html(customContent);
				}				
				
				yesnoModal.find(".yesbtn").unbind();
				yesnoModal.find(".yesbtn").bind("click", function() {
					closeModal("all");
					var tmpSource = source.closest( '.mymenu-day' );
					source.parents( ".recipe" ).remove();						
					
					// Check All Blob
					var dailymacrosBlob = '';
					
					tmpSource.find( '.recipe' ).each( function() {
						if ( dailymacrosBlob == '' )
						{
							dailymacrosBlob = jQuery( this ).attr( 'recipeid' );
						}
						else
						{
							dailymacrosBlob = dailymacrosBlob + '^)^@' + jQuery( this ).attr( 'recipeid' );
						}
					});								
					
					showLoadingAnimation();
					
					jQuery.ajax({	
						url: template_url + "/sys/checkmacros.php",				
						type: "POST",
						data: { 'currentdayrecipes' : dailymacrosBlob},
						success: function(data)
						{
							if ( data ) {
								var arrData = jQuery.parseJSON(data);
								var flagExceed = flagCalories = flagFat = flagCarbs = 0;
								var warningCalories = warningFat = warningCarbs = '';

								if (typeof userRestrictCalories == 'undefined')
									userRestrictCalories = 200000;

								if (typeof userRestrictFat == 'undefined')
									userRestrictFat = 200000;	
								
								if (typeof userRestrictCarbs == 'undefined')
									userRestrictCarbs = 200000;	
		
								// Check Warning
								if ( arrData['calories'] > userRestrictCalories ) {
									flagCalories++; flagExceed++; warningCalories = 'Calories is exceeding ' + userRestrictCalories + '<br>';
								}
								
								if ( arrData['fat'] > userRestrictFat ) {
									flagFat++; flagExceed++; warningFat = 'Fat is exceeding ' + userRestrictFat + '<br>';
								}
								
								if ( arrData['carbohydrate'] > userRestrictCarbs ) {
									flagCarbs++; flagExceed++; warningCarbs = 'Carbohydrate is exceeding ' + userRestrictCarbs + '<br>';
								}
								
								tmpSource
									.removeClass( 'active-warning' )
									.find( '.stage-warning' ).remove();
								
								if ( flagExceed > 0 )
								{
									tmpSource
										.addClass( 'active-warning' )
										.find( '.mymenu-addrecipe-wrapper' )
										.append( '<div class="stage-warning">' + warningCalories + warningFat + warningCarbs + '</div>' );
								}
								
								closeLightbox( "continued" );
							}							
						}
					});				
				});
				
				yesnoModal.fadeIn(100);				
			}	
			
			else if ( modalType == "removenote" ) {
				
				if ( customContent != '' ) {
					yesnoModal.find(".modal-scrutiny").html(customContent);
				}				
				
				yesnoModal.find(".yesbtn").unbind();
				yesnoModal.find(".yesbtn").bind("click", function() {
					closeModal("all");
					source.parents( ".notes" ).remove();					
				});
				
				yesnoModal.fadeIn(100);				
			}
			
			else if ( modalType == "newmenu" ) {
				
				if ( customContent != '' ) {
					yesnoModal.find(".modal-scrutiny").html(customContent);
				}	
				
				yesnoModal.find(".yesbtn").unbind();
				yesnoModal.find(".yesbtn").bind("click", function() {
					closeModal("all");
					
					newmenu();
				});
				
				yesnoModal.fadeIn(100);				
			}
			
			else if ( modalType == "menuremove" ) {
				
				if ( customContent != '' ) {
					yesnoModal.find(".modal-scrutiny").html(customContent);
				}				
				
				yesnoModal.find(".yesbtn").unbind();
				yesnoModal.find(".yesbtn").bind("click", function() {
					closeModal("all");
					
					/* Get Member Press ID & CA */
					var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
					var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );					

					jQuery.ajax({	
						url: template_url + "/sys/removemenu.php",				
						type: "POST",
						data: { 'id': memberpressID, 'ca': memberpressCA, 'menuid': source.val() },
						success: function(data)
						{							
							if ( data ) {
								newmenu();
							}							
						}
					});					
				});
				
				yesnoModal.fadeIn(300);	
			}	
			
			else if ( modalType == "custommenuremove" ) {
				overlay2El.fadeIn(100);
				
				if ( customContent != '' ) {
					yesnoModal2.find(".modal-scrutiny").html(customContent);
				}				
				
				yesnoModal2.find(".yesbtn").unbind();
				yesnoModal2.find(".yesbtn").bind("click", function() {
					closeModal2("all");
					
					/* Get Member Press ID & CA */
					var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
					var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );					
					
					jQuery.ajax({	
						url: template_url + "/sys/removecustommenu.php",				
						type: "POST",
						data: { 'id': memberpressID , 'ca': memberpressCA, 'menuid': source },
						success: function(data)
						{							
							if ( data ) {
								callCustomMenu();
								
								if ( jQuery( '#currentcustommenuid' ).val() == source ) {
									// Deleted menu is currently on display
									jQuery( '#custommenu-title' ).html( '*Deleted*' );
									jQuery( '#saveasnewmenu' ).hide();
									jQuery( '#saveweek' ).hide();
								}
							}							
						}
					});					
				});
				
				yesnoModal2.fadeIn(300);	
			}		

			else if ( modalType == "groupsharing" ) {
				
				if ( customContent != '' ) {
					yesnoModal.find(".modal-scrutiny").html(customContent);
				}	
				
				yesnoModal.find(".yesbtn").unbind();
				yesnoModal.find(".yesbtn").bind("click", function() {
					closeModal("all");
					
					groupsharing(source);
				});
				
				yesnoModal.fadeIn(300);				
			}			
			
			else if ( modalType == "error" ) {
						
				if ( customContent != '' ) {
					staticModal.find(".modal-scrutiny").html(customContent);
					staticModal.addClass("errormodal");
				}				
				
				staticModal.fadeIn(300);				
			}				
			else if ( modalType == "success" ) {
				if ( customContent != '' ) {
					staticModal.find(".modal-scrutiny").html(customContent);
				}				
				
				staticModal.fadeIn(300);				
			}	
			
			else if ( modalType == "updatemenuinschedule" ) {
				
				if ( customContent != '' ) {
					
					var menu = "";
					for(i = 0; i < customContent.length; i++){
						menu += customContent[i].name;
						
						if( i != customContent.length-1)
							menu += ", ";
					}
					yesnoModal.find(".modal-scrutiny").html("The following menu has been updated: "+ menu +". <br><br>Do you want to update your schedule to follow the latest update?");
				}				
				
				yesnoModal.find(".yesbtn").unbind();
				yesnoModal.find(".yesbtn").bind("click", function() {
					closeModal("all");

					var currDate = jQuery("#currentdate").val();
					
					/* Get Member Press ID & CA */
					var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
					var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );					
					
					jQuery.ajax({	
						url: template_url + "/sys/updatemenuinschedule.php",				
						type: "POST",
						data: { 'id': memberpressID , 'ca': memberpressCA, 'targetDate': currDate },
						success: function(data)
						{							
							if ( data ) {
								location.reload();
							} else {
								staticModal.find(".modal-scrutiny").html("An error occured but your schedule is safe. Please contact 20Dishes team for support.");								
							}	
							staticModal.fadeIn(300);								
						}
					});					
				});
				
				yesnoModal.fadeIn(300);					
			}
			
			else if ( modalType == "saveasnewmenu" ) {
				// Clear Input Text and Focus it
				var inputModal = textinputModal.find("#textinputModal-input");
				var errorInputModal = textinputModal.find(".textinputModal-error");
				var user_id = jQuery("#user_id").val();
				
				inputModal.val('');				
				textinputModal.fadeIn(300);				
				inputModal.focus();
				
				// Yes Click Event
				textinputModal.find(".yesbtn").unbind();
				textinputModal.find(".yesbtn").bind("click", function() {
					if ( inputModal.val().trim() == '' ) {
						// Kosong
						errorInputModal.html( 'Please pick a name.' );			
						inputModal.focus();	
					}
					else {
						// Cek Nama						
						jQuery.ajax({	
							url: template_url + "/sys/checkcustommenuname.php",				
							type: "POST",
							data: {'menuname': inputModal.val(), 'user_id': user_id },
							success: function(data)
							{			
								if ( data == false ) {	
									// Nama exist
									errorInputModal.html('Menu name already exist.');									
									inputModal.focus();	
									
									return false;
								}
								else {
									showLoadingAnimation();
									saveAsNewMenu( 'new', inputModal.val() );	
									
									errorInputModal.html( '' );
									closeModal("all");
								}
							}
						});
					}
				});
			}
		
		}
		
		function closeModal( modalType ) {
			if ( modalType == 'removerecipe' ) {
				yesnoModal.fadeOut(100);		
			}
			else if ( modalType == 'static' ) {
				staticModal.fadeOut(100, function() {
					staticModal.removeClass("errormodal");
				});
			}
			else if ( modalType == 'all' ) {
				yesnoModal.fadeOut(100);					
				textinputModal.fadeOut(100);					
				staticModal.fadeOut(100, function() {
					staticModal.removeClass("errormodal");
				});
			}										
			overlayEl.removeClass("generic-wait-background").fadeOut(300);			
		}
		
		function closeModal2( modalType ) {
			if ( modalType == 'all' ) {
				yesnoModal2.fadeOut(100);					
			}										
			overlay2El.fadeOut(300);			
		}
		
		jQuery( "body" ).on( "click", "#massDelete", function() {
			var thisEl = jQuery( this );
			if ( thisEl.hasClass( 'activated' ) ) {
				thisEl.removeClass( 'activated' );
			}
			else {
				thisEl.addClass( 'activated' );	
			}
		});
	
		jQuery( "body" ).on( "click", ".recipe-remove", function() {
			var massDelete = jQuery( '#massDelete' );
			if ( massDelete.hasClass( 'activated' ) ) {
				// No question asked, just delete it
				jQuery( this ).parents( ".recipe" ).remove();		
			}
			else {
				modalPls( "removerecipe", jQuery( this ), '' );
			}
			recipesChanged = 1;
			stepChanged = 1;
			shoppingChanged = 1;
		});
		
		jQuery( "body" ).on( "click", ".notes-remove", function() {
			modalPls( "removenote", jQuery( this ), 'Are you sure want to remove this note?' );
		});
		
		jQuery( "body" ).on("click", ".overlay .nobtn", function() {
			closeModal("all");
		});
		
		jQuery( "body" ).on("click", ".overlay-2 .nobtn", function() {
			closeModal2("all");
		});
		
	// Save Click
		jQuery("body").on("click", "#saveweek", function() {
			saveWeekClick();
		});
		
		if ( jQuery( ".mymenu-saveloop").length ) {
			setInterval(function() {
				saveWeekClick( 'onloop' );				
			}, 45000);	
		}
		
		function saveWeekClick( onloop ) {			
			var onloopFlag = '';

			if ( onloop == 'onloop' ) {
				onloopFlag = 'onloop';		
			}
		
			var arrTargetDate = jQuery("#currentdate").val().split("-");
		
			targetDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date(arrTargetDate[0], arrTargetDate[1] - 1, arrTargetDate[2]) );
			if (targetDate == '') 
				targetDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date());	
			
			saveWeek(targetDate, onloopFlag);	
		}
		
		function saveWeek(targetDate, onloopFlag) {
			var blob = '';
			var thisEl;
			var recipeid;
			var menuid;
			var tanggal;
			var notes;
			var orders;
			var delimiter = '^@%^';
			var delimiterDetail = '^)^@';
			
			if ( onloopFlag == '' ) {
				showLoadingAnimation();
			}
			jQuery(".mymenu-day").each(function() {
				orders = 1;
				jQuery(this).find(".mymenu-stage").find("li").each(function() {
					thisEl = jQuery(this);						
					tanggal = thisEl.parents(".mymenu-day").attr('targetdate');
					recipeid = '';
					menuid = '';
					notes = '';
					 
					if ( thisEl.hasClass("recipe") ) {							
						recipeid = thisEl.attr('recipeid');
						menuid = thisEl.attr('menuid');
					}
					else if ( thisEl.hasClass("notes") ) {
						notes = jQuery.trim(thisEl.find(".notes-content").text());
					}
					
					if ( blob == '' ) {
						blob = recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes;
					}
					else {
						blob += delimiter + recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes;
					}
					orders++;
				});
			});

			jQuery.ajax({	
				url: template_url + "/sys/saveweek.php",				
				type: "POST",
				data: {'userid': userid , 'targetDate': targetDate, 'blob' : blob},
				success: function(data)
				{	
					if ( onloopFlag == '' ) {
						closeLightbox("continued");
						
						if ( data == true ) 
							modalPls('success', '', 'Menu has been saved.');
						else 
							modalPls('error', '', 'Error occured, please try again or contact administrator.');
					}
				}
			});
		}	
		
	// Save as New Menu
		jQuery("body").on("click", "#saveasnewmenu", function() {		
			var thisEl = jQuery(this);
			var currentcustommenuid = jQuery("#currentcustommenuid").val().trim();
			
			if ( currentcustommenuid == 'none' ) {
				// New Custom Menu, give it a name
				modalPls('saveasnewmenu','','');	
			}
			else {
				// Update Existing Custom Menu				
				showLoadingAnimation();
				saveAsNewMenu( 'update', jQuery('#custommenu-title').html() );
			}			
		});
		
		function saveAsNewMenu( neworupdate, menuname ) {
			// Check Name
			var user_id = jQuery("#user_id").val();
			var currentcustommenuid = jQuery("#currentcustommenuid").val().trim();
			var targetDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date());
						
			var blob = '';
			var thisEl;
			var recipeid;
			var menuid;
			var tanggal;
			var notes;
			var orders;
			var days;
			var delimiter = '^@%^';
			var delimiterDetail = '^)^@';
			var dietid = 0; // Unused for Custom Menu
			var shared = 'N'; // Always N for Custom Menu
			
			// Get Menu Data
			days = 1;
			jQuery(".mymenu-day").each(function() {
				orders = 1;
				
				jQuery(this).find(".mymenu-stage").find("li").each(function() {
					thisEl = jQuery(this);						
					tanggal = 0; //thisEl.parents(".mymenu-day").attr('targetdate'); --> USELESS in this table
					recipeid = '';
					menuid = '';
					notes = '';
					 
					if ( thisEl.hasClass("recipe") ) {							
						recipeid = thisEl.attr('recipeid');
						menuid = thisEl.attr('menuid');
					}
					else if ( thisEl.hasClass("notes") ) {
						notes = jQuery.trim(thisEl.find(".notes-content").text());
					}
					
					if ( blob == '' ) {
						blob = recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes + delimiterDetail + days;
					}
					else {
						blob += delimiter + recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes + delimiterDetail + days;
					}
					orders++;
					
				});
				
				days++;
			});
			
			// Execute
			if ( neworupdate == 'new' ) {
				// New Custom menu

				jQuery.ajax({	
					url: template_url + "/sys/savecustommenu.php",				
					type: "POST",
					data: {'new' : 'newmenu', 'userid': userid , 'dietid' : dietid, 'menuname' : menuname, 'shared' : shared, 'targetDate': targetDate, 'menuid' : currentcustommenuid, 'blob' : blob},
					success: function(data)
					{
						closeLightbox("continued");
						
						if ( data != false ) {
							modalPls('success', '', 'Custom Menu has been saved as ' + menuname + '.');
							
							// Set This Planner Menu as Custom Menu
							jQuery('#currentcustommenuid').val( data.trim() ); // Tell the Newly saved custom menu's ID
							jQuery('#saveasnewmenu').html( 'SAVE ' + menuname ); // Let people know this is currently as custom menu
							jQuery('#saveweek').hide(); // Don't let people save official menu
							jQuery('#custommenu-title').html(menuname).show();							
							
							// Reload Custom Menu
							callCustomMenu();
						}
						else {
							modalPls('error', '', 'Error occured, please try again or contact administrator.');
						}					
					}
				});
				
			}
			else if ( neworupdate == 'update' ) {
				jQuery.ajax({	
					url: template_url + "/sys/savecustommenu.php",				
					type: "POST",
					data: {'new' : 'updatemenu', 'userid': userid , 'dietid' : dietid, 'menuname' : menuname, 'shared' : shared, 'targetDate': targetDate, 'menuid' : currentcustommenuid, 'blob' : blob},
					success: function(data)
					{
						closeLightbox("continued");
						
						if ( data != false ) {
							modalPls('success', '', menuname + ' has been saved.');
							
							// Set This Planner Menu as Custom Menu
							//jQuery('#currentcustommenuid').val( data.trim() ); // Tell the Newly saved custom menu's ID
							//jQuery('#saveasnewmenu').html( 'SAVE ' + menuname ); // Let people know this is currently as custom menu
							//jQuery('#saveweek').hide(); // Don't let people save to official menu
							//jQuery('#custommenu-title').html(menuname).show();
							
						}
						else {
							modalPls('error', '', 'Error occured, please try again or contact administrator.');
						}					
					}
				});
			}
			
		}	
		
	// Add Card Favorite - Replace Load Custom Menu Below	
		jQuery("body").on("click", "#addcardfavorite", function() {
			callLightbox('addcardfavorite', 'mymenu-sunday');
		});
		
	// Create / Load Custom Menu
		// Menu on Click
		/*jQuery("body").on("click", "#customMenuButton", function() {
			if ( jQuery( this ).hasClass( 'menuopened' ) )
			{
				jQuery( this ).removeClass( 'menuopened' );	
			}
			else
			{
				jQuery( this ).addClass( 'menuopened' );	
			}
		});*/
		
		// The Rest
		jQuery("body").on("click", "#loadCustomMenu", function() {
			callLightbox('loadcustommenu');
		});
		
		jQuery("body").on("click", ".custommenu", function() {
			if ( jQuery( this ).hasClass( 'newcustommenu' ) )
			{
				createCustomMenu();
			}
			else
			{
				loadCustomMenu( jQuery(this).attr('menuid'), jQuery(this).attr('menuname') );
			}
		});
		
		jQuery("body").on("click", "#resetMenu", function() {
			//jQuery("#planpanel").html(data);	
			jQuery( '.mymenu-day .mymenu-stage li' ).remove();			
			
			jQuery( ".mymenu-stage" ).sortable({
				//revert: true
				handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
				placeholder: "recipe-placeholder",
				connectWith: ".mymenu-stage",
				zIndex: 10000,
			}).disableSelection(); 
			
			recipesChanged = 1;
			stepChanged = 1;
			shoppingChanged = 1;
		});
		
		function createCustomMenu() {
			// Change the plan
			//jQuery("#planpanel").html(data);	
			jQuery( '.mymenu-day .mymenu-stage li' ).remove();			
			
			jQuery( ".mymenu-stage" ).sortable({
				//revert: true
				handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
				placeholder: "recipe-placeholder",
				connectWith: ".mymenu-stage",
				zIndex: 10000,
			}).disableSelection(); 
			
			recipesChanged = 1;
			stepChanged = 1;
			shoppingChanged = 1;
			
			// Reset this custom menu planner
			jQuery( '#currentdate' ).val('');
			jQuery( '#currentweek' ).val('');
			jQuery('#currentcustommenuid').val( 'none' ); // It's new
			jQuery('#saveasnewmenu').html( 'SAVE AS...' ).show(); // Let people know it's new and unsaved
			jQuery('#saveweek').hide(); // Don't let people save to official menu
			jQuery('#custommenu-title').html('New Custom Menu').show();
			
		}
		
		function loadCustomMenu(menuid, menuname) {
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );			
			
			jQuery.ajax({	
				url: template_url + "/sys/createmenu_loadcustomweek.php",				
				type: "POST",
				data: {'id': memberpressID , 'ca': memberpressCA, 'menuid': menuid},
				success: function(data)
				{
					if ( data ) {
						// Change the plan
						jQuery("#planpanel").html(data);	
						
						jQuery( ".mymenu-stage" ).sortable({
							//revert: true
							handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
							placeholder: "recipe-placeholder",
							connectWith: ".mymenu-stage",
							zIndex: 10000,
						}).disableSelection(); 
						
						recipesChanged = 1;
						stepChanged = 1;
						shoppingChanged = 1;
						/*
						if(currentTab == "plan") {
							closeLightbox("generic");
						}														
						if(currentTab == "shopping") {
							jQuery("#shoppinglistTab").click();
						}
						if(currentTab == "step") {
							jQuery("#stepTab").click();
						}	
						if (currentTab == "recipes") {
							jQuery("#recipesdetailTab").click();	
						}*/
						/*jQuery("#currentweek").val(targetWeek);
						checkNextWeek(jQuery);	
						checkRecipe();*/
						
						closeLightbox("all");
						
						// Prepare this custom menu planner
						jQuery('#currentcustommenuid').val( menuid ); // Tell the Newly saved custom menu's ID
						jQuery('#saveasnewmenu').html( 'SAVE' ).show(); // Let people know this is currently as custom menu
						jQuery('#saveweek').hide(); // Don't let people save to official menu
						jQuery('#custommenu-title').html(menuname).show();
					}							
				}
			});		
		}
		
	// Save Shopping Click
		jQuery("body").on("click", "#saveshopping", function() {

			var arrTargetDate = jQuery("#currentdate").val().split("-");
		
			targetDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date(arrTargetDate[0], arrTargetDate[1] - 1, arrTargetDate[2]) );
			
			if (arrTargetDate.length == '1') {
				
				menuid = jQuery("#currentcustommenuid").val();
				
				if (typeof menuid != 'undefined')
					targetDate = menuid + "-01-01";
				else
					targetDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date());
			}		

			saveShopping(targetDate);
		});		
		
		function saveShopping(targetDate) {
			var blob = '';
			var thisEl;
			var ingredient;
			var quantity;
			var crossed;
			var delimiter = '^@%^';
			var delimiterDetail = '^)^@';
			var note = jQuery(".textareaadditem").val();
			
			showLoadingAnimation();
			jQuery(".ingredient_list_ingredient").each(function() {
				
				thisEl = jQuery(this);
				ingredient = thisEl.find(".col_ingredient").text();
				quantity = thisEl.find(".col_amount").val();
				
				if ( thisEl.find(".col_ingredient").hasClass( "stroked" ) ) 
					crossed = 1;
				else
					crossed = 0;
				
				if ( blob == '' ) {
					blob = ingredient + delimiterDetail + quantity + delimiterDetail + crossed;
				}
				else {
					blob += delimiter + ingredient + delimiterDetail + quantity + delimiterDetail + crossed;
				}				
			});
		
			if ( blob != '' ) {				

				jQuery.ajax({	
					url: template_url + "/sys/saveshopping.php",				
					type: "POST",
					data: {'userid': userid , 'targetDate': targetDate, 'blob' : blob, 'note' : note},
					success: function(data)
					{					

						if ( data == true ) {
							generateShoppingData(2);
							closeLightbox("continued");
							modalPls('success', '', 'Shopping list has been saved.');
						}
						else {
							closeLightbox("continued");
							modalPls('error', '', 'Error occured, please try again or contact administrator.');
						}
						
					}
				});
			}
		}			
		
	// Reload Shopping Click
		jQuery("body").on("click", "#reloadshopping", function() {

				generateShoppingData(1);				
		});				
		
	// Create Menu Save Click
		jQuery("body").on("click", "#saveeditmenu", function() {

			var thisEl = jQuery(this);
			if ( thisEl.hasClass("ready") ) {
				thisEl.removeClass("ready");
				thisEl.addClass("saving");	
				thisEl.empty();
				
				var targetDate = '';
				if ( jQuery("#createmenu_selectDate").length ) {
					targetDate = jQuery.datepicker.formatDate("yy-mm-dd", jQuery("#createmenu_selectDate").datepicker('getDate'));
				}
				
				if (targetDate == '') {
					targetDate = jQuery.datepicker.formatDate('yy-mm-dd', new Date());	
				}
				
				//saving
				saveMenu(targetDate);
			}
		});
		
	// Create Menu Save
		function saveMenu(targetDate) {
			// Check Name
			var menuname = jQuery("#menuname-input").val();
			var savebutton = jQuery("#saveeditmenu");
			var currentmenuid = jQuery("#currentmenuid").val();
			var owner = jQuery("#ownership").val();
			
			if ( menuname == '' ) {
				modalPls('error', '', 'Please type the name of the menu.');
				
				savebutton.hide();
				savebutton.html('Save');
				savebutton.addClass("ready");		
				savebutton.removeClass("message");
				savebutton.removeClass("saving");
				savebutton.show(300);
				
				return;	
			}
			
			if ( owner == 'new' || currentmenuid == 'new' ) {
				// Check
				jQuery.ajax({	
					url: template_url + "/sys/checkmenuname.php",				
					type: "POST",
					data: {'menuname': menuname},
					success: function(data)
					{			
						if ( data == false ) {				
							modalPls('error', '', 'Menu with the same name has already exist.');
							
							setTimeout(function() {
								savebutton.hide();
								savebutton.html('Save');
								savebutton.addClass("ready");		
								savebutton.removeClass("message");
								savebutton.removeClass("saving");
								savebutton.fadeIn(300);
							}, 2000);
							return false;
						}	
						else {
							var blob = '';
							var thisEl;
							var recipeid;
							var menuid;
							var tanggal;
							var notes;
							var orders;
							var days;
							var delimiter = '^@%^';
							var delimiterDetail = '^)^@';
							
							//Diet
							var dietid = jQuery("#diet-input").val();			
							var shared = '';
							
							if ( jQuery("#createmenu-saving-wrapper input").length ) {				
								var sharedStatus = JSON.stringify(jQuery("#createmenu-saving-wrapper input:checkbox:checked").map(function(){
													return jQuery(this).val();
												}).get());
												
								if ( sharedStatus == "[]" ) {
									shared = 'N';
								}
								else {
									shared = 'Y';	
								}
							}
							else {
								shared = 'N';	
							}
				
							days = 1;
							jQuery(".mymenu-day").each(function() {
								orders = 1;
								
								jQuery(this).find(".mymenu-stage").find("li").each(function() {
									thisEl = jQuery(this);						
									tanggal = 0; //thisEl.parents(".mymenu-day").attr('targetdate'); --> USELESS in this table
									recipeid = '';
									menuid = '';
									notes = '';
									 
									if ( thisEl.hasClass("recipe") ) {							
										recipeid = thisEl.attr('recipeid');
										menuid = thisEl.attr('menuid');
									}
									else if ( thisEl.hasClass("notes") ) {
										notes = jQuery.trim(thisEl.find(".notes-content").text());
									}
									
									if ( blob == '' ) {
										blob = recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes + delimiterDetail + days;
									}
									else {
										blob += delimiter + recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes + delimiterDetail + days;
									}
									orders++;
									
								});
								
								days++;
							});
							/*if ( blob == '' ) {			
								// Cancel Save	
								savebutton.html('Save');
								savebutton.removeClass("saving");
								savebutton.addClass("ready");
							}
							else {			*/
								//alert(userid + '<br>' + dietid + '<br>' + menuname + '<br>' + shared + '<br>' + targetDate + '<br>' + owner + '<br>' + currentmenuid + '<br>' + blob);
								jQuery.ajax({	
									url: template_url + "/sys/savemenu.php",				
									type: "POST",
									data: {'userid': userid , 'dietid' : dietid, 'menuname' : menuname, 'shared' : shared, 'targetDate': targetDate, 'ownership' : owner, 'menuid' : currentmenuid, 'blob' : blob},
									success: function(data)
									{
										if ( data == true) {
											// update data
											savebutton.fadeOut(300, function() {						
												savebutton.show();
												savebutton.html('Saved!');
												savebutton.addClass("message");
												
												setTimeout(function() {
													savebutton.hide();
													savebutton.html('Save');
													savebutton.addClass("ready");		
													savebutton.removeClass("message");
													savebutton.removeClass("saving");
													savebutton.fadeIn(300);
												}, 2000);
											});
										}
											
										else if ( data == false ) {
											// error
											savebutton.fadeOut(300, function() {						
												/*savebutton.show();
												savebutton.html('Try Again');
												savebutton.addClass("message");*/
												
												setTimeout(function() {
													savebutton.hide();
													savebutton.html('Save');
													savebutton.addClass("ready");		
													savebutton.removeClass("message");
													savebutton.removeClass("saving");
													savebutton.fadeIn(300);
													
													modalPls('error', '', 'Error occured, please try again or contact administrator.');
												}, 2000);
											});	
										}
										else {
											// new data
											savebutton.fadeOut(300, function() {						
												savebutton.show();
												savebutton.html('Saved!');
												savebutton.addClass("message");
												
												setTimeout(function() {
													savebutton.hide();
													savebutton.html('Save');
													savebutton.addClass("ready");		
													savebutton.removeClass("message");
													savebutton.removeClass("saving");
													savebutton.fadeIn(300);
												}, 2000);
											});
											
											jQuery('#ownership').val(userid);						
											jQuery('#currentmenuid').val(data);								
										}					
									}
								});
							//}	
						}
					}
				});
			}
			else {
			
				var blob = '';
				var thisEl;
				var recipeid;
				var menuid;
				var tanggal;
				var notes;
				var orders;
				var days;
				var delimiter = '^@%^';
				var delimiterDetail = '^)^@';
				
				
				
				//Diet
				var dietid = jQuery("#diet-input").val();			
				var shared = '';
				
	
				if ( jQuery("#createmenu-saving-wrapper input").length ) {				
					var sharedStatus = JSON.stringify(jQuery("#createmenu-saving-wrapper input:checkbox:checked").map(function(){
										return jQuery(this).val();
									}).get());
									
					if ( sharedStatus == "[]" ) {
						shared = 'N';
					}
					else {
						shared = 'Y';	
					}
				}
				else {
					shared = 'N';	
				}
	
				days = 1;
				jQuery(".mymenu-day").each(function() {
					orders = 1;
					
					jQuery(this).find(".mymenu-stage").find("li").each(function() {
						thisEl = jQuery(this);						
						tanggal = 0; //thisEl.parents(".mymenu-day").attr('targetdate'); --> USELESS in this table
						recipeid = '';
						menuid = '';
						notes = '';
						 
						if ( thisEl.hasClass("recipe") ) {							
							recipeid = thisEl.attr('recipeid');
							menuid = thisEl.attr('menuid');
						}
						else if ( thisEl.hasClass("notes") ) {
							notes = jQuery.trim(thisEl.find(".notes-content").text());
						}
						
						if ( blob == '' ) {
							blob = recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes + delimiterDetail + days;
						}
						else {
							blob += delimiter + recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes + delimiterDetail + days;
						}
						orders++;
						
					});
					
					days++;
				});

				jQuery.ajax({	
					url: template_url + "/sys/savemenu.php",				
					type: "POST",
					data: {'userid': userid , 'dietid' : dietid, 'menuname' : menuname, 'shared' : shared, 'targetDate': targetDate, 'ownership' : owner, 'menuid' : currentmenuid, 'blob' : blob},
					success: function(data)
					{
						if ( data == true) {
							// update data
							savebutton.fadeOut(300, function() {						
								savebutton.show();
								savebutton.html('Saved!');
								savebutton.addClass("message");
								
								setTimeout(function() {
									savebutton.hide();
									savebutton.html('Save');
									savebutton.addClass("ready");		
									savebutton.removeClass("message");
									savebutton.removeClass("saving");
									savebutton.fadeIn(300);
								}, 2000);
							});
						}
							
						else if ( data == false ) {
							// error
							savebutton.fadeOut(300, function() {						

								setTimeout(function() {
									savebutton.hide();
									savebutton.html('Save');
									savebutton.addClass("ready");		
									savebutton.removeClass("message");
									savebutton.removeClass("saving");
									savebutton.fadeIn(300);
									
									modalPls('error', '', 'Error occured, please try again or contact administrator.');
								}, 2000);
							});	
						}
						else {
							// new data
							savebutton.fadeOut(300, function() {						
								savebutton.show();
								savebutton.html('Saved!');
								savebutton.addClass("message");
								
								setTimeout(function() {
									savebutton.hide();
									savebutton.html('Save');
									savebutton.addClass("ready");		
									savebutton.removeClass("message");
									savebutton.removeClass("saving");
									savebutton.fadeIn(300);
								}, 2000);
							});
							
							jQuery('#ownership').val(userid);						
							jQuery('#currentmenuid').val(data);								
						}					
					}
				});

			}
		}	
		
	// Group Sharing
	function groupsharing(group) {

		var blob = '';
		var thisEl;
		var recipeid;
		var menuid;
		var tanggal;
		var notes;
		var orders;
		var days;
		var delimiter = '^@%^';
		var delimiterDetail = '^)^@';
	
		days = 1;
		jQuery(".mymenu-day").each(function() {
			orders = 1;
			
			jQuery(this).find(".mymenu-stage").find("li").each(function() {
				thisEl = jQuery(this);						
				tanggal = jQuery("#groupshare_selectDate").val(); 
				recipeid = '';
				menuid = '';
				notes = '';
				 
				if ( thisEl.hasClass("recipe") ) {							
					recipeid = thisEl.attr('recipeid');
					menuid = thisEl.attr('menuid');
				}
				else if ( thisEl.hasClass("notes") ) {
					notes = jQuery.trim(thisEl.find(".notes-content").text());					
				}
				
				if ( blob == '' ) {
					blob = recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes + delimiterDetail + days;
				}
				else {
					blob += delimiter + recipeid + delimiterDetail + menuid + delimiterDetail + tanggal + delimiterDetail + orders + delimiterDetail + notes + delimiterDetail + days;
				}
				orders++;
				
			});
			
			days++;
		});

		jQuery.ajax({	
			url: template_url + "/sys/groupsharing.php",				
			type: "POST",
			data: {'group' : group, 'blob' : blob},
			success: function(data)
			{
				if ( data == true) 
					modalPls('success', '', 'Menu has been shared successfully.');
				else if ( data == false ) 
					modalPls('error', '', 'Error occured, please try again or contact administrator.');
			}
		});
	}		
		
	// -------------------
	// Create Menu Section
	// -------------------
	
	function newmenu() {
		/* Get Member Press ID & CA */
		var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
		var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );		
		
		jQuery.ajax({	
			url: template_url + "/sys/createmenu_loadweek.php",				
			type: "POST",
			data: {'id': memberpressID , 'ca': memberpressCA, 'menuid': ''},
			success: function(data)
			{
				if ( data ) {
					jQuery("#createmenu-all-wrapper").html(data);	
					
					jQuery( ".mymenu-stage" ).sortable({
						//revert: true
						handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
						placeholder: "recipe-placeholder",
						connectWith: ".mymenu-stage",
						zIndex: 10000,
					}).disableSelection(); 		
					
					jQuery("#createmenu_selectDate").datepicker({
						buttonImage: template_url + '/sys/img/tabicons2.png',
						buttonImageOnly: true,
						changeMonth: true,
						changeYear: true,
						showOn: 'both',
						dateFormat: "M d, yy"
					});		
					
					jQuery("#createmenu-stage-wrapper").on("click", ".addcard", function() {			
						callLightbox( 'addcard', jQuery(this).parents('.mymenu-day').attr('id') );
					});
					
					jQuery("#deletemenu").hide();	
				}							
			}
		});		
	}
	
	jQuery("body").on("click", "#newmenu", function() {
		modalPls("newmenu", '', 'Creating new menu will discard any unsaved changes. Proceed?');
	});
	
	jQuery("body").on("click", "#editmenu", function() {
		callLightbox('editmenu', '');		
	});
	
	jQuery("body").on("click", "#deletemenu", function() {
		modalPls("menuremove", jQuery("#currentmenuid"), 'Are you sure want to delete the entire menu?');
	});
	
	jQuery("body").on("click", ".custommenu-delete", function() {
		modalPls("custommenuremove", jQuery(this).attr("menuid"), 'Are you sure want to delete ' + jQuery(this).attr('menuname') + '?' );
	});
	
	jQuery("body").on( "mouseover","#menupublishdate-input-wrapper img", function() {
		jQuery(this).attr('src', template_url + '/sys/img/tabicons2hover.png');
	});
	
	jQuery("body").on( "mouseout","#menupublishdate-input-wrapper img", function() {
		jQuery(this).attr('src', template_url + '/sys/img/tabicons2.png');
	});
	
	jQuery('#selectShare').on('change', function() {
		var currentmenuid = jQuery("#currentmenuid").val();	
		
		if(this.value != "...") {
			
			if(currentmenuid == "new") {
				modalPls("error", "", "You have to save this menu before sharing it to a group.");
				return;
			}
			modalPls( "groupsharing", this.value, "Are you sure you want to share this menu with group <b>" + this.value + "</b>?")
		}
	});	
	
	jQuery("#groupshare_selectDate").datepicker({
		buttonImage: template_url + '/sys/img/tabicons2.png',
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		showOn: 'both',
		dateFormat: "M d, yy"
	});	

	jQuery("body").on("click", "#prevweek", function() {
		callAdjacentPlan('prevweek', jQuery(this).html());			
	});
	
	jQuery("body").on("click", "#thisweek", function() {
		callAdjacentPlan('thisweek', jQuery(this).html());			
	});
	
	jQuery("body").on("click", "#nextweek", function() {		
		callAdjacentPlan('nextweek', jQuery(this).html());			
	});
	
	function callAdjacentPlan(targetWeek, dateString) {
		var today = new Date(); 
		var targetDate = '';
		
		if ( targetWeek == 'prevweek' ) {			
			targetDate = new Date(today.getFullYear(), parseInt( today.getMonth() ), parseInt(today.getDate()) - 7);
		
		}
		else if ( targetWeek == 'nextweek' ) {			
			targetDate = new Date(today.getFullYear(), parseInt( today.getMonth() ), parseInt(today.getDate()) + 7);
		}
		else {
			targetDate = new Date();	
		}
		var resultDate = jQuery.datepicker.formatDate("yy-mm-dd", targetDate);
		
		showLoadingAnimation();
		
		/* Get Member Press ID & CA */
		var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
		var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );
		
		jQuery.ajax({	
			url: template_url + "/sys/loadweek.php",				
			type: "POST",
			data: {'id': memberpressID , 'ca': memberpressCA, 'targetDate': resultDate},
			success: function(data)
			{
				if ( data ) {
					
					// Change the text
					jQuery("#selectedWeek").html(dateString);
					
					var tmpTitle = jQuery("#selectedWeek").html().split('<br>');
					jQuery("#custommenu-title").html(tmpTitle[1]);
					
					// Change the plan
					jQuery("#planpanel").html(data);	
					
					jQuery( ".mymenu-stage" ).sortable({
						//revert: true
						handle: ".recipe-header, .recipe-thumbnail, .recipe-content, .notes-handle, .notes-content",
						placeholder: "recipe-placeholder",
						connectWith: ".mymenu-stage",
						zIndex: 10000,
					}).disableSelection(); 
					
					recipesChanged = 1;
					stepChanged = 1;
					shoppingChanged = 1;
					
					if(currentTab == "plan") {
						closeLightbox("generic");
					}														
					if(currentTab == "shopping") {
						jQuery("#shoppinglistTab").click();
					}
					if(currentTab == "step") {
						jQuery("#stepTab").click();
					}	
					if (currentTab == "recipes") {
						jQuery("#recipesdetailTab").click();	
					}
					jQuery("#currentweek").val(targetWeek);
					checkNextWeek(jQuery);	
					checkRecipe();
					
					// Prepare this menu planner to be saved as Custom Menu
					jQuery("#currentcustommenuid").val("none"); // let us know the planner currently filled with official menu
					jQuery("#saveweek").show(); // show save menu button (only if previously hidden by saveAsNewMenu function)
					jQuery("#saveasnewmenu").hide(); //.html( 'SAVE MENU TO ARCHIVE' ); // restore button text to the original (only if previously changed by saveAsNewMenu function)
				}							
			}
		});	
	}
	
	jQuery('body').on('click', '.fav', function(event) {
		if (jQuery(event.target).is('.fav')){
			event.stopPropagation();
			event.preventDefault();
			
			var thisEl = jQuery(this);
			thisEl.addClass( 'onadd onspin' );
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' ) ;
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' ) ;			
			
			if ( thisEl.hasClass('blankstar') ) {
				jQuery.ajax({	
					url: template_url + "/sys/addfav.php",				
					type: "POST",
					data: {'id': memberpressID , 'ca': memberpressCA, 'recipeid': thisEl.attr('recipeid')},
					success: function(data)
					{
						if ( data ) {
							if ( data == true ) {
								setTimeout( function() {
									thisEl.removeClass( 'onadd onspin' );								
									thisEl.removeClass('blankstar').addClass('fullstar');	
								}, 500);
							}
						}				
					}
				});
			}
			else if ( thisEl.hasClass('fullstar') ) {
				jQuery.ajax({	
					url: template_url + "/sys/removefav.php",				
					type: "POST",
					data: {'id': memberpressID , 'ca': memberpressCA, 'recipeid': thisEl.attr('recipeid')},
					success: function(data)
					{
						if ( data ) {
							if ( data == true ) {
								thisEl.removeClass('fullstar').addClass('blankstar');	
								
								setTimeout( function() {
									thisEl.removeClass( 'onadd onspin' );								
									thisEl.removeClass('fullstar').addClass('blankstar');	
								}, 500);
							}
						}										
					}
				});
			}
		}		
	});
	
	jQuery('body').on('click', '#recipesdetail-accordion input', function(event) {
		if (jQuery(event.target).is('input')){
			event.stopPropagation();
			event.preventDefault();
			
		}		
	});
	
	jQuery('body').on('click', '#readme', function(event) {
		window.open("http://20dishes.com/start-here/",'_blank');
	});
	
	// Select All
	jQuery('#recipesdetail > .savewrapper > input').iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal'
	});
	
	jQuery('#recipesdetail-stage > input').iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal'		
	});
	
	jQuery('.wizard-panel input').iCheck({
		checkboxClass: 'icheckbox_minimal',
		radioClass: 'iradio_minimal'
	});
	
	jQuery('#recipeSelectAll').on('ifChecked', function(){ 
		jQuery('.recipe-check').each(function() { 
			jQuery(this).iCheck('check');              
		});
	});
	
	jQuery('#recipeSelectAll').on('ifUnchecked', function(){ 
		jQuery('.recipe-check').each(function() { 
			jQuery(this).iCheck('uncheck');              
		});
	});
	
	// Print Check
	jQuery('#recipePrintPlan').on('ifChecked', function(){ 
		jQuery("#plan").removeClass("notprinted");
	});	
	jQuery('#recipePrintPlan').on('ifUnchecked', function(){ 
		jQuery("#plan").addClass("notprinted");
	});
	
	jQuery('#recipePrintStep').on('ifChecked', function(){ 
		jQuery("#stepbystep").removeClass("notprinted");
	});	
	jQuery('#recipePrintStep').on('ifUnchecked', function(){ 
		jQuery("#stepbystep").addClass("notprinted");
	});
	
	jQuery('#recipePrintShopping').on('ifChecked', function(){ 
		jQuery("#shoppinglist").removeClass("notprinted");
	});	
	jQuery('#recipePrintShopping').on('ifUnchecked', function(){ 
		jQuery("#shoppinglist").addClass("notprinted");
	});
	
	jQuery('#recipePrintRecipe').on('ifChecked', function(){ 
		jQuery("#recipesdetail").removeClass("notprintedparent");
		jQuery("#recipesdetail-print").removeClass("notprinted");
		jQuery("#recipesdetail-stage").show();
	});	
	jQuery('#recipePrintRecipe').on('ifUnchecked', function(){ 
		jQuery("#recipesdetail").addClass("notprintedparent");
		jQuery("#recipesdetail-print").addClass("notprinted");
		jQuery("#recipesdetail-stage").hide();
	});
	
	
	

	jQuery('body').on('ifUnchecked', '#recipesdetail-accordion input', function() {		
		jQuery('#recipeSelectAll').iCheck('indeterminate');
	});
	
	jQuery('body').on('click', '.recipe-hover', function() {
		var insightEl;
		insightEl = jQuery(this).find('.recipe-insight');
		 
		if ( insightEl.hasClass('shown') ) {			
			insightEl.fadeOut(200);	
			insightEl.removeClass('shown');
		}
		else {			
			var recipeIdInsight = insightEl.parents(".recipe").attr("recipeid");
			
			jQuery('.recipe-insight').removeClass("shown").hide();
			
			insightEl.html("loading...").fadeIn(200);
			
			// Find Fav from blob
			var str_parentblob = jQuery(this).parents('.recipe').attr('blobmeta');		
			
			if (str_parentblob != null) {	
				
				var arr_parentblob = str_parentblob.split("@&^");					
				var blob_fav = arr_parentblob[7];
				
			} else {
				
				blob_fav = 0;
				
			}
			
			/* Get Member Press ID & CA */
			var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
			var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );
			
			jQuery.ajax({
				url: template_url + "/sys/loadinsight.php",
				type: "POST",
				data: { 'id': memberpressID , 'ca': memberpressCA, recipeId: recipeIdInsight, fav: blob_fav},
				success: function(data) 
				{
					if (data) {
						insightEl.html(data);
					}					
					else {
						insightEl.html("No data.");
					}
				}
			});
			
			insightEl.addClass('shown');
		}
	});
	
	jQuery('body').on('click', '.step-done-check', function() {
		 
		jQuery(this).parent().parent().toggleClass("step-done-active");	
		
	});
	
	// Mymenu Start		
		if ( jQuery( 'body' ).hasClass( 'page-template-template-mymenupublic' ) )
		{
			// Init
			checkRecipe();
			
			// Intro Js
			var checkExist2 = setInterval(function() 
			{		  
				if (jQuery("#mealplan-icon").length) 
				{
					//firstentry = "true"; // TWI
				
					// IntroJS initialization				
					if ( firstentry ) {
						// Setup Wizard
						callLightbox( 'wizard',	'firstsetup' );	
						//introJs().start(); // TWI
					}
					
					clearInterval(checkExist2);	   
				}		   
			}, 200);
		}
		
		// Start Menu - Add introjs step 10
		jQuery( '#menu-item-4475' ).find( 'a' ).attr({
			'data-step' : '10',
			'data-intro' : 'Click here for more detailed instructions' 
		});	
	
		// Restart Guide		
		jQuery( 'body' ).on( 'click', '#startguide', function() {
			 // Start Menu - Add introjs step 10
			  $j( '#menu-item-4475' ).find( 'a' ).attr({
				 'data-step' : '10',
				 'data-intro' : 'To get more detailed orientation, watch our tutorial videos here' 
			  });
			  
			  // introJs().start(); TWI
		});
		
		// Wizard
		jQuery( '.wizard-footer > #wizard-next' ).click(function() {
			var wizWrap = jQuery( this ).parents( '#wizard-lightbox' );
			
			if ( wizWrap.hasClass( 'currentwizard2' ) )
			{
				var dietPlan = JSON.stringify(jQuery("#wizard-lightbox #dietOptions input:checkbox:checked").map(function(){
								return jQuery(this).val();
							}).get());
				if(dietPlan == "[]") {				
				
					jQuery( '.wizard-error' ).html( 'Please select at least one mode of diet.' );
					return;
				}
				else
				{
					jQuery( '.wizard-error' ).html('');	
				}
			} 			
			
			if ( wizWrap.hasClass( 'currentwizard0' ) )
			{
				wizWrap.removeClass( 'currentwizard0' ).addClass( 'currentwizard1' );
			}
			else if ( wizWrap.hasClass( 'currentwizard1' ) )
			{
				wizWrap.removeClass( 'currentwizard1' ).addClass( 'currentwizard2' );
			}
			else if ( wizWrap.hasClass( 'currentwizard2' ) )
			{
				wizWrap.removeClass( 'currentwizard2' ).addClass( 'currentwizard3' );
			}
			else if ( wizWrap.hasClass( 'currentwizard3' ) )
			{
				wizWrap.removeClass( 'currentwizard3' ).addClass( 'currentwizard4' );
			}
			else if ( wizWrap.hasClass( 'currentwizard4' ) )
			{
				wizWrap.removeClass( 'currentwizard4' ).addClass( 'currentwizard5' );
			}			
		});
		
		jQuery( '.wizard-footer > #wizard-back' ).click(function() {
			var wizWrap = jQuery( this ).parents( '#wizard-lightbox' );
			
			if ( wizWrap.hasClass( 'currentwizard2' ) )
			{
				var dietPlan = JSON.stringify(jQuery("#wizard-lightbox #dietOptions input:checkbox:checked").map(function(){
								return jQuery(this).val();
							}).get());
				if(dietPlan == "[]") {				
				
					jQuery( '.wizard-error' ).html( 'Please select at least one mode of diet.' );
					return;
				}
				else
				{
					jQuery( '.wizard-error' ).html('');	
				}
			} 			
			
			if ( wizWrap.hasClass( 'currentwizard5' ) )
			{
				wizWrap.removeClass( 'currentwizard5' ).addClass( 'currentwizard4' );
			}
			else if ( wizWrap.hasClass( 'currentwizard4' ) )
			{
				wizWrap.removeClass( 'currentwizard4' ).addClass( 'currentwizard3' );
			}
			else if ( wizWrap.hasClass( 'currentwizard3' ) )
			{
				wizWrap.removeClass( 'currentwizard3' ).addClass( 'currentwizard2' );
			}
			else if ( wizWrap.hasClass( 'currentwizard2' ) )
			{
				wizWrap.removeClass( 'currentwizard2' ).addClass( 'currentwizard1' );
			}
			else if ( wizWrap.hasClass( 'currentwizard1' ) )
			{
				wizWrap.removeClass( 'currentwizard1' ).addClass( 'currentwizard0' );
			}
		});

});

function checkNextWeek() {
	// Check Next Week
	/*currentDate = jQuery("#currentdate").val().split('-');
	targetYear = currentDate[0];
	targetMonth = parseInt(currentDate[1]) - 1;
	targetDay = parseInt(currentDate[2]) + 7;
	targetDate = new Date(targetYear, targetMonth, targetDay);						*/
	
	var today = new Date(); 
	/*var nextsunday = new Date(today.getFullYear(),today.getMonth(),today.getDate()+(7-today.getDay()));
	var next2sunday = new Date(today.getFullYear(),today.getMonth(),today.getDate()+(14-today.getDay()));
	
	if (targetDate >= nextsunday) {
		// berarti uda minggu yg current
		jQuery("#nextweek").hide();	
		
		if ( today.getDay() == 6 && targetDate < next2sunday) {
			// sabtu only
			jQuery("#nextweek").show();							
		}						
	}
	else {
		jQuery("#nextweek").show();							
	}*/
		
	if ( today.getDay() >= 4 ) {
		// sabtu only
		jQuery("#nextweek").show();							
	}	
	else {
		jQuery("#nextweek").hide();	
	}
}

function checkRecipe() {
	
	var currDate = jQuery("#currentdate").val();	
	
	/* Get Member Press ID & CA */
	var memberpressID = ( getUrlParameter( 'ID' ) === undefined ) ? '' : getUrlParameter( 'ID' );
	var memberpressCA = ( getUrlParameter( 'ca' ) === undefined ) ? '' : getUrlParameter( 'ca' );
	
	jQuery.ajax({	
		url: template_url + "/sys/checkupdate.php",				
		type: "POST",
		data: {'id': memberpressID , 'ca': memberpressCA, 'targetDate': currDate},
		success: function(data)
		{
			if ( data ) {
				
				var updatedMenu = jQuery.parseJSON(data);
				
				if(updatedMenu.length > 0)
					modalPls( "updatemenuinschedule", jQuery( this ), updatedMenu );
			}				
		}
	});	
	
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};