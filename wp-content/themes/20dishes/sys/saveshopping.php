<?php
	if ( isset( $_POST['targetDate'] ) && isset( $_POST['userid'] ) && isset( $_POST['blob'] ) && isset( $_POST['note'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = saveweek($_POST['userid'], $_POST['targetDate'], $_POST['blob'], $_POST['note'] );
		echo $ajaxResult;
	}	
	
	function saveweek($userid, $targetDate, $blob, $note) {	
		global $wpdb;		
		$responseErr = '';
		
		$wpdb->query( 'SET autocommit = 0;' );
		$wpdb->query( 'START TRANSACTION' );
		
		$targetDateFormatted = date( 'm/d/Y', strtotime( $targetDate ) );
		$targetDay = date( 'w', strtotime( $targetDate ) );
		$arrTargetDate = array();
		$arrTargetDate[0] = date( 'Y-m-d', strtotime( $targetDateFormatted . '-' . $targetDay . ' days' ) );
		
		//DELETE
		if ( $wpdb->delete(
						"jp_shoppinglist",
						array(
							'user_id' => $userid,
							'schedule' => $arrTargetDate[0]
						),
						array(
							'%d',
							'%s'
			)
		) === false ) {
			$responseErr .= 'error';
		}
		
		if ( $wpdb->delete(
						"jp_shoppingnote",
						array(
							'user_id' => $userid,
							'schedule' => $arrTargetDate[0]
						),
						array(
							'%d',
							'%s'
			)
		) === false ) {
			$responseErr .= 'error';
		}		
		
		//INSERT
		$arrBlob = explode("^@%^", $blob);		
				
		foreach($arrBlob as $singleblob) {
			$arrDetail = explode("^)^@", $singleblob);
			
			$ingredient = $arrDetail[0];
			$quantity = $arrDetail[1];
			$crossed = $arrDetail[2];
			
			if ( $wpdb->insert(
							"jp_shoppinglist",
							array(
								'user_id' => $userid,
								'schedule' => $arrTargetDate[0],
								'ingredient' => $ingredient,
								'quantity' => $quantity,
								'crossed' => $crossed
							),
							array(
								'%d',
								'%s',
								'%s',
								'%s',
								'%d'
							)
			) === false ) {
				$responseErr .= 'error';	
			}	
		}				
		
		if ( $wpdb->insert(
						"jp_shoppingnote",
						array(
							'user_id' => $userid,
							'schedule' => $arrTargetDate[0],
							'note' => $note
						),
						array(
							'%d',
							'%s',
							'%s'
						)
		) === false ) {
			$responseErr .= 'error';	
		}				
				
		if ( $responseErr == '' ) {	
			$wpdb->query( 'COMMIT' );
			$wpdb->query( 'SET autocommit = 1;' );
			
			return true;	
		}
		else {
			$wpdb->query('ROLLBACK');
			$wpdb->query( 'SET autocommit = 1;' );
			
			return false;	
		}
	}
?>