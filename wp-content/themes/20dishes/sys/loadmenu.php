<?php
	require_once 'globalfunction.php';
	
	if ( isset( $_POST['searchTerm'] ) && isset( $_POST['pageNum'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = loadmenu($_POST['searchTerm'], $_POST['pageNum']);
		echo $ajaxResult;
	}	

	function loadmenu($search, $pageNum) {		
		global $wpdb;

		// Execution
		//$current_user = wp_get_current_user();
		$current_user = checkCorporateAccount();		
		
		$sql = $wpdb->prepare( 'SELECT * FROM jp_menu WHERE (shared = \'Y\' OR user_id = ' . $current_user . ') AND name LIKE \'%%%s%%\'', esc_sql( like_escape( $search ) ) ); 	
		$result = $wpdb->get_results( $sql );
		
		$allthemenu = '';
		
		if ( $result ) {
			foreach ( $result as $key ){
				$allthemenu .= '<div class="searchmenu" menuid="' . $key->id . '">' . $key->name . '</div><br>';
			}			
		}
		else {
			echo '<div class="addmenu-empty">Sorry, no result :(</div>';		
		}
		
		return $allthemenu;
	}
?>