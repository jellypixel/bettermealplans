<?php
	if ( isset( $_POST['new'] ) && isset( $_POST['targetDate'] ) && isset( $_POST['userid'] ) && isset( $_POST['blob'] ) && isset( $_POST['dietid'] ) && isset( $_POST['menuname'] ) && isset( $_POST['shared'] )  && isset( $_POST['menuid'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = savemenu($_POST['userid'], $_POST['targetDate'], $_POST['dietid'], $_POST['menuname'], $_POST['shared'], $_POST['new'], $_POST['menuid'], $_POST['blob'] );
		echo $ajaxResult;
	}	
	
	function savemenu($userid, $targetDate, $dietid, $menuname, $shared, $new, $currentmenuid, $blob) {	
	
		global $wpdb;		
		$responseErr = '';
		
		$wpdb->query( 'SET autocommit = 0;' );
		$wpdb->query( 'START TRANSACTION' );
		
		$lastid = '';
		
		if ( $new == 'newmenu' ) {
			// New Menu	- JP_Menu			
				if ( $wpdb->insert(
								"jp_menu",
								array(
									'user_id' => $userid,
									'diet_id' => $dietid,
									'name' => $menuname,
									'schedule' => $targetDate,
									'shared' => $shared,
									'notes' => ''
								),
								array(
									'%d',
									'%d',
									'%s',
									'%s',
									'%s',
									'%s'
								)
				) === false ) {
					$responseErr .= 'error';
					$wpdb->query('ROLLBACK');
				}
				else {
					$wpdb->query( 'COMMIT' );
				}
				
				$lastid = $wpdb->insert_id;
			
			// New Menu - JP_Menu_Recipe
				if ( !empty( $blob ) ) {
					$arrBlob = explode("^@%^", $blob);		
						
					foreach($arrBlob as $singleblob) {
						$arrDetail = explode("^)^@", $singleblob);
						
						$recipeid = $arrDetail[0];
						$menuid = $arrDetail[1];
						$tanggal = $arrDetail[2];
						$orders = $arrDetail[3];
						$notes = $arrDetail[4];
						$days = $arrDetail[5];
						
						if ( $wpdb->insert(
										"jp_menu_recipe",
										array(
											'menu_id' => $lastid,
											'recipe_id' => $recipeid,
											'day' => $days,
											'part' => 0,
											'orders' => $orders,
											'notes' => $notes
										),
										array(
											'%d',
											'%d',
											'%d',
											'%d',
											'%d',
											'%s'
										)
						) === false ) {
							$responseErr .= 'error';
							$wpdb->query('ROLLBACK');
						}
						else {
							$wpdb->query( 'COMMIT' );
						}
					}
				}
		}
		else if ( $new == 'updatemenu' ) {
			// Edit	Menu - JP_Menu
				$wpdb->update( 
							'jp_menu', 
							array( 
								'user_id' => $userid,
								'diet_id' => $dietid,
								'name' => $menuname,
								'schedule' => $targetDate,
								'shared' => $shared,
								'notes' => ''
							), 
							array( 'id' => $currentmenuid ), 
							array( 
								'%d',
								'%d',
								'%s',
								'%s',
								'%s',
								'%s'
							), 
							array( '%d' ) 
				);
				
			// Edit Menu - JP_Menu_Recipe
			
				/* DElETE First */
				if ( $wpdb->delete(
								"jp_menu_recipe",
								array(
									'menu_id' => $currentmenuid
								),
								array(
									'%d'
					)
				) === false ) {
					$responseErr .= 'error';
					$wpdb->query('ROLLBACK');
				}
				else {
					$wpdb->query( 'COMMIT' );
				}
				
				/* INSERT THEN */
				if ( !empty( $blob ) ) {
					$arrBlob = explode("^@%^", $blob);		
							
					foreach($arrBlob as $singleblob) {
						$arrDetail = explode("^)^@", $singleblob);
						
						$recipeid = $arrDetail[0];
						$menuid = $arrDetail[1];
						$tanggal = $arrDetail[2];
						$orders = $arrDetail[3];
						$notes = $arrDetail[4];
						$days = $arrDetail[5];
						
						if ( $wpdb->insert(
										"jp_menu_recipe",
										array(
											'menu_id' => $currentmenuid,
											'recipe_id' => $recipeid,
											'day' => $days,
											'part' => 0,
											'orders' => $orders,
											'notes' => $notes
										),
										array(
											'%d',
											'%d',
											'%d',
											'%d',
											'%d',
											'%s'
										)
						) === false ) {
							$responseErr .= 'error';
							$wpdb->query('ROLLBACK');
						}
						else {
							$wpdb->query( 'COMMIT' );
						}
					}
				}
		}
				
		$wpdb->query( 'SET autocommit = 1;' );
		
		if ( $responseErr == '' ) {	
			if ( $lastid != '' ) {
				return $lastid;
			}
			else {
				return true;		
			}
		}
		else {
			return false;	
		}
	}
?>