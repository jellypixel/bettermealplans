<?php
	require_once 'globalfunction.php';
	
	if ( isset( $_POST['recipes'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
		
		$ajaxResult = loaddailymacros($_POST['recipes']);
	}	
	
	function loaddailymacros($recipes) 
	{
		$current_user = checkCorporateAccount();
		
		// Master / Slave
		$master = false;	
		if ( $current_user != get_current_user_id() )
		{
			$master = true;
		}
				
		try {
			// Prepare Nutritional
			$arrDetail = array_filter(explode("^)^@", $recipes));
			$recipeNutritionals = array ();
			$recipeNutritionals["calories"] = 0;
			$recipeNutritionals["carbohydrate"] = 0;
			$recipeNutritionals["protein"] = 0;
			$recipeNutritionals["fat"] = 0;
			$recipeNutritionals["saturated_fat"] = 0;
			$recipeNutritionals["polyunsaturated_fat"] = 0;
			$recipeNutritionals["monounsaturated_fat"] = 0;
			$recipeNutritionals["trans_fat"] = 0;
			$recipeNutritionals["cholesterol"] = 0;
			$recipeNutritionals["sodium"] = 0;
			$recipeNutritionals["potassium"] = 0;	
			$recipeNutritionals["fiber"] = 0;	
			$recipeNutritionals["sugar"] = 0;	
			$recipeNutritionals["vitamin_a"] = 0;	
			$recipeNutritionals["vitamin_c"] = 0;	
			$recipeNutritionals["calcium"] = 0;	
			$recipeNutritionals["iron"] = 0;		
				
			foreach($arrDetail as $key){

				$recipe = new TwentyDishes_Recipe($key);
				if($recipe->title == "recipe deleted")
					continue;
					
				$nutrition = $recipe->nutrition;

				$recipeNutritionals["calories"] += $nutrition["calories"];
				$recipeNutritionals["carbohydrate"] += $nutrition["carbohydrates"];
				$recipeNutritionals["protein"] += $nutrition["protein"];
				$recipeNutritionals["fat"] += $nutrition["fat"];
				$recipeNutritionals["saturated_fat"] += $nutrition["saturated_fat"];
				$recipeNutritionals["polyunsaturated_fat"] += $nutrition["polyunsaturated_fat"];
				$recipeNutritionals["monounsaturated_fat"] += $nutrition["monounsaturated_fat"];
				$recipeNutritionals["trans_fat"] += $nutrition["trans_fat"];
				$recipeNutritionals["cholesterol"] += $nutrition["cholesterol"];
				$recipeNutritionals["sodium"] += $nutrition["sodium"];
				$recipeNutritionals["potassium"] += $nutrition["potassium"];	
				$recipeNutritionals["fiber"] += $nutrition["fiber"];	
				$recipeNutritionals["sugar"] += $nutrition["sugar"];	
				$recipeNutritionals["vitamin_a"] += $nutrition["vitamin_a"];	
				$recipeNutritionals["vitamin_c"] += $nutrition["vitamin_c"];	
				$recipeNutritionals["calcium"] += $nutrition["calcium"];	
				$recipeNutritionals["iron"] += $nutrition["iron"];		
			}
			
			?>
			<div class="wprm-nutrition-label">
		        <div class="nutrition-title">Total Daily Macros</div>
		        <!--<div class="nutrition-recipe"></div>-->
		        <div class="nutrition-line nutrition-line-big"></div>
		        <div class="nutrition-serving">
		            Amount Per Serving        
		        </div>
		        <div class="nutrition-item">
		            <span class="nutrition-main">
		            	<strong>Calories</strong> <?php echo $recipeNutritionals["calories"]; ?></span>
		                <span class="nutrition-percentage">Calories from Fat <?php echo ($recipeNutritionals["fat"] * 9); ?></span>
		        </div>
		        <div class="nutrition-line"></div>
		        <div class="nutrition-item">
		            <span class="nutrition-percentage"><strong>% Daily Value*</strong></span>
		        </div>
		            <div class="nutrition-item">
		                <span class="nutrition-main"><strong>Total Fat</strong> <?php echo $recipeNutritionals["fat"]; ?>g</span>
		                <span class="nutrition-percentage"><strong><?php echo round($recipeNutritionals["fat"] / 65 * 100); ?>%</strong></span>
		            </div>
		            <div class="nutrition-sub-item">
		                <span class="nutrition-sub">Saturated Fat <?php echo $recipeNutritionals["saturated_fat"]; ?>g</span>
		                <span class="nutrition-percentage"><strong><?php echo round($recipeNutritionals["saturated_fat"] / 20 * 100); ?>%</strong></span>
		            </div>
		            <div class="nutrition-sub-item">
		                <span class="nutrition-sub">Polyunsaturated Fat <?php echo $recipeNutritionals["polyunsaturated_fat"]; ?>g</span>
		            </div>
		            <div class="nutrition-sub-item">
		                <span class="nutrition-sub">Monounsaturated Fat <?php echo $recipeNutritionals["monounsaturated_fat"]; ?>g</span>
		            </div>
		            <div class="nutrition-item">
		                <span class="nutrition-main"><strong>Sodium</strong> <?php echo $recipeNutritionals["sodium"]; ?>mg</span>
		                <span class="nutrition-percentage"><strong><?php echo round($recipeNutritionals["sodium"] / 2400 * 100); ?>%</strong></span>
		            </div>
		            <div class="nutrition-item">
		                <span class="nutrition-main"><strong>Potassium</strong> <?php echo $recipeNutritionals["potassium"]; ?>mg</span>
		                <span class="nutrition-percentage"><strong><?php echo round($recipeNutritionals["potassium"] / 3500 * 100); ?>%</strong></span>
		            </div>
		            <div class="nutrition-item">
		                <span class="nutrition-main"><strong>Total Carbohydrates</strong> <?php echo $recipeNutritionals["carbohydrate"]; ?>g</span>
		                <span class="nutrition-percentage"><strong><?php echo round($recipeNutritionals["carbohydrate"] / 300 * 100); ?>%</strong></span>
		            </div>
		            <div class="nutrition-sub-item">
		                <span class="nutrition-sub">Dietary Fiber <?php echo $recipeNutritionals["fiber"]; ?>g</span>
		                <span class="nutrition-percentage"><strong><?php echo round($recipeNutritionals["fiber"] / 25 * 100); ?>%</strong></span>
		            </div>
		            <div class="nutrition-sub-item">
		                <span class="nutrition-sub">Sugars <?php echo $recipeNutritionals["sugar"]; ?>g</span>
		            </div>
		            <div class="nutrition-item">
		                <span class="nutrition-main"><strong>Protein</strong> <?php echo $recipeNutritionals["protein"]; ?>g</span>
		                <span class="nutrition-percentage"><strong><?php echo round($recipeNutritionals["protein"] / 50 * 100); ?>%</strong></span>
		            </div>
		                
		            <div class="nutrition-line nutrition-line-big"></div>
		                
		            <div class="nutrition-item">
		                <span class="nutrition-main">Vitamin A</span>
		                <span class="nutrition-percentage"><?php echo $recipeNutritionals["vitamin_a"]; ?>%</span>
		            </div>
		                        
		            <div class="nutrition-item">
		                <span class="nutrition-main">Vitamin C</span>
		                <span class="nutrition-percentage"><?php echo $recipeNutritionals["vitamin_c"]; ?>%</span>
		            </div>
		                        
		            <div class="nutrition-item">
		                <span class="nutrition-main">Calcium</span>
		                <span class="nutrition-percentage"><?php echo $recipeNutritionals["calcium"]; ?>%</span>
		            </div>
		                        
		            <div class="nutrition-item">
		                <span class="nutrition-main">Iron</span>
		                <span class="nutrition-percentage"><?php echo $recipeNutritionals["iron"]; ?>%</span>
		            </div>
		            <div class="nutrition-warning">
		            	* Percent Daily Values are based on a 2000 calorie diet.         
		            </div>
		    </div>
			<?php
				
			exit();
				
		} catch (Exception $ex) {
			
			echo "<span class='error-warning'>Unknown error occured. Please contact your administrator.</span>";
		}
	}
?>