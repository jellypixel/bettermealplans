<?php
	if ( isset( $_POST['menuid'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = addMenu($_POST['menuid']);
		echo $ajaxResult;
	}	
	
	function addMenu($menuid) {		
		global $wpdb;
		
		// Query
		$sql = $wpdb->prepare( 'SELECT a.notes, a.recipe_id, a.day as hari, a.part, a.orders, b.name as menu_name, c.colordark as diet_colordark, c.colorlight as diet_colorlight
								FROM jp_menu_recipe a, jp_menu b, jp_diet c
								WHERE a.menu_id = %d 
								AND a.day in (1,2,3,4,5,6,7)
								AND a.menu_id = b.id
								AND b.diet_id = c.id
								', $menuid );
	
		$result = $wpdb->get_results( $sql );
		
		/*Get color based on diet*/
		$allDiets = $wpdb->get_results('SELECT name, colordark, colorlight FROM jp_diet', ARRAY_A);
		$allDietsName = array_column($allDiets, 'name');			
		
		if ( $result ) {	
			$sundayEl = $mondayEl = $tuesdayEl = $wednesdayEl = $thursdayEl = $fridayEl = $saturdayEl = '';
		
			foreach ( $result as $key ){
				$recipeId = $key->recipe_id;

				$recipe = new TwentyDishes_Recipe($recipeId);
				if($recipe->title == "recipe deleted")
					 continue;
					 
				$titleRecipe = $recipe->title;
				$thumbnailUrl = $recipe->thumb;
								
				$menuname = $key->menu_name;
				
				/* Look at recipe categories and determine color */
				$categories = $wpdb->get_results( "SELECT wt.Name		
									FROM $wpdb->posts, $wpdb->term_relationships as wtr, $wpdb->term_taxonomy as wtt, $wpdb->terms as wt
									WHERE $wpdb->posts.ID = wtr.object_id AND $wpdb->posts.id = $recipeId AND wtr.term_taxonomy_id = wtt.term_taxonomy_id AND wtt.term_id = wt.term_id
											AND wtt.taxonomy IN ('category') AND wt.slug IN ("."'".implode("','", $allDietsName). "'".")", ARRAY_A
								);
				$categories = array_column($categories, 'Name');	

				if ( empty( $categories ) ) {
					// Has no diet 
					$cardColor = 'background-color:#f4f4f4; color:#555555; border:1px solid #555555;';
					$headerColor = 'border-bottom:1px solid #555555;';
					$buttonColor = 'color:#555555;';
				}
				else if ( count($categories) > 1 ) {
					// Has multiple diet
					$cardColor = 'background-color:#c0ecff; color:#256a8e; border:1px solid #256a8e;';
					$headerColor = 'border-bottom:1px solid #256a8e;';
					$buttonColor = 'color:#256a8e;';	
				}
				else {
					
					foreach($allDiets as $oneDiet){
						if($oneDiet["name"] == $categories[0]) {
							$cardColor = 'background-color:' . $oneDiet["colorlight"] . '; color:' . $oneDiet["colordark"] . '; border:1px solid ' . $oneDiet["colordark"]. ';';
							$headerColor = 'border-bottom:1px solid ' . $oneDiet["colordark"] . ';';
							$buttonColor = 'color:' . $oneDiet["colordark"] . ';';		
						}
					}
				}	
				
				$menuName = $key->menu_name;
				$notes = $key->notes;
				
				$recipeEl = '';		
				if ( !empty( $notes ) ) {
					$recipeEl = '<li class="notes">
									<div class="notes-handle">
									</div>
									<div class="notes-content">
										' . $notes . '
									</div>
									<div class="notes-remove">
										<div data-icon="&#xde;" class="icon" style="color:#777777;"></div>
									</div>
								</li>';	
				}
				else {	
					$recipeEl = '<li class="recipe" style="' . $cardColor . '" recipeid="' . $recipeId . '" menuid="' . $menuid . '">
									<div class="recipe-header" style="' . $headerColor . '">
										' . $menuName . '
									</div>
									<div class="recipe-content-wrapper">
										<div class="recipe-thumbnail" style="background-image:url(' . $thumbnailUrl . ');">
											<img src="' . $thumbnailUrl . '" />											
										</div>
										<div class="recipe-content">                                            	
											' . $titleRecipe . '
										</div>
										<div class="recipe-remove">
											<div data-icon="&#xde;" class="icon" style="' . $buttonColor . '"></div>
										</div>
										<div class="recipe-hover">											
											<div class="icon tdicon-notes" style="' . $buttonColor . '"></div>										
											<div class="recipe-insight">												
											</div>
										</div>										
										<div class="clear"></div>
									</div>
								</li>';	
				}
			
				if ( $key->hari == 1 ) {
					$sundayEl .= $recipeEl;
				}
				else if ( $key->hari == 2 ) {
					$mondayEl .= $recipeEl;
				}
				else if ( $key->hari == 3 ) {
					$tuesdayEl .= $recipeEl;
				}
				else if ( $key->hari == 3 ) {
					$wednesdayEl .= $recipeEl;
				}
				else if ( $key->hari == 5 ) {
					$thursdayEl .= $recipeEl;
				}
				else if ( $key->hari == 6 ) {
					$fridayEl .= $recipeEl;
				}
				else if ( $key->hari == 7 ) {
					$saturdayEl .= $recipeEl;
				}
			}			
		}	
			
		// Return			
		return $sundayEl . '|' . $mondayEl . '|' . $tuesdayEl . '|' . $wednesdayEl . '|' . $thursdayEl . '|' . $fridayEl . '|' . $saturdayEl ;
	}
?>