<?php
	if ( isset( $_GET['term'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
		
		$ajaxResult = loadingredients( $_GET['term'] );
		echo $ajaxResult;
	}	

	function loadingredients($term) {		
		
		$search = $_GET['term'];
		
		$terms = get_terms( 'ingredient', array(
			'orderby'    => 'name',
			'fields' => 'names',
			'name__like' => $search
		) );
		
		echo json_encode($terms);
		die();
	}
?>
