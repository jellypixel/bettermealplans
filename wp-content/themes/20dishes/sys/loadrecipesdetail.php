<?php
	require_once 'globalfunction.php';

	if ( isset( $_POST['arrRecipeId'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
		
		$ajaxResult = loadrecipesdetail( $_POST['arrRecipeId'] );
		echo $ajaxResult;
	}	
	
	function loadrecipesdetail($arrRecipeId) {		
		$allrecipes = '';
		
		foreach( $arrRecipeId as $recipeid ) {

			// Get the Recipe Details
			$recipe = new TwentyDishes_Recipe($recipeid);
			if($recipe->title == "recipe deleted")
				continue;
			
			$title = $recipe->title; 
			$yield = $recipe->yield;
			$readytime = $recipe->readytime; 
			$cooktime = $recipe->cooktime; 
			$difficulty = $recipe->difficulty; 
			$thumb = $recipe->thumbmedium;
			$ingredients = $recipe->ingredients;
			$instructions = $recipe->instructions;
			$cats = $recipe->cats;
			$nutritional = $recipe->nutritional;
			$star = $recipe->getFavClass();
			$summary = $recipe->summary;

			$current_user = checkCorporateAccount();
		
			$userServingSize = get_user_meta( $current_user, "servings", true); 
			
			$allrecipes .= '<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle clearfix" data-toggle="collapse" data-parent="#recipesdetail-stage" href="#recipesaccordion-' . $recipeid . '">
								<div class="fav ' . $star . '" recipeid="' . $recipeid . '"></div>
								<div class="accordion-title">' . $title . '</div>								
								<input class="recipe-check" id="checkPrint-' . $recipeid . '" type="checkbox" name="checkPrint" recipeid="' . $recipeid . '" />
								<label for="checkPrint-' . $recipeid . '" class="checkPrint-label">Print</label> 
							</a>
						</div>
						<div id="recipesaccordion-' . $recipeid . '" class="accordion-body collapse">
							<div class="accordion-inner">
								<div class="addrecipe-header clearfix">
									<img class="addrecipe-thumbnail" src="' . $thumb . '"></img>
									<div class="addrecipe-topmeta">
										<div class="addrecipe-title">' . $title . '</div>	
										<div class="addrecipe-cat">' . $cats[ count($cats) -1 ]->name . '</div>
										Serves ' . $yield . ' (Ingredients adjusted for ' . $userServingSize . ' servings) <br> Preparation time: ' . $readytime . ', Cook time: ' . $cooktime . '<br>
										' . $summary . '
									</div>
								</div>
								<div class="addrecipe-rest">
									' . $nutritional . '	
									<div class="print-bold">Ingredients:</div>' . $recipe->printIngredients(true) . '<br>' . '<div class="print-bold-top">Instructions:</div>' . $recipe->printInstructions() . '
								</div>
							</div>
						</div>
					</div>';
		}

		return $allrecipes;
	}
?>
