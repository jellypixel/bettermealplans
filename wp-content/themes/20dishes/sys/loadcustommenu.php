	<?php
	require_once 'globalfunction.php';
	
	if ( isset( $_POST['user_id'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = loadmenu($_POST['user_id']);
		echo $ajaxResult;
	}	

	function loadmenu($user_id) {		
		global $wpdb;

		// Execution
		//$current_user = wp_get_current_user();
		//require_once 'globalfunction.php';
		$current_user = checkCorporateAccount();
		
		$sql = $wpdb->prepare( 'SELECT * FROM jp_menu WHERE user_id = %d AND shared = "N" ORDER BY id DESC LIMIT 3 ', $user_id ); 	
		$result = $wpdb->get_results( $sql );
		
		$allthemenu = '';
		
		if ( $result ) {			
			foreach ( $result as $key ){
				$allthemenu .= '<div class="custommenu-wrapper">
									<div class="custommenu" menuid="' . $key->id . '" menuname="' . $key->name . '">
										' . $key->name . '
									</div>
									<div class="custommenu-delete" menuid="' . $key->id . '" menuname="' . $key->name . '" title="Delete">
										×
									</div>
								</div>';
			}			
		}
		
		return $allthemenu;
	}
?>