<?php
	if ( 
			isset( $_POST['userid'] ) &&
			isset( $_POST['addrecipe'] ) &&
			isset( $_POST['addnote'] ) &&
			isset( $_POST['removerecipe'] ) &&
			isset( $_POST['startfromscratch'] ) &&
			isset( $_POST['portion'] ) &&
			isset( $_POST['diet'] ) &&
			isset( $_POST['avoidfood'] ) &&
			isset( $_POST['excludeadditionalingredients'] ) &&
			isset( $_POST['restrictcalories'] ) &&
			isset( $_POST['restrictfat'] ) &&
			isset( $_POST['restrictcarbs'] )
	   ) 
	{
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = saveusersettings( 
							$_POST['userid'],
							$_POST['addrecipe'], 
							$_POST['addnote'],
							$_POST['removerecipe'],
							$_POST['startfromscratch'],
							$_POST['portion'],
							$_POST['diet'],
							$_POST['avoidfood'],
							$_POST['excludeadditionalingredients'],
							$_POST['restrictcalories'],
							$_POST['restrictfat'],
							$_POST['restrictcarbs']
					);
		
		echo $ajaxResult;
	}	
	
	function saveusersettings( 
				$userid,
				$addrecipe,
				$addnote,
				$removerecipe,
				$startfromscratch,
				$portion,
				$diet,
				$avoidfood,
				$excludeadditionalingredients,
				$restrictcalories,
				$restrictfat,
				$restrictcarbs
			) 
	{		
		try 
		{
			update_user_meta( $userid, 'restrict-addrecipe', $addrecipe); 
			update_user_meta( $userid, 'restrict-addnote', $addnote); 
			update_user_meta( $userid, 'restrict-removerecipe', $removerecipe); 
			update_user_meta( $userid, 'restrict-startfromscratch', $startfromscratch); 
			update_user_meta( $userid, 'restrict-portion', $portion); 
			update_user_meta( $userid, 'restrict-diet', $diet); 
			update_user_meta( $userid, 'restrict-avoidfood', $avoidfood); 
			update_user_meta( $userid, 'restrict-excludeadditionalingredients', $excludeadditionalingredients); 
			update_user_meta( $userid, 'restrict-calories', $restrictcalories); 
			update_user_meta( $userid, 'restrict-fat', $restrictfat); 
			update_user_meta( $userid, 'restrict-carbs', $restrictcarbs); 
			
			echo "1";
			
		} 
		catch (Exception $ex) 
		{			
			echo "0";
		}
	}
?>