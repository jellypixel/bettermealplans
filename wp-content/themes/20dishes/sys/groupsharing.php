<?php
	if ( isset( $_POST['group'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
		 
		$ajaxResult = groupsharing($_POST['group'], $_POST['blob']);
		echo $ajaxResult;
	}	
	
	function groupsharing($group, $blob) {

		if($group == '...')
			return;
		else if (empty($group))
			return;
		
		global $wpdb;		
		$responseErr = '';
		
		$wpdb->query( 'SET autocommit = 0;' );
		$wpdb->query( 'START TRANSACTION' );

		// Sharing Menu Happily
		if ( !empty( $blob ) ) {
			$arrBlob = explode("^@%^", $blob);		
				
			// Get all user on this group.
			$userGroup = $wpdb->get_results( "
							SELECT DISTINCT data.user_id FROM wp_cimy_uef_data data, wp_cimy_uef_fields fields
							WHERE fields.Name = 'USER_GROUP' AND data.Field_ID = fields.ID AND data.value='".$group."'"	
						);			

			foreach($userGroup as $user){

				foreach($arrBlob as $singleblob) {
					$arrDetail = explode("^)^@", $singleblob);
					
					$recipeid = $arrDetail[0];
					$menuid = $arrDetail[1];
					$day = (int)$arrDetail[5];
					$tanggal = date('Y-m-d H:i:s', strtotime("+". $day-1 ." day", strtotime($arrDetail[2])));
					$orders = $arrDetail[3];
					$notes = $arrDetail[4];
					
					if ( $wpdb->insert(
									"jp_userschedule",
									array(
										'user_id' => $user->user_id,
										'recipe_id' => $recipeid,
										'menu_id' => $menuid,
										'tanggal' => $tanggal,
										'orders' => $orders,
										'notes' => $notes
									),
									array(
										'%d',
										'%d',
										'%d',
										'%s',
										'%d',
										'%s'
									)
					) === false ) {
						$responseErr .= 'error';
						$wpdb->query('ROLLBACK');
					}
					else {
						$wpdb->query( 'COMMIT' );
					}
				}
			}
		}
				
		$wpdb->query( 'SET autocommit = 1;' );
					
		if ( $responseErr == '' ) {	
				return true;		
		}
		else {
			return false;	
		}
	}
?>