<?php
	require_once 'globalfunction.php';		

	if ( isset( $_POST['searchTerm'] ) && isset( $_POST['pageNum'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = loadeditmenu($_POST['searchTerm'], $_POST['pageNum']);
		echo $ajaxResult;
	}	

	function loadeditmenu($search, $pageNum) {		
		global $wpdb;
		
		if ( is_admin() ) {
		
		}
		else {
			$extraQuery = ' a.shared = "Y" AND ';
		}
		
		// User ID
		//$user_id = get_current_user_id();
		$user_id = checkCorporateAccount();

		// Execution
		$sql = $wpdb->prepare( 'SELECT *
								FROM jp_menu a
								WHERE (a.shared = "Y" OR a.user_id = %d)
								AND a.name LIKE \'%%%s%%\'', $user_id, esc_sql( like_escape( $search ) ) );

		$result = $wpdb->get_results( $sql );
		
		$allthemenu = '';
		
		if ( $result ) {
			foreach ( $result as $key ){
				$allthemenu .= '<div class="searchmenu" menuid="' . $key->id . '">' . $key->name . '</div><br>';
			}			
		}
		else {
			echo '<div class="addmenu-empty">Sorry, no result :(</div>';		
		}
		
		return $allthemenu;
	}
?>