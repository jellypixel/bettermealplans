<?php
	if ( isset( $_POST['recipeId'] ) && isset( $_POST['blobmeta'] ) && isset( $_POST['currentdayrecipes'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = addRecipe($_POST['recipeId'], $_POST['blobmeta'], $_POST['currentdayrecipes']);
		echo $ajaxResult;
	}	
	
	function addRecipe($recipeId, $blobmeta, $currentdayrecipes) 
	{		
		// Check Calories/Fat/Carbs
			$arrDetail = array_filter(explode("^)^@", $currentdayrecipes));
			$addrecipeReturn = array ();
			$addrecipeReturn["calories"] = 0;
			$addrecipeReturn["carbohydrate"] = 0;
			$addrecipeReturn["fat"] = 0;
				
			foreach($arrDetail as $key)
			{
				$recipe = new TwentyDishes_Recipe($key);
				if($recipe->title == "recipe deleted")
					continue;
					
				$nutrition = $recipe->nutrition;

				$addrecipeReturn["calories"] += $nutrition["calories"];
				$addrecipeReturn["carbohydrate"] += $nutrition["carbohydrates"];
				$addrecipeReturn["fat"] += $nutrition["fat"];
			}	
	
		// Execute
			global $wpdb;
			$recipe = new TwentyDishes_Recipe($recipeId);
			
			$addrecipeReturn["newrecipeelement"] =
				 '<li class="recipe" style="background-color:#ffffff; color:#444444; border:1px solid #cccccc;" recipeid="' . $recipeId . '" menuid="0" blobmeta="' .  str_replace ( "\"", "&quot;", $blobmeta ) . '">
						<div class="recipe-header">
							<div class="recipe-thumbnail" style="background-image:url(' . $recipe->thumb . ');">												
							</div>
						</div>
						<div class="recipe-content-wrapper">											
							<div class="recipe-content">                                            	
								' . $recipe->title . '
							</div>
							<div class="recipe-remove" title="Remove">
								<div class="icon tdicon-thin-close"></div>										
							</div>
							<div class="recipe-hover" title="Notes">											
								<div class="icon tdicon-notes" style="color:#555555;"></div>										
								<div class="recipe-insight">												
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</li>';
					
		// Return
			return json_encode( $addrecipeReturn );
	}
?>