<?php
	if ( isset( $_POST['currentdayrecipes'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = checkMacros( $_POST['currentdayrecipes'] );
		echo $ajaxResult;
	}	
	
	function checkMacros( $currentdayrecipes ) 
	{		
		// Check Calories/Fat/Carbs
			$arrDetail = array_filter(explode("^)^@", $currentdayrecipes));
			$checkmacrosReturn = array ();
			$checkmacrosReturn["calories"] = 0;
			$checkmacrosReturn["carbohydrate"] = 0;
			$checkmacrosReturn["fat"] = 0;
				
			foreach($arrDetail as $key)
			{
				$recipe = new TwentyDishes_Recipe($key);
				if($recipe->title == "recipe deleted")
					continue;
					
				$nutrition = $recipe->nutrition;

				$checkmacrosReturn["calories"] += $nutrition["calories"];
				$checkmacrosReturn["carbohydrate"] += $nutrition["carbohydrates"];
				$checkmacrosReturn["fat"] += $nutrition["fat"];
			}				
					
		// Return
			return json_encode( $checkmacrosReturn );
	}
?>