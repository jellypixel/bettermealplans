<?php
	require_once 'globalfunction.php';

	if ( isset( $_POST['recipeId'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';

		$ajaxResult = loadinsight( $_POST['recipeId'], $_POST['fav'] );
		echo $ajaxResult;
	}	
	
	function loadinsight($recipeid, $hasFav) {		

		// Get the Recipe Details
		$recipe = new TwentyDishes_Recipe($recipeid);
		
		$title = $recipe->title; 
		$yield = $recipe->yield;
		$readytime = $recipe->readytime; 
		$cooktime = $recipe->cooktime; 
		$difficulty = $recipe->difficulty; 
		$thumb = $recipe->thumbmedium;
		$ingredients = $recipe->ingredients;
		$instructions = $recipe->instructions;
		$cats = $recipe->cats;
		$nutritional = $recipe->nutritional;
		$star = $recipe->getFavClass();
		$summary = $recipe->summary;

		//$userServingSize = get_user_meta(wp_get_current_user()->ID, "servings", true); 
		$userServingSize = get_user_meta( checkCorporateAccount(), "servings", true ); 

		return '	<div class="insight-header">
							<div class="insight-thumb" style="background-image:url(' . $thumb . ');">

							</div>
							<div class="insight-meta">
								<div class="insight-title">' . $title . '</div>
								<div class="insight-fav clearfix">
									<div class="fav'. $star .'" recipeid="' . $recipeid . '"></div>
								</div>
								<div class="insight-cat">' . $cats[ count($cats) -1 ]->name . '</div>
								<div class="insight-moremeta">
									Serves ' . $yield[0] . ' (Ingredients adjusted for ' . $userServingSize . ' servings) <br> Preparation time: ' . $readytime . ', Cook time: ' . $cooktime . '<br>
										' . $summary . '
								</div>
							</div>							
						</div>
						<div class="insight-line"></div>
						<div class="insight-content">
						   <div class="addrecipe-longcontent">' . $nutritional . '</div>
							Ingredients:<br>' . $recipe->printIngredients(true) . '<br>' . 'Instructions:' . '<br>' . $recipe->printInstructions() . '
						</div>';
	}
?>
