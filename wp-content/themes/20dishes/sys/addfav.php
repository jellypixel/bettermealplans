<?php
	require_once 'globalfunction.php';

	if ( isset( $_POST['recipeid'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = addfav($_POST['recipeid']);
		echo $ajaxResult;
	}
	
	function addfav($recipeid) {		
		
		//$current_user = wp_get_current_user();
		//$current_user->ID;
		
		$current_user = checkCorporateAccount();
		
		$meta_value = 'yes';
		update_user_meta( $current_user, 'favorite-' . $recipeid, $meta_value );
		
		return true;
	}
?>