<?php
	// Check whether it is a normal account, a valid corporate account or an invalid corporate account.
	// This function returns the -999 if it's invalid and returns the user ID of the user that needs the functionality
	// No user use -999 as ID so most of the functionality will break (which is what we want)
	function checkCorporateAccount () {
		if(!empty($_POST["id"]) && !empty($_POST["ca"])) {
			
			// Add corporate account class and check if valid corporate account for the user
			$controller = new MPCA_Account_Controller();
			if(false === ($ca = $controller->validate_manage_sub_accounts_page()) ) { 
				return -999;
			} else {
				// Check whether the current ID is manageable by this user
				$manageable = false;
				$accounts = $ca->sub_account_list_table('last_name','ASC',1,500);
				foreach($accounts['results'] as $account) {
					if($account->ID == $_POST["id"]) {
						$manageable = true;
						break;
					}
				}

				if(!$manageable) {
					return -999;
				}
			}

			return $_POST["id"];
		} else {
			return wp_get_current_user()->ID;
		}
	}
?>