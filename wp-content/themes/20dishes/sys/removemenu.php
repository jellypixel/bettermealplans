<?php
	require_once 'globalfunction.php';

	if ( isset( $_POST['menuid'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = removemenu($_POST['menuid']);
		echo $ajaxResult;
	}	
	
	function removemenu($menuid) {	
		global $wpdb;		
		$responseErr = '';
		
		$resultMenu = $wpdb->get_row( 'SELECT * FROM jp_menu WHERE id = ' . $menuid );
		
		$current_user = checkCorporateAccount();

		if ( $resultMenu ) {
			if ( $resultMenu->user_id != $current_user ) {
				return false;	
			}
		}
		
		$wpdb->query( 'SET autocommit = 0;' );
		$wpdb->query( 'START TRANSACTION' );
				
		// Remove Menu	
		/* DElETE Detail */
			if ( $wpdb->delete(
							"jp_menu_recipe",
							array(
								'menu_id' => $menuid
							),
							array(
								'%d'
				)
			) === false ) {
				$responseErr .= 'error';
				$wpdb->query('ROLLBACK');
			}
			else {
				$wpdb->query( 'COMMIT' );
			}
			
		/* DElETE Master */
			if ( $wpdb->delete(
							"jp_menu",
							array(
								'id' => $menuid
							),
							array(
								'%d'
				)
			) === false ) {
				$responseErr .= 'error';
				$wpdb->query('ROLLBACK');
			}
			else {
				$wpdb->query( 'COMMIT' );
			}
		
				
		$wpdb->query( 'SET autocommit = 1;' );
				
		if ( $responseErr == '' ) {	
			return true;	
		}
		else {
			return false;	
		}
	}
?>