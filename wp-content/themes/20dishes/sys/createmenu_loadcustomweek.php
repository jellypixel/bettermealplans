<?php	
	require_once 'globalfunction.php';

	if ( isset( $_POST['menuid'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$ajaxResult = createmenu_loadcustomWeek($_POST['menuid']);
		echo $ajaxResult;
	}	
	
	function createmenu_loadcustomWeek($menuid) {		
		global $wpdb;
		
		$extraQuery = '';
				
		if ( !empty( $menuid ) ) { 
			// Non-empty = Load Something, Empty = New Post
			
			// User ID
				//$user_id = get_current_user_id();
				$user_id = checkCorporateAccount();
				
			// Admin or User?
				if ( $menuid == 'admin' ) {
					$extraQuery .= '';	
				}
				else {
					$extraQuery .= '';
				}
			
			// Execution
			$sql = $wpdb->prepare( 'SELECT b.* 
									FROM jp_menu a, jp_menu_recipe b
									WHERE 
									a.id = b.menu_id AND								
									a.shared = "N" AND
									a.id = %d', 
									$menuid ); // Get menu
		
			$result = $wpdb->get_results( $sql );
			
			/*Get color based on diet*/
			$allDiets = $wpdb->get_results('SELECT name, colordark, colorlight FROM jp_diet', ARRAY_A);
			$allDietsName = array_column($allDiets, 'name');		
			
			if ( $result ) {			
				$sundayEl = $mondayEl = $tuesdayEl = $wednesdayEl = $thursdayEl = $fridayEl = $saturdayEl = '';
				$sundayNotes = $mondayNotes = $tuesdayNotes = $wednesdayNotes = $thursdayNotes = $fridayNotes = $saturdayNotes = '';
	
				foreach ( $result as $key ){

					$recipe = new TwentyDishes_Recipe($key->recipe_id);
					if($recipe->title == "recipe deleted")
						continue;
					 
					$titleRecipe = $recipe->title;
					$thumbnailUrl = $recipe->thumbsmall;
								
					$tanggalDateOnly = date_create( $key->tanggal );							
					$tanggalRecipe = date_format( $tanggalDateOnly, 'F j' );
					$dayRecipe = date_format( $tanggalDateOnly, 'l' );
					$dateorderRecipe = $key->day;
					$ordersRecipe = $key->orders;
					
					$notes = $key->notes;
					
					$recipeId = $key->recipe_id;
					$menuId = $key->menu_id;
					$tanggal = $key->tanggal;
					
					/* Look at recipe categories and determine color */
					$categories = $wpdb->get_results( "SELECT wt.Name		
										FROM $wpdb->posts, $wpdb->term_relationships as wtr, $wpdb->term_taxonomy as wtt, $wpdb->terms as wt
										WHERE $wpdb->posts.ID = wtr.object_id AND $wpdb->posts.id = $recipeId AND wtr.term_taxonomy_id = wtt.term_taxonomy_id AND wtt.term_id = wt.term_id
												AND wtt.taxonomy IN ('category') AND wt.slug IN ("."'".implode("','", $allDietsName). "'".")", ARRAY_A
									);
																		
					$categories = array_column($categories, 'Name');	
					
					$cardColor = 'background-color:#ffffff; color:#444444; border:1px solid #cccccc;';
					$buttonColor = 'color:#555555;';				
					
					$notesEl = '';
					$recipeEl = '';
					
					if ( !empty( $notes ) ) {
						// ganti $notesEl klo mo
						$recipeEl = '<li class="notes">
										<div class="notes-handle">
										</div>
										<div class="notes-content">
											' . $notes . '
										</div>
										<div class="notes-remove">
											<div class="tdicon-thin-close"></div>
										</div>
									</li>';
					}
					else {		
						$recipeEl = '<li class="recipe" style="' . $cardColor . '" recipeid="' . $recipeId . '" menuid="' . $menuId . '">
										<div class="recipe-header">
											<div class="recipe-thumbnail" style="background-image:url(' . $thumbnailUrl . ');">												
											</div>
										</div>
										<div class="recipe-content-wrapper">											
											<div class="recipe-content">                                            	
												' . $titleRecipe . '
											</div>
											<div class="recipe-remove" title="Remove">
												<div class="icon tdicon-thin-close"></div>										
											</div>
											<div class="recipe-hover" title="Notes">											
												<div class="icon tdicon-notes" style="' . $buttonColor . '"></div>										
												<div class="recipe-insight">												
												</div>
											</div>
											<div class="clear"></div>
										</div>
									</li>';
					}
					
					//var_dump( $recipeEl . '<br><br>' );
					
					if ( $dateorderRecipe == 1 ) {
						$sundayEl .= $recipeEl;
						$sundayNotes .= $notesEl;
						$sundayDate = $tanggalRecipe;
						$sundayDay = 'Sunday';						
					}
					else if ( $dateorderRecipe == 2 ) {
						$mondayEl .= $recipeEl;
						$mondayNotes .= $notesEl;
						$mondayDate = $tanggalRecipe;
						$mondayDay = 'Monday';											
					}
					else if ( $dateorderRecipe == 3 ) {
						$tuesdayEl .= $recipeEl;
						$tuesdayNotes .= $notesEl;
						$tuesdayDate = $tanggalRecipe;
						$tuesdayDay = 'Tuesday';
					}
					else if ( $dateorderRecipe == 4 ) {
						$wednesdayEl .= $recipeEl;
						$wednesdayNotes .= $notesEl;
						$wednesdayDate = $tanggalRecipe;
						$wednesdayDay = 'Wednesday';
					}
					else if ( $dateorderRecipe == 5 ) {
						$thursdayEl .= $recipeEl;
						$thursdayNotes .= $notesEl;
						$thursdayDate = $tanggalRecipe;
						$thursdayDay = 'Thursday';
					}
					else if ( $dateorderRecipe == 6 ) {
						$fridayEl .= $recipeEl;
						$fridayNotes .= $notesEl;
						$fridayDate = $tanggalRecipe;
						$fridayDay = 'Friday';
					}
					else if ( $dateorderRecipe == 7 ) {
						$saturdayEl .= $recipeEl;
						$saturdayNotes .= $notesEl;
						$saturdayDate = $tanggalRecipe;
						$saturdayDay = 'Saturday';
					}																				
				}
			}
		}
		
		// Result
		$alltheweek = 		
			'<div id="mymenu-sunday" class="mymenu-day">
				<div class="mymenu-date">Day 1</div>                                                               
				<ul class="mymenu-stage">'
					. $sundayEl . $separatorEl . $sundayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						<div class="addcard" data-step="5" data-intro="Click here to choose another recipe from the database">
							Add Recipe
						</div>
						<div class="addcard addnote">
							Add Note
						</div>
						<div class="dailymacros">
							Macro
						</div>
					</div>' . 
				'</ul>
			</div>' .		

			'<div id="mymenu-monday" class="mymenu-day">
				<div class="mymenu-date">Day 2</div>                             
				<ul class="mymenu-stage">' 
					. $mondayEl . $separatorEl . $mondayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						<div class="addcard">
							Add Recipe							
						</div>
						<div class="addcard addnote">
							Add Note
						</div>
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .
			
			'<div id="mymenu-tuesday" class="mymenu-day">
				<div class="mymenu-date">Day 3</div>                       
				<ul class="mymenu-stage">' . $tuesdayEl . $separatorEl . $tuesdayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						<div class="addcard">
							Add Recipe
						</div>
						<div class="addcard addnote">
							Add Note
						</div>
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .
			
			'<div id="mymenu-wednesday" class="mymenu-day">
				<div class="mymenu-date">Day 4</div>                              
				<ul class="mymenu-stage">' . $wednesdayEl . $separatorEl . $wednesdayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						<div class="addcard">
							Add Recipe
						</div>
						<div class="addcard addnote">
							Add Note
						</div>
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .
			
			'<div id="mymenu-thursday" class="mymenu-day">
				<div class="mymenu-date">Day 5</div>                               
				<ul class="mymenu-stage">' . $thursdayEl . $separatorEl . $thursdayNotes .
					'<div class="mymenu-addrecipe-wrapper">
						<div class="addcard">
							Add Recipe
						</div>
						<div class="addcard addnote">
							Add Note
						</div>
						<div class="dailymacros">
							Macro
						</div>
					</div>' .
				'</ul>
			</div>' .
			
			'<div id="mymenu-friday" class="mymenu-day">
				<div class="mymenu-date">Day 6</div>                                                  
				<ul class="mymenu-stage">' . $fridayEl . $separatorEl . $fridayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						<div class="addcard">
							Add Recipe
						</div>
						<div class="addcard addnote">
							Add Note
						</div>
						<div class="dailymacros">
							Macro
						</div>
					</div>' .                             
				'</ul>
			</div>' .
			
			'<div id="mymenu-saturday" class="mymenu-day">
				<div class="mymenu-date">Day 7</div>                     
				<ul class="mymenu-stage">' . $saturdayEl . $separatorEl . $saturdayNotes . 
					'<div class="mymenu-addrecipe-wrapper">
						<div class="addcard">
							Add Recipe
						</div>
						<div class="addcard addnote">
							Add Note
						</div>
						<div class="dailymacros">
							Macro
						</div>
					</div>' .                               
				'</ul>
			</div>
			
			<input id="currentdate" type="hidden" value="' . $targetDate . '">
			<input id="currentweek" type="hidden" value="">
			<input id="currentcustommenuid" type="hidden" value="none">';

			
		// Return			
		return $alltheweek;
	}
?>