<?php
	require_once 'globalfunction.php';

	if ( isset( $_POST['targetDate'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
			
		$targetDateFormatted = date( 'm/d/Y', strtotime( $_POST['targetDate'] ) );
		$targetDay = date( 'w', strtotime( $_POST['targetDate'] ) );
		$targetSunday = date( 'Y-m-d', strtotime( $targetDateFormatted . '-' . ($targetDay + 7) . ' days' ) );
		$targetSaturday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 6 - $targetDay + 7) . ' days') );
		
		$ajaxResult = checkupdate($targetSunday, $targetSaturday);
		echo $ajaxResult;
	}	
	
	function checkupdate($fromDate, $toDate) {
		global $wpdb;
		
		$current_user = checkCorporateAccount();
		
		$menuUpdate = $wpdb->get_results( "	SELECT DISTINCT a.id, a.name FROM jp_menu a, jp_userschedule b
											WHERE a.id = b.menu_id AND a.updates != b.updates AND a.Shared = 'Y' AND b.user_id = " . $current_user . " 
											AND a.schedule >= '$fromDate' AND a.schedule <= '$toDate' AND b.tanggal >= '$fromDate' AND b.tanggal <= '$toDate'" );
		
		$menuUpdate = (array)$menuUpdate;
		
		return json_encode($menuUpdate);
	}
	
?>