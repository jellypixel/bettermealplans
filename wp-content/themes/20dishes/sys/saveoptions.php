<?php
	require_once 'globalfunction.php';

	if ( isset( $_POST['servingSize'] ) ) {
		define('WP_USE_THEMES', false);
		
		if(file_exists('../../../../wp-load.php'))
			require_once '../../../../wp-load.php';
		else 
			require_once '../../../../.wordpress/wp-load.php';
	
		
			
		$jsonDiet = json_decode(str_replace('\\', '', $_POST['dietPlans']));
		$jsonExclude = json_decode(str_replace('\\', '', $_POST['excludeList']));
		$ajaxResult = saveoptions($_POST['servingSize'], $jsonDiet, $_POST['exIngredient'], $_POST['fontPrintSize'], $jsonExclude, $_POST['checkedPrint']);
		
		echo $ajaxResult;
	}	
	
	function saveoptions($servingSize, $dietPlans, $exIngredient, $fontPrintSize, $excludeList, $checkedPrint) {		
		try {
			//$current_user = wp_get_current_user();
			$current_user = checkCorporateAccount();
			
			update_user_meta( $current_user, "servings", $servingSize); 
			update_user_meta( $current_user, "diets", $dietPlans);
			update_user_meta( $current_user, "excludeList", $excludeList);
			update_user_meta( $current_user, "fontprintsize", $fontPrintSize);
			update_user_meta( $current_user, "printoption", $checkedPrint);
			
			$terms = get_terms( 'ingredient', array(
				'orderby'    => 'name',
				'fields' => 'names'
			) );
			
			//var_dump( $terms );
			
			$ingredients = explode(",",$exIngredient);
			//$finalExcluded = array();
			foreach($terms as $term) {
				
				if (in_array($term, $ingredients)) 
					//if (!in_array($term, finalExcluded)) 
						$finalExcluded[] = $term;
				
			}
			
			update_user_meta( $current_user, "exingredients", $finalExcluded);

			echo "1";
			
		} catch (Exception $ex) {
			
			echo "0";
		}
	}
?>