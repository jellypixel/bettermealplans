<?php
	/*
		Template Name: Profile
	*/
	
	get_header();	
	
	// Do some registration functionality
	global $current_user, $wp_roles;
	
	$error = array();    
	/* If profile was saved, update profile. */
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'update-user' ) {

		/* Update user password. */
		if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
			if ( $_POST['pass1'] == $_POST['pass2'] )
				wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['pass1'] ) ) );
			else
				$error[] = __('The passwords you entered do not match.  Your password was not updated.', 'profile');
		}
		
		/* Update user information. */
		if ( !empty( $_POST['url'] ) )
			wp_update_user( array( 'ID' => $current_user->ID, 'user_url' => esc_url( $_POST['url'] ) ) );
		if ( !empty( $_POST['email'] ) ){
			if (!is_email(esc_attr( $_POST['email'] )))
				$error[] = __('The Email you entered is not valid.  please try again.', 'profile');
			elseif(email_exists(esc_attr( $_POST['email'] )))
				$error[] = __('This email is already used by another user.  try a different one.', 'profile');
			else{
				wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['email'] )));
			}
		}
		
		if ( !empty( $_POST['first-name'] ) )
			update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['first-name'] ) );
		if ( !empty( $_POST['last-name'] ) )
			update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['last-name'] ) );
		if ( !empty( $_POST['description'] ) )
			update_user_meta( $current_user->ID, 'description', esc_attr( $_POST['description'] ) );

	}
	
?>

	<div class="beginpage container">
    	<?php get_template_part( 'section', 'title' ); ?>    
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					
					<div id="post-<?php the_ID(); ?>">
						<div class="entry-content entry">
							<?php if ( !is_user_logged_in() ) : ?>
									<p class="warning">
										<?php _e('You must be logged in to edit your profile.', 'profile'); ?>
									</p><!-- .warning -->
							<?php else : ?>
							
								<div class="blog-cat-title">
									Contact Information
								</div>		
				
								<?php if ( count($error) > 0 ) echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>
								<form method="post" id="adduser" action="<?php the_permalink(); ?>">
									<input class="text-input" placeholder="<?php _e('First Name', 'profile'); ?>" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
									<input class="text-input" placeholder="<?php _e('Last Name', 'profile'); ?>" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
									<input class="text-input" placeholder="<?php _e('E-mail', 'profile'); ?>" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
									<input class="text-input" placeholder="<?php _e('Password', 'profile'); ?>" name="pass1" type="password" id="pass1" />
									<input class="text-input" placeholder="<?php _e('Repeat Password', 'profile'); ?>" name="pass2" type="password" id="pass2" />

									<p class="form-submit">
										<input name="updateuser" type="submit" id="updateuser" class="submit button" value="<?php _e('Update Profile', 'profile'); ?>" />
										<?php wp_nonce_field( 'update-user' ) ?>
										<input name="action" type="hidden" id="action" value="update-user" />
									</p><!-- .form-submit -->
								</form><!-- #adduser -->
								
								<div class="blog-cat-title" style="width: 100%; float:left; margin-top: 40px;">
									Membership Plan
								</div>								
								
								<form method="post" id="changeusermembership" action="<?php the_permalink(); ?>">
								
									<p class="form-submit">
										<div class="current">
											
											<?php if (!empty($current_user->membership_level->name)) { ?>
											
												<?php if ($current_user->membership_level->name == 'The Good Ol\' Times') { ?>
												
													
												<?php } else {?>
												
													Current Membership Status : <b><?php echo $current_user->membership_level->name; ?></b> 
													from <?php echo date('m/d/Y', $current_user->membership_level->startdate); ?><br>
													
													<?php
													if($current_user->membership_level->cycle_period != '') { ?>
														
														Your account will be billed recurringly every <?php echo $current_user->membership_level->cycle_number . ' ' . $current_user->membership_level->cycle_period;  ?> from that date.

													<?php } ?>
																										
													<br><br> To cancel your account please click <a href="/membership-account/membership-cancel/">here<a>.
												<?php } ?>
												
											<?php } else { ?>
												Current Membership Status : <b>Expired / No Membership</b> <br>
												Please purchase your membership at our shop.
											<?php } ?>
										</div>
										
										<?php 
											/* if($selectedMembershipData != 0 ) { ?>
											<input name="cancelmembership" type="submit" id="cancelmembership" 
												class="submit button" value="<?php _e('Cancel Today', 'profile'); ?>" />
											<?php wp_nonce_field( 'cancel-user-membership' ) ?>
											<input name="action" type="hidden" id="action" value="cancel-user-membership" />
											
											<div class="warning">
												By fully cancelling, you will forfeit the rest of your paid subscription <b>immediately</b>.
											</div>
										<?php } */?>
									</p><!-- .form-submit -->
								</form><!-- #adduser -->								
								
							<?php endif; ?>
						</div><!-- .entry-content -->
					</div><!-- .hentry .post -->
					<?php endwhile; ?>
				<?php else: ?>
					<p class="no-data">
						<?php _e('Sorry, no page matched your criteria.', 'profile'); ?>
					</p><!-- .no-data -->
				<?php endif; ?>				
				
            </div>             
        </div>
	</div>

<?php
	get_footer();
?>