<?php
/* --------------------------------------------------------------------------------------------------------------------- */

	// Vars
	$shortcodeid = '';
	
/* --------------------------------------------------------------------------------------------------------------------- */
	
	require_once 'sys/globalfunction.php';
	
	// Functions
	if( !function_exists( 'file_get_contents_curl' ) )
	{
		function file_get_contents_curl($url) {
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
			curl_setopt($ch, CURLOPT_URL, $url);
			
			$data = curl_exec($ch);
			curl_close($ch);
			
			return $data;
		}	
	}

	// Check Apple if membership comes from iOS
	function checkApple( $user, $username, $password ) {

		wp_set_current_user($user->ID);
		if(current_user_can('memberpress_product_authorized_25456') && !current_user_can( 'update_core' ) && !current_user_can('memberpress_product_authorized_24386') && !current_user_can('memberpress_product_authorized_24567') && !current_user_can('memberpress_product_authorized_25004') && !current_user_can('memberpress_product_authorized_24312') && !current_user_can('memberpress_product_authorized_24361') && !current_user_can('memberpress_product_authorized_24314') && !current_user_can('memberpress_product_authorized_25678') && !current_user_can('memberpress_product_authorized_25969') && !current_user_can('memberpress_product_authorized_25635')) {
			//iOS Member check token
			$applesharedsecret = "0546f945455c4622a37a964b553ffc54";
			$receiptbytes      = get_user_meta($user->ID, "transactionReceipt", true);
			$appleurl          = "https://buy.itunes.apple.com/verifyReceipt"; //Sandbox: https://sandbox.itunes.apple.com/verifyReceipt

			$request = json_encode(array("receipt-data" => $receiptbytes,"password"=>$applesharedsecret));
			$ch = curl_init($appleurl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
			$jsonresult = curl_exec($ch);

			if ($jsonresult === false) {
				$user = new WP_Error( 'denied', __("Unable to contact Apple Server. Please try again later.") );
				wp_redirect ( home_url("/itunes-subscription-expired/") ); exit;
			}
			
			curl_close($curl);
			$decoded = json_decode($jsonresult);
			
			if(empty($decoded->latest_receipt_info)) {
				$user = new WP_Error( 'denied', __("Unable to contact Apple Server. Please try again later.") );
				wp_redirect ( home_url("/itunes-subscription-expired/") ); exit;
			} else {
				$howmany = count($decoded->latest_receipt_info);
				$expirems = $decoded->latest_receipt_info[$howmany-1]->expires_date_ms;
				$nowms = round(microtime(true) * 1000);

				if($expirems <= $nowms) {
					$user = new WP_Error( 'denied', __("Your subscription to Better Meal Plans from Apple has expired. Please resubscribe using iTunes or purchase a new membership here.") );
					wp_redirect ( home_url("/itunes-subscription-expired/") ); exit;
				}
			}
		}

		return $user;
	}
	add_filter('authenticate', 'checkApple', 30, 3);

	// Add free user tag
	add_filter( 'body_class','my_body_classes' );
	function my_body_classes( $classes ) {
	
		if(current_user_can('memberpress_product_authorized_25969') && !current_user_can('administrator')) {
			$classes[] = 'free-user';
		}

		return $classes;
		
	}
	
	// Contact Us
	add_filter( 'wpcf7_validate_text', 'my_site_conditional_required', 10, 2 );
	add_filter( 'wpcf7_validate_text*', 'my_site_conditional_required', 10, 2 );	
	
	function my_site_conditional_required($result, $tag) {
	    $tag = new WPCF7_Shortcode( $tag );
	
	    $name = $tag->name;
	
	    $value = isset( $_POST[$name] )
	        ? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
	        : '';
	
	    if ( "username" == $name && 'General Questions or Feedback' != $_POST['contact-reason'] ) :
	        if ( '' == $value  ) :
	            $result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	        endif;
	    endif;
	   

	    if ( "device" == $name && 'Technical Support' == $_POST['contact-reason'] ) :
	        if ( '' == $value  ) :
	            $result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	        endif;
	    endif;	   
	    
	    if ( "browser" == $name && 'Technical Support' == $_POST['contact-reason'] ) :
	        if ( '' == $value  ) :
	            $result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	        endif;
	    endif;	   	    
	
	    return $result;
	}	
		
	// Numbered Pagination
	if( !function_exists( 'numbered_pagination_for_ajax' ) )
	{
		function numbered_pagination_for_ajax() 
		{
			// Vars
			global $wp_query, $paged;
			$countPage = $wp_query->max_num_pages;
			// Check
			if ( empty( $paged ) == true )
			{
				$paged = 1;
			}
			if ( empty( $countPage ) == true )
			{				
				$countPage = 1;
			}
			
			$allNav = '';
			
			// Show
			if ( $countPage > 1 )
			{
				// First / Prev
				if ( $paged > 1 )
				{
					$allNav .= '<div target="1" class="nav-numbered">First</div>';
					$allNav .= '<div target="' . ($paged - 1) . '" class="nav-numbered">Prev</div>';
				}
				else {
					$allNav .= '<div class="nav-numbered disabled">First</div>';
					$allNav .= '<div class="nav-numbered disabled">Prev</div>';
				}
				
				for ( $i = 1; $i <= $countPage; $i++ )
				{
					if ( $paged == $i )
					{	// Current Page						
						$allNav .= '<div class="nav-numbered static">' . $i . '</div>';
					}
					else
					{	// Other Pages
						if ( $i < ( $paged - 3 ) || $i > ( $paged + 3 ) ) {
							
						}
						else {
							$allNav .= '<div target="' . $i . '" class="nav-numbered">' . $i . '</div>';
						}
					}
				}
				// Last / Next
				if ( $paged < $countPage )
				{
					$allNav .= '<div target="' . ($paged + 1) . '" class="nav-numbered">Next</div>';
					$allNav .= '<div target="' . ($countPage) . '" class="nav-numbered">Last</div>';
				}
				else {
					$allNav .= '<div class="nav-numbered disabled">Next</div>';
					$allNav .= '<div class="nav-numbered disabled">Last</div>';	
				}
			}
			return $allNav;
		}
	}
	
	if( !function_exists( 'float2rat' ) ) {
		// Floating point to rational
		function float2rat($n, $tolerance = 1.e-6) {
			if(is_float($n) && strpos($n,'.') !== false)
			{
			
				if($n>1)
				{
					$org_val = $n;
					$int_val = explode(".",''.$n);
					$n = ".".$int_val[1];
				}
				$h1=1; $h2=0;
				$k1=0; $k2=1;
				$b = 1/$n;
				do {
					$b = 1/$b;
					$a = floor($b);
					$aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
					$aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
					$b = $b-$a;
				} while (abs($n-$h1/$k1) > $n*$tolerance);
				
				if($org_val>1)
					return $int_val[0]." $h1/$k1";
				else
					return "$h1/$k1";
			}
			elseif(is_int((int)$n))
			{	
				return $n;		
			}
		}
	}

	// Theme Setup
	add_action( 'after_setup_theme', 'initial_setup' );
	
	function initial_setup()
	{
		add_action( 'wp_enqueue_scripts', 'enqueue_list' );
		//add_action( 'widgets_init', 'widget_list' );
		
		add_post_type_support( 'page', 'excerpt' );	
		
		if (!current_user_can('administrator') && !is_admin()) 
			show_admin_bar(false);
			
		if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
			wp_redirect( home_url() );
			exit;
		}						
	}

	function login_styling() { 
    	wp_enqueue_style( 'custom-login',  get_template_directory_uri() . '/css/login-styling.css' );
   	}
	add_action( 'login_enqueue_scripts', 'login_styling' );	
	
	// Register Style and Script
	function enqueue_list() {
		// Style
		wp_register_style( 'font-montserrat', '//fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,700i,800,800i' );
		wp_register_style( 'font-breeserif', 'https://fonts.googleapis.com/css?family=Bree+Serif&display=swap' );
		wp_register_style( 'font-robotocondensed', 'https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&display=swap' );
		wp_register_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
		wp_register_style( 'bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.css' );
		wp_register_style( 'yamm', get_template_directory_uri() . '/css/yamm.css' );		
		wp_register_style( 'shortcodestyle', get_template_directory_uri() . '/css/shortcodestyle.css' );			
		wp_register_style( 'typography', get_template_directory_uri() . '/css/typography.css' );
		wp_register_style( 'flexslider-css', get_template_directory_uri() . '/css/flexslider.css' );
		wp_register_style( 'icheck-skins', get_template_directory_uri() . '/css/minimal/minimal.css' );
		wp_register_style( 'introjs-css', get_template_directory_uri() . '/css/introjs.css' );
		wp_register_style( 'introjs-theme', get_template_directory_uri() . '/css/introjs-themes/introjs-flattener.css' );
		
		wp_enqueue_style( 'font-montserrat' );
		wp_enqueue_style( 'font-breeserif' );
		wp_enqueue_style( 'font-robotocondensed' );
		wp_enqueue_style( 'bootstrap' );
		wp_enqueue_style( 'bootstrap-theme' );		
		wp_enqueue_style( 'yamm' );
		wp_enqueue_style( 'shortcodestyle' );
		wp_enqueue_style( 'typography' );
		wp_enqueue_style( 'flexslider' );
		wp_enqueue_style( 'icheck-skins' );
		wp_enqueue_style( 'introjs-css' );
		wp_enqueue_style( 'introjs-theme' );
		wp_dequeue_style( 'wpurp_style9' ); //This styling makes it impossible to have green padlock
	
		// Script				
		wp_register_script( 'jqueryui', get_template_directory_uri() . '/js/jqueryui.js', null, null, true );
		wp_register_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', null, null, true );
		wp_register_script( 'bootstrap-theme', get_template_directory_uri() . '/js/bootstrap-theme.js', null, null, true );
		wp_register_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.js', null, null, true );			
		wp_register_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', null, null, true );			
		wp_register_script( 'grid', get_template_directory_uri() . '/js/grid.js', null, null, true );			
		wp_register_script( 'icheck', get_template_directory_uri() . '/js/icheck.min.js', null, null, true );			
		wp_register_script( 'introjs', get_template_directory_uri() . '/js/intro.js', null, null, true );			
		wp_register_script( 'Xportability-PDF', get_template_directory_uri(). '/js/xepOnline.jqPlugin.js', null, null, true );
		wp_register_script( 'scrollstop', get_template_directory_uri() . '/js/scrollstop.js', null, null, true );			
		wp_register_script( 'default', get_template_directory_uri() . '/js/default.js', null, null, true );					
		wp_register_script( 'flipclock', get_template_directory_uri() . '/js/flipclock.min.js', null, null, true );	
		
		wp_enqueue_script( 'jquery' );		
		wp_enqueue_script( 'jqueryui' );	
		wp_enqueue_script( 'bootstrap' );
		wp_enqueue_script( 'bootstrap-theme' );
		wp_enqueue_script( 'modernizr' );
		wp_enqueue_script( 'grid' );
		wp_enqueue_script( 'icheck' );		
		wp_enqueue_script( 'flexslider' );	
		wp_enqueue_script( 'introjs' );	
		wp_enqueue_script( 'Xportability-PDF' );	
		wp_enqueue_script( 'scrollstop' );	
		wp_enqueue_script( 'flipclock' );		
		
		wp_enqueue_script( 'default' );
		
		wp_localize_script('default', 'this_url', array( 'template_url' => get_template_directory_uri() ) );
	}	

/* --------------------------------------------------------------------------------------------------------------------- */

	// Theme Options
	if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/lib/ReduxFramework/ReduxCore/framework.php' ) ) {
		require_once( dirname( __FILE__ ) . '/lib/ReduxFramework/ReduxCore/framework.php' );
	}
	if ( !isset( $theme_options ) && file_exists( dirname( __FILE__ ) . '/lib/ReduxFramework/themeoptions/themeoptions.php' ) ) {
		require_once( dirname( __FILE__ ) . '/lib/ReduxFramework/themeoptions/themeoptions.php' );
	}

/* --------------------------------------------------------------------------------------------------------------------- */	

	// Menu
	require_once( 'lib/menu/jellypixel_menu.php' );	

	new jellypixel_menu();	
	
	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menu('topmenu-location', 'Main Menu');
		register_nav_menu('membermenu-location', 'Member Menu');
		register_nav_menu('freemembermenu-location', 'Free Member Menu');
		register_nav_menu('advisormain-location', 'Advisor Main Menu');
		register_nav_menu('advisorsub-location', 'Advisor Sub Menu');
		register_nav_menu('product-location', 'Product Menu');
	}

/* --------------------------------------------------------------------------------------------------------------------- */	
	
	// Admin
	if ( is_admin() ) {
		add_action('admin_init', 'admin_hook');
		add_action('admin_head', 'admin_css');		
	}
	
	function admin_hook() {	
	
	}

	function admin_css() {	
		echo 
			'<style>
				/* Page Parameters */
				.meta-table { width:100%; border-collapse:collapse; }
				.meta-table tr { border-bottom:1px solid #e7e7e7; }
				.meta-table tr:last-child { border-bottom:none; }
				.meta-table > tbody > tr > th { width:25%; text-align:left; }
				.meta-table > tbody > tr > th > .meta-table-header { margin:20px 0 20px 0; }
				.meta-table > tbody > tr > td > .meta-table-fieldset { margin:20px 0 20px 0; }
				.meta-table > tbody > tr > td > .meta-table-fieldset > select { display:block; }
				.meta-table > tbody > tr > td > .meta-table-fieldset > select option { padding-right:20px; }
				.meta-table > tbody > tr > td > .meta-table-fieldset > .meta-table-description { font-size:0.9em; margin:20px 0 0 3px; color:#666666; line-height:1.8em; }
				.meta-table > tbody > tr > td > .meta-table-fieldset > .meta-table-description > .meta-table-bold { font-weight:bold; }
				.meta-table > tbody > tr > td > .meta-table-fieldset > .meta-table-description > .meta-table-italic { font-style:italic; }
				.meta-table-section-header { background:url("' . get_template_directory_uri() . '/img/themeoptions/pattern_black.png") repeat scroll 0 0 rgba(0, 0, 0, 0); border:none;	padding:5px 0px 5px 10px; font-weight:bold; display:block; line-height:2em; margin-top:0; text-transform:uppercase; color:#ffffff; font-family: Lato, Arial, sans-serif; font-size:1.2em; }
				
				/* Menu Icon UI */
				.field-icon {
					margin-top:20px !important;	
				}
				
				/* Megamenu UI */
				.description input, .description select { margin-top:3px; }
				.menu-edit p.description-section { float:left; margin-bottom:5px; margin-right:10px; font-weight:bold; }				
				.menu-item-megamenu-options { background-color:#f4f4f4; margin:0 12px 0 0; }
				.menu-item-megamenu-options .description-thin { width:180px; }				
				.menu-item-megamenu-options .megamenu-options-header { background-color:#1a93c3; padding: 0 10px 0 10px; }
				.menu-item-megamenu-options .megamenu-options-header p.description { color:#ffffff; }
				.menu-item-megamenu-options .megamenu-options-section { padding:5px 10px 15px 10px; }
				.menu-item-megamenu-options .megamenu-options-section > p:first-child { margin-top:0; }
				.menu-item-megamenu-options .megamenu-options-section > p { margin-top:10px; }
				.menu-item-megamenu-options:after, 
				.menu-item-megamenu-options .megamenu-options-header:after, 
				.menu-item-megamenu-options .megamenu-options-section:after { 
					content:""; display:block; clear: both; 
				}				
				.menu-item-megamenu-options .megamenu-type-description { margin-bottom:0; font-style:italic; clear:both; }
				
				/* Megamenu UI - Uploader */
				#featured-footer-image-info { display: none; }
				.megamenu-preview { max-width:368px; height:auto; display:block; margin-bottom:10px; }
				.megamenu-label { margin-bottom:3px !important; margin-top:20px !important; }
				
				.megamenu-bg-remove {
					background:#f7f7f7;
					border-color:#cccccc;
					box-shadow: 0 1px 0 #fff inset, 0 1px 0 rgba(0, 0, 0, 0.08);
					color:#555555;
					vertical-align:top;	
					font-style:normal;
					margin-top:5px;
				}
				
				.megamenu-bg-remove {
					color:#ef521d !important;
					margin-left:10px;		
				}
				
				.megamenu-bg-remove:hover {
					color:#ff0000 !important;	
				}
				
				/* Redux Framwework */
				.redux-container .redux-group-tab h3 {
					border-bottom: 3px solid #0073aa;
					color:#23282d;
					text-transform:uppercase;
					padding:3px;
					font-size:2em;
					font-weight:400;
					margin-bottom:15px;
					margin-top:25px;
				}
				
				.redux-group-tab .redux-section-desc {
					padding:10px 0 10px 0;	
					margin-top:-15px;
					margin-bottom:25px !important;
					line-height:1.8em;
					font-style:italic;
				}
				
				.redux-notice-field {
					margin-bottom:15px;
					margin-top:0px;	
				}
				
				.redux-info-title {
					font-size:1.4em;
					color:#888888;
					margin-bottom:10px;	
					text-transform:uppercase;
				}
				
				.redux-notice-field.redux-info {
					background-color:#f4f4f4;	
				}
				
				.redux-main .field-desc {
					line-height:20px;	
				}
				
				.redux-container-image_select ul.redux-image-select li {
					margin: 0 10px 3px 0 !important;
				}
				
				.redux-image-select {
					font-size:13px;	
				}
				
				.bold {
					font-weight:bold;	
				}
				
				.italic {
					font-style:italic;	
				}
				
				.normal {
					font-style:normal;	
				}
			</style>';
	}

/* --------------------------------------------------------------------------------------------------------------------- */

	//Title
	add_filter('wp_title', 'theme_title', 10, 2);

	function theme_title($title, $sep) {
		global $paged, $page;
	
		if (is_feed()) {
			return $title;
		}
	
		$title .= get_bloginfo( 'name' );
	
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			$title = "$title $sep $site_description";
			
		if ( $paged >= 2 || $page >= 2 )
			$title = "$title $sep " . sprintf( __( 'Page %s', 'jellypixel' ), max( $paged, $page ) );
	
		return $title;
	}

/* --------------------------------------------------------------------------------------------------------------------- */

	//Theme Support
	if ( function_exists( 'add_theme_support' ) ) {		
		add_theme_support( 'post-thumbnails', array( 'post', 'video', 'product', 'testimonial', 'blog' ) );
	}
	
	// changing the logo link from wordpress.org to your site
    function mb_login_url() {  return home_url(); }
    add_filter( 'login_headerurl', 'mb_login_url' );
    
    // changing the alt text on the logo to show your site name
    function mb_login_title() { return get_option( 'blogname' ); }
    add_filter( 'login_headertitle', 'mb_login_title' );
	
/* --------------------------------------------------------------------------------------------------------------------- */

	// Image Size
	if ( function_exists( 'add_image_size' ) ) {
		add_image_size( 'post-thumb', 1170 );
		add_image_size( 'gallery-thumb', 500 );		
	}
		
/* --------------------------------------------------------------------------------------------------------------------- */

	// Sidebar
    if ( function_exists( 'register_sidebar' ) ) {
		// Blog
		register_sidebar( array(
			'id'			=> 'blog_sidebar',
			'name'			=> __( 'Blog Sidebar', 'jellypixel' ),			
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'	=> '</section>',
			'before_title'	=> '<h1 class="widget-title">',
			'after_title'	=> '</h1>' 
		) );	
		
		// Footer Widget 
		register_sidebar( array(
			'id'			=> 'footer-widget',
			'name'			=> __( 'Footer Widget', 'jellypixel' ),			
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'	=> '</section>',
			'before_title'	=> '<h1 class="widget-title">',
			'after_title'	=> '</h1>' 
		));
		
	}
	

/* --------------------------------------------------------------------------------------------------------------------- */

	add_shortcode( 'button', 'button' );
	add_shortcode( 'row', 'row' );
	add_shortcode( 'column', 'column' );
	add_shortcode( 'table', 'table' );	
	add_shortcode( 'table_row', 'table_row' );
	add_shortcode( 'table_column', 'table_column' );	
	add_shortcode( 'title', 'title' );	
	add_shortcode( 'toggles', 'toggles' );	
	add_shortcode( 'toggle', 'toggle' );	
	add_shortcode( 'notification', 'notification' );
	add_shortcode( 'code', 'code' );
	add_shortcode( 'lists', 'lists' );
	add_shortcode( 'listitem', 'listitem' );	
	add_shortcode( 'link_button', 'link_button' );
	add_shortcode( 'label', 'label' );
	add_shortcode( 'lead', 'lead' );
	add_shortcode( 'dropcap', 'dropcap' );
	add_shortcode( 'line', 'line' );
	add_shortcode( 'action', 'action' );
	add_shortcode( 'center', 'center' );
	add_shortcode( 'background', 'background' );
	add_shortcode( 'fontsize', 'fontsize' );
	add_shortcode( 'cart', 'cart' );
	add_shortcode( 'advisoremail', 'advisoremail' );
	add_shortcode( 'verticalspace', 'verticalspace' );
	
	add_shortcode( 'get_started', 'get_started' );
	add_shortcode( 'get_started_step', 'get_started_step' );
	
	add_shortcode( 'carousel', 'carousel' );
	add_shortcode( 'slide', 'slide' );
	
	add_shortcode( 'satisfaction', 'satisfaction' );
	
	function get_started($atts, $content = null) 
	{
		extract( shortcode_atts( array(  
			'title' => ''
		), $atts ) );
		
		return 
			'<div class="get_started_wrapper">
				<div class="content-title">
					' . $title . '
				</div>
				' . do_shortcode( $content ) . '
			</div>';
	}
	
	function get_started_step($atts, $content = null ) {
		extract( shortcode_atts( array(  
			'title' => 'Step',
			'link' => '',
			'link_text' => 'Button'
		), $atts ) );
		
		if ( !empty( $link ) ) {
			$linkEl =
				'<div class="get_started_step_link_wrapper">
					<a class="orange-button" href="' . $link . '">' . $link_text . '</a>
				</div>';
		}
		
		return
			'<div class="get_started_step">
				<div class="get_started_step_title">
					' . $title . '
				</div>
				<div class="get_started_step_content">
					' . do_shortcode( $content ) . '
				</div>
				' . $linkEl . '			
			</div>';
	}
	
	
	function verticalspace($atts, $content = null) 
	{
		extract( shortcode_atts( array(  
			'height' => ''
		), $atts ) );
		
		return '<div style="height:'.$height.'"></div>';
	}	
	
	function cart( $atts, $content = null ) {
		extract( shortcode_atts( array(  
			'product' => '',
			'desc' => '',
			'price' => 0,
			'promo_price' => NULL,
			'terms_conditions_link' => '',
			'pay_link' => ''
		), $atts ) );
		
		if ( empty( $product ) || empty( $desc ) || empty( $terms_conditions_link ) )
		{
			return;	
		}
		
		if ( $promo_price != NULL ) {
			// promo
			$priceEl = '<div class="cart_strikethrough_price">' . $price . '</div><div class="cart_promo_price">' . $promo_price . '</div>';
			$finalPriceEl = $promo_price;
		}
		else {
			// normal	
			$priceEl = $price;
			$finalPriceEl = '<div class="cart_normal_price">' . $price . '</div>';
		}
		
		$payEl = "";
		if (!empty( $pay_link )) {
		
			$payEl = ' <div class="cart_button_wrapper clearfix">
							<div class="cart_pay_now" href="' . $pay_link . '">
								Pay Now
							</div>
						</div>
					';
		}
		
		$el = 
			'<div class="cart_table table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th width="50%">Products</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><div class="cart_product_wrapper" style="background-image:url(' . get_bloginfo('template_url') . '/img/20dishescart.png);"><div class="cart_product_title">' . $product . '</div><div class="cart_product_desc">' . $desc . '</div></div></td>
							<td>' . $priceEl . '</td>
							<td>1</td>
							<td>' . $finalPriceEl . '</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td>Subtotal</td>
							<td></td>
							<td></td>
							<td>' . $finalPriceEl . '</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="cart_footer">
				<div class="cart_terms_conditions">
					<a href="' . $terms_conditions_link . '">
						<div class="cart_terms_conditions_title">
							Read Terms and Conditions Here
						</div>
					</a>
					<div class="term_wrapper">
						<input class="term_check" id="cart_term_1" type="checkbox" name="cart_term_1" />
						<label for="cart_term_1" class="term_label">I accept the terms and conditions for this product.</label>
					</div>
					<div class="term_wrapper">
						<input class="term_check" id="cart_term_2" type="checkbox" name="cart_term_2" />
						<label for="cart_term_2" class="term_label">I understand that this is a recurring membership service and I will be rebilled according to the plan I select.</label>				
					</div>
				</div>
				' . $payEl . '
			</div>';
			
		return $el;
	}
		
	function center( $atts, $content = null ) 
	{	
		extract( shortcode_atts( array(  
			"padding" 		=> "0px"
		), $atts ) );
		
		return '<div class="center" style="padding:0px ' . $padding . ' 0px ' . $padding . ';">' . do_shortcode( $content ) . '</div>';
	}
	
	function background( $atts, $content = null ) 
	{	
		extract( shortcode_atts( array( 
			"top"		=> "no", 
			"color" 	=> "#ffffff"
		), $atts ) );
		
		$topClass = "";
		
		if ( $top == "no" ) {
			$topClass = "backgroundtop"	;
		}
		
		return '<div class="background ' . $topClass . '" style="background-color:' . $color . ';">' . do_shortcode( $content ) . '</div>';
	}
	
	function fontsize( $atts, $content = null ) 
	{	
		extract( shortcode_atts( array(  
			"size" 		=> "15px"
		), $atts ) );
		
		return '<span class="fontsize" style="font-size:' . $size . ';">' . do_shortcode( $content ) . '</span>';
	}
	
	
	function title( $atts, $content = null ) 
	{
		
		extract( shortcode_atts( array(  
			"top" 		=> "10px",
			"bottom" 	=> "10px",
		), $atts ) );
		
		$con = do_shortcode( $content );
		
		$titleEl = '<div class="generic_title" style="margin-top:'.$top.'; margin-bottom: '.$bottom.';">' . $con . '</div>';	
		
		return 
			'<div class="shortcode_wrapper steppes">
				' . $titleEl . '
			</div>';
	}	
	
	function button( $atts, $content = null ) 
	{
		extract( shortcode_atts( array(  
			"target" => "new",
			"color" => "red",
			"icon" => "",
			"size" => "small",			
			"url" => null,
			"text" => null,
			"type" => "",
			"style" => ""
		), $atts ) );
		
		if( $target == "new" ) 
		{
			$target = "_blank";
		}
		else {
			$target = "_self";
		}
			
		if ( empty( $text ) ) 
		{
			$text = "Click Here";	
		}
		
		if ( !empty( $style ) ) {
			$styleEl = 'style="' . $style . '"';	
		}
		else {
			$styleEl = '';	
		}
		
		if ( empty ( $type ) ) {			
			return '<a class="linkbtn-wrapper" href="'.$url.'" target="'.$target.'"> <button class="linkbtn linkbtn-' . $size . ' linkbtn-' . $color . ' fa fa-lg ' . strtolower(str_replace(' ', '-', $color)) . ' ' . strtolower(str_replace(' ', '-', $icon)) .' "><span>' . $text . '<span></button></a>';
		}
		else if ( $type == "orange" ) {
			return 
				'<div class="orange-button-default">
					<a class="orange-button bigger" href="'.$url.'" target="'.$target.'"> ' . $text . '</a>
				</div>';
		}
		else if ( $type == "orange-big" ) {
			return 
				'<div class="orange-button-center" ' . $styleEl . '>
					<a class="orange-button bigger center" href="'.$url.'" target="'.$target.'"> ' . $text . '</a>
				</div>';
		}
	}		
	
	function row( $atts, $content = null ) 
	{
		extract( shortcode_atts( array(  
			"showgrid" => "no"
		), $atts ) );	
		
		if ($showgrid == "no")
		{
			$el = '<div class="row">' . do_shortcode( $content ) . '</div>';		
		}
		else
		{
			$el = '<div class="row showgrid">' . do_shortcode( $content ) . '</div>';
		}
			
		return $el;
	}
	
	function line( $atts, $content = null ) {
		$el = '<hr>';		
		return parse_shortcode_content($el);
	}	
	
	function column( $atts, $content = null ) {
		extract(shortcode_atts(array(  
			"size" => "4"
		), $atts));
		$el = '<div class="col-lg-' . $size . ' col-md-' . $size . ' col-sm-' . $size . ' col-xs-12">' . do_shortcode( $content ) . '</div>';		
		return parse_shortcode_content($el);
	}	
	
	function table( $atts, $content = null ) {
		extract(shortcode_atts(array(  
			"bordered" => "no",
			"striped" => "no",
			"hovered" => "no",
			"condensed" => "no",
		), $atts));	
		
		$classString = $bordered=="yes"? "table-bordered": "";
		$classString .= $striped=="yes"? " table-striped": "";
		$classString .= $hovered=="yes"? " table-hover": "";
		$classString .= $condensed=="yes"? " table-condensed": "";
		
		$el = '<div class="table-responsive"><table class="table '.$classString.'">' . do_shortcode( $content ) . '</table></div>';		
		
		$headOpening = false; $headClosing = false;
		$bodyOpening = false; $bodyClosing = 0;
		$haveBody = false; 
		
		for($i = 1; $i <= strlen($el); $i++) {
		
			if(substr($el, $i, 8) == "</tbody>") {

				$el = substr_replace($el, '', $i, 8);
				$bodyClosing = $i;		
			}			
		
			if(substr($el, $i, 7) == "<thead>") {
				
				if($headOpening == false) 
					$headOpening = true;
				else
					$el = substr_replace($el, '', $i, 7);
			}		
			
			if(substr($el, $i, 8) == "</thead>") {

				if($headClosing == false) 
					$headClosing = true;
				else
					$el = substr_replace($el, '', $i, 8);				
			}

		}
		$el = substr_replace($el, "</tbody>", $bodyClosing, 0);
		
		for($i = 1; $i <= strlen($el); $i++) {
			if(substr($el, $i, 7) == "<tbody>") {
				
				if($bodyOpening == false) {
					$bodyOpening = true;
					$el = substr_replace($el, '', $i, 7);
				} else {

					if($haveBody == false) 
						$haveBody = true;
					else 
						$el = substr_replace($el, '', $i, 7);
				}
			}		
		}			
		
		return parse_shortcode_content($el);
	}
	
	function table_row( $atts, $content = null ) {
		extract(shortcode_atts(array(  
			"context" => ""	
		), $atts));
		
		if($context == "normal") $context = "";
		
		$el = '<thead><tbody><tr class="'.$context.'">' . parse_shortcode_content(do_shortcode( $content )) . '</tr></tbody></thead>';		
		return $el;
	}
	
	function table_column( $atts, $content = null ) {
		extract(shortcode_atts(array(  
			"header" => "no"	
		), $atts));
		
		$el = "";
		
		if($header == "no")
			$el = '<td>' . do_shortcode( $content ) . '</td>';		
		else  
			$el = '<th>' . do_shortcode( $content ) . '</th>';	
		
		return $el;
	}		
	
	function label( $atts, $content = null ) {

		return parse_shortcode_content('<div class="format-label">' . do_shortcode($content) . '</div>');
	}	

	function lead( $atts, $content = null ) {

		extract(shortcode_atts(array(  
			"dropcap" => "yes"
		), $atts));
		
		if ( $dropcap == "yes" ) {
			$dropClass = 'dropcap';
		}
		else {
			$dropClass = '';	
		}
		
		return parse_shortcode_content('<div class="lead ' . $dropClass . '">' . do_shortcode($content) . '</div>');
	}	

	function dropcap( $atts, $content = null ) {
		$el = '<p class="dropcap">' . do_shortcode($content) . '</p>';
		return parse_shortcode_content( $el );
	}	
	
	function carousel( $atts, $content = null ) 
	{
		extract( shortcode_atts( array(  
			"title" => ""
		), $atts ) );	
		
		$el = '<div class="shortcode-testimonial flexslider"><div class="shortcode-testimonial-title">' . $title . '</div><ul class="slides">' . do_shortcode( $content ) . '</ul></div>';		
			
		return $el;
	}
	
	function slide( $atts, $content = null ) {
		extract(shortcode_atts(array(  
			"image" => "",
			"name" => "",
			"occupation" => ""
		), $atts));
		
		$occupationEl = '';
		if ( !empty( $occupation )) 
		{
			$occupationEl = '<span class="shortcode-testimonial-occupation">, ' . $occupation . '</span>';		
		}
		
		$el = 
			'<li><div class="shortcode-testimonial-wrapper"><div class="shortcode-testimonial-thumb" style="background-image:url(' . $image . ');"></div><div class="shortcode-testimonial-content">' . do_shortcode( $content ) . '</div><div class="shortcode-testimonial-footer"><span class="shortcode-testimonial-name">' . $name . '</span>' . $occupationEl . '</div></div></li>';		
			
		return parse_shortcode_content($el);
	}		
	
	function satisfaction( $atts, $content = null ) 
	{
		extract( shortcode_atts( array(  
			"title" => "",
			"image" => ""			
		), $atts ) );	
		
		$el = '<div class="row shortcode-satisfaction"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 shortcode-satisfaction-left"><img src="' . $image . '" alt="" /></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 shortcode-satisfaction-right"><div class="shortcode-satisfaction-title">' . $title . '</div><div class="shortcode-satisfaction-content">' . do_shortcode( $content ) . '</div></div></div>';		
			
		return $el;
	}
		
	
	
	
	/* SHORTCODE - Homepage Introduction */
		add_shortcode( 'homepage_introduction', 'homepage_introduction' );	
		
		function homepage_introduction( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"heading_line1"	=> "",
				"heading_line2"	=> "",
				"heading_line3"	=> "",
				"heading_highlight_text" => "",
				"belt_text" 	=> "",
				"button_link" 	=> "",
				"phone" 		=> "",
				"mail" 			=> "",
				"site" 			=> "",
				"button_text" 	=> "",
				"featured_title"	=> "",
				"featured_image"	=> ""
			), $atts ) );
			
			$phoneEl = $mailEl = $siteEl = $titleEl = $headingEl = $headingHighlightEl = $featuredEl = '';
			
			if ( !empty( $phone ) ) {
				$phoneEl =
					'<div class="topbar-contact-wrapper">
						<div id="phone" class="topbar-contact-image">
						</div>	
						<div class="topbar-contact-value">
							' . $phone . '
						</div>	
					</div>';
			}			
			if ( !empty( $mail ) ) {
				$mailEl =
					'<div class="topbar-contact-wrapper">
						<div id="mail" class="topbar-contact-image">
						</div>	
						<div class="topbar-contact-value">
							' . $mail . '
						</div>	
					</div>';	
			}			
			if ( !empty( $site ) ) {
				$siteEl =
					'<div class="topbar-contact-wrapper">
						<div id="site" class="topbar-contact-image">
						</div>	
						<div class="topbar-contact-value">
							' . $site . '
						</div>	
					</div>';	
			}
			
			if ( !empty( $featured_image ) )
			{
				if ( !empty( $featured_title ) ) {
					$featuredTitleEl = 
						'<div class="featured_title">' . $featured_title . '</div>';	
				}
			
				$featuredEl = 
					'<div class="shortcode_wrapper featured">
						<div class="container">
							' . $featuredTitleEl . '
							<img src="' . $featured_image . '" alt="Better Meal Plans Featured on" />
						</div>
					</div>';
			}
			
			$headingEl = '<span class="first">';
			
			if ( !empty( $heading_line1 ) ) 
			{
				$headingEl = $headingEl . $heading_line1;					
			}
			
			if ( !empty( $heading_line2 ) ) 
			{
				$headingEl = $headingEl . '<br>' . $heading_line2;					
			}
			
			if ( !empty( $heading_line3 ) ) 
			{
				$headingEl = $headingEl . '<br>' . $heading_line3;					
			}
			
			$headingEl .= '</span>';
			
			if ( !empty( $heading_highlight_text ) ) 
			{
				$headingHighlightEl = ' <span class="black">' . $heading_highlight_text . '</span>';
			}
			
			// Planning and cooking dinner can be a challenge.<br>We get it, and that\'s why we exist!<br>We\'ll help you plan dinner and tell you how to prepare it quickly <span class="black">(and stress-free)</span>!
			
			/*$el =
				'<div class="home-bg">
					<div class="container">
						<div class="shortcode_wrapper homepage_introduction row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="homepage_introduction_title">       
									' . $headingEl . $headingHighlightEl . '
								</div>
								<div class="orange-button-center">
									<a class="orange-button" href="'.$button_link.'">'.$button_text.'</a>
								</div>
							</div>					
						</div>
					</div>
                </div>
				<div class="topbar">				
					' . $phoneEl . '
					' . $mailEl . '
					' . $siteEl . '
				</div>
				
				<div class="home-greenbelt">
					<div class="container">
						' . $belt_text . '
					</div>
				</div>';*/
				
			$el = 
				'<div class="home-bg">
					<div class="container">
						<div class="shortcode_wrapper homepage_introduction row row0">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 introtop">
								<h1>Our AIP Diet Meal Plans
</h1> 
								<h2>are making AIP meals a breeze
									
									<span class="underline"> - Saving Members An Average of $300+
and 16 Hours Every Single Month!
</span>
								</h2>
								<div class="">
									<a href="'.$button_link.'" class="orange-button ">
										Try us Free 
									</a>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 introbottom">
								<img src="' . get_template_directory_uri() . '/img/headerbg.png" alt="Customized Meal Prep Plans" />
							</div>
						</div>
					</div>
				</div>
				<div class="topbar">				
					' . $phoneEl . '
					
					' . $mailEl . '
					' . $siteEl . '
				</div>
				' . $featuredEl . '				
				<div class="home-greenbelt">
					<div class="container">
						' . $belt_text . '
					</div>
				</div>';
				
			return $el;			
		
		}
		
	/* SHORTCODE - Homepage How it Works */
		add_shortcode( 'homepage_howitworks', 'homepage_howitworks' );
			
		function homepage_howitworks( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 				=> "How it Works:",
				"plan_title"			=> "Customize Your Plan",
				"plan_description" 		=> "",
				"shopping_title"		=> "Go Shopping",
				"shopping_description"	=> "",
				"guide_title"			=> "Follow the Simple Food Prep Guide",
				"guide_description"		=> "",
				"dinner_title"			=> "Get dinner on the table in 30 minutes or less",
				"dinner_description"	=> "",
				"button_text"			=> "",
				"button_link"			=> "",
				"vimeo"					=> ""
			), $atts ) );
			
			$el =
				'<div class="home-howitworks">
					<div class="container">
						<div class="howitworks_title">
							' . $title . '
						</div>
						<a href="' . $vimeo . '" class="video-button video-howitworks" videotype="vimeo">Take a peek<span class="tdicon-play-circle"></span></a><br>
						<div class="howitworkslist-wrapper">
							<div class="row">
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
									<div class="howitworks-wrapper">
										<div id="customize" class="howitworks-icon">
										</div>
										<div class="howitworks-content">
											<div class="howitworks-title">
												' . $plan_title . '
											</div>
											<div class="howitworks-description">
												' . $plan_description . '
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
									<div class="howitworks-wrapper">
										<div id="shopping" class="howitworks-icon">
										</div>
										<div class="howitworks-content">
											<div class="howitworks-title">
												' . $shopping_title . '
											</div>
											<div class="howitworks-description">
												' . $shopping_description . '
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
									<div class="howitworks-wrapper">
										<div id="follow" class="howitworks-icon">
										</div>
										<div class="howitworks-content">
											<div class="howitworks-title">
												' . $guide_title . '
											</div>
											<div class="howitworks-description">
												' . $guide_description . ' 
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
									<div class="howitworks-wrapper">
										<div id="get" class="howitworks-icon">
										</div>
										<div class="howitworks-content">
											<div class="howitworks-title">
												' . $dinner_title . '
											</div>
											<div class="howitworks-description">
												' . $dinner_description . '
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="orange-button-fullwidth">
							<a href="' . $button_link . '" class="orange-button ">
								' . $button_text . '
							</a>
						</div>
					</div>
				</div>';
				
			return $el;			
		}
	
	/* SHORTCODE - Tabs */
		add_shortcode( 'tabs', 'tabs' );	
		add_shortcode( 'tab', 'tab' );	
		
		function tabs( $atts, $content = null ) {
			extract( shortcode_atts( array(  
				"title" 		=> "",
				"subtitle" 		=> "",
				"navigation"	=> "no"
			), $atts ) );
			
			$con = do_shortcode($content);	
			$arr1 = explode ( "$#@$%ThisIsMagnificentSeparation2$#@$%" , $con );
			
			$el = 
				'<div class="tabs-title">
					<div class="tabs-title-1">
						' . $title . '
					</div>
					<div class="tabs-title-2 container">
						 ' . $subtitle . '
					</div>					
				</div>';
			
			$el .= '<ul class="nav nav-tabs">';
			foreach ($arr1 as $item) {
			
				$arr2 = explode ( "$#@$%ThisIsMagnificentSeparation$#@$%" , $item );
				$el .= $arr2[0];
			}
			$el .= '</ul>';
			
			$el .= '<div class="tab-content container">';
			foreach ($arr1 as $item) {
			
				$arr2 = explode ( "$#@$%ThisIsMagnificentSeparation$#@$%" , $item );
				$el .= $arr2[1];
			}
			
			if ( $navigation == "yes" ) {			
				$el .= 
					'<div class="tab-navigation-wrapper">
						<div class="tab-navigation-left tdicon-big-arrow-left"></div>
						<div class="tab-navigation-right tdicon-big-arrow-right"></div>
					</div>';
			}
			
			$el .= '</div>';
			
			return '<div class="shortcode_wrapper tab_wrapper">' .  parse_shortcode_content($el) . '</div>';
		}		
		
		function tab( $atts, $content = null ) {
	
			static $staticid = 0; $staticid++;
			
			extract(shortcode_atts(array(  
				"active"	 	=> null,
				"title"		 	=> null,
				"subtitle"		=> "",
				"thumbnail"	 	=> null
			), $atts));
			
			$el = "";
			if ( $title != null ) {
				
				if ( $active != "active" ) {
					$active = "";
				}
				
				$el = 
					'<li class="' . $active . '">
						<a href="#tab-' . $staticid . '" data-toggle="tab">' . $title . '</a>
					</li>';								
				
				$el .= '$#@$%ThisIsMagnificentSeparation$#@$%';
				
				if ( !empty( $thumbnail ) ) {
					$el .= 
						'<div class="tab-pane ' . $active . '" id="tab-'.$staticid.'">
							<div class="row">
								<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
									<img src="' . $thumbnail . '" />
								</div>
								<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
									<div class="tab-pane-title">
										' . $title . '
									</div>
									<div class="tab-pane-subtitle">
										' . $subtitle . '
									</div>	
									<div class="tab-pane-content">
										' . do_shortcode($content) . '
									</div>
								</div>
							</div>							
						</div>';			
				}
				else {
					$el .= 
						'<div class="tab-pane ' . $active . '" id="tab-'.$staticid.'">
							<div class="tab-pane-title">
								' . $title . '
							</div>
							<div class="tab-pane-subtitle">
								' . $subtitle . '
							</div>	
							<div class="tab-pane-content">
								' . do_shortcode($content) . '
							</div>
						</div>';			
				}
					
				$el .= '$#@$%ThisIsMagnificentSeparation2$#@$%';	
			}		
			
			return $el;
		}
	
	/* SHORTCODE - Video Button */
		add_shortcode( 'video_button', 'video_button' );
		
		function video_button( $atts, $content = null ) {
			extract( shortcode_atts( array(  
				"vimeo"			=> "",
				"youtube"		=> ""
			), $atts ) );
			
			if ( empty( $vimeo ) && empty( $youtube ) ) {
				return;
			}
			
			if ( !empty( $vimeo ) ) {
				$videoType = 'vimeo';
				$link = $vimeo;
			}
			else {
				$videoType = 'youtube';
				$link = $youtube;
			}
			
			if ( empty( $content ) ) {
				$content = "Take a peek";
			}
			
			$el = 
				'<a href="' . $link . '" class="video-button" videotype="' . $videoType . '">' . $content . '<span class="tdicon-play-circle"></span></a><br>';
			
			return shortcode_empty_paragraph_fix($el);
		}
		
	/* SHORTCODE - Orange Button */
		add_shortcode( 'orange_button', 'orange_button' );
	
		function orange_button( $atts, $content = null ) {
			extract( shortcode_atts( array(  
				"link"			=> "#",
				"type"			=> "",
				"align"			=> ""
			), $atts ) );
			
			$classEl = "";
			
			if ( $type == "bigger" ) {
				$classEl .= " bigger";	
			}
			if ( $align == "center" ) {
				$classEl .= " center";	
				
				$el =
					'<div class="orange-button-center"><a href="' . $link . '" class="orange-button ' . $classEl . '">' . $content . '</a></div><br>';
			}
			else {				
				$el = 
					'<div class="orange-button-default"><a href="' . $link . '" class="orange-button ' . $classEl . '">' . $content . '</a></div><br>';
			}
			
			return $el;
		}
		
	/* SHORTCODE - Homepage Steppes */
		add_shortcode( 'steppes', 'steppes' );	
		add_shortcode( 'step', 'step' );	
		
		function steppes( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
			), $atts ) );
			
			$con = do_shortcode( $content );
			
			if ( !empty( $title ) ) {
				$titleEl = 
					'<div class="steppes_title">' . $title . '</div>';	
			}
			
			return 
				'<div class="shortcode_wrapper steppes">
					' . $titleEl . '
					<div class="row">
						' . $con . '
					</div>
				</div>';
		}
		
		function step( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
				"image"			=> ""
			), $atts ) );
			
			if ( !empty( $title ) ) {
				$title = 
					'<div class="step_title">' . $title . '</div>';	
			}
			
			return 
				'<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 step_wrapper">
					<div class="step_image" style="background-image:url(' . $image . ')">
					</div>
					<div class="step_text">
						' . $title . '
						<div class="step_content">
							' . $content . '
						</div>
					</div>
				</div>';
		}	
		
	/* SHORTCODE - Homepage Mission */
		add_shortcode( 'homepage_mission', 'homepage_mission' );	
		
		function homepage_mission( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
				"link"			=> ""
			), $atts ) );
			
			$el =
				'<div class="container">
					<div class="row mission-wrapper">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mission-left">
							<img src="' . get_bloginfo( 'template_url' ) . '/img/smilinggirl.jpg">
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mission-right">
							<h3>
								' . $title . '		
							</h3>
							<p>
								' . do_shortcode( $content ) . '
							</p>					
							<br>
							<a href="' . $link . '">
								Learn More Here
							</a>
						</div>
					</div>    
				</div>';
				
			return $el;
		}
		
	/* SHORTCODE - Greenbox */
		add_shortcode( 'greenbox', 'greenbox' );	
		
		function greenbox( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
			), $atts ) );
			
			if ( !empty( $title ) ) {
				$titleEl = 
					'<div class="greenbox_title">' . $title . '</div>';	
			}
			
			return 
				'<div class="shortcode_wrapper greenbox">					
					' . $titleEl . '
					' . do_shortcode($content) . '
				</div>';
		}
		
	/* SHORTCODE - Testimonials */
		add_shortcode( 'homepage_testimonials', 'homepage_testimonials' );	
		add_shortcode( 'testimonial', 'testimonial' );	
		
		function homepage_testimonials( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
			), $atts ) );
			
			$con = do_shortcode( $content );
			
			if ( !empty( $title ) ) {
				$titleEl = 
					'<div class="testimonial_title">' . $title . '</div>';	
			}
			
			return 
				'<div class="shortcode_wrapper homepage_testimonial">
					' . $titleEl . '
					<div class="homepage_testimonial_outer">
						<div class="homepage_testimonial_inner">
							<div class="container">
								<ul class="slides">
									' . $con . '
								</ul>
							</div>
						</div>
					</div>
				</div>';
		}
		
		function testimonial( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"who" 		=> ""
			), $atts ) );
			
			if ( !empty( $who ) ) {
				$who = 
					'<div class="testimonial_who">~ ' . $who . '</div>';	
			}
			
			return 
				'<li>"' . $content . '"' . $who . '</li>';
		}
	
	/* SHORTCODE - Homepage FAQ */
		add_shortcode( 'accordions', 'accordions' );	
		add_shortcode( 'accordion', 'accordion' );	
		
		function accordions( $atts, $content = null ) 
		{
			static $staticid = 0; $staticid++;
			global $shortcodeid; $shortcodeid = $staticid;
			
			extract( shortcode_atts( array(  
				"title" 		=> "",
			), $atts ) );
			
			$con = do_shortcode( $content );
			
			if ( !empty( $title ) ) {
				$titleEl = 
					'<div class="accordions_title">' . $title . '</div>';	
			}
			
			return 
				'<div class="container">
					<div class="shortcode_wrapper accordions">
						' . $titleEl . '
						<div class="panel-group" id="accordions-' . $shortcodeid . '" role="tablist" aria-multiselectable="true">					
							' . $con . '
						</div>
					</div>
				</div>';
		}
		
		function accordion( $atts, $content = null ) 
		{
			static $staticid = 0; $staticid++;
			global $shortcodeid;
			
			extract( shortcode_atts( array(  
				"active"		=> null,
				"question" 		=> ""
			), $atts ) );
			
			if ( empty( $question ) ) {
				return;
			}
			
			if ( $active == "active" ) {
				$activeEl = 'in';
				$activeHeading = 'active';
			}
			else {
				$activeEl = '';
				$activeHeading = '';
			}
			
			return 
				'<div class="panel panel-default">
					<div class="panel-heading ' . $activeHeading . '" role="tab" id="accordion-heading-' . $shortcodeid . '-' . $staticid . '">
						<div class="tdicon-switchplusmin" data-toggle="collapse" data-parent="#accordions-'.$shortcodeid.'" href="#accordion-' . $shortcodeid . '-' . $staticid . '"></div>
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordions-'.$shortcodeid.'" href="#accordion-' . $shortcodeid . '-' . $staticid . '" aria-expanded="true" aria-controls="accordion-' . $shortcodeid . '-' . $staticid . '">
								' . $question . '
							</a>
						</h4>
					</div>
					<div id="accordion-' . $shortcodeid . '-' . $staticid . '" class="panel-collapse collapse ' . $activeEl . '" role="tabpanel" aria-labelledby="accordion-heading-' . $shortcodeid . '-' . $staticid . '">
						<div class="panel-body">
							' . $content . '
						</div>
					</div>
				</div>';
		}
		
	/* SHORTCODE - Featured */
		add_shortcode( 'featured', 'featured' );	
		
		function featured( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
				"image"			=> ""
			), $atts ) );
			
			$con = do_shortcode( $content );
			
			if ( empty( $image ) ) {
				return;
			}
			
			if ( !empty( $title ) ) {
				$titleEl = 
					'<div class="featured_title">' . $title . '</div>';	
			}
			
			return 
				'<div class="container">
					<div class="shortcode_wrapper featured">
						' . $titleEl . '
						<img src="' . $image . '" />
					</div>
				</div>';
		}

	/* SHORTCODE - VIDEOS */
		add_shortcode( 'video', 'video' );	
		
		function video( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"vimeo" 		=> "7 Day Free Trial on All Plans",
				"youtube" 		=> "The amazing 3 step process that allows you to prep a week's worth of mouth watering dinner in 60 minutes or less!"
			), $atts ) );
			
			$videoEl = '';
			
			if ( !empty( $vimeo ) ) {
				$vimeoID = $vimeo;//substr(parse_url($vimeo, PHP_URL_PATH), 1);
				
				//$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/" . $vimeoID . ".php"));
				$hash = json_decode( file_get_contents_curl('http://vimeo.com/api/oembed.json?url=http://vimeo.com/' . $vimeoID), true );
				
				if ( $hash ) {
					$vimeoThumb = $hash['thumbnail_url'];  
					$vimeoTitle = $hash['title'];
					
					$videoEl = 
						'<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
							<div style="background-image:url(' . $vimeoThumb . ');"><div class="vimeo_title">' . $vimeoTitle . '</div><div class="vimeo_play" vimeoid="' . $vimeoID . '">							</div>
						</div>';
				}
			}
			else if ( !empty ( $youtube ) ) {
				//if ( preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtube, $match ) ) {
					$youtubeID = $youtube;//$match[1];				
					
					$youtubeTitle = '';
					$youtubeThumb = 'http://img.youtube.com/vi/' . $youtubeID . '/hqdefault.jpg';
					
					$videoEl =
						'<div class="homepage_introduction_video" style="background-image:url(' . $youtubeThumb . ');"><div class="youtube_title">' . $youtubeTitle . '</div><div class="youtube_play" youtubeid="' . $youtubeID . '"></div></div>';					
					
				//}
			}
			
			return 
				'<div class="shortcode_wrapper homepage_introduction row">
					<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
						<div class="homepage_introduction_title">
							' . $title . '
						</div>
						<div class="homepage_introduction_content">
							' . $content_text . '
						</div>
						<a href="' . $button_link . '" class="homepage_introduction_button">
							<div class="homepage_introduction_button_text">
								' . $button_text . '
							</div>
						</a>
						<div class="homepage_introduction_slogan">
							' . $slogan . '
						</div>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
							<div style="background-image:url(' . $vimeoThumb . ');"><div class="vimeo_title">' . $vimeoTitle . '</div><div class="vimeo_play" vimeoid="' . $vimeoID . '">							</div>
						</div>
					</div>
				</div>';
		}
		
	/* SHORTCODE - HOWTO */
		add_shortcode( 'howto', 'howto' );	
		
		function howto( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
				"vimeo"			=> null,
				"youtube"		=> null
			), $atts ) );
			
			$videoEl = '';
			
			if ( !empty( $vimeo ) ) {
				$vimeoID = $vimeo;
				
				$hash = json_decode( file_get_contents_curl('http://vimeo.com/api/oembed.json?url=http://vimeo.com/' . $vimeoID), true );
				
				if ( $hash ) {
					$vimeoThumb = $hash['thumbnail_url'];  
					$vimeoTitle = $hash['title'];
					
					$pinterestLink = '<a href="http://pinterest.com/pin/create/button/?url=' . urlencode(get_permalink()) . '&amp;description=' . urlencode(get_the_title()) . '&amp;media=' . urlencode($vimeoThumb). '" target="_blank" class="pinterest-overlay" title="Pinterest"><span class="tdicon-pinterest-circle" title="Pinterest"></span></a>';
					
					$videoEl = 
						'<div class="homepage_introduction_video" style="background-image:url(' . $vimeoThumb . ');"><div class="video_highlight_overlay vimeo_overlay" vimeoid="' . $vimeoID . '" vidheight="350">' . $pinterestLink . '<div class="tdicon-play-circle video_play video_play_vimeo"></div></div></div>';
				}
			}
			else if ( !empty ( $youtube ) ) {
				
					$youtubeID = $youtube;
					$youtubeThumb = 'http://img.youtube.com/vi/' . $youtubeID . '/hqdefault.jpg';
					
					$pinterestLink = '<a href="http://pinterest.com/pin/create/button/?url=' . urlencode(get_permalink()) . '&amp;description=' . urlencode(get_the_title()) . '&amp;media=' . urlencode($youtubeThumb). '" target="_blank" class="pinterest-overlay" title="Pinterest"><span class="tdicon-pinterest-circle" title="Pinterest"></span></a>';
					
					$videoEl =
						'<div class="homepage_introduction_video" style="background-image:url(' . $youtubeThumb . ');"><div class="video_highlight_overlay youtube_overlay" youtubeid="' . $youtubeID . '" vidheight="350">' . $pinterestLink . '<div class="tdicon-play-circle video_play video_play_youtube"></div></div></div>';
		
			}
			
			return 
				'<div class="shortcode_wrapper howto row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 howto-video">
						<div class="homepage_introduction_video" style="background-image:url(' . $vimeoThumb . ');"><div class="video_highlight_overlay vimeo_overlay" vimeoid="' . $vimeoID . '" vidheight="350">' . $pinterestLink . '<div class="tdicon-play-circle video_play video_play_vimeo"></div></div></div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 howto-content">						
						<div class="howto-title">
							' . $title . '
						</div>
						' . do_shortcode( $content ) . '
					</div>					
				</div>';
		}
		
	/* SHORTCODE - Subscribe */
		add_shortcode( 'subscribes', 'subscribes' );	
		add_shortcode( 'subscribe', 'subscribe' );	
		
		function subscribes( $atts, $content = null ) 
		{
			$con = do_shortcode( $content );
			
			return 
				'<div class="shortcode_wrapper subscribes clearfix">					
					<div class="row">
						' . $con . '	
					</div>				
				</div>';
		}
		
		function subscribe( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
				"subtitle"		=> "",
				"mostpopular"	=> "",
				"price"			=> "",
				"note"			=> "",
				"link"			=> "",
				"numcol"		=> "3",
				"color"			=> "#97bd41",
				"button"		=> "SIGN UP"
			), $atts ) );
			
			if ( empty( $title ) ) {
				return;
			}
			
			if ( $mostpopular == "yes" ) {
				$mostpopular_el = 
					'<div class="subscribe_mostpopular_wrapper">						
						<div class="subscribe_mostpopular">
						</div>
					</div>';
			}
			else {
				$mostpopular_el = '';
			}	
			
			return 
				'<div class="col-lg-'.$numcol.' col-md-6 col-sm-6 col-xs-12 subscribe_wrapper">
					<div class="subscribe_inner" style="border:3px solid ' . $color . ';">
						' . $mostpopular_el . '
						<div class="subscribe_subtitle">
							- ' . $subtitle . ' -
						</div>
						<div class="subscribe_title" style="color:' . $color .';">
							' . $title . '
						</div>
						<div class="subscribe_price">
							<span class="subscribe_nominal">$ ' . $price . '</span>
						</div>
						<div class="subscribe_note">
							' . $note . '
						</div>
						<div class="subscribe_content">
							' . do_shortcode( $content ) . '
						</div>
						<a href="' . $link . '" class="subscribe_select_link">
							<div class="subscribe_select" style="background-color:' . $color . ';">
								' . $button . '						
							</div>
						</a>
					</div>
				</div>';
		}
		
	/* SHORTCODE - CENTERED NOTE */
	
		add_shortcode( 'centered_note', 'centered_note' );	
		
		function centered_note( $atts, $content = null ) 
		{
			return 
				'<div class="shortcode_wrapper centered_note">										
					' . do_shortcode( $content ) . '						
				</div>';
		}
		
	/* SHORTCODE - Slow Cooker */	
		add_shortcode( 'whitespace', 'whitespace' );		
		
		function whitespace( $atts, $content = null ) 
		{
			$con = do_shortcode( $content );
			
			return 
				'<div class="whitespace clearfix">					
					<div class="row">
						' . $con . '	
					</div>				
				</div>';
		}		
		
		add_shortcode( 'coolbreak', 'coolbreak' );		
		
		function coolbreak( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"height" 		=> "10px",
			), $atts ) );
			
			return 
				'<div style="height:'.$height.'">								
				</div>';
		}	
		
	/* SHORTCODE - Host Introduction */
		add_shortcode( 'host_introduction', 'host_introduction' );
		
		function host_introduction( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> ""
			), $atts ) );
			
			return 
				'<div class="host-wrapper transparent-wrapper clearfix">
					<div class="container">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 host-desc-wrapper">
								<div class="section-title">
									' . $title . '
								</div>
								' . do_shortcode( $content ) . '
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 host-image-wrapper">
								<img src="' . get_bloginfo( 'template_url' ) . '/img/host.jpg" alt="" />
							</div>
						</div>
					</div>
				</div>';
		}					

	/* SHORTCODE - Host Expectation */
		add_shortcode( 'host_expectation', 'host_expectation' );
		
		function host_expectation( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> ""
			), $atts ) );
			
			return 
				'<div class="hostexpect-white">
					<div class="container">
						<div class="joinus_title">
							' . $title . '
						</div>
						<div class="row">
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
								' . do_shortcode( $content ) . '
							</div>
						</div>
					</div>
				</div>';
		}	
		
	/* SHORTCODE - Host Reward Wrapper */
		add_shortcode( 'host_reward_wrapper', 'host_reward_wrapper' );
		
		function host_reward_wrapper( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title" 		=> "",
				"description"	=> "",
				"button_text"	=> "",
				"button_link"	=> ""
			), $atts ) );
			
			return 
				'<div class="hostbonus-wrapper transparent-wrapper">   
					<div class="container">
						<div class="joinus_title">
							' . $title . '
						</div>
						<div class="row">
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
								' . $description . ' 
								<div class="rewardtable">
									<div class="rewardtable-row">
										<div class="rewardtable-title">
											Amount of Sales
										</div>
										<div class="rewardtable-title-description">
											Reward
										</div>
									</div>
									
									' . do_shortcode( $content ) . '
								</div>
								
								<div class="orange-button-fullwidth">
									<a class="orange-button " href="' . $button_link . '">
										' . $button_text . '
									</a>
								</div>
			
							</div>
						</div>
					</div>
				</div>';
		}	
		
	/* SHORTCODE - Host Reward */
		add_shortcode( 'host_reward', 'host_reward' );
		
		function host_reward( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"sales" 	=> ""
			), $atts ) );
			
			return 
				'<div class="rewardtable-row">
					<div class="rewardtable-reward">
						<div class="rewardtable-amount">
							' . $sales . '
						</div>
					</div>
					<div class="rewardtable-description">
						<div class="rewardtable-description-inner">
							' . do_shortcode( $content ) . '
						</div>
					</div>
				</div>';
		}	
		
	/* SHORTCODE - Join Us Introduction */
		add_shortcode( 'joinus_introduction', 'joinus_introduction' );
		
		function joinus_introduction( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"button_text"	=> "",
				"button_link"	=> ""
			), $atts ) );
			
			return 
				'<div class="container">        
					<div class="joinus-transparent-wrapper">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 joinus-desc-wrapper">
								' . do_shortcode( $content ) . '
								<div class="orange-button-fullwidth-small">
									<a href="' . $button_link . '" class="orange-button ">
										' . $button_text . '
									</a>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 joinus-image-wrapper">
								<img src="' . get_bloginfo( 'template_url' ) . '/img/joinus1.jpg" alt="Join Our Mission" />
							</div>
						</div>
					</div>
				</div>';
		}
		
	/* SHORTCODE - Why Join */
		add_shortcode( 'why_join', 'why_join' );
		
		function why_join( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title"	=> ""
			), $atts ) );
			
			return 
				'<div class="joinus-white">
					<div class="container">
						<div class="joinus_title">
							' . $title . '
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 joinus-white-image-wrapper">
								<img src="' . get_bloginfo( 'template_url' ) . '/img/joinus2.jpg" alt="Join Our Mission" />
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								' . do_shortcode( $content ) . '
							</div>
						</div>
					</div>    	
				</div>';
		}
		
	/* SHORTCODE - Get Started Price */
		add_shortcode( 'ready', 'ready' );
		
		function ready( $atts, $content = null ) 
		{
			extract( shortcode_atts( array(  
				"title"			=> "",
				"subtitle"		=> "",
				"price"			=> "",
				"button_text"	=> "",
				"button_link"	=> ""
			), $atts ) );
			
			return 
				'<div class="joinus-finale">
					<div class="container">
						<div class="joinus-triangle">
						</div>
						<div class="joinus_title">
							' . $title . '
						</div>
						<div class="joinus-content">
							<div class="join-wrapper">
								<div class="topbox">
									<div class="bottombox">
										<div class="joinus_title">
											' . $subtitle . '
										</div>
										<div class="joinus-foronly">
											for only
										</div>
										<div class="joinus-price">
											<span class="joinus-currency">$</span>
											<span>' . $price . '</span>
										</div>
										<div class="joinus-button-wrapper">
											<div class="orange-button-small">
												<a href="' . $button_link . '" class="orange-button">
													' . $button_text . '
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>';
		}
	
/* --------------------------------------------------------------------------------------------------------------------- */	
	
	// Comment Format	
	if ( ! function_exists( 'comment_format' ) ) 
	{	 
		function comment_format( $comment, $args, $depth )
		{
			$GLOBALS['comment'] = $comment;
			
			$comment_type = $comment->comment_type;
			
			// Pingback
			if ( $comment_type == 'pingback' ) 
			{
				// Hide pingback	
			}			
			// Trackback
			else if ( $comment_type == 'trackback' ) 
			{
				?>
                    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                    	<article>
                        	<div class="comment-avatar trackback">                            	
                            	<div class="glyphicon glyphicon-link"></div>
                            </div>
                        	<section>
                            	<div class="comment-body">
                                	<div class="comment-meta">
                                        <div class="comment-author">
                                        	<div class="comment-trackback-highlight">
                                        		<?php _e( 'Trackback', 'jellypixel' ); ?>
                                            </div>
                                        </div>
                                        <div class="comment-meta-bottom clearfix">
                                            <div class="comment-edit">
                                                <?php 
                                                    edit_comment_link( __( 'Edit', 'jellypixel' ) );
                                                ?>
                                            </div>
                                        </div>
                                        <div class="clear">
                                    </div>
                                    <div class="comment-content">
                                    	<p>                                        	
											<?php
                                                comment_author_link();
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                <?php
			}
			// Comment
			else
			{				
				global $post;					
				?>
					
                <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                    <article>
                        <header>
                        	<div class="comment-avatar">
                            	<?php 
									echo get_avatar( $comment, 60 ); 
								?>
                            </div>
                        </header>                         
                        <section>
                            <div class="comment-body">
                            	<div class="comment-meta">
                                	<div class="comment-author">
                                    	<?php
											// Post Author											
											if ( $comment->user_id === $post->post_author ) 
											{
												echo '<div class="comment-author-name">' . get_comment_author_link() . '</div><div class="comment-author-highlight">' . __( 'Author', 'jellypixel' ) . '</div>';
												
											}
											// Others											
											else 
											{
												echo '<div class="comment-author-name">' . get_comment_author_link() . '</div>';
											}
										?>
                                    </div>
                                    <div class="comment-meta-bottom clearfix">
                                        <div class="comment-edit">
                                            <?php
                                                edit_comment_link(__('Edit', 'jellypixel'));
                                            ?>
                                        </div>                                        
										<?php
                                            if ( NULL != comment_reply_link(array_merge( $args, array('reply_text' => __('Reply', 'jellypixel'), 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) )
                                            {
                                                echo '<div class="comment-reply">';
                                                
												comment_reply_link(array_merge( $args, array('reply_text' => __('Reply', 'jellypixel'), 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth'])));
                                                
                                                echo '</div>';
                                            }
                                        ?>
                                    </div>                                    
                                    <div class="clear"></div>
                                </div>
                                <div class="comment-date">
									<?php											
                                        printf( __( '%1$s at %2$s', 'jellypixel'), get_comment_date(), get_comment_time() );
                                    ?>
                                </div>
                                <div class="comment-content">
                                	<?php
										if ( $comment->comment_approved == '0') :
											?>
												<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'jellypixel' ); ?></p>
											<?php
										endif;
									?>
								
									<?php									
										comment_text();
									?>
                                </div>                                
                            </div>
                        </section>                        
                    </article>
                    <div class="clear"></div>
                <?php
			}
		}
	}
	
/* --------------------------------------------------------------------------------------------------------------------- */	
	
	// Numbered Pagination
	if( !function_exists( 'numbered_pagination' ) )
	{
		function numbered_pagination() 
		{
			// Vars
			global $wp_query, $paged;
			$countPage = $wp_query->max_num_pages;
			// Check
			if ( empty( $paged ) == true )
			{
				$paged = 1;	
			}			
			if ( empty( $countPage ) == true )
			{
				$countPage = 1;	
			}
			// Show
			if ( $countPage > 1 )
			{
				// First / Prev
				if ( $paged > 1 )
				{
					echo '<a href="' . get_pagenum_link( 1 ) . '"><div class="nav-numbered np-first">' . __( 'First', 'jellypixel' ) . '</div></a>';
					echo '<a href="' . get_pagenum_link( $paged - 1 ) . '"><div class="nav-numbered np-prev">' . __( 'Prev', 'jellypixel' ) . '</div></a>';				
				}
				// Numbers		
				$min = $paged-4; $max = $paged + 4;		
				
				if($min < 1)
					$min = 1;

				if($max > $countPage)
					$max = $countPage;					
						
				for ( $i = $min; $i <= $max; $i++ ) 
				{
					if ( $paged == $i )
					{	// Current Page						
						echo '<a href="' . get_pagenum_link( $i ) . '"><span class="nav-numbered np-current">' . $i . '</span></a>';
					}
					else
					{	// Other Pages						
						echo '<a href="' . get_pagenum_link( $i ) . '"><span class="nav-numbered">' . $i . '</span></a>';
					}
				}
				// Last / Next
				if ( $paged < $countPage )
				{
					echo '<a href="' . get_pagenum_link( $paged + 1 ) . '"><span class="nav-numbered np-next">' . __( 'Next', 'jellypixel' ) . '</span></a>';
					echo '<a href="' . get_pagenum_link( $countPage ) . '"><span class="nav-numbered np-last">' . __( 'Last', 'jellypixel' ) . '</span></a>';
				}
			}
		}
	}
	
/* --------------------------------------------------------------------------------------------------------------------- */	

	// Breadcrumb
	if ( !function_exists( 'show_breadcrumb' ) )
	{
		function show_breadcrumb()
		{
			global $post;
			
			$showHome = 0; 
			$home = __( 'Home', 'jellypixel' );
			$homeURL = get_bloginfo('url');
			$separator = '&#47;'; 			 
			$showCurrent = 1; 
			$beforeTag = '<span class="currentpage">'; 
			$afterTag = '</span>'; 						
			
			if ( is_home() || is_front_page() ) 
			{			
				if ($showHome == 1) 
				{
					echo '<div id="path"><a href="' . $homeURL . '">' . $home . '</a></div>';				
				}
			} 
			else
			{				
				echo '<div id="path"><a href="' . $homeURL . '">' . $home . '</a> ' . $separator . ' ';
				
				if ( is_category() ) {
					$thisCat = get_category( get_query_var( 'cat' ), false );
					if ( $thisCat->parent != 0 ) 
					{
						echo get_category_parents( $thisCat->parent, TRUE, ' ' . $separator . ' ' );
					}
					echo $beforeTag . 'Archive by category "' . single_cat_title('', false) . '"' . $afterTag;				
				} 
				elseif ( is_search() ) 
				{
					//echo $beforeTag . 'Search results for "' . get_search_query() . '"' . $afterTag;				
					echo $beforeTag . 'Search' . $afterTag;				
				} 
				elseif ( is_day() ) 
				{
					echo '<a href="' . get_year_link( get_the_time( 'Y' ) ) . '">' . get_the_time( 'Y' ) . '</a> ' . $separator . ' ';
					echo '<a href="' . get_month_link( get_the_time( 'Y' ),get_the_time( 'm' ) ) . '">' . get_the_time( 'F' ) . '</a> ' . $separator . ' ';
					echo $beforeTag . get_the_time( 'd' ) . $afterTag;
				} 
				elseif ( is_month() ) 
				{
					echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>  ' . $separator . ' ';
					echo $beforeTag . get_the_time('F') . $afterTag;				
				} 
				elseif ( is_year() ) 
				{
					echo $beforeTag . get_the_time('Y') . $afterTag;				
				} 
				elseif ( is_single() && !is_attachment() ) 
				{
					if ( get_post_type() != 'post' ) 
					{
						$post_type = get_post_type_object( get_post_type() );
						$slug = $post_type->rewrite;
						//echo '<a href="' . $homeURL . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
						echo $post_type->labels->singular_name;
						if ($showCurrent == 1) 
						{
							echo ' ' . $separator . ' ' . $beforeTag . get_the_title() . $afterTag;
						}
					} 
					else 
					{
						$cat = get_the_category(); $cat = $cat[0];
						$cats = get_category_parents($cat, TRUE, ' ' . $separator . ' ');
						if ($showCurrent == 0) 
						{
							$cats = preg_replace("#^(.+)\s$separator\s$#", "$1", $cats);
						}
						echo $cats;
						if ($showCurrent == 1) 
						{	
							echo $beforeTag . get_the_title() . $afterTag;
						}
					}				
				} 
				elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) 
				{
					$post_type = get_post_type_object( get_post_type() );
					echo $beforeTag . $post_type->labels->singular_name . $afterTag;				
				} 
				elseif ( is_attachment() ) 
				{
					// Attachment
					$parent = get_post( $post->post_parent );
					$cat = get_the_category( $parent->ID ); $cat = $cat[0];
					echo is_wp_error( $cat_parents = get_category_parents($cat, TRUE, '' . $separator . '') ) ? '' : $cat_parents;
					
					echo '<a href="' . get_permalink( $parent ) . '">' . $parent->post_title . '</a>';
					if ( $showCurrent == 1 ) 
					{
						echo ' ' . $separator . ' ' . $beforeTag . get_the_title() . $afterTag;
					}				
				} 
				elseif ( is_page() && !$post->post_parent ) 
				{
					echo $beforeTag . get_the_title() . $afterTag;
				} 
				elseif ( is_page() && $post->post_parent ) 
				{
					$parent_id  = $post->post_parent;
					$breadcrumbs = array();
					while ( $parent_id ) {
						$page = get_page( $parent_id );
						$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
						$parent_id  = $page->post_parent;
					}
					$breadcrumbs = array_reverse( $breadcrumbs );
					for ( $i = 0; $i < count( $breadcrumbs ); $i++ ) 
					{
						echo $breadcrumbs[$i];
						if ($i != count($breadcrumbs)-1) 
						{
							echo ' ' . $separator . ' ';
						}
					}
					if ($showCurrent == 1) 
					{
						echo ' ' . $separator . ' ' . $beforeTag . get_the_title() . $afterTag;
					}				
				} 
				elseif ( is_tag() ) 
				{
					// Tag
					echo $beforeTag . 'Posts tagged "' . single_tag_title( '', false ) . '"' . $afterTag;				
				} 
				elseif ( is_author() ) 
				{
					// Author
					global $author;
					$userdata = get_userdata( $author );
					echo $beforeTag . 'Articles posted by ' . $userdata->display_name . $afterTag;				
				} 
				elseif ( is_404() ) 
				{
					// Page Not Found
					echo $beforeTag . 'Error 404' . $afterTag;
				}
				
				if ( get_query_var('paged') ) 
				{
					if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) 
					{
						echo ' (';
					}
					echo __( 'Page' ) . ' ' . get_query_var( 'paged' );
					if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) 
					{
						echo ')';
					}
				}
								
				echo '</div>';
			}
		}
	}
	
/* --------------------------------------------------------------------------------------------------------------------- */		

	// Check empty
	function get_opt( $option, $default )
	{
		if ( $default === NULL )
		{
			$default = '';	
		}
		
		if ( $option === NULL )
		{
			$value = $default;	
		}
		else 
		{
			$value = $option;	
		}
		
		return $value;
	}	
	

	function convertToBoolean( $string )
	{
		if ( $string == '0' )
		{
			return "false";
		}
		else if ( $string == '1' )
		{
			return "true";
		} 
		else
		{
			return $string;
		}
	}
	
	// Shortcodes Clean Up
	function parse_shortcode_content( $content ) { 
	 
		/* Parse nested shortcodes and add formatting. */ 
		$content = trim( wpautop( do_shortcode( $content ) ) ); 
	 
		/* Remove '</p>' from the start of the string. */ 
		if ( substr( $content, 0, 4 ) == '</p>' ) 
			$content = substr( $content, 4 ); 
	 
		/* Remove '<p>' from the end of the string. */ 
		if ( substr( $content, -3, 3 ) == '<p>' ) 
			$content = substr( $content, 0, -3 ); 
	 
		/* Remove any instances of '<p></p>'. */ 
		$content = str_replace( array( '<p></p>' ), '', $content ); 
	 
		return $content; 
	} 	
	
	// More Shortcode Cleanup
	function shortcode_empty_paragraph_fix( $content ) {

        $array = array (
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']'
        );

        $content = strtr( $content, $array );

        return $content;
    }

    add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );
	
/* --------------------------------------------------------------------------------------------------------------------- */
	
	// Custom Get Comments_popup_link()	
	function get_comments_popup_link( $zero = false, $one = false, $more = false, $css_class = '', $none = false ) {
		global $wpcommentspopupfile, $wpcommentsjavascript;
	 
		$id = get_the_ID();
	 
		if ( false === $zero ) $zero = __( 'No Comments' );
		if ( false === $one ) $one = __( '1 Comment' );
		if ( false === $more ) $more = __( '% Comments' );
		if ( false === $none ) $none = __( 'Comments Off' );
	 
		$number = get_comments_number( $id );
	 
		$getcommentpopup = '';
	 
		if ( 0 == $number && !comments_open() && !pings_open() ) {
			$getcommentpopup = '<span' . ((!empty($css_class)) ? ' class="' . esc_attr( $css_class ) . '"' : '') . '>' . $none . '</span>';
			return $getcommentpopup;
		}
	 
		if ( post_password_required() ) {
			$getcommentpopup = __('Enter your password to view comments.');
			return $getcommentpopup;
		}
	 
		$getcommentpopup = '<a href="';
		if ( $wpcommentsjavascript ) 
		{
			if ( empty( $wpcommentspopupfile ) )
				$home = home_url();
			else
				$home = get_option('siteurl');
			$getcommentpopup .= $home . '/' . $wpcommentspopupfile . '?comments_popup=' . $id;
			$getcommentpopup .= '" onclick="wpopen(this.href); return false"';
		} else 
		{ // if comments_popup_script() is not in the template, display simple comment link
			if ( 0 == $number )
				$getcommentpopup .= get_permalink() . '#respond';
			else
				$getcommentpopup .= get_comments_link();
			$getcommentpopup .= '"';
		}
	 
		if ( !empty( $css_class ) ) {
			$getcommentpopup .= ' class="'.$css_class.'" ';
		}
		
		$title = the_title_attribute( array('echo' => 0 ) );
	 
		$getcommentpopup .= apply_filters( 'comments_popup_link_attributes', '' );	 
		$getcommentpopup .= ' title="' . esc_attr( sprintf( __('Comment on %s'), $title ) ) . '">';
		$getcommentpopup .= custom_get_comments_number( $zero, $one, $more );
		$getcommentpopup .= '</a>';
		 
		return $getcommentpopup;
	}
	 
	// Custom Get comments_number
	function custom_get_comments_number( $zero = false, $one = false, $more = false, $deprecated = '' ) {
		if ( !empty( $deprecated ) )
			_deprecated_argument( __FUNCTION__, '1.3' );
	 
		$number = get_comments_number();
	 
		if ( $number > 1 )
			$output = str_replace('%', number_format_i18n($number), ( false === $more ) ? __('% Comments') : $more);
		elseif ( $number == 0 )
			$output = ( false === $zero ) ? __('No Comments') : $zero;
		else // must be one
			$output = ( false === $one ) ? __('1 Comment') : $one;
	 
		return apply_filters('comments_number', $output, $number);
	}	
	
	// Remove conflict notice with Nav_Menu_Roles plugin	
	add_action( 'admin_init', 'nmr_remove_notice' );
	function nmr_remove_notice(){
		remove_action( 'admin_notices', array( Nav_Menu_Roles(), 'admin_notice' ) );
	}
	
	// Get Blog Page
	if ( ! function_exists( 'get_blogpage' ) ) {
		function get_blogpage( $info ) {
			if( get_option('show_on_front') == 'page') {
				$posts_page_id = get_option( 'page_for_posts');
				$posts_page = get_page( $posts_page_id);
				$posts_page_title = $posts_page->post_title;
				$posts_page_url = get_page_uri($posts_page_id  );
			}
			else $posts_page_title = $posts_page_url = '';
		
			if ($info == 'url') {
				return get_bloginfo('url') . '/' . $posts_page_url;
			} elseif ($info == 'title') {
				return $posts_page_title;
			} else {
				return false;
			}
		}
	}
	
	// Change Excerpt Style
	function excerpt_style( $more ) {
		return '...';
	}
	add_filter('excerpt_more', 'excerpt_style');
	
	// Register Custom Post Type Video
	add_action('init', 'register_cpt_video');
	
	function register_cpt_video() {
		register_post_type(
			'video',
			array(
				'labels' => array(
					'name' 			=> __( 'Videos', 'jellypixel' ),
					'singular_name' => __( 'Video', 'jellypixel' ),
					'menu_name' 	=> __( 'Video', 'jellypixel' ),					
					'add_new'		=> __( 'Add New Video', 'jellypixel' ),
					'add_new_item'	=> __( 'Add New Video', 'jellypixel' ),
					'edit_item'		=> __( 'Edit Video', 'jellypixel' ),					
					'search_items'	=> __( 'Search Videos', 'jellypixel' ),
					'not_found'		=> __( 'No video found', 'jellypixel' ),
					'not_found_in_trash' => __( 'No video found in trash', 'jellypixel' ),					
				),
				'public' => true,
				'has_archive' => '404',
				'rewrite' => array( 
					'slug' => 'video_list',
					'with_front' => false
				),
				'supports' => array( 
					'title'
				),
				'can_export' => true,
			)
		);
		//flush_rewrite_rules();
					
		// Add TAXONOMY, Skill/Tags/Etc	
		register_taxonomy(
			'video_category',
			'video', 
			array(
				'labels' => array(
					'name' 			=> __( 'Video Categories', 'jellypixel' ),
					'singular_name' => __( 'Video Category', 'jellypixel' ),
					'menu_name'		=> __( 'Categories', 'jellypixel' ),
					'add_new'		=> __( 'Add New Category', 'jellypixel' ),
					'add_new_item'	=> __( 'Add New Category', 'jellypixel' ),
					'edit_item'		=> __( 'Edit Category', 'jellypixel' ),
					'search_items'	=> __( 'Search Categories', 'jellypixel' ),
					'not_found'		=> __( 'No category found', 'jellypixel' ),
					'not_found_in_trash' => __( 'No category found in Trash', 'jellypixel' )
				),
				'hierarchical' => true, 
				'query_var' => true, 
				'rewrite' => true
			)
		);		
	}	
	
	// Register Custom Post Type Testimonial
	add_action('init', 'register_cpt_testimonial');
	
	function register_cpt_testimonial() {
		register_post_type(
			'testimonial',
			array(
				'labels' => array(
					'name' 			=> __( 'Testimonials', 'jellypixel' ),
					'singular_name' => __( 'Testimonial', 'jellypixel' ),
					'menu_name' 	=> __( 'Testimonial', 'jellypixel' ),					
					'add_new'		=> __( 'Add New Testimonial', 'jellypixel' ),
					'add_new_item'	=> __( 'Add New Testimonial', 'jellypixel' ),
					'edit_item'		=> __( 'Edit Testimonial', 'jellypixel' ),					
					'search_items'	=> __( 'Search Testimonials', 'jellypixel' ),
					'not_found'		=> __( 'No testimonial found', 'jellypixel' ),
					'not_found_in_trash' => __( 'No testimonial found in trash', 'jellypixel' ),					
				),
				'public' => true,
				'has_archive' => '404',
				'rewrite' => array( 
					'slug' => 'testimonial_list',
					'with_front' => false
				),
				'supports' => array( 
					'title',
					'editor', 
					'excerpt', 
					'author', 
					'thumbnail', 
					'comments', 
					'revisions'
				),
				'menu_icon' => 'dashicons-format-chat',
				'can_export' => true,
			)
		);
	}	
	
	// Register Custom Post Type Blog
	add_action('init', 'register_cpt_blog');
	
	function register_cpt_blog() {
		register_post_type(
			'blog',
			array(
				'labels' => array(
					'name' 			=> __( 'Blogs', 'jellypixel' ),
					'singular_name' => __( 'Blog', 'jellypixel' ),
					'menu_name' 	=> __( 'Blog', 'jellypixel' ),					
					'add_new'		=> __( 'Add New Blog', 'jellypixel' ),
					'add_new_item'	=> __( 'Add New Blog', 'jellypixel' ),
					'edit_item'		=> __( 'Edit Blog', 'jellypixel' ),					
					'search_items'	=> __( 'Search Blogs', 'jellypixel' ),
					'not_found'		=> __( 'No blog found', 'jellypixel' ),
					'not_found_in_trash' => __( 'No blog found in trash', 'jellypixel' ),					
				),
				'public' => true,
				'has_archive' => '404',
				'rewrite' => array( 
					'slug' => 'blog',
					'with_front' => false
				),
				'supports' => array( 
					'title',
					'editor', 
					'excerpt', 
					'author', 
					'thumbnail', 
					'comments', 
					'revisions'
				),
				'can_export' => true,
				'menu_icon' => 'dashicons-format-status',
			)
		);
		//flush_rewrite_rules();
					
		// Add TAXONOMY, Skill/Tags/Etc	
		register_taxonomy(
			'blog_category',
			'blog', 
			array(
				'labels' => array(
					'name' 			=> __( 'Blog Categories', 'jellypixel' ),
					'singular_name' => __( 'Blog Category', 'jellypixel' ),
					'menu_name'		=> __( 'Categories', 'jellypixel' ),
					'add_new'		=> __( 'Add New Category', 'jellypixel' ),
					'add_new_item'	=> __( 'Add New Category', 'jellypixel' ),
					'edit_item'		=> __( 'Edit Category', 'jellypixel' ),
					'search_items'	=> __( 'Search Categories', 'jellypixel' ),
					'not_found'		=> __( 'No category found', 'jellypixel' ),
					'not_found_in_trash' => __( 'No category found in Trash', 'jellypixel' )
				),
				'hierarchical' => true, 
				'query_var' => true, 
				'rewrite' => true
			)
		);		
	}	
	
	// Post Meta Video
	add_action( 'add_meta_boxes', 'add_meta_video_options' );
	add_action( 'save_post', 'save_meta_video_options' );
	
	function add_meta_video_options()
	{
		add_meta_box( 'meta_video_options', __( 'Video', 'jellypixel' ), 'show_meta_video_options', 'video', 'normal', 'high' );
	}
		
	function show_meta_video_options( $post )
	{	
		wp_nonce_field( 'show_meta_video_options', 'nonce_show_meta_video_options' ); 
		
		// Get existing variables
		$videoMeta  = get_post_custom($post->ID);		
		$currVideoLink = isset( $videoMeta['_video_post_embed_url'] ) ? esc_attr( $videoMeta['_video_post_embed_url'][0] ) : '';
		
		// Options
		?>          	
            <table class="meta-table">
                <tbody>            	
                	<?php // Subtitle ?>                       
                    <tr valign="top">
                        <th scope="row">
                            <div class="meta-table-header">
                                Video Link
                            </div>
                        </th>
                        <td>
                            <div class="meta-table-fieldset">
                                <?php
                                    echo '<input type="text" id="_video_post_embed_url" name="_video_post_embed_url" value="' . $currVideoLink . '" style="width:100%;" />';		
									echo '<div class="meta-table-description">';
                                        echo 'Type Video URL.';
                                    echo '</div>';
                                ?>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        <?php	
	}
	
	function save_meta_video_options( $post_id )
	{
		if ( ! isset( $_POST['nonce_show_meta_video_options'] ) )
		{
			return $post_id;
		}

		$nonce = $_POST['nonce_show_meta_video_options'];
		
		if ( ! wp_verify_nonce( $nonce, 'show_meta_video_options' ) )
        {
			return $post_id;
		}
		
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
        {
			return $post_id;
		}

		if ( 'page' == $_POST['post_type'] )
		{
			if ( ! current_user_can( 'edit_page', $post_id ) )
			{
				return $post_id;
			}
		}
		else
		{
			if ( ! current_user_can( 'edit_post', $post_id ) )
			{
				return $post_id;
			}
		}

		//wp_kses, esc_attr
		if( isset( $_POST['_video_post_embed_url'] ) )
		{
	        update_post_meta( $post_id, '_video_post_embed_url', $_POST['_video_post_embed_url'] );
		}
	}
		
	function my_skip_mail($f){
		
	    global $post, $wpcf7;
	    
	    if ($wpcf7->id == 2861) {
	        return true; // DO NOT SEND E-MAIL
	    }
	}
	add_filter('wpcf7_skip_mail','my_skip_mail');	

	// Jellypixel Widgets
	require_once( 'lib/widgets/jellypixel-widget-blogcategories/jellypixel_widget_blogcategories.php' );		
	require_once( 'lib/widgets/jellypixel-widget-productcategories/jellypixel_product_categories.php' );		
		
	register_widget( 'jellypixel_widget_blogcategories' );
	register_widget( 'jellypixel_widget_product_categories' );
	
	global $wp_version;
	if( version_compare( $wp_version, '4.4', '<' ) ) {
		if ( ! function_exists( 'is_embed' ) ) {
		    function is_embed() { return false; }
		}
	}
	
	if( version_compare( $wp_version, '4.4', '<' ) ) {
		if ( ! function_exists( 'widget_list' ) ) {
		    function widget_list() { return false; }
		}
	}
	
	function wc_ninja_remove_password_strength() {
		if ( wp_script_is( 'wc-password-strength-meter', 'enqueued' ) ) {
			wp_dequeue_script( 'wc-password-strength-meter' );
		}
	}
	add_action( 'wp_print_scripts', 'wc_ninja_remove_password_strength', 100 );
	
/* --------------------------------------------------------------------------------------------------------------------- */
	
	// Infinite Scroll
	add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate');           // for logged in user
	add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate');    // if user not logged in
	
	function wp_infinitepaginate(){ 
		$loopFile        = $_POST['loop_file'];
		$paged           = $_POST['page_no'];
		$posts_per_page  = $_POST['postperpage'];//get_option('posts_per_page');		
		$type			 = $_POST['type'];
		$cat 			 = $_POST['cat'];
		
		query_posts(array(
			'cat' => $cat,
			'paged' => $paged, 
			'post_type' => 'post', 
			'post_status' => 'publish', 
			'posts_per_page' => $posts_per_page
		)); 
		//get_template_part( $loopFile );
				
		// SUPER CUSTOM RETURN
		while ( have_posts() ) {
			the_post();
			
			// Thumb
			$attachment_image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-thumb' );
			$attachment_image_thumb_url = $attachment_image_thumb[0];												

			// Title
			$getTitle = get_the_title();
			$titlecount = 12;
		
			if ( str_word_count($getTitle, 0) > $titlecount ) {
				$words = str_word_count( $getTitle, 2 );
				$pos = array_keys( $words );
				$resultTitle = substr( $getTitle, 0, $pos[$titlecount] ) . '...';
			}
			else {
				$resultTitle = $getTitle;
			}
			
			// Tag	
			$postMeta = get_post_custom($post->ID);	
			$youtubeUrl = isset( $postMeta['post_youtube'] ) ? esc_attr( $postMeta['post_youtube'][0] ) : '';							
			parse_str( parse_url( $youtubeUrl, PHP_URL_QUERY ), $my_array_of_vars );
			$videoID =  $my_array_of_vars['v'];
			
			if ( $type == 2 )
			{
				echo '<div class="livingentertainingpage-wrapper col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<div class="livingentertainingpage-thumb-wrapper">
							<a href="' . get_the_permalink() . '">
								<div class="livingpage-thumb" style="background-image:url(' . $attachment_image_thumb_url . ');">																	
								</div>
							</a>
							<a href="http://pinterest.com/pin/create/button/?url=' . urlencode(get_permalink()) . '&amp;description=' . urlencode(get_the_title()) . '&amp;media=' . urlencode($attachment_image_thumb_url). '" target="_blank" class="livingentertainingpage-pinterest" title="Pinterest"><span class="glyph glyph-pinterest-circle" title="Pinterest"></span></a>
							<div class="livingentertainingpage-social">
								<a href="http://www.facebook.com/sharer.php?u=' . get_the_permalink() . '&amp;t=' . get_the_title(). '" target="_blank" class="share-social"><span class="glyph glyph-facebook" title="Facebook"></span></a>        
								<a href="http://twitter.com/home?status=' . get_the_title() . ' ' . get_the_permalink() . '" target="_blank" class="share-social"><span class="glyph glyph-twitterbird" title="Twitter"></span></a>		
							</div>						
						</div>						
						<div class="livingpage-title">
							<a href="' . get_the_permalink() . '">
								' . $resultTitle . '
							</a>
						</div>								
					</div>';
			}
			else if ( $type == 3 )
			{
				echo '<div class="livingentertainingpage-wrapper col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="livingentertainingpage-thumb-wrapper">
										<a href="' . get_the_permalink() . '">
											<div class="livingentertainingpage-thumb videos-section-big" style="background-image:url(http://img.youtube.com/vi/' . $videoID . '/0.jpg)">								
												<div class="videos-info">
													<div class="videos-play-wrapper">
														<div class="videos-play-inner">
															<div class="videos-play-icon">
															</div>
														</div>
													</div>
												</div>
											</div>
										</a>
										<a href="http://pinterest.com/pin/create/button/?url=' . urlencode(get_permalink()) . '&amp;description=' . urlencode(get_the_title()) . '&amp;media=' . urlencode("http://img.youtube.com/vi/" . $videoID . "/0.jpg"). '" target="_blank" class="livingentertainingpage-pinterest" title="Pinterest"><span class="glyph glyph-pinterest-circle" title="Pinterest"></span></a>
										<div class="livingentertainingpage-social">
											<a href="http://www.facebook.com/sharer.php?u=' . get_the_permalink() . '&amp;t=' . get_the_title(). '" target="_blank" class="share-social"><span class="glyph glyph-facebook" title="Facebook"></span></a>        
											<a href="http://twitter.com/home?status=' . get_the_title() . ' ' . get_the_permalink() . '" target="_blank" class="share-social"><span class="glyph glyph-twitterbird" title="Twitter"></span></a>											
										</div>
									</div>
									<div class="livingentertainingpage-meta clearfix">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 livingentertainingpage-date">
											<a href="' . $archiveDateLink . '" title="' . get_the_date() . '" rel="bookmark">
												<time class="entry-date" datetime="2014-01-01T11:11:11+00:00">
													' . get_the_date() . '
												</time>
											</a>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 livingentertainingpage-comments">
											' . get_comments_popup_link( __( '0 Comment', 'jellypixel' ), __( '1 Comment', 'jellypixel' ), __( '% Comments', 'jellypixel' ) ) . '
										</div>
									</div>								
									<div class="livingentertainingpage-title">
										<a href="' . get_the_permalink() . '">
											' . $resultTitle . '
										</a>
									</div>								
								</div>';
			}
			else 
			{
				echo '<div class="livingentertainingpage-wrapper col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="livingentertainingpage-thumb-wrapper">
										<a href="' . get_the_permalink() . '">
											<div class="livingentertainingpage-thumb" style="background-image:url(' . $attachment_image_thumb_url . ');">																				
											</div>
										</a>
										<a href="http://pinterest.com/pin/create/button/?url=' . urlencode(get_permalink()) . '&amp;description=' . urlencode(get_the_title()) . '&amp;media=' . urlencode($attachment_image_thumb_url). '" target="_blank" class="livingentertainingpage-pinterest" title="Pinterest"><span class="glyph glyph-pinterest-circle" title="Pinterest"></span></a>
										<div class="livingentertainingpage-social">
											<a href="http://www.facebook.com/sharer.php?u=' . get_the_permalink() . '&amp;t=' . get_the_title(). '" target="_blank" class="share-social"><span class="glyph glyph-facebook" title="Facebook"></span></a>        
											<a href="http://twitter.com/home?status=' . get_the_title() . ' ' . get_the_permalink() . '" target="_blank" class="share-social"><span class="glyph glyph-twitterbird" title="Twitter"></span></a>											
										</div>
									</div>
									<div class="livingentertainingpage-meta clearfix">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 livingentertainingpage-date">
											<a href="' . $archiveDateLink . '" title="' . get_the_date() . '" rel="bookmark">
												<time class="entry-date" datetime="2014-01-01T11:11:11+00:00">
													' . get_the_date() . '
												</time>
											</a>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 livingentertainingpage-comments">
											' . get_comments_popup_link( __( '0 Comment', 'jellypixel' ), __( '1 Comment', 'jellypixel' ), __( '% Comments', 'jellypixel' ) ) . '
										</div>
									</div>								
									<div class="livingentertainingpage-title">
										<a href="' . get_the_permalink() . '">
											' . $resultTitle . '
										</a>
									</div>								
								</div>';
			}
		}
	 
		exit;
	}
	

/* --------------------------------------------------------------------------------------------------------------------- */
	

	// Better Meal Plans Recipe Class
	class TwentyDishes_Recipe {

		public $parent_id;
		public $recipe_id;
		public $title;
		public $yield;
		public $readytime;
		public $cooktime;
		public $difficulty;
		public $thumb;
		public $thumbsmall;
		public $thumbmedium;
		public $ingredients;
		public $instructions;
		public $cats;
		public $nutrition;
		public $nutritional;
		public $fav;
		public $summary;

		public function __construct( $recipeid ) {

			$this->parent_id = (int) $recipeid;
			
			$this->recipe_id = (int) $this->convertRecipe($recipeid);
			if($this->recipe_id == 0) {
				$tmpContent = get_post_field('post_content', $recipeid);
				$this->recipe_id = WPRM_Recipe_Manager::get_recipe_ids_from_content( $tmpContent );
				$this->recipe_id = $this->recipe_id[0];
			}
			$recipe = WPRM_Recipe_Manager::get_recipe( $this->recipe_id );
			
			if(!$recipe) {
			    $this->title = "recipe deleted";
			    return ;
			}
			
			$this->title = get_the_title($recipeid); 
			$this->yield = (empty($recipe->servings())? 1: $recipe->servings());
			$this->readytime = $this->minutestohours($recipe->prep_time() . " minutes");
			$this->cooktime = $this->minutestohours($recipe->cook_time() . " minutes");
			$this->difficulty = 'Normal';
			
			$this->thumb = wp_get_attachment_url( get_post_thumbnail_id( $this->recipe_id ) );
			if ( !$this->thumb )
				$this->thumb = get_bloginfo('template_url') . '/sys/img/noimage.jpg'; 
				
			$this->thumbmedium = get_the_post_thumbnail_url( $this->recipe_id, 'medium' );
			if ( !$this->thumb )
				$this->thumb = get_bloginfo('template_url') . '/sys/img/noimage.jpg'; 
				
			$this->thumbsmall = get_the_post_thumbnail_url( $this->recipe_id, 'thumbnail' );
			if ( !$this->thumb )
				$this->thumb = get_bloginfo('template_url') . '/sys/img/noimage.jpg'; 

			$this->ingredients = $recipe->ingredients(); $tempArray = array();
			for($i = 0; $i < count($this->ingredients); $i++ ) {
				$tmpGroup = array(array("text" => $this->ingredients[$i]["name"], "image" => -10));
				
				$tempArray = array_merge($tempArray, $tmpGroup);
				$tempArray = array_merge($tempArray, $this->ingredients[$i]["ingredients"]);
			}
			$this->ingredients = $tempArray;

			$this->instructions = $recipe->instructions(); $tempArray = array();
			for($i = 0; $i < count($this->instructions); $i++ ) {
				
				$tmpGroup = array(array("text" => $this->instructions[$i]["name"], "image" => -10));
				
				$tempArray = array_merge($tempArray, $tmpGroup);
				$tempArray = array_merge($tempArray, $this->instructions[$i]["instructions"]);
			}
			$this->instructions = $tempArray;

			$this->cats = get_the_terms($recipeid, 'category');

			$this->nutrition = $recipe->nutrition();
			$this->nutritional = do_shortcode(WPRM_Template_Helper::nutrition_label( $this->recipe_id ));
			
			$this->fav = get_user_meta( checkCorporateAccount(), 'favorite-' . $recipeid, true );

			$this->summary = $recipe->summary();
		}

		// Convert Recipe from WP Ultimate Recipe to Recipe Maker
		private function convertRecipe($recipeid) {

			global $wpdb;
			$realRecipe = $wpdb->get_results( "	SELECT DISTINCT post_id FROM wp_postmeta a
													WHERE a.meta_key = 'wprm_parent_post_id' AND a.meta_value = " . $recipeid);
			$realRecipe = $realRecipe[0];
			
			return $realRecipe->post_id;
		}

		private function minutestohours($minutes){
		
			$minutesarr = explode(" ", $minutes);
			
			if($minutesarr[0] >= 60) {
				
				$hour = (int) $minutesarr[0] / 60;
				$minutes = $minutesarr[0] - ($hour*60);
				
				if($hour == 1)
					$hour = $hour . ' hour';
				else 
					$hour = $hour . ' hours';
				
				if($minutes == 0)
					$minutes = '';
				else if($minutes == 1)
					$minutes = $minutes . ' $minute';
				else 
					$minutes = $minutes . ' $minutes';
							
				return $hour . ' ' . $minutes;
			}
			
			return $minutes;
		}	

		private function fraction($decimal){
		
			$big_fraction = float2rat($decimal);
			
			if($big_fraction === "33/100")
				$big_fraction = "1/3";	
	
			if($big_fraction === "3/10")
				$big_fraction = "1/3";		
				
			if($big_fraction === "67/100")
				$big_fraction = "2/3";				
			
			return $big_fraction;
		}

		public function printIngredients($adjusted = false) {

			$userServingSize = get_user_meta(checkCorporateAccount(), "servings", true); 
			$ingCount = 1; $ingEl = '';

			foreach( $this->ingredients as $ingredient ) {	
				
				if($ingredient["image"] != -10) {	
					
					if($adjusted) {
						$singleServe = $ingredient["amount"] / $this->yield;
						$ingredient["amount"] = ceil(($singleServe * $userServingSize) * 4) / 4;
					}
					
					$frac = $this->fraction($ingredient["amount"]);
					$frac = ($frac == "0")? "" : $frac;
				
					if(trim($ingredient["notes"]) != "")
						$ingEl .= $frac . ' ' . $ingredient["unit"] . ' ' . $ingredient["name"] . ' (' . $ingredient["notes"] . ')<br>';	
					else
						$ingEl .= $frac . ' ' . $ingredient["unit"] . ' ' . $ingredient["name"] . '<br>';
						
					$ingCount++;
					
				}
				else {
					if($ingredient["text"] != '') {
						$ingEl .= '<br>' . wp_strip_all_tags($ingredient["text"]) . '<br>';
						$ingCount = 1;	
					}
				}

				
			}

			return $ingEl;
		}

		public function printInstructions() {

			$insCount = 1; $insEl = '';

			foreach( $this->instructions as $instruction ) {
				if($instruction["image"] != -10) {	
					$insEl .= $insCount . '. ' . wp_strip_all_tags($instruction["text"]) . '<br>';			
					$insCount++;
				}
				else {
					if($instruction["text"] != '') {
						$insEl .= '<br>' . wp_strip_all_tags($instruction["text"]) . '<br>';
						$insCount = 1;	
					}
				}
			}

			return $insEl;
		}

		public function getFavClass() {
			
			if ( $this->fav == 'yes' ) {
				return ' fullstar';
			}
			else {
				return ' blankstar';
			}	
		}

	}

/* --------------------------------------------------------------------------------------------------------------------- */	

	// Enable some user meta on the profile page
	add_action( 'show_user_profile', 'display_user_custom_meta' );
	add_action( 'edit_user_profile', 'display_user_custom_meta' );

	function display_user_custom_meta( $user ) { ?>
		<h3>USERMETA Fields</h3>
		<table class="form-table">
			<tr>
					<th><label>First Visit Key</label></th>
					<td><input type="text" name="firstentry" value="<?php echo get_user_meta( $user->ID, 'firstentry', true ); ?>" class="regular-text" /></td>
			</tr>
		</table>
		<?php
	}

	add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
	add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );
	function save_extra_user_profile_fields( $user_id ) {
		if ( !current_user_can( 'edit_user', $user_id ) ) { 
			return false; 
		}
		update_user_meta( $user_id, 'firstentry', $_POST['firstentry'] );
	}
	
?>