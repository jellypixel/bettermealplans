<?php
	get_header();
?>

	<div class="beginpage container">
    	<div class="blog-cat-title">
        	<h1>
	        	Recipes
            </h1>
        </div>
        
        <div class="blog-wrappers">
        	<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				
				$args = array (
					'cat'				=> '-18',
					'ignore_sticky_posts' => true,
					'posts_per_page' 	=> 12, //get_option('posts_per_page'),
					'paged'				=> $paged,
					'post_type' 		=> 'post',
				);	
				
				$wp_query = new WP_Query( $args );				
				$max = $wp_query->max_num_pages;
			
				// The Loop
				if ( $wp_query->have_posts() ) 
				{
					?>
                    	<div class="row">
                        	<?php
								while ( $wp_query->have_posts() ) 
								{
									$wp_query->the_post();
									global $theme_options;
									
									// Get All Recipes
									$all_recipes = WPRM_Recipe_Manager::get_recipe_ids_from_content( get_the_content() );				
									
									// Get First Recipe
									if ( isset( $all_recipes[0] ) ) {
										$first_recipe_id = $all_recipes[0];
										$first_recipe = WPRM_Recipe_Manager::get_recipe( $first_recipe_id );
									}
									
									// Title
									$title = get_the_title();
									?>					
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-12 blog-outer">                                        	    
                                            <div class="blog-wrapper">
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> 
                                                    <div class="blog-thumbnail">
                                                    	<?php
															echo do_shortcode('[wprm-recipe-image id="' . $first_recipe_id .'"]');
														?>
                                                    </div>
                                                </a>
                                                <div class="blog-content">
                                                    <div class="blog-meta">
                                                        <?php 
															$all_cats = get_the_category(); 
															$all_cats_el = '';
															if ( !empty( $all_cats) )
															{
																foreach ( $all_cats as $cat )
																{
																	if ( empty( $all_cats_el ) )
																	{
																		$all_cats_el = $cat->name;
																	}
																	/*else
																	{
																		$all_cats_el .= ', ' . $cat->name;
																	}*/
																}
															}
															
															echo $all_cats_el;
														?>
                                                    </div>
                                                    <div class="blog-title">
                                                        <h2 class="post-title">
                                                            <a rel="bookmark" href="<?php the_permalink(); ?>">
                                                                <?php echo $title; ?>
                                                            </a>
                                                        </h2> 
                                                    </div>
                                                    <div class="blog-readmore">
                                                        <a rel="bookmark" href="<?php the_permalink(); ?>">
                                                            Read more
                                                        </a>
                                                    </div>																								
                                                </div>                                               
                                            </div>
                                        </div>
									<?php
								}
							?>
                        </div>
                    <?php				
				} 
								
				// Reset
				wp_reset_postdata();
			?>
            
            <div class="col-12">
            	<div class="blog-nav">
					<?php
                        get_template_part( 'section', 'paging' );
                    ?>
                </div>
            </div>
        </div>
	</div>

<?php
	get_footer();
?>