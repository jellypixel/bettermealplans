<?php
	get_header();	
?>

	<div class="beginpage container">
    	<?php 
			include(locate_template('section-title.php'));
		?>    
        <!--
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>     
            <div class="post-content default-page">
                <?php
                    // The Content					
					
                    if ( have_posts() ) 
                    {					
                        while ( have_posts() ) 
                        {
                            the_post();
                            the_content();
                        }
                    } 
                    
                    // Reset
                    wp_reset_query();			
                ?>                
            </div>
        </article>        
        -->
        <article id="post-4523" class="post-4523 page type-page status-publish hentry pmpro-has-access">     
            <div class="post-content default-page">
                <div id="pmpro_level-28">
<form id="pmpro_form" class="pmpro_form" action="" method="post">

	<input id="level" name="level" value="28" type="hidden">
	<input id="checkjavascript" name="checkjavascript" value="1" type="hidden">
	
			<div id="pmpro_message" class="pmpro_message" style="display: none;"></div>
	
	
	<table id="pmpro_pricing_fields" class="pmpro_checkout" width="100%" cellspacing="0" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>
				<span class="pmpro_thead-name">Membership Level</span>
				<span class="pmpro_thead-msg"><a href="https://bettermealplans.com/membership-account/membership-levels/">change</a></span>			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<p>
					You have selected the <strong>One Year</strong> membership level.				</p>

				
				<div id="pmpro_level_cost">
										<p>The price for membership is <strong>$0.00</strong> now and then <strong>$108.00 every 12 Months</strong>  after your <strong>7 day trial</strong>.</p>
									</div>

				
				
											<p id="other_discount_code_p" class="pmpro_small">Do you have a discount code? <a id="other_discount_code_a" href="javascript:void(0);">Click here to enter your discount code</a>.</p>
					
							</td>
		</tr>
				<tr id="other_discount_code_tr" style="display: none;">
			<td>
				<div>
					<label for="other_discount_code">Discount Code</label>
					<input id="other_discount_code" name="other_discount_code" class="input " size="20" value="" type="text">
					<input name="other_discount_code_button" id="other_discount_code_button" value="Apply" type="button">
				</div>
			</td>
		</tr>
			</tbody>
	</table>

		
	
	
		<table id="pmpro_user_fields" class="pmpro_checkout" width="100%" cellspacing="0" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>
				<span class="pmpro_thead-name">Account Information</span>
				<span class="pmpro_thead-msg">Already have an account? <a href="https://bettermealplans.com/wp-login.php?redirect_to=https%3A%2F%2Fbettermealplans.com%2Fmembership-account%2Fmembership-checkout%2F%3Flevel%3D28">Log in here</a></span>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<div>
					<label for="username">Username</label>
					<input id="username" name="username" class="input pmpro_required" size="30" value="" type="text"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
				</div>

				
				<div>
					<label for="password">Password</label>
					<input id="password" name="password" class="input pmpro_required" size="30" value="" type="password"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
				</div>
									<div>
						<label for="password2">Confirm Password</label>
						<input id="password2" name="password2" class="input pmpro_required" size="30" value="" type="password"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
					</div>
					
					<div>
	<label for="first_name">First Name</label>
	<input id="first_name" name="first_name" class="input pmpro_required" size="30" value="" type="text"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
	</div>
	<div>
	<label for="last_name">Last Name</label>
	<input id="last_name" name="last_name" class="input pmpro_required" size="30" value="" type="text"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
	</div> 
	
				<div>
					<label for="bemail">E-mail Address</label>
					<input id="bemail" name="bemail" class="input pmpro_required" size="30" value="" type="email"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
				</div>
									<div>
						<label for="bconfirmemail">Confirm E-mail Address</label>
						<input id="bconfirmemail" name="bconfirmemail" class="input pmpro_required" size="30" value="" type="email"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>

					</div>
					
				
				<div class="pmpro_hidden">
					<label for="fullname">Full Name</label>
					<input id="fullname" name="fullname" class="input " size="30" value="" type="text"> <strong>LEAVE THIS BLANK</strong>
				</div>

				<div class="pmpro_captcha">
								</div>

				
			</td>
		</tr>
	</tbody>
	</table>
	
	
				<div id="pmpro_checkout_box-checkout_boxes" class="pmpro_checkout">
				<h2>	
					<span class="pmpro_thead-name">More Information</span>
				</h2>
				<div class="pmpro_checkout-fields">
											<div id="phone_div" class="pmpro_checkout-field">
									<label for="phone">Phone</label>
										<input id="phone" name="phone" value="" size="30" class="phone input " type="text"><span class="pmpro_asterisk"> *</span>								
							</div>	
							</div> <!-- end pmpro_checkout-fields -->
			</div> <!-- end pmpro_checkout_box-name -->
				<div id="pmpro_payment_method" class="pmpro_checkout">
		<h2>Choose your Payment Method</h2>
		<div class="pmpro_checkout-fields">
			<span class="gateway_authorizenet">
				<input name="gateway" value="authorizenet" checked="checked" type="radio">
				<a href="javascript:void(0);" class="pmpro_radio">Check Out with a Credit Card Here</a>
			</span>
			<span class="gateway_paypalexpress">
				<input name="gateway" value="paypalexpress" type="radio">
				<a href="javascript:void(0);" class="pmpro_radio">Check Out with PayPal</a>
			</span>	
		</div>
	</div> <!--end pmpro_payment_method -->
		
	
	
	
		<table id="pmpro_billing_address_fields" class="pmpro_checkout top1em" width="100%" cellspacing="0" cellpadding="0" border="0">
	<thead>
		<tr>
			<th>Billing Address</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<div>
					<label for="bfirstname">First Name</label>
					<input id="bfirstname" name="bfirstname" class="input " size="30" value="" type="text">
				</div>
				<div>
					<label for="blastname">Last Name</label>
					<input id="blastname" name="blastname" class="input " size="30" value="" type="text">
				</div>
				<div>
					<label for="baddress1">Address 1</label>
					<input id="baddress1" name="baddress1" class="input " size="30" value="" type="text">
				</div>
				<div>
					<label for="baddress2">Address 2</label>
					<input id="baddress2" name="baddress2" class="input " size="30" value="" type="text">
				</div>

									<div>
						<label for="bcity">City</label>
						<input id="bcity" name="bcity" class="input " size="30" value="" type="text">
					</div>
					<div>
						<label for="bstate">State</label>
						<input id="bstate" name="bstate" class="input " size="30" value="" type="text">
					</div>
					<div>
						<label for="bzipcode">Postal Code</label>
						<input id="bzipcode" name="bzipcode" class="input " size="30" value="" type="text">
					</div>
				
								<div>
					<label for="bcountry">Country</label>
					<select name="bcountry" id="bcountry" class=" ">
													<option value="AF">Afghanistan</option>
														<option value="AX">Aland Islands</option>
														<option value="AL">Albania</option>
														<option value="DZ">Algeria</option>
														<option value="AS">American Samoa</option>
														<option value="AD">Andorra</option>
														<option value="AO">Angola</option>
														<option value="AI">Anguilla</option>
														<option value="AQ">Antarctica</option>
														<option value="AG">Antigua and Barbuda</option>
														<option value="AR">Argentina</option>
														<option value="AM">Armenia</option>
														<option value="AW">Aruba</option>
														<option value="AU">Australia</option>
														<option value="AT">Austria</option>
														<option value="AZ">Azerbaijan</option>
														<option value="BS">Bahamas</option>
														<option value="BH">Bahrain</option>
														<option value="BD">Bangladesh</option>
														<option value="BB">Barbados</option>
														<option value="BY">Belarus</option>
														<option value="BE">Belgium</option>
														<option value="BZ">Belize</option>
														<option value="BJ">Benin</option>
														<option value="BM">Bermuda</option>
														<option value="BT">Bhutan</option>
														<option value="BO">Bolivia</option>
														<option value="BA">Bosnia and Herzegovina</option>
														<option value="BW">Botswana</option>
														<option value="BV">Bouvet Island</option>
														<option value="BR">Brazil</option>
														<option value="IO">British Indian Ocean Territory</option>
														<option value="VG">British Virgin Islands</option>
														<option value="BN">Brunei</option>
														<option value="BG">Bulgaria</option>
														<option value="BF">Burkina Faso</option>
														<option value="BI">Burundi</option>
														<option value="KH">Cambodia</option>
														<option value="CM">Cameroon</option>
														<option value="CA">Canada</option>
														<option value="CV">Cape Verde</option>
														<option value="KY">Cayman Islands</option>
														<option value="CF">Central African Republic</option>
														<option value="TD">Chad</option>
														<option value="CL">Chile</option>
														<option value="CN">China</option>
														<option value="CX">Christmas Island</option>
														<option value="CC">Cocos (Keeling) Islands</option>
														<option value="CO">Colombia</option>
														<option value="KM">Comoros</option>
														<option value="CG">Congo (Brazzaville)</option>
														<option value="CD">Congo (Kinshasa)</option>
														<option value="CK">Cook Islands</option>
														<option value="CR">Costa Rica</option>
														<option value="HR">Croatia</option>
														<option value="CU">Cuba</option>
														<option value="CY">Cyprus</option>
														<option value="CZ">Czech Republic</option>
														<option value="DK">Denmark</option>
														<option value="DJ">Djibouti</option>
														<option value="DM">Dominica</option>
														<option value="DO">Dominican Republic</option>
														<option value="EC">Ecuador</option>
														<option value="EG">Egypt</option>
														<option value="SV">El Salvador</option>
														<option value="GQ">Equatorial Guinea</option>
														<option value="ER">Eritrea</option>
														<option value="EE">Estonia</option>
														<option value="ET">Ethiopia</option>
														<option value="FK">Falkland Islands</option>
														<option value="FO">Faroe Islands</option>
														<option value="FJ">Fiji</option>
														<option value="FI">Finland</option>
														<option value="FR">France</option>
														<option value="GF">French Guiana</option>
														<option value="PF">French Polynesia</option>
														<option value="TF">French Southern Territories</option>
														<option value="GA">Gabon</option>
														<option value="GM">Gambia</option>
														<option value="GE">Georgia</option>
														<option value="DE">Germany</option>
														<option value="GH">Ghana</option>
														<option value="GI">Gibraltar</option>
														<option value="GR">Greece</option>
														<option value="GL">Greenland</option>
														<option value="GD">Grenada</option>
														<option value="GP">Guadeloupe</option>
														<option value="GU">Guam</option>
														<option value="GT">Guatemala</option>
														<option value="GG">Guernsey</option>
														<option value="GN">Guinea</option>
														<option value="GW">Guinea-Bissau</option>
														<option value="GY">Guyana</option>
														<option value="HT">Haiti</option>
														<option value="HM">Heard Island and McDonald Islands</option>
														<option value="HN">Honduras</option>
														<option value="HK">Hong Kong S.A.R., China</option>
														<option value="HU">Hungary</option>
														<option value="IS">Iceland</option>
														<option value="IN">India</option>
														<option value="ID">Indonesia</option>
														<option value="IR">Iran</option>
														<option value="IQ">Iraq</option>
														<option value="IE">Ireland</option>
														<option value="IM">Isle of Man</option>
														<option value="IL">Israel</option>
														<option value="IT">Italy</option>
														<option value="CI">Ivory Coast</option>
														<option value="JM">Jamaica</option>
														<option value="JP">Japan</option>
														<option value="JE">Jersey</option>
														<option value="JO">Jordan</option>
														<option value="KZ">Kazakhstan</option>
														<option value="KE">Kenya</option>
														<option value="KI">Kiribati</option>
														<option value="KW">Kuwait</option>
														<option value="KG">Kyrgyzstan</option>
														<option value="LA">Laos</option>
														<option value="LV">Latvia</option>
														<option value="LB">Lebanon</option>
														<option value="LS">Lesotho</option>
														<option value="LR">Liberia</option>
														<option value="LY">Libya</option>
														<option value="LI">Liechtenstein</option>
														<option value="LT">Lithuania</option>
														<option value="LU">Luxembourg</option>
														<option value="MO">Macao S.A.R., China</option>
														<option value="MK">Macedonia</option>
														<option value="MG">Madagascar</option>
														<option value="MW">Malawi</option>
														<option value="MY">Malaysia</option>
														<option value="MV">Maldives</option>
														<option value="ML">Mali</option>
														<option value="MT">Malta</option>
														<option value="MH">Marshall Islands</option>
														<option value="MQ">Martinique</option>
														<option value="MR">Mauritania</option>
														<option value="MU">Mauritius</option>
														<option value="YT">Mayotte</option>
														<option value="MX">Mexico</option>
														<option value="FM">Micronesia</option>
														<option value="MD">Moldova</option>
														<option value="MC">Monaco</option>
														<option value="MN">Mongolia</option>
														<option value="ME">Montenegro</option>
														<option value="MS">Montserrat</option>
														<option value="MA">Morocco</option>
														<option value="MZ">Mozambique</option>
														<option value="MM">Myanmar</option>
														<option value="NA">Namibia</option>
														<option value="NR">Nauru</option>
														<option value="NP">Nepal</option>
														<option value="NL">Netherlands</option>
														<option value="AN">Netherlands Antilles</option>
														<option value="NC">New Caledonia</option>
														<option value="NZ">New Zealand</option>
														<option value="NI">Nicaragua</option>
														<option value="NE">Niger</option>
														<option value="NG">Nigeria</option>
														<option value="NU">Niue</option>
														<option value="NF">Norfolk Island</option>
														<option value="KP">North Korea</option>
														<option value="MP">Northern Mariana Islands</option>
														<option value="NO">Norway</option>
														<option value="OM">Oman</option>
														<option value="PK">Pakistan</option>
														<option value="PW">Palau</option>
														<option value="PS">Palestinian Territory</option>
														<option value="PA">Panama</option>
														<option value="PG">Papua New Guinea</option>
														<option value="PY">Paraguay</option>
														<option value="PE">Peru</option>
														<option value="PH">Philippines</option>
														<option value="PN">Pitcairn</option>
														<option value="PL">Poland</option>
														<option value="PT">Portugal</option>
														<option value="PR">Puerto Rico</option>
														<option value="QA">Qatar</option>
														<option value="RE">Reunion</option>
														<option value="RO">Romania</option>
														<option value="RU">Russia</option>
														<option value="RW">Rwanda</option>
														<option value="BL">Saint Barthelemy</option>
														<option value="SH">Saint Helena</option>
														<option value="KN">Saint Kitts and Nevis</option>
														<option value="LC">Saint Lucia</option>
														<option value="MF">Saint Martin (French part)</option>
														<option value="PM">Saint Pierre and Miquelon</option>
														<option value="VC">Saint Vincent and the Grenadines</option>
														<option value="WS">Samoa</option>
														<option value="SM">San Marino</option>
														<option value="ST">Sao Tome and Principe</option>
														<option value="SA">Saudi Arabia</option>
														<option value="SN">Senegal</option>
														<option value="RS">Serbia</option>
														<option value="SC">Seychelles</option>
														<option value="SL">Sierra Leone</option>
														<option value="SG">Singapore</option>
														<option value="SK">Slovakia</option>
														<option value="SI">Slovenia</option>
														<option value="SB">Solomon Islands</option>
														<option value="SO">Somalia</option>
														<option value="ZA">South Africa</option>
														<option value="GS">South Georgia and the South Sandwich Islands</option>
														<option value="KR">South Korea</option>
														<option value="ES">Spain</option>
														<option value="LK">Sri Lanka</option>
														<option value="SD">Sudan</option>
														<option value="SR">Suriname</option>
														<option value="SJ">Svalbard and Jan Mayen</option>
														<option value="SZ">Swaziland</option>
														<option value="SE">Sweden</option>
														<option value="CH">Switzerland</option>
														<option value="SY">Syria</option>
														<option value="TW">Taiwan</option>
														<option value="TJ">Tajikistan</option>
														<option value="TZ">Tanzania</option>
														<option value="TH">Thailand</option>
														<option value="TL">Timor-Leste</option>
														<option value="TG">Togo</option>
														<option value="TK">Tokelau</option>
														<option value="TO">Tonga</option>
														<option value="TT">Trinidad and Tobago</option>
														<option value="TN">Tunisia</option>
														<option value="TR">Turkey</option>
														<option value="TM">Turkmenistan</option>
														<option value="TC">Turks and Caicos Islands</option>
														<option value="TV">Tuvalu</option>
														<option value="VI">U.S. Virgin Islands</option>
														<option value="USAF">US Armed Forces</option>
														<option value="UG">Uganda</option>
														<option value="UA">Ukraine</option>
														<option value="AE">United Arab Emirates</option>
														<option value="GB">United Kingdom</option>
														<option value="US" selected="selected">United States</option>
														<option value="UM">United States Minor Outlying Islands</option>
														<option value="UY">Uruguay</option>
														<option value="UZ">Uzbekistan</option>
														<option value="VU">Vanuatu</option>
														<option value="VA">Vatican</option>
														<option value="VE">Venezuela</option>
														<option value="VN">Vietnam</option>
														<option value="WF">Wallis and Futuna</option>
														<option value="EH">Western Sahara</option>
														<option value="YE">Yemen</option>
														<option value="ZM">Zambia</option>
														<option value="ZW">Zimbabwe</option>
												</select>
				</div>
								<div>
					<label for="bphone">Phone</label>
					<input id="bphone" name="bphone" class="input " size="30" value="" type="text">
				</div>
							</td>
		</tr>
	</tbody>
	</table>
	
	
	
			<table id="pmpro_payment_information_fields" class="pmpro_checkout top1em" width="100%" cellspacing="0" cellpadding="0" border="0">
		<thead>
			<tr>
				<th>
					<span class="pmpro_thead-name">Payment Information</span>
					<span class="pmpro_thead-msg">We Accept Visa, Mastercard, American Express and Discover</span>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr valign="top">
				<td>
					
											<input id="CardType" name="CardType" value="Unknown Card Type" type="hidden">
						
						
					<div class="pmpro_payment-account-number">
						<label for="AccountNumber">Card Number</label>
						<input id="AccountNumber" name="AccountNumber" class="input pmpro_required" size="25" value="" data-encrypted-name="number" autocomplete="off" type="text"><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
					</div>

					<div class="pmpro_payment-expiration">
						<label for="ExpirationMonth">Expiration Date</label>
						<select id="ExpirationMonth" name="ExpirationMonth" class=" pmpro_required">
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>/<select id="ExpirationYear" name="ExpirationYear" class=" pmpro_required">
															<option value="2017">2017</option>
															<option value="2018">2018</option>
															<option value="2019">2019</option>
															<option value="2020">2020</option>
															<option value="2021">2021</option>
															<option value="2022">2022</option>
															<option value="2023">2023</option>
															<option value="2024">2024</option>
															<option value="2025">2025</option>
															<option value="2026">2026</option>
													</select><span class="pmpro_asterisk"> <abbr title="Required Field">*</abbr></span>
					</div>

										<div class="pmpro_payment-cvv">
						<label for="CVV">CVV</label>
						<input class="input" id="CVV" name="CVV" size="4" value="" type="text">  <small>(<a href="javascript:void(0);" onclick="javascript:window.open('https://bettermealplans.com/wp-content/plugins/paid-memberships-pro/pages/popup-cvv.html','cvv','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=600, height=475');">what's this?</a>)</small>
					</div>
					
										<div class="pmpro_payment-discount-code">
						<label for="discount_code">Discount Code</label>
						<input class="input " id="discount_code" name="discount_code" size="20" value="" type="text">
						<input id="discount_code_button" name="discount_code_button" value="Apply" type="button">
						<p id="discount_code_message" class="pmpro_message" style="display: none;"></p>
					</div>
					
				</td>
			</tr>
		</tbody>
		</table>
		

	
			<table id="pmpro_tos_fields" class="pmpro_checkout top1em" width="100%" cellspacing="0" cellpadding="0" border="0">
		<thead>
		<tr>
			<th>Terms of Service</th>
		</tr>
	</thead>
		<tbody>
			<tr class="odd">
				<td>
					<div id="pmpro_license">
<h1 class="subTitle">Better Meal Plans&nbsp;Terms of Service</h1>
<p><i>Last updated:&nbsp;August 2017</i></p>
<p>Welcome to Better Meal Plans! Every week we provide our customers with healthy, affordable and quick&nbsp;meal plans.</p>
<p>Below you will find the terms and conditions (“Terms” or “Agreement”) of the Better Meal Plans&nbsp;website (“Site”), email list subscriptions, meal plan content subscriptions and other features (collectively referred to as “Services”).</p>
<p>Please read this Agreement carefully before accessing or using the Website. By accessing or using any part of the website, you agree to become bound by the terms and conditions of this agreement. If you do not agree to all the terms and conditions of this agreement, then you may not access the Website or use any services. If these terms and conditions are considered an offer by Better Meal Plans, acceptance is expressly limited to these terms.</p>
<p>The Website is not directed to children younger than 13, and service on the Website is only offered to users 13 years of age or older. If you are under 13 years old, please do not register to use the Website. Any person who registers as a user or provides their personal information to the Website represents that they are 13 years of age or older.</p>
<p>Better Meal Plans&nbsp;&nbsp;may change, modify, add or remove sections of these Terms at any time by posting the revised Terms on our Site. The changes take effect when we post the Terms on the Site.</p>
<p>We always welcome your feedback. If you have any questions about the Terms, please email us at info@bettermealplans.com.</p>
<h2 class="subTitle">1. Overview</h2>
<p>By accessing our Site or Services, you agree to the Terms. You are legally bound by this Agreement between you and Vibrant Living Media LLC&nbsp;&nbsp;(“Better Meal Plans”). The Agreement defines your rights and responsibilities as a user (“User”) of the Site. The Site and Services are operated in the United States of America and access to the services is governed by these Terms under the laws of the State of Colorado&nbsp;and of the United States.</p>
<p>Registration as a User with our Services results in your personal information being stored and processed in the United States, and you specifically consent to allow the storage and processing of your personal data in the United States.</p>
<p>These Terms, together with our <a href="http://bettermealplans.com/privacy-policy/" target="_blank" rel="noopener">Privacy Policy </a>shall constitute the entire agreement between you and Better Meal Plans. If you do not agree with any of the Terms of this Agreement or the <a href="http://bettermealplans.com/privacy-policy/">Privacy Policy</a>, you must not use the Services.</p>
<h2 class="subTitle">2. Use of the Site</h2>
<p>The Site contains text, software, scripts, graphics, pictures, data, videos, user-generated information, editorial and other content (the “Content”) accessible by Users. All Content is owned, licensed to and/or copyrighted by Better Meal Plans&nbsp;and may be used only as described in the Terms.</p>
<p>The trademarks, logos, and service marks contained on the Site are owned by or licensed by Better Meal Plans. Better Meal Plans&nbsp;or its licensors retain title, ownership and all other rights to the Content on the Site.</p>
<p>We make our best effort to ensure that all Content on the site is complete and accurate. Despite our efforts, the Content may occasionally be incomplete or contain errors.</p>
<p>You are granted a limited use license to access the Site and its Content for your own personal use. The Content, in whole or in part, may not be republished, redistributed or resold without the explicit written permission of&nbsp;Better Meal Plans. The licenses granted by Better Meal Plans&nbsp;terminates if you violate this Agreement and may result in legal action against you.</p>
<p>Some content may be downloaded to your computer or device through use of the Site and Services. This Content remains subject to the limited use license contained in this Agreement.</p>
<p>The use of bots, crawlers, spiders, data miners, scraping and other automated data collection tools are prohibited without the approval of Better Meal Plans.</p>
<h2 class="subTitle">3. Subscription Terms, Fees and Payments</h2>
<p>Better Meal Plans&nbsp;is a fee-based subscription service offered at varying subscription durations. When you sign up for a subscription, you are committed to the term that you have chosen. Users of the Site may be unregistered Users, registered Users and paying subscribers (“Subscribers”).</p>
<p><strong>User Requirements.</strong> You must be 18 years or older to subscribe to any Services and provide Better Meal Plans&nbsp;with accurate and complete registration information. Failure to comply with either of these requirements will constitute a breach of this Agreement.</p>
<p>During registration you will create a username and password that will be used to authenticate your ongoing access to the Services. You will not:</p>
<ul>
<li>Use the name of another person with the intent to impersonate them</li>
<li>Use a name that Better Meal Plans, in our sole discretion, deems inappropriate</li>
<li>Use the rights of another person</li>
</ul>
<p>You are responsible for the use of your account and you must make your best effort to keep your password secure. You should not share your password with others. If you believe your account has been compromised, you must immediately notify Better Meal Plans&nbsp;of the suspected breach by emailing info@bettermealplans.com.</p>
<p><strong>Payments.</strong> The payment options and fees will be displayed on the Site at the time the subscription is offered. The terms applying to that subscription are incorporated into this Agreement. Prices for all Subscriptions exclude all applicable taxes unless stated otherwise and are in the form of US Dollars. To the extent that the law allows, you are responsible for any applicable taxes, whether or not they are listed on the Site.</p>
<p>Better Meal Plans&nbsp;uses a designated third-party payment platform to process credit and debit card transactions for your Subscription. You are responsible for all transactions processed through the Service. Better Meal Plans&nbsp;is not liable for any loss or damage from errant or invalid transactions processed through the third-party payment platform.</p>
<p><strong>7-Day Free Trial and Initial Purchase.</strong>&nbsp;Better Meal Plans offers a 7-day Free Trial for all new members. Monthly membership charge will be processed the morning of the 8th day after new membership sign-up. In order to avoid being charged, you must cancel your account before the end of business day (5pm PST) on the 6th day after your initial sign-up.&nbsp;There is no further action required by you if you would like to continue to subscribe to the meal plan after the initial 7-Day Free Trial Period.</p>
<p><strong>Automatic Renewal.</strong> With the exception of gift certificate purchases, all of our meal plan subscriptions renew automatically at the end of their term. Once you sign up for a trial or become a Subscriber, your subscriptions will be automatically renewed and your credit card will be charged at the end of your term. Your subscription will be renewed based on the term you selected when initially setting up the subscription&mdash;unless you choose to opt-out or cancel your subscription as described below. The renewal of subscriptions takes place according to the Terms in place on the date of the renewal. If a promotion or other offers were made available at the time of purchase, be sure to take note of any rules, conditions, cancellation dates or price changes that may take effect when the promotional period ends. If a renewal charge is unsuccessful,Better Meal Plans may retry charges to your credit card for up to 180 days.</p>
<p><strong>Valid Credit Card.</strong> It is your responsibility to ensure that Better Meal Plans&nbsp;has valid credit card information for your account. Better Meal Plans&nbsp;may also use third-party services to retrieve updated credit card information for your account.</p>
<p><strong>For a Single Household.</strong> The Content provided through your subscription is intended for your personal use and should not be shared with others outside of your household.</p>
<p><strong>Cancellation and Opting Out of Renewal.</strong> You may opt out of your next renewal by email ONLY. You may not opt out of your renewal by leaving a voicemail. To avoid the processing of a renewal payment, you must opt out at least two business days prior to your subscription renewal date. You may cancel your membership&nbsp;at anytime during the initial 7 Day Free Trial&nbsp;&nbsp;period. When cancelling a during this 7-day period,&nbsp;or opting out of renewal, you may continue to use the subscription until the end of the current subscription.</p>
<p><strong>Refunds.</strong>&nbsp; Requests for a refund can only be made by email. Gift Certificates and eBooks are non-refundable.&nbsp;We do not provide prorated refunds. Refunds are not provided if the account was not canceled within the 7-day trial period or at least&nbsp;two business days prior to your subscription renewal date, as stated in the Cancellation and Opting Out of Renewal section. You are eligible for a full refund for 7&nbsp;days after a charge is processed if you cancelled during your initial 7-Day trial period. Refunds will be made to the credit card that was originally charged and may take several days to appear on your credit card account.</p>
<p><strong>Termination of your Account.</strong> If Better Meal Plans, in its sole opinion, believes that you are acting in a spirit inconsistent with this Agreement or have breached the Agreement, we may terminate your account. In such a case, you are not eligible for a refund. We also reserve the right to terminate your subscription for any reason even if the reason does not cause a breach of this Agreement. In such a case, we will refund any remaining unused portion of your subscription. The refund will be your sole and exclusive remedy to such a termination.</p>
<p><strong>Prices Subject to Change.</strong>&nbsp;Better Meal Plans may change our base subscription prices at anytime. However, with the exception of any discount from coupon codes or promotional offers, the price for any renewal will be at the same base price that you were originally charged when you subscribed&mdash;unless we notify you otherwise. Should your base subscription price change, we will notify you by email and give you a time period of at least two weeks to opt out of the renewal if you do not want to continue your subscription at the new price.</p>
<h2>4. Communication Between You and&nbsp;Better Meal Plans</h2>
<p>We will generally contact you to let you know about changes to our Services and the products related to our Services. You may opt out of our email communications by following the unsubscribe instructions located within the email communications. You agree that any disclosure, notice, agreement, or other communication that we send to you electronically will satisfy any legal requirement, including that such communication be in writing.</p>
<h2 class="subTitle">5. Content Submissions</h2>
<p>By submitting recipes, photographs, ratings or reviews, you grant Better Meal Plans&nbsp;an irrevocable, non-exclusive, perpetual, worldwide, royalty-free right and license to use, display, modify, reproduce, publish, distribute in any manner existing now or to be developed in the future without compensation of any kind to you or any third party.</p>
<p>Better Meal Plans&nbsp;reserves the right to delete or refuse to post, at our sole discretion, any submitted content for any reason.</p>
<p>Better Meal Plans&nbsp;does not allow submitted items that contain:</p>
<ul>
<li>Copyright materials for which the person submitting does not have the authority to provide a release of the Copyright</li>
<li>Any content that infringes on intellectual property rights of a third party</li>
<li>Obscene, profane, or pornographic content</li>
<li>Personal attacks on others such as slanderous, defamatory, threatening or harassing content</li>
</ul>
<p>Better Meal Plans&nbsp;may monitor content submitted, but we cannot be responsible for the submissions of third parties. If you see something that you believe violates these Terms or spirit of our Services, please let us know by contacting the Customer Experience team.</p>
<h2 class="subTitle">6. Dietary Restrictions &amp; Preferences</h2>
<p>Better Meal Plans&nbsp;is committed to providing simple, balanced meals to help individuals make healthy choices in consultation with their personal physician.</p>
<p>Better Meal Plans&nbsp;does not:</p>
<ul>
<li>Guarantee the accuracy, completeness, or usefulness of any nutritional information in the food database</li>
<li>Adopt, endorse, or accept responsibility for the accuracy, completeness or usefulness of any nutritional information</li>
</ul>
<p>Under no circumstances will Better Meal Plans&nbsp;be responsible for any loss or damage resulting from your reliance on nutritional information and for ensuring that the food you and the members of your household prepare or consume are in accordance with your specific dietary needs and restrictions. You should always seek the advice of a physician or Registered Dietician for your own specific conditions or dietary needs. Better Meal Plans&nbsp;will not be liable for any health issues resulting from the consumption of ingredients to which you or a member of your household is allergic or that is harmful to you in any way.</p>
<h2 class="subTitle">7. Disclaimer</h2>
<p>Our Content and Services are provided on an ‘as is’ basis without warranties of any kind, whether explicit or implied. Use of our Content and Services are at your own risk.</p>
<p>Additionally we make no representation or warrant that any Content within our Services is accurate, complete, reliable, or error-free. We do not make representation that the Content or Services is suitable for a particular use by you or one of your family members.</p>
<p>Better Meal Plans&nbsp;does not warrant that our Content, Services, servers, or emails are free from viruses or any other harmful components.</p>
<p><strong>Limited Liability.</strong> To the fullest extent permissible under applicable law, Better Meal Plans&nbsp;limits our liability. In particular we will not be liable for any damages that we cause unintentionally and we will not be liable to you for any actual, incidental, indirect or consequential loss or damage however caused, provided that nothing in this Agreement will be interpreted so as to limit or exclude any liability which may not be excluded or limited by law. For example, we will not be liable to you for any of the following types of damages, whether in contract, tort (including negligence and strict liability) or otherwise (whether such loss or damage was foreseeable, known or otherwise): (i) loss of revenue; (ii) loss of actual or anticipated profits; (iii) loss of the use of money; (iv) loss of anticipated savings; or (v) loss or corruption of, or damage to, data, systems or programs. Because some states/jurisdictions do not allow exclusions as broad as those stated above or limitations of liability for consequential or incidental damages, the above limitations may, in whole or in part, not apply to you. If you are dissatisfied with any portion of the Site or the Services, or with any clause of these terms, as your sole and exclusive remedy you may discontinue using the Site and the Services. Although we will not be liable for your losses caused by any unauthorized use of your account, you may be liable to others as well as to us if your account is used in violation of the terms and conditions of this Agreement.</p>
<p><strong>Indemnity.</strong> You agree to defend, indemnify and hold harmless Better Meal Plans, its affiliates, officers, directors, employees and agents from and against any and all claims, damages, obligations, losses, liabilities, costs or expenses (including but not limited to attorney's fees) arising from: (i) your use of and access to the Sites and Services; (ii) your violation of any term of this Agreement; (iii) your violation of any third-party right, including without limitation any copyright, property, or privacy right; or (iv) any claim that your Content Submissions caused damage to a third party. This defense and indemnification obligation will survive this Agreement and your use of the Sites and Services.</p>
<h2 class="subTitle">8. Governing Law; Disputes</h2>
<p>By using the Services or the Site, you agree that the Federal Arbitration Act, applicable federal law, and the law of the State of Colorado, without regard to its principles on conflicts of laws, will govern these Terms, your use of the Site and the Services, and any dispute of any sort that might arise between you and Better Meal Plans.</p>
<p>If a dispute arises between you and Better Meal Plans, our goal is to provide you a neutral and cost effective way to resolve the dispute quickly. You agree to first contact the Better Meal Plans&nbsp;Customer Experience team by phone or email via the contact information below to describe the problem and seek a resolution. If that does not resolve the issue, then you and Better Meal Plans&nbsp;agree that any dispute or claim relating to your use of the Services or the Site will be resolved through binding arbitration, rather than in court, except that you may assert claims in small claims court if your claims qualify. In addition, you and Better Meal Plans&nbsp;both agree that either party may bring suit in court to enjoin infringement or other misuse of intellectual property rights. If for any reason a claim proceeds in court rather than in arbitration, you and Better Meal Plans&nbsp;each waive any right to a jury trial.</p>
<p>You and Better Meal Plans&nbsp;agree that each may bring claims against the other only in your or its individual capacity, and not as a plaintiff or class member in any purported class, consolidated, or representative action. Further, unless both you and Better Meal Plans&nbsp;agree otherwise, the arbitrator may not consolidate more than one person’s claims, and may not otherwise preside over any form of a representative or class proceeding. Notwithstanding the foregoing, this arbitration agreement does not preclude you from bringing issues to the attention of federal, state, or local agencies. Such agencies can, if the law allows, seek relief against us on your behalf. This entire arbitration provision will survive termination of this Agreement and the termination of your Better Meal Plans&nbsp;account.</p>
<p>To begin an arbitration proceeding, you must send a certified letter requesting arbitration and describing your claim to Vibrant Living Media LLC, PO Box 27414, Denver, CO 80227. The arbitration will be conducted by the American Arbitration Association (AAA) under its rules, including the Commercial Dispute Resolution Procedures and the Supplementary Procedures for Consumer Related Disputes of the AAA, as modified by this Agreement (collectively, "AAA Rules"). The AAA Rules and costs are available online at www.adr.org.</p>
<h2 class="subTitle">9. Assignment</h2>
<p>Better Meal Plans&nbsp;reserve the right to assign or transfer our rights and obligations under this Agreement. These terms are personal to you and, as a result, you may not, without the written consent of Better Meal Plans, assign or transfer any of your rights and obligations under this Agreement. There will be no third-party beneficiaries to this Agreement.</p>
<h2 class="subTitle">10. Severability</h2>
<p>In the event that any term of this Agreement is held to be invalid or unenforceable by a court of competent jurisdiction, the remainder of these terms will remain valid and enforceable. We can replace any term that is not valid and enforceable with a term of similar meaning, which is valid and enforceable.</p>
<h2 class="subTitle">11. Waiver</h2>
<p>Any failure by us to enforce any aspect of the terms of this Agreement will not affect our right to require performance at any subsequent time, nor will the waiver by us of any breach by you of any provisions of these terms be taken to be a waiver of the provision or provisions itself.</p>
<h2 class="subTitle">12. Complete Agreement</h2>
<p>This Agreement, including any terms, conditions and policies expressly referenced herein, together with any legal notices published on the Site, will constitute the complete understanding and agreement between you and Better Meal Plans, and will supersede and cancel any prior or contemporaneous understandings and agreements, except as expressly provided otherwise by Better Meal Plans.</p>
<h2 class="subTitle">13. Copyright Complaints</h2>
<p>Better Meal Plans&nbsp;respects the intellectual property rights of others and requires that users of our Site and Services do the same. U.S. copyright law does not protect recipes that include only listings of ingredients. However, Copyright protection may extend to a description, explanation, or illustration that accompanies a recipe or formula or to a combination of recipes such as in a cookbook. Should you have a copyright complaint, please contact our Customer Experience team.</p>
<h2 class="subTitle">Contacting&nbsp;Better Meal Plans</h2>
<p>For questions or comments regarding our Terms or Services, please contact the Better Meal Plans&nbsp;Customer Experience team by email at&nbsp;info@bettermealplans.com&nbsp;or by phone at 888-220-MY20 (6920).</p>
					</div>
					<input name="tos" value="1" id="tos" type="checkbox"> <label class="pmpro_normal pmpro_clickable" for="tos">I agree to the Terms of Service</label>
				</td>
			</tr>
		</tbody>
		</table>
		
	    <table id="pmpro_mailing_lists" class="pmpro_checkout top1em" width="100%" cellspacing="0" cellpadding="0" border="0">
        <thead>
        <tr>
            <th>
                Join our mailing list.            </th>
        </tr>
        </thead>
        <tbody>
        <tr class="odd">
            <td>
                                    <input id="additional_lists_1" name="additional_lists[]" value="01bee3ed6b" type="checkbox">
                    <label for="additional_lists_1" class="pmpro_normal pmpro_clickable">New Customer Training</label><br>
                                </td>
        </tr>
        </tbody>
    </table>
    
	
	<div class="pmpro_submit">
		
							<span id="pmpro_submit_span">
					<input name="submit-checkout" value="1" type="hidden"><input name="javascriptok" value="1" type="hidden">
					<input class="pmpro_btn pmpro_btn-submit-checkout" value="Submit and Check Out »" type="submit">
				</span>
				
		
		<span id="pmpro_processing_message" style="visibility: hidden;">
			Processing...		</span>
	<span id="pmpro_paypalexpress_checkout" style="display: none;">
		<input name="submit-checkout" value="1" type="hidden"><input name="javascriptok" value="1" type="hidden">		
		<input class="pmpro_btn-submit-checkout" value="Check Out with PayPal »" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" type="image">
	</span></div>

</form>


</div> <!-- end pmpro_level-ID -->



				<?php
				echo do_shortcode('[carousel title="What People Are Saying..."][slide image="https://informa.com/Global/Who%20we%20are/Management%20Team/Stephen-A-Carter.jpg" name="Stephen A Carter" occupation="Informa Global Manager"]Lorem ipsum dolor sit amet, his tota eripuit ea, per te ludus blandit. Ad paulo euripidis vix. Eum vidisse patrioque ad, ut admodum accusata conceptam vis. Nihil placerat constituto pri cu, eum principes splendide intellegebat ad. Pro at dolor equidem. Quo an nibh consulatu, te justo mucius theophrastus nam, eam partem qualisque necessitatibus ex.[/slide][slide image="https://p-airnz.com/cms/assets/NZ/nz-executive-jodie-king-700x622__Resampled.jpg" name="Jodie King" occupation="NZ Executive"]Est error essent concludaturque id. Eripuit perpetua partiendo et vim. Falli alterum moderatius mel ad, has in brute volumus. Laoreet appetere definiebas ius ut, ignota putant no pri. No legere blandit sea, pri eu omnium ponderum luptatum.[/slide][slide image="https://informa.com/Global/Who%20we%20are/Management%20Team/Stephen-A-Carter.jpg" name="Stephen A Carter" occupation="Informa Global Manager"]Lorem ipsum dolor sit amet, his tota eripuit ea, per te ludus blandit. Ad paulo euripidis vix. Eum vidisse patrioque ad, ut admodum accusata conceptam vis. Nihil placerat constituto pri cu, eum principes splendide intellegebat ad. Pro at dolor equidem. Quo an nibh consulatu, te justo mucius theophrastus nam, eam partem qualisque necessitatibus ex.[/slide][slide image="https://p-airnz.com/cms/assets/NZ/nz-executive-jodie-king-700x622__Resampled.jpg" name="Jodie King" occupation="NZ Executive"]Est error essent concludaturque id. Eripuit perpetua partiendo et vim. Falli alterum moderatius mel ad, has in brute volumus. Laoreet appetere definiebas ius ut, ignota putant no pri. No legere blandit sea, pri eu omnium ponderum luptatum.[/slide][/carousel]');
				
				echo do_shortcode('[satisfaction title="Your Satisfaction Is 100% Guaranteed." image="http://bettermealplans.com/wp-content/themes/20dishes/img/satisfaction.png"]If you\'re unhappy with a Better Meal Plans for any reason, just return it for a refund! Contact us at info@bettermealplans.com within 7 days of your purchase, and our friendly customer experience team will help you out.[/satisfaction]');
			?>
                
            </div>
        </article>
	</div>	
            
<?php
	get_footer();
?>