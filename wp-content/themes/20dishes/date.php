<?php
	get_header();	
?>

	<div class="beginpage container">
    	<?php get_template_part( 'section', 'subscribe' ); ?>   
    
    	<div class="blog-cat-title">
        	<?php
				if ( is_day() ) 
				{			
					// Day	
					echo 
						'Post from ' . 
							get_the_time( 'F' ) . ' ' . 
							get_the_time( 'd' ) . ', ' .
							get_the_time( 'Y' );

				} 
				elseif ( is_month() ) 
				{
					// Month
					echo 
						'Post from ' . 
							get_the_time( 'F' ) . ', ' . 
							get_the_time( 'Y' );
				} 
				elseif ( is_year() ) 
				{
					// Year
					echo 
						'Post from ' . 
							get_the_time( 'Y' );
				} 
			?>
        </div>
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <?php
					if ( have_posts() ) 
                    {	
						while ( have_posts() ) 
                        {
                            the_post();
							
							global $theme_options;
							
							// Thumb 
							$attachment_image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-thumb' );
							$attachment_image_thumb_url = $attachment_image_thumb[0];
							
							// Excerpt
							$excSource = get_the_excerpt();							
							
							// Title
							$title = get_the_title();
							?>					
    	                        <article id="post-<?php the_ID(); ?>" <?php post_class('blog-wrapper'); ?>>           
                                
                                	<a href="<?php the_permalink(); ?>">                                                
                                        <div class="blog-thumbnail" style="background-image:url(<?php echo $attachment_image_thumb_url; ?>);">
                                        </div>
                                    </a>
                                    
                                    <div class="blog-content">
                                        <div class="blog-title">
                                        	<h1 class="post-title">
                                                <a rel="bookmark" href="<?php the_permalink(); ?>">
                                                    <?php echo $title; ?>
                                                </a>
                                            </h1> 
                                        </div>
                                        <div class="blog-excerpt">
                                        	<?php echo $excSource; ?>
                                        </div>
                                        <div class="blog-meta clearfix">
                                        	<?php			
											 	$archiveDateLink = get_month_link( get_the_time('Y'), get_the_time('n') );
											?>
												 
                                           	<div class="blog-meta-left">
                                                <!--<a href="<?php echo $archiveDateLink; ?>" title="<?php the_date(); ?>" rel="bookmark">-->
                                                    <time class="entry-date" datetime="2014-01-01T11:11:11+00:00">
                                                        <?php echo get_the_date(); ?>
                                                    </time>
                                                <!--</a>-->
                                                
                                                
                                                <?php											 
                                                    _e( 'by ', 'jellypixel' );	
                                                    echo the_author_posts_link();
                                                ?>
                                            </div>
                                            <div class="blog-meta-right">
                                           		<?php
													edit_post_link( __( 'Edit', 'jellypixel' ), '<span class="edit-link">', '</span>' );
												?>
                                            </div>
                                        </div>
                                    </div>
                            	</article>
							<?php
						}
					}					
					else 
                    {
                        // No posts found
                        get_template_part( 'format','empty' );
                    }
                ?>
                
                <?php
                    get_template_part( 'section', 'paging' );
                ?>
            </div>        
            <div class="sidebar-right col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <aside role="complementary">
                    <?php get_sidebar( 'blog' ); ?>
                </aside>
            </div>        
        </div>
	</div>

<?php
	get_footer();
?>