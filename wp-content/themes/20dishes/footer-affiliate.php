<?php
 	$theme_options = get_option( 'theme_options' );
	$copyright_text = get_opt( $theme_options['copyright_text'], 'Better Meal Plans &copy; ' . date('Y') . ' Vibrant Living Media, LLC' );		
	$social_facebook = get_opt( $theme_options['social_facebook'], '' );
	$social_twitter = get_opt( $theme_options['social_twitter'], '' );
	$social_pinterest = get_opt( $theme_options['social_pinterest'], '' );
	$social_youtube = get_opt( $theme_options['social_youtube'], '' );
?>

	<div class="footer-advisor">
    	<div class="footer-top">        	
        	<?php dynamic_sidebar('footer-widget'); ?>            
        </div>
        <div class="footer-middle">
        	<?php
				if ( !empty( $social_facebook ) ) {
					echo '<a href="' . $social_facebook . '" class="footer-facebook"><div class="tdicon-facebook-circle"></div>Facebook</a>';
				}
				if ( !empty( $social_twitter ) ) {
					echo '<a href="' . $social_twitter . '" class="footer-twitter"><div class="tdicon-twitterbird-circle"></div>Twitter</a>';
				}
				if ( !empty( $social_pinterest ) ) {
					echo '<a href="' . $social_pinterest . '" class="footer-pinterest"><div class="tdicon-pinterest-circle"></div>Pinterest</a>';
				}
				if ( !empty( $social_youtube ) ) {
					echo '<a href="' . $social_youtube . '" class="footer-youtube"><div class="tdicon-youtube-circle"></div>Youtube</a>';
				}
			?>
        </div>
        <div class="footer-bottom">
        	<?php echo $copyright_text; ?>
        </div>
    </div>
 
<?php 
	wp_footer();
?>
</body>
</html>