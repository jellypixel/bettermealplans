<?php
	if ( !empty ( $subtitleStyle ) ) 
	{
		$subtitleStyleClass = 'subtitle-' . $subtitleStyle;	
	}
	else 
	{
		$subtitleStyleClass = '';
	}
?>

<div class="bmp-title-wrapper nomargin">
    <div class="bmp-title">
        <?php 
			if( !empty( $customTitle ) ) {
				echo $customTitle;
			}
			else {
				echo get_the_title(); 
			}
		?>
    </div>
    <div class="bmp-subtitle">
        <?php			
			if(!empty($customTitle))
				echo '<p>' . $customSubtitle . '</p>'; 
			else
				echo '<p>' . get_the_title() . '</p>'; 
		?>
    </div>
</div>

<?php
/*
<div class="general-wrapper nomargin clearfix">
    <div class="general-title <?php echo $subtitleStyleClass; ?>">
    	<?php
			if ( !empty( $customSubtitle ) ) 
			{		
				?>
                	<div class="bubble-title">
                    	<?php 
                            if( !empty( $customTitle ) ) {
                                echo $customTitle;
							}
                            else {
                                echo get_the_title(); 
							}
                        ?>                        
                    </div>
                    <div class="bubble-subtitle">
                    	<?php
							echo $customSubtitle; 						
						?>
                    </div>
                <?php			
			}
			else {
				?>
                    <div class="bubble">                        
						<?php 
                            if(!empty($customTitle))
                                echo $customTitle;
                            else
                                echo get_the_title(); 
                        ?>                        
                    </div>
                <?php
			}
        ?>
    </div>
</div>
*/
?>