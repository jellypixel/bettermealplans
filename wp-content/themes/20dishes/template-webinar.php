<?php
	/*
		Template Name: Webinar
	*/
	
	get_header();	
?>

    <style>
		.navbar, .logo, .page-bg, .nc_socialPanel, .general-wrapper, .footer {
			display: none;
		}
		
		.wssection1 {
			margin-top: 0px;
		}
	</style>

	<div class="beginpage webinarsalespage">
		<div class="wssection1">
        	<div class="wssection1-background" style="background-image:url(<?php the_field( 'section_1_image' ); ?>);">
            	<div class="wssection1-overlay">
                    <div class="wssection1-inner">
                    	<div class="container">
                            <div class="wssection1-line1">
                                <?php the_field( 'title_line_1' ); ?>
                            </div>
                            <div class="wssection1-line2">
                                <?php the_field( 'title_line_2' ); ?>
                            </div>
                            <div class="wssection1-line3">
                                <?php the_field( 'title_line_3' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="section2">
            	<div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">  
                    	<div class="section2-title">
							<?php
                                the_field( 'title' );
                            ?>
                        </div>                      
                        <div class="section2-content">
							<?php
                                the_field( 'content' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="section4">
            	<div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">
                        <div class="section4-list">
                        	<?php
								if( have_rows( 'features' ) )
								{
									?>
                                    	<ul>
											<?php
                                                while( have_rows( 'features') ) 
                                                {
                                                    the_row(); 
                                                    
                                                    echo '<li>' . get_sub_field( 'feature' ) . '</li>';
                                                }
                                            ?>
                                    	</ul>
                                    <?php
								}
							?>
                        </div>
                        <div class="section4-cta-desc">
                        	<?php the_field( 'call_to_action_description' ); ?>
                        </div>
                        <div class="section4-cta-button">
                        	<?php the_field( 'call_to_action_button' ); ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>   
            
<?php
	get_footer();
?>