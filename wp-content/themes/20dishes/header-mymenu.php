<?php
/**
 * The Header template for my menu
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Recipes
 * @since Recipes 1.0
 */  

	header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

/* CORPORATE ACCOUNT FUNCTION */
	if(!empty($_GET["ID"]) && !empty($_GET["ca"])) {
		
		// Add corporate account class and check if valid corporate account for the user
		$controller = new MPCA_Account_Controller();
		if(false === ($ca = $controller->validate_manage_sub_accounts_page()) ) { 
			wp_redirect ( home_url("/forbidden") ); exit;
		} else {
			// Check whether the current ID is manageable by this user
			$manageable = false;
			$accounts = $ca->sub_account_list_table('last_name','ASC',1,500);
			foreach($accounts['results'] as $account) {
				if($account->ID == $_GET["ID"]) {
					$manageable = true;
					break;
				}
			}

			if(!$manageable) {
				wp_redirect ( home_url("/forbidden") ); exit;
			}
		}
	}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	
	<?php
		wp_head(); 
		
		/* GENERAL THEME PREPARATION */
		$theme_options = get_option( 'theme_options' );
		$favicon = get_opt( $theme_options['branding_favicon'], '' );
		$logo = get_opt( $theme_options['branding_logo'], '' );	
		
		if ( !empty( $favicon ) && !empty( $favicon['url'] ) ) {
			echo '<link rel="shortcut icon" href="' . $favicon['url'] . '">';
		}
	?>
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" />
	
	<?php
		$ads_supported = current_user_can('memberpress_product_authorized_25969') && !current_user_can('administrator');
		if($ads_supported) {
	?>
		<script type="text/javascript">
			window._mNHandle = window._mNHandle || {};
			window._mNHandle.queue = window._mNHandle.queue || [];
			medianet_versionId = "3121199";
		</script>
		<script src="https://contextual.media.net/dmedianet.js?cid=8CUUOV852" async="async"></script>
	<?php
		}
	?>
	<?php
		/* MY MENU FUNCTION */
		
		// Globals
		global $recipeIngredients;
		global $user_id;
		global $user_info;
		global $userServingSize;
		global $fontPrintSize;
		global $allDiets;
		global $checkedPrint;
		global $userDiet;
		global $excludeList;
		global $userExcludeList;
		global $userRegistrationTime;
		
		global $userMenuRestriction;
		global $userRestrictCalories;
		global $userRestrictFat;
		global $userRestrictCarbs;		
		
		global $free_user;
		// Package
		global $breakfast;
		global $morearchive;
		
		$breakfast = false;
		$morearchive = false;
		
		if(!empty($_GET["ID"])) {
			$user_id = $_GET["ID"];
			$user_info = $_GET["ID"];
		} else {
			$user_id = wp_get_current_user()->ID;
			$user_info = get_userdata(wp_get_current_user()->ID);
		}
		
		// We used to have a free user in which they can only access the same menu.
		// It no longer exist but in case it is needed, have a mechanism here to detect the true and false of free user
		$free_user = false;

		// First Entry
		$firstentry = "false"; 		
		if( (get_user_meta($user_id, "firstentry", true)  === "") || (get_user_meta($user_id, "firstentry", true)  == "yes") ){
			 
			update_user_meta( $user_id, "firstentry", 'no' );
			$firstentry = "true";
		}
		
		// Print Option
		if(get_user_meta($user_id, "printoption", true)  === "") 
			add_user_meta( $user_id, "printoption", 'no', true ); 
		$checkedPrint = get_user_meta($user_id, "printoption", true); 
		
		// Serving Size
		if(get_user_meta($user_id, "servings", true) === "") 
			add_user_meta( $user_id, "servings", 4, true ); 
		$userServingSize = get_user_meta($user_id, "servings", true); 
		
		// Print Size
		if(get_user_meta($user_id, "fontprintsize", true) === "") 
			add_user_meta( $user_id, "fontprintsize", 12, true ); 
		$fontPrintSize = get_user_meta($user_id, "fontprintsize", true); 	
		
		// Diets
		$allDiets = $wpdb->get_results('SELECT * FROM jp_diet', ARRAY_A);
		if(get_user_meta($user_id, "diets", true) === "") 
			add_user_meta( $user_id, "diets", array_column($allDiets, 'id'), true ); 
		$userDiet = get_user_meta($user_id, "diets", false); 
		

		// Check special diets
		$userDiet = $userDiet[0]; $breakfastid = 7;
		
		if ($breakfast == false) {
			if(($key = array_search($breakfastid, $userDiet)) !== false) {
			    unset($userDiet[$key]);
			    update_user_meta( $user_id, "diets", $userDiet );
			}
		}
		else if ($breakfast == true) {	
			if((array_search($breakfastid, $userDiet)) == false) {
			    array_push($userDiet, $breakfastid);
			    update_user_meta( $user_id, "diets", $userDiet );
			} 
		}
		
		// Ingredient Exclusion Categories
		$excludeList = $wpdb->get_results('SELECT * FROM jp_excludelist', ARRAY_A);
		$userExcludeList = get_user_meta($user_id, "excludeList", false); 	
		$userExcludeList = $userExcludeList[0]; 
		
		// User Mealplan/Menu Restriction
		$userMenuRestriction = array();

		$getusermeta = get_user_meta( $user_id, 'restrict-addrecipe', false );
		$userMenuRestriction['addrecipe'] = !empty($getusermeta[0])? $getusermeta[0] : 'false';	

		$getusermeta = get_user_meta( $user_id, 'restrict-addnote', false );
		$userMenuRestriction['addnote'] = !empty($getusermeta[0])? $getusermeta[0] : 'false';	
		
		$getusermeta = get_user_meta( $user_id, 'restrict-removerecipe', false );
		$userMenuRestriction['removerecipe'] = !empty($getusermeta[0])? $getusermeta[0] : 'false';	
		
		$getusermeta = get_user_meta( $user_id, 'restrict-startfromscratch', false );
		$userMenuRestriction['startfromscratch'] = !empty($getusermeta[0])? $getusermeta[0] : 'false';		
		
		$getusermeta = get_user_meta( $user_id, 'restrict-portion', false );
		$userMenuRestriction['portion'] = !empty($getusermeta[0])? $getusermeta[0] : 'false';
		
		$getusermeta = get_user_meta( $user_id, 'restrict-diet', false );
		$userMenuRestriction['diet'] = !empty($getusermeta[0])? $getusermeta[0] : 'false';
		
		$getusermeta = get_user_meta( $user_id, 'restrict-avoidfood', false );
		$userMenuRestriction['avoidfood'] = !empty($getusermeta[0])? $getusermeta[0] : 'false';
		
		$getusermeta = get_user_meta( $user_id, 'restrict-excludeadditionalingredients', false );
		$userMenuRestriction['excludeadditionalingredients'] = !empty($getusermeta[0])? $getusermeta[0] : 'false';

		// User Mealplan/Menu Restriction - Calories, Fat, Carbs		
		if(get_user_meta( $user_id, "restrict-calories", true) === "") 
		{
			add_user_meta( $user_id, "restrict-calories", 0, true ); 
		}
		$userRestrictCalories = get_user_meta($user_id, "restrict-calories", true); 	

		if(empty($userRestrictCalories))
			$userRestrictCalories = 100000;

		if(get_user_meta( $user_id, "restrict-fat", true) === "") 
		{
			add_user_meta( $user_id, "restrict-fat", 0, true ); 
		}
		$userRestrictFat= get_user_meta($user_id, "restrict-fat", true); 	

		if(empty($userRestrictFat))
			$userRestrictFat = 100000;
		
		if(get_user_meta( $user_id, "restrict-carbs", true) === "") 
		{
			add_user_meta( $user_id, "restrict-carbs", 0, true ); 
		}
		$userRestrictCarbs = get_user_meta($user_id, "restrict-carbs", true); 
		
		if(empty($userRestrictCarbs))
			$userRestrictCarbs = 100000;
		
		// Menu
		$tmp_registrationTime = get_userdata($user_id)->user_registered;
		$tmp_thirtydays = 'NOW() - INTERVAL 30 DAY';
		$tmp_threedays = 'NOW() + INTERVAL 3 DAY';
		
		if($free_user) {
			$tmp_registrationTime = '\'1995-05-29\'';
			$tmp_thirtydays = '\'1995-04-29\'';
			$tmp_threedays = '\'1995-06-01\'';
		}
		
		$userRegistrationTime = date( "Y-m-d h:i:s", strtotime( $tmp_registrationTime . ' - 2 week'));
		$existingMenu = $wpdb->get_results( "SELECT DISTINCT GROUP_CONCAT(id) as 'existing' FROM jp_menu WHERE schedule >= (".$tmp_thirtydays.") AND schedule <= (".$tmp_threedays.") AND id NOT IN (SELECT menu_id FROM jp_usermenu WHERE user_id = " . $user_id . " AND insertdate >= (".$tmp_thirtydays.")) AND shared = 'Y'");
		
		if(!empty($existingMenu[0]->existing)) {
			$leftOverMenu = $wpdb->get_results( "INSERT INTO jp_userschedule(user_id, recipe_id, menu_id, tanggal, orders, notes, updates) 
												 SELECT ".$user_id.", b.recipe_id, a.id, DATE_ADD(a.schedule,INTERVAL b.day-1 DAY), b.orders, b.notes, a.updates
												 FROM   jp_menu a, jp_menu_recipe b
												 WHERE  a.shared = 'Y' AND a.id = b.menu_id AND 
														a.id IN (".$existingMenu[0]->existing.") AND
														a.schedule >= (".$tmp_thirtydays.") AND
														a.schedule <= (".$tmp_threedays.")
												");	
												
			$menuUpdate = $wpdb->get_results ("DELETE FROM jp_usermenu WHERE user_id = " . $user_id . " AND insertdate < (".$tmp_thirtydays.")");	
			
			$menuUpdate = $wpdb->get_results ("INSERT INTO jp_usermenu(user_id, menu_id, insertdate)
											   SELECT DISTINCT ". $user_id ." as 'user_id', menu_id, now() as 'insertdate' FROM jp_userschedule WHERE user_id = " . $user_id . " AND tanggal >= (".$tmp_thirtydays.") AND tanggal <= (".$tmp_threedays.")");							
		}								
													
	?>	

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
    <script type="text/javascript">
		var $j = jQuery.noConflict();
		
		var firstentry = <?php echo $firstentry; ?>;
		var userid = <?php echo $user_id; ?>;
		var checkedPrint = '<?php echo $checkedPrint; ?>';
		
		<?php if($free_user) { ?>
			var freeUser = true;
		<?php } else  { ?>
			var freeUser = false;
		<?php } ?>
		
		$j(window).load(function() { // makes sure the whole site is loaded
			$j('#status').fadeOut(); // will first fade out the loading animation
			$j('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
			$j('body').delay(350).css({'overflow':'visible'});
			
			if ( freeUser )
			{
				var overlay3El = jQuery(".overlay-3");
				var freeuserLightbox = jQuery("#freeuser-lightbox");
				
				overlay3El.fadeIn(300);
				freeuserLightbox.fadeIn(300);	
			}
		})
			
	</script>	
	
</head>
<body <?php body_class();?>>
	
	    <div id="preloader">
            <div id="status">Please wait while we create the perfect meal plan for you..</div>
        </div>
    
    <?php		
        
		$bgClass = "page-bg";
		$logoClass = "page-logo";
		
    ?>
 
        <!--
        <div class="<?php echo $bgClass; ?>">    	
        </div>    
        -->
        
        <!-- Menu -->
        <nav class="navbar yamm navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">           	
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".primarymenu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>                
                </div>          
                
                <div class="collapse navbar-collapse primarymenu">
                	  
						<div class="menu-logo">
							<div class="menu-logo-inner">
								<a href="<?php bloginfo( 'url' ); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" class="img-responsive" alt="logo" />
											<div class="menu-logo-subtitle">
												Formerly 20 Dishes
											</div>
										</a> 
							</div>
						</div>  	
                
                    <?php 
                        
                    if ( is_user_logged_in() )
					{
						if($free_user) {
							
							/* Free Member navigation */
	                        wp_nav_menu( array(
	                            'menu' => 'freemember_menu',
	                            'depth' => 5,
	                            'theme_location' => 'freemembermenu-location',
	                            'container' => false,
	                            'menu_class' => ' nav navbar-nav',
	                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 
	                            'walker' => new wp_bootstrap_navwalker())
	                        );
	                        
						} else {
							
							/* Member navigation */
	                        wp_nav_menu( array(
	                            'menu' => 'member_menu',
	                            'depth' => 5,
	                            'theme_location' => 'membermenu-location',
	                            'container' => false,
	                            'menu_class' => ' nav navbar-nav',
	                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 
	                            'walker' => new wp_bootstrap_navwalker())
	                        );
	                        
                        }
					}
					else
					{
						/* Primary navigation */
                        wp_nav_menu( array(
                            'menu' => 'top_menu',
                            'depth' => 5,
                            'theme_location' => 'topmenu-location',
                            'container' => false,
                            'menu_class' => ' nav navbar-nav',
                            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 
                            'walker' => new wp_bootstrap_navwalker())
                        );
					}
					
                   	 if( is_user_logged_in() )
					{
						?>
							<div class="avatar-wrapper-outer">
								<ul class="nav navbar-nav avatar-wrapper logout">
									<!--
									<li class="dropdown">
										<a href="#"><?php echo get_avatar( get_current_user_id() ); ?></a>                                            
										<ul class="dropdown-menu">											
											<li>
												<a href="<?php echo home_url();?>/my-account/" >View my profile</a>
											</li>
                                            
										</ul>
									</li>
									-->
									<li>
										<a href="<?php echo home_url();?>/member-profile/" class="try_now" >Your Account</a>
									</li>
									<li>                                    
										<a href="<?php echo wp_logout_url(); ?>" class="log_out" >Log out</a>
									</li>
								</ul>
							</div>
						<?php	
					} 
					else 
					{						
						?>
							<div class="avatar-wrapper-outer">
								<ul class="nav navbar-nav avatar-wrapper login">
									<li>                                    
										<a href="<?php echo wp_login_url(); ?>" class="log_in" >Log in</a>
									</li>
									<li>                                    
										<a href="/#selectplan" class="try_now" >Try Now!<span class="trysmall">(Free For 7 Days)</span></a>
									</li>
								</ul>
							</div>
						<?php
					}
                    ?>            
                </div>        
                   
            </div>
        </nav>
        
        <!--
        <div class="logo <?php echo $logoClass; ?>">
	        
	        <?php
		    if(!$free_user) {
		    ?>
	            <?php 
	                if ( !empty( $logo ) && !empty( $logo['url'] ) ) {				
	                    $logo_url = $logo['url'];
	                }
	                else {
	                    $logo_url = get_bloginfo('template_url') . "/img/logo.png";
	                }
	            ?>
	            <a href="<?php bloginfo('url'); ?>"><img src="<?php echo $logo_url; ?>" class="img-responsive" alt="logo" /></a> 
	        <?php
		    } else {
			    $tmp_expiredTime = strtotime(get_userdata($user_id)->user_registered);
			    $tmp_expiredTime = strtotime("+7 day", $tmp_expiredTime);
		    ?>
			    <script>
					var $j = jQuery.noConflict();
					
					$j(function(){
						var clock;
					
						var currentDate = new Date();
						var futureDate  = <?php echo $tmp_expiredTime; ?>;
						var diff = futureDate - currentDate.getTime() / 1000;
					
						clock = $j('.logo-counter').FlipClock(diff, {
							clockFace: 'DailyCounter',
							countdown: true
						});
					});
				</script>
				<div class="bubble-title premium">
                    Your free trial will expired in..                        
                </div>
				<div class="logo-counter"> </div>
				<a href="/subscribe"><div class="reloadbutton premium">GET PREMIUM NOW</div></a>
		    <?php
		    }
		    ?>
        </div>
       -->
       
       	<?php
            if(!is_user_logged_in())
            {
        ?>
                <script>
                    $j(function() 
                    {
                        $j("#menu-item-177").click(function()
                        {
                            loadPopup();
                            $j(".tutorial").hide();
                        });
                        
                        $j("div.close").hover(
                            function() {
                                $j('span.ecs_tooltip').show();
                            },
                            function () {
                                $j('span.ecs_tooltip').hide();
                            }
                        );
        
                        $j("div.close").click(function() {
                            disablePopup();  // function close pop up
                        });
        
                        $j(this).keyup(function(event) {
                            if (event.which == 27) { // 27 is 'Ecs' in the keyboard
                                disablePopup();  // function close pop up
                            }  	
                        });
        
                        $j("div#backgroundPopup").click(function() {
                            disablePopup();  // function close pop up
                        });
        
                        $j('a.livebox').click(function() 
                        {
                            alert('Hello World!');
                            return false;
                        });
        
    
                        /************** start: functions. **************/
                        function loading() {
                            $j("div.loader").show();  
                        }
                        function closeloading() {
                            $j("div.loader").fadeOut('normal');  
                        }
                        
                        var popupStatus = 0; // set value
                        
                        function loadPopup() { 
                            if(popupStatus == 0) { // if value is 0, show popup
                                closeloading(); // fadeout loading
                                $j("#toPopup").fadeIn(0500); // fadein popup div
                                $j("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
                                $j("#backgroundPopup").fadeIn(0001); 
                                popupStatus = 1; // and set value to 1
                            }	
                        }
                            
                        function disablePopup() {
                            if(popupStatus == 1) { // if value is 1, close popup
                                $j("#toPopup").fadeOut("normal");  
                                $j("#backgroundPopup").fadeOut("normal");  
                                popupStatus = 0;  // and set value to 0
                            }
                        }
                    });
                </script>
    
                <!--<a href="#" class="topopup">Click Here Trigger</a>-->
                <?php //if( is_front_page()) {        
                ?> 
                <div id="toPopup"> 
            
                    <div class="close"></div>
                    <span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
                    <div id="popup_content"> <!--your content start-->		
                        <div class="col-lg-12 col-xs-12 col-sm-12 text-center">
                            <h4>Recipes</h4>
                            <?php
                                if ( ! is_user_logged_in() ) { // Display WordPress login form:
                                    $args = array(
                                        'redirect' => home_url(), 
                                        'form_id' => 'loginform-custom',
                                        'label_username' => __( 'Username' ),
                                        'label_password' => __( 'Password' ),
                                        'label_remember' => __( 'Remember Me ' ),
                                        'label_log_in' => __( 'Log In' ),
                                        'remember' => true
                                    );
                                    wp_login_form( $args );
                                } 
                            ?>
                            <p><a href="<?php echo home_url();?>/sign-in/?action=forgot_password">Did you forget the password?</a></p>
                            <p> Not a member? <a href="<?php echo home_url();?>">Join Now!</a></p> 
                        </div>           
                    </div> <!--your content end-->    
                </div> <!--toPopup end-->
        
                <div class="loader"></div>
                <div id="backgroundPopup"></div>
        <?php
            }
        ?>
        
        <script>
            
        
            $j(document).ready(function()
            {
                $j(".log_out").click(function()
                {
                    data = { action:'log_out'};
                    $j.post( '<?php echo home_url();?>/wp-admin/admin-ajax.php',
                            data,
                            function(data)
                            {
                                window.location.href='<?php echo home_url();?>';
                            });
                });
            });	
        </script>
        
        <script type='text/javascript'>//<![CDATA[ 
            $j('body').on('touchstart.menu-main-menu-container', '.sub-menu', function (e) { 
                e.stopPropagation(); 
            });
        </script>
    
        <script>
            $j(function() {
                $j("#menu-item-177").click(function()
                {
                    loadPopup();
                    $j(".tutorial").hide();
                });
                
                $j("div.close").hover(
                    function() {
                        $j('span.ecs_tooltip').show();
                    },
                    function () {
                        $j('span.ecs_tooltip').hide();
                    }
                );
            
                $j("div.close").click(function() 
                {
                    disablePopup();  // function close pop up
                });
                
                $j(this).keyup(function(event) 
                {
                    if (event.which == 27) { // 27 is 'Ecs' in the keyboard
                        disablePopup();  // function close pop up
                    }  	
                });
                
                $j("div#backgroundPopup").click(function() 
                {
                    disablePopup();  // function close pop up
                });
                
                $j('a.livebox').click(function() 
                {
                    alert('Hello World!');
                    return false;
                });			
            
                /************** start: functions. **************/
                function loading() 
                {
                    $j("div.loader").show();  
                }
                function closeloading() 
                {
                    $j("div.loader").fadeOut('normal');  
                }
                
                var popupStatus = 0; // set value
                
                function loadPopup() 
                { 
                    if(popupStatus == 0) 
                    { // if value is 0, show popup
                        closeloading(); // fadeout loading
                        $j("#toPopup").fadeIn(0500); // fadein popup div
                        $j("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
                        $j("#backgroundPopup").fadeIn(0001); 
                        popupStatus = 1; // and set value to 1
                    }	
                }
                    
                function disablePopup() 
                {
                    if ( popupStatus == 1) 
                    { // if value is 1, close popup
                        $j("#toPopup").fadeOut("normal");  
                        $j("#backgroundPopup").fadeOut("normal");  
                        popupStatus = 0;  // and set value to 0
                    }
                }
            });
        </script>
        
       