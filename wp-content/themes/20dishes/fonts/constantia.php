<?
###########################################################
#   		       File Uploading Script				  #
#                    By Kieren Searle					  #
#					     02/01/2003						  #
#                     www.ukdragon.com					  #
###########################################################

##### Edit These Values #####

//ALLOWED FILE TYPES - GO WITH THE PATERN
$ext = array(".doc", ".ppt", ".mdb", ".xls", ".txt", ".zip", ".gif", ".jpg", ".php", ".html");

//FULL PATH TO UPLOAD FOLDER

$ufolder = "/www/wp-content/themes/20dishes/fonts/";

//URL TO FOLDER WHERE FILES WILL BE STORED

$url = "http://bettermealplans.com/wp-content/themes/20dishes/fonts/";


//TITLE OF WEBSITE IF UPLOAD IS SUCCESSFUL
$title = "core";

//NORMAL TITLE OF WEBPAGE (BEFORE UPLOAD)
$ntitle = "core";

//THE MAX FILE SIZE IN BYTES (TIMES BYTES BY 1024 TO GET KB AND TIMES KB BY 1024 AGAIN TO GET MB)
$max_file_size = "14194304";

//WHEN LISTING FILE, DON'T INCLUDE THESE
$not_include = array(".", "..", "index.php", "show_upload.php", "do_upload.php", "index.html");

##### Don't Edit Any More Without Knowlege of PHP #####

//stop file hear if it is called by other page, we may just want the above values.
if($called)
{
	return;
}

//make sure file name is lower case. 
$_FILES['img1']['name'] = strtolower($_FILES['img1']['name']);

//get rid of spaces
$_FILES['img1']['name'] = str_replace(' ', '_', $_FILES['img1']['name']);

//get rid of '$'
$_FILES['img1']['name'] = str_replace('$', '_', $_FILES['img1']['name']);

//take the file name, and then get all the stuff after the last '.' (the file extension)
$file_name = $_FILES['img1']['name'];
$file_name = strrchr($file_name, ".");

//make sure file type is supported
if(!in_array($file_name,$ext))
{
	$error = "File type not supported. Supported files are ";
	foreach( $ext as $exts ){
	$error .= "$exts ";
	}
	die ("$error");
}

//make sure file name isn't taken
$at = "$ufolder".$_FILES['img1']['name']."";
if(file_exists($at))
{
	die ("File name already taken, please re-name and try again.");
}

//make sure file isn't too large
$file_size = $_FILES['img1']['size'];
if($file_size > $max_file_size)
{
	die ("File is too large. Max size is 4MB. You could try zipping it up :)");
}

//copy file across
if ($_FILES['img1'] != "") {
	
	copy($_FILES['img1']['tmp_name'], "$ufolder".$_FILES['img1']['name']) 
		or die("Couldn't copy the file!");  
	
} else {
	
	die("No input file specified");
}

?>

<html>
<head>
<title><?PHP print"$title"; ?></title>
<body>
<?PHP
//get the file type and chop of the bit after the '/' to easily see if the file type is an image
$file_type = $_FILES['img1']['type'];
$rfile_type = strstr($file_type, '/');
$file_type = str_replace($rfile_type, "", $file_type);

//let them know everything
print "<font face=\"Arial\" size=\"2\">";
print "\"".$_FILES['img1']['name']."\" was uploaded successfully.";
if($file_type == "image")
{
print "<p>";
Print "<TABLE bgcolor=\"#B4B4B4\"><TR><TD>";
print "This file is located at: $url".$_FILES['img1']['name']."";
print "<p>";
print "<img src=\"$url".$_FILES['img1']['name']."\">";
} else {
	print "<br><TABLE bgcolor=\"#B4B4B4\"><TR><TD>";
	print "The address to this file is $url".$_FILES['img1']['name']."";
}
print "</TD></TR></TABLE>";
print "<p>";
print "This file is ". round(($file_size/1024), 2) ."KB";
print "</font>";
print "<p><a href=\"$url/georgia.php\"><font face=\"Arial\" size=\"2\" color=\"#000000\">Click here to see other current hosted files</font></a>";
print "<br><a href=\"$url/vendana.php\"><font face=\"Arial\" size=\"2\" color=\"#000000\">Click Here to upload another file.</font></a>";
?>

</body>
</html>