<?php
	/*
        Template Name: Landing Page
	*/	

	get_header();	
?>
    <script>
		jQuery(document).ready(function(){
			// Add trust symbols to accessally
			var trustSymbols = $j(".accessally-order-form-item-desc").html();
			/*$j(trustSymbols).appendTo(".accessally-order-form-step-1");*/
			$j(trustSymbols).prependTo(".accessally-order-form-guarantee-section");
			
			/*$j(".accessally-order-form-item-desc").html("");*/
			
		});
	</script>
	
	<style>
		.hd-seal {
			display:inline-block;
			padding-top: 10px;
			vertical-align: middle;
			padding-right:8px;
			padding-left:8px;
			margin-top: 10px;
			margin-bottom: 10px;
		}
		
		.sealimage {
			margin-top: 5px;
			margin-bottom:5px;
			float:none;
			display: inline-block;
		}
		
		.trustsymbols {
			display: inline-block;
			text-align: center;
			width: 100%;
			margin-top: 0px;
		}
		
		.accessally-order-form-guarantee-section {
			text-align: center;
		}
		
		.accessally-order-form-guarantee-section .trustsymbols {
			margin-top: 0px;
			margin-bottom: 15px;
		}
		
		.accessally-order-form-guarantee-section .wp-image-14691, .accessally-order-form-guarantee-section .accessally-order-form-guarantee-image {
			display: none;
		}
	</style>

	<div class="beginpage container">
    	<?php 
			include(locate_template('section-title-for-white-background.php'));
		?>    
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>     
            <div class="post-content default-page">
                <?php
                    // The Content					
					
                    if ( have_posts() ) 
                    {					
                        while ( have_posts() ) 
                        {
                            the_post();
                            the_content();
                        }
                    } 
                    
                    // Reset
                    wp_reset_query();			
                ?>                
            </div>
        </article>        
	</div>	
 
	<style>
		.navbar, .logo, .page-bg, .nc_socialPanel, .general-wrapper {
			display: none;
		}
	</style>