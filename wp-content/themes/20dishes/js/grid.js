var $jj = jQuery.noConflict();

/*
* debouncedresize: special jQuery event that happens once after a window resize
*
* latest version and complete README available on Github:
* https://github.com/louisremi/jquery-smartresize/blob/master/jquery.debouncedresize.js
*
* Copyright 2011 @louis_remi
* Licensed under the MIT license.
*/
var $jevent = $j.event,
$jspecial,
resizeTimeout;

$jspecial = $jevent.special.debouncedresize = {
	setup: function() {
		$j( this ).on( "resize", $jspecial.handler );
	},
	teardown: function() {
		$j( this ).off( "resize", $jspecial.handler );
	},
	handler: function( event, execAsap ) {
		// Save the context
		var context = this,
			args = arguments,
			dispatch = function() {
				// set correct event type
				event.type = "debouncedresize";
				$jevent.dispatch.apply( context, args );
			};

		if ( resizeTimeout ) {
			clearTimeout( resizeTimeout );
		}

		execAsap ?
			dispatch() :
			resizeTimeout = setTimeout( dispatch, $jspecial.threshold );
	},
	threshold: 250
};

// ======================= imagesLoaded Plugin ===============================
// https://github.com/desandro/imagesloaded

// $j('#my-container').imagesLoaded(myFunction)
// execute a callback when all images have loaded.
// needed because .load() doesn't work on cached images

// callback function gets image collection as argument
//  this is the container

// original: MIT license. Paul Irish. 2010.
// contributors: Oren Solomianik, David DeSandro, Yiannis Chatzikonstantinou

// blank image data-uri bypasses webkit log warning (thx doug jones)
var BLANK = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

$j.fn.imagesLoaded = function( callback ) {
	var $jthis = this,
		deferred = $j.isFunction($j.Deferred) ? $j.Deferred() : 0,
		hasNotify = $j.isFunction(deferred.notify),
		$jimages = $jthis.find('img').add( $jthis.filter('img') ),
		loaded = [],
		proper = [],
		broken = [];

	// Register deferred callbacks
	if ($j.isPlainObject(callback)) {
		$j.each(callback, function (key, value) {
			if (key === 'callback') {
				callback = value;
			} else if (deferred) {
				deferred[key](value);
			}
		});
	}

	function doneLoading() {
		var $jproper = $j(proper),
			$jbroken = $j(broken);

		if ( deferred ) {
			if ( broken.length ) {
				deferred.reject( $jimages, $jproper, $jbroken );
			} else {
				deferred.resolve( $jimages );
			}
		}

		if ( $j.isFunction( callback ) ) {
			callback.call( $jthis, $jimages, $jproper, $jbroken );
		}
	}

	function imgLoaded( img, isBroken ) {
		// don't proceed if BLANK image, or image is already loaded
		if ( img.src === BLANK || $j.inArray( img, loaded ) !== -1 ) {
			return;
		}

		// store element in loaded images array
		loaded.push( img );

		// keep track of broken and properly loaded images
		if ( isBroken ) {
			broken.push( img );
		} else {
			proper.push( img );
		}

		// cache image and its state for future calls
		$j.data( img, 'imagesLoaded', { isBroken: isBroken, src: img.src } );

		// trigger deferred progress method if present
		if ( hasNotify ) {
			deferred.notifyWith( $j(img), [ isBroken, $jimages, $j(proper), $j(broken) ] );
		}

		// call doneLoading and clean listeners if all images are loaded
		if ( $jimages.length === loaded.length ){
			setTimeout( doneLoading );
			$jimages.unbind( '.imagesLoaded' );
		}
	}

	// if no images, trigger immediately
	if ( !$jimages.length ) {
		doneLoading();
	} else {
		$jimages.bind( 'load.imagesLoaded error.imagesLoaded', function( event ){
			// trigger imgLoaded
			imgLoaded( event.target, event.type === 'error' );
		}).each( function( i, el ) {
			var src = el.src;

			// find out if this image has been already checked for status
			// if it was, and src has not changed, call imgLoaded on it
			var cached = $j.data( el, 'imagesLoaded' );
			if ( cached && cached.src === src ) {
				imgLoaded( el, cached.isBroken );
				return;
			}

			// if complete is true and browser supports natural sizes, try
			// to check for image status manually
			if ( el.complete && el.naturalWidth !== undefined ) {
				imgLoaded( el, el.naturalWidth === 0 || el.naturalHeight === 0 );
				return;
			}

			// cached images don't fire load sometimes, so we reset src, but only when
			// dealing with IE, or image is complete (loaded) and failed manual check
			// webkit hack from http://groups.google.com/group/jquery-dev/browse_thread/thread/eee6ab7b2da50e1f
			if ( el.readyState || el.complete ) {
				el.src = BLANK;
				el.src = src;
			}
		});
	}

	return deferred ? deferred.promise( $jthis ) : $jthis;
};

var Grid = (function() {

		// list of items
	var $jgrid = $j( '#og-grid' ),
		// the items
		$jitems = $jgrid.children( 'li' ),
		// current expanded item's index
		current = -1,
		// position (top) of the expanded item
		// used to know if the preview will expand in a different row
		previewPos = -1,
		// extra amount of pixels to scroll the window
		scrollExtra = 0,
		// extra margin when expanded (between preview overlay and the next items)
		marginExpanded = 10,
		$jwindow = $j( window ), winsize,
		$jbody = $j( 'html, body' ),
		// transitionend events
		transEndEventNames = {
			'WebkitTransition' : 'webkitTransitionEnd',
			'MozTransition' : 'transitionend',
			'OTransition' : 'oTransitionEnd',
			'msTransition' : 'MSTransitionEnd',
			'transition' : 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		// support for csstransitions
		support = Modernizr.csstransitions,
		// default settings
		settings = {
			minHeight : 500,
			speed : 350,
			easing : 'ease'
		};

	function init( config ) {
		
		// the settings..
		settings = $j.extend( true, {}, settings, config );

		// preload all images
		$jgrid.imagesLoaded( function() {

			// save item´s size and offset
			saveItemInfo( true );
			// get window´s size
			getWinSize();
			// initialize some events
			initEvents();

		} );

	}

	// add more items to the grid.
	// the new items need to appended to the grid.
	// after that call Grid.addItems(theItems);
	function addItems( $jnewitems ) {

		$jitems = $jitems.add( $jnewitems );

		$jnewitems.each( function() {
			var $jitem = $j( this );
			$jitem.data( {
				offsetTop : $jitem.offset().top,
				height : $jitem.height()
			} );
		} );

		initItemsEvents( $jnewitems );

	}

	// saves the item´s offset top and height (if saveheight is true)
	function saveItemInfo( saveheight ) {
		$jitems.each( function() {
			var $jitem = $j( this );
			$jitem.data( 'offsetTop', $jitem.offset().top );
			if( saveheight ) {
				$jitem.data( 'height', $jitem.height() );
			}
		} );
	}

	function initEvents() {
		
		// when clicking an item, show the preview with the item´s info and large image.
		// close the item if already expanded.
		// also close if clicking on the item´s cross
		initItemsEvents( $jitems );
		
		// on window resize get the window´s size again
		// reset some values..
		$jwindow.on( 'debouncedresize', function() {
			
			scrollExtra = 0;
			previewPos = -1;
			// save item´s offset
			saveItemInfo();
			getWinSize();
			var preview = $j.data( this, 'preview' );
			if( typeof preview != 'undefined' ) {
				hidePreview();
			}

		} );

	}

	function initItemsEvents( $jitems ) {
		$jitems.on( 'click', 'span.og-close', function() {
			hidePreview();
			return false;
		} ).children( 'a' ).on( 'click', function(e) {

			var $jitem = $j( this ).parent();
			// check if item already opened
			current === $jitem.index() ? hidePreview() : showPreview( $jitem );
			return false;

		} );
	}

	function getWinSize() {
		winsize = { width : $jwindow.width(), height : $jwindow.height() };
	}

	function showPreview( $jitem ) {

		var preview = $j.data( this, 'preview' ),
			// item´s offset top
			position = $jitem.data( 'offsetTop' );

		scrollExtra = 0;

		// if a preview exists and previewPos is different (different row) from item´s top then close it
		if( typeof preview != 'undefined' ) {

			// not in the same row
			if( previewPos !== position ) {
				// if position > previewPos then we need to take te current preview´s height in consideration when scrolling the window
				if( position > previewPos ) {
					scrollExtra = preview.height;
				}
				hidePreview();
			}
			// same row
			else {
				preview.update( $jitem );
				return false;
			}
			
		}

		// update previewPos
		previewPos = position;
		// initialize new preview for the clicked item
		preview = $j.data( this, 'preview', new Preview( $jitem ) );
		// expand preview overlay
		preview.open();

	}

	function hidePreview() {
		current = -1;
		var preview = $j.data( this, 'preview' );
		preview.close();
		$j.removeData( this, 'preview' );
	}

	// the preview obj / overlay
	function Preview( $jitem ) {
		this.$jitem = $jitem;
		this.expandedIdx = this.$jitem.index();
		this.create();
		this.update();
	}

	Preview.prototype = {
		create : function() {
			// create Preview structure:
			
			//TWI
			/*
			this.$jtitle = $j( '<h3></h3>' );
			this.$jdescription = $j( '<p></p>' );
			this.$jhref = $j( '<a href="#">Visit website</a>' );
			this.$jdetails = $j( '<div class="og-details"></div>' ).append( this.$jtitle, this.$jdescription, this.$jhref );
			this.$jloading = $j( '<div class="og-loading"></div>' );
			this.$jfullimage = $j( '<div class="og-fullimg"></div>' ).append( this.$jloading );
			this.$jclosePreview = $j( '<span class="og-close"></span>' );
			this.$jpreviewInner = $j( '<div class="og-expander-inner"></div>' ).append( this.$jclosePreview, this.$jfullimage, this.$jdetails );
			this.$jpreviewEl = $j( '<div class="og-expander"></div>' ).append( this.$jpreviewInner );
			*/
			
			this.$jtitle = $j( '<h3></h3>' );
			this.$jdescription = $j( '<p></p>' );
			this.$jhref = $j( '<a href="#">Visit website</a>' );
			this.$jdetails = $j( '<div class="og-details container"></div>' ).append( this.$jdescription );
			/*this.$jloading = $j( '<div class="og-loading"></div>' );
			this.$jfullimage = $j( '<div class="og-fullimg"></div>' ).append( this.$jloading );*/
			this.$jclosePreview = $j( '<span class="og-close"></span>' );
			this.$jpreviewInner = $j( '<div class="og-expander-inner"></div>' ).append( this.$jclosePreview, this.$jdetails );
			this.$jpreviewEl = $j( '<div class="og-expander"></div>' ).append( this.$jpreviewInner );
			
			
			// append preview element to the item
			this.$jitem.append( this.getEl() );
			// set the transitions for the preview and the item
			if( support ) {
				this.setTransition();
			}
		},
		update : function( $jitem ) {

			if( $jitem ) {
				this.$jitem = $jitem;
			}
			
			// if already expanded remove class "og-expanded" from current item and add it to new item
			if( current !== -1 ) {
				var $jcurrentItem = $jitems.eq( current );
				$jcurrentItem.removeClass( 'og-expanded' );
				this.$jitem.addClass( 'og-expanded' );
				// position the preview correctly
				this.positionPreview();
			}

			// update current value
			current = this.$jitem.index();

			// update preview´s content
			var $jitemEl = this.$jitem.children( 'a' ),
				eldata = {
					href : $jitemEl.attr( 'href' ),
					largesrc : $jitemEl.data( 'largesrc' ),
					title : $jitemEl.data( 'title' ),
					description : $jitemEl.data( 'description' )
				};

			this.$jtitle.html( eldata.title );
			
			// TWI
			//this.$jdescription.html( eldata.description );			
			this.$jdescription.html( decodeURIComponent( eldata.description ) );
			
			this.$jhref.attr( 'href', eldata.href );

			var self = this;
			
			// remove the current image in the preview
			if( typeof self.$jlargeImg != 'undefined' ) {
				self.$jlargeImg.remove();
			}

			// preload large image and add it to the preview
			// for smaller screens we don´t display the large image (the media query will hide the fullimage wrapper)
			
			// TWI
			/*
			if( self.$jfullimage.is( ':visible' ) ) {
				this.$jloading.show();
				$j( '<img/>' ).load( function() {
					var $jimg = $j( this );
					if( $jimg.attr( 'src' ) === self.$jitem.children('a').data( 'largesrc' ) ) {
						self.$jloading.hide();
						self.$jfullimage.find( 'img' ).remove();
						self.$jlargeImg = $jimg.fadeIn( 350 );
						self.$jfullimage.append( self.$jlargeImg );
					}
				} ).attr( 'src', eldata.largesrc );	
			}
			*/
		},
		open : function() {

			setTimeout( $j.proxy( function() {	
				// set the height for the preview and the item
				this.setHeights();
				// scroll to position the preview in the right place
				this.positionPreview();
			}, this ), 25 );

		},
		close : function() {

			var self = this,
				onEndFn = function() {
					if( support ) {
						$j( this ).off( transEndEventName );
					}
					self.$jitem.removeClass( 'og-expanded' );
					self.$jpreviewEl.remove();
				};

			setTimeout( $j.proxy( function() {

				if( typeof this.$jlargeImg !== 'undefined' ) {
					this.$jlargeImg.fadeOut( 'fast' );
				}
				this.$jpreviewEl.css( 'height', 0 );
				// the current expanded item (might be different from this.$jitem)
				var $jexpandedItem = $jitems.eq( this.expandedIdx );
				$jexpandedItem.css( 'height', $jexpandedItem.data( 'height' ) ).on( transEndEventName, onEndFn );

				if( !support ) {
					onEndFn.call();
				}

			}, this ), 25 );
			
			return false;

		},
		calcHeight : function() {

			var heightPreview = winsize.height - this.$jitem.data( 'height' ) - marginExpanded,
				itemHeight = winsize.height;

			if( heightPreview < settings.minHeight ) {
				heightPreview = settings.minHeight;
				itemHeight = settings.minHeight + this.$jitem.data( 'height' ) + marginExpanded;
			}

			this.height = heightPreview;
			this.itemHeight = itemHeight;

		},
		setHeights : function() {

			var self = this,
				onEndFn = function() {
					if( support ) {
						self.$jitem.off( transEndEventName );
					}
					self.$jitem.addClass( 'og-expanded' );
				};

			this.calcHeight();
			this.$jpreviewEl.css( 'height', this.height );
			this.$jitem.css( 'height', this.itemHeight ).on( transEndEventName, onEndFn );

			if( !support ) {
				onEndFn.call();
			}

		},
		positionPreview : function() {

			// scroll page
			// case 1 : preview height + item height fits in window´s height
			// case 2 : preview height + item height does not fit in window´s height and preview height is smaller than window´s height
			// case 3 : preview height + item height does not fit in window´s height and preview height is bigger than window´s height
			var position = this.$jitem.data( 'offsetTop' ),
				previewOffsetT = this.$jpreviewEl.offset().top - scrollExtra,
				scrollVal = this.height + this.$jitem.data( 'height' ) + marginExpanded <= winsize.height ? position : this.height < winsize.height ? previewOffsetT - ( winsize.height - this.height ) : previewOffsetT;
			
			$jbody.animate( { scrollTop : scrollVal }, settings.speed );

		},
		setTransition  : function() {
			this.$jpreviewEl.css( 'transition', 'height ' + settings.speed + 'ms ' + settings.easing );
			this.$jitem.css( 'transition', 'height ' + settings.speed + 'ms ' + settings.easing );
		},
		getEl : function() {
			return this.$jpreviewEl;
		}
	}

	return { 
		init : init,
		addItems : addItems
	};

})();