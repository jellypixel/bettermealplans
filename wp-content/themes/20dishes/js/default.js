var $j = jQuery.noConflict();
var adminBar = 0;
var windowSize = 0;
var topPoint;
var scrollPoint;
var navEl;
var initFloatingMenu = false;
var $_GET = {};

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
    function decode(s) {
        return decodeURIComponent(s.split("+").join(" "));
    }

    $_GET[decode(arguments[1])] = decode(arguments[2]);
});

$j(function(){
	setDefaultVar('init');	

	/* MODAL DIALOG */
		var yesnoModal = jQuery("#yesnoModal");
		var overlayEl = jQuery(".overlay");
		
		window.modalDialog = function( modalType, source, customContent ) {
			
			yesnoModal.hide();
			overlayEl.fadeIn(100);
			overlayEl.addClass("generic-wait-background");
			
			if ( modalType == "removeworkshop" ) {
				
				if ( customContent != '' ) {
					yesnoModal.find(".modal-scrutiny").html(customContent);
				}				
				
				yesnoModal.find(".yesbtn").unbind();
				yesnoModal.find(".yesbtn").bind("click", function() {
					closeModal();
					
					window.location.href = "http://20dishes.com/advisor-area/workshop-list/?action=delete&workshopid=" + source.attr('workshopid');		
				});
				
				yesnoModal.fadeIn(100);				
			}	
		
		}
		
		function closeModal( modalType ) {
					
			yesnoModal.fadeOut(100);							
			overlayEl.removeClass("generic-wait-background").fadeOut(300);
		}	
		
		$j( "body" ).on("click", ".overlay .nobtn", function() {
			closeModal("all");
		});		
		
	/* MODAL DIALOG IMPLEMENTATION */
		$j(".workshops-list-button-delete").click(function() {
				
			modalDialog( "removeworkshop", $j(this), 'Are you sure you want to delete this workshop?' );		
		});
		
		$j(".workshops-list-button-edit").click(function() {
				
			window.location.href = "http://20dishes.com/advisor-area/workshop-list/edit/?workshopid=" + $j(this).attr('workshopid');	
		});		
	
	/* PULL LEFT MENU */		
		var children;
		var childrenPos = 0;
		var windowWidth = 0;
	
		$j( 'body' ).on( 'mouseover', '.dropdown-submenu.menu-item-has-children', function() {			
			var thisEl = $j( this );
			if ( !thisEl.parents( 'pull-left-first').length )
			{			
				children = thisEl.children( '.dropdown-menu' );
				childrenPos = children.offset().left + 1 + children.width();
				console.log(childrenPos);
				
				windowWidth = $j( window ).width();
				
				if ( childrenPos > windowWidth )
				{
					thisEl.addClass( 'pull-left pull-left-first' );	
				}
			}
		});
		
		$j( 'body' ).on( 'mouseout', '.dropdown-submenu.menu-item-has-children', function() {
			$j(this).removeClass( 'pull-left pull-left-first' );
		});
		
	/* CONTACT US */
    //Hide the field initially
    $j(".contact-technical").hide();

    //Show the text field only when the third option is chosen - this doesn't
    $j('[name=contact-reason]').change(function() {
            if ($j("[name=contact-reason]").val() != "General Questions or Feedback") {
                    $j(".contact-technical").show();
                    
                    if($j("[name=contact-reason]").val() == "Technical Support") {
                    	$j("[name=device]").show();
                    	$j("[name=browser]").show();
                    	$j("[name=os]").show();
                    }
                    else {
                    	$j("[name=device]").hide();
                    	$j("[name=browser]").hide();
                    	$j("[name=os]").hide();
                    }
            }   
            else {
                    $j(".contact-technical").hide();
            }
    });	
	
	
		
	/* TAB NAVIGATION */
		// New cylic nextprev function
		$j.fn.loopPrev = function(selector){
			var selector = selector || '';
			return this.prev(selector).length ? this.prev(selector) : this.siblings(selector).addBack(selector).last();
		}
		$j.fn.loopNext = function(selector){
			var selector = selector || '';
			return this.next(selector).length ? this.next(selector) : this.siblings(selector).addBack(selector).first();
		}	
	
		$j('.tab-navigation-wrapper').each(function() {
			var tabEl = $j(this).parent('.tab-content');
			var currActive, currHeaderActive;
			
			$j(this).find('.tab-navigation-left').click(function() {
				currActive = tabEl.find('.active');
				currHeaderActive = tabEl.siblings(".nav-tabs").find('.active');
				
				// Tab
				currHeaderActive.loopPrev().addClass('active');
				currHeaderActive.removeClass('active');
				
				// Content
				if ( currActive.loopPrev().attr('class') != 'tab-navigation-wrapper' ) {
					currActive.loopPrev().addClass('active');
				}
				else {
					currActive.loopPrev().loopPrev().addClass('active');
				}
				currActive.removeClass('active');
			});
			
			$j(this).find('.tab-navigation-right').click(function() {
				currActive = tabEl.find('.active');
				currHeaderActive = tabEl.siblings(".nav-tabs").find('.active');
				
				// Tab
				currHeaderActive.loopNext().addClass('active');
				currHeaderActive.removeClass('active');
				
				// Content
				if ( currActive.loopNext().attr('class') != 'tab-navigation-wrapper' ) {
					currActive.loopNext().addClass('active');
				}
				else {
					currActive.loopNext().loopNext().addClass('active');
				}
				currActive.removeClass('active');
			});
		});
	
	/* VIDEO BUTTON */		
		$j('.video-button').click(function(e) {
			e.preventDefault();
			var videoID;
			var videoType;
			var wrapperEl;
			
			videoType = $j(this).attr('videotype');
			
			if ( videoType == 'youtube' ) {			
				videoID = $j(this).attr('href'); //getYoutubeId( $j(this).attr('href') );
					
				var iframe = document.createElement("iframe");
				//iframe.setAttribute("src", "//www.youtube.com/embed/" + videoID + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=0");
				iframe.setAttribute("src", "//www.youtube.com/embed/" + videoID + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=0");
				iframe.setAttribute("frameborder", "0");
				iframe.setAttribute("class", "video-youtube-iframe");
				iframe.setAttribute("width", "100%");
				iframe.setAttribute("height", "300px");
			}
			else {
				videoID = $j(this).attr('href'); //getVimeoId( $j(this).attr('href') );
			
				var iframe = document.createElement("iframe");
				iframe.setAttribute("src", "//player.vimeo.com/video/" + videoID + "?autoplay=1");
				iframe.setAttribute("frameborder", "0");
				iframe.setAttribute("class", "video-vimeo-iframe");
				iframe.setAttribute("width", "100%");
				iframe.setAttribute("height", "300px");
				iframe.setAttribute("webkitallowfullscreen", "");
				iframe.setAttribute("mozallowfullscreen", "");
				iframe.setAttribute("allowfullscreen", "");			
			}
			
			wrapperEl = $j('<div class="video-button-wrapper"></div>').append(iframe);
			
			$j(this).replaceWith(wrapperEl); 
			
		});
		
		function getYoutubeId(url){
			if(url.indexOf('?') != -1 ) {
				var query = decodeURI(url).split('?')[1];
				var params = query.split('&');
				for(var i=0,l = params.length;i<l;i++)
					if(params[i].indexOf('v=') === 0)
						return params[i].replace('v=','');
			} else if (url.indexOf('youtu.be') != -1) {
				return decodeURI(url).split('youtu.be/')[1];
			}
			return null;
		}	
		
		function getVimeoId(url) {
			var regExp = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
			var match = url.match(regExp);
			
			if (match){
				return match[3];
			}else{
				return null;
			}	
		}
		
		
		
	/* HOMEPAGE INTRODUCTION - VIMEO PLAY */
		$j('.vimeo_overlay').click(function(e) {
			var vimeoID = $j(this).attr('vimeoid');
			var heightEl = $j(this).attr('vidheight');	
			var vidHeight = "300px";
			
			if ( heightEl != "" ) {
				vidHeight = heightEl + "px";	
			}
			
			var iframe = document.createElement("iframe");
			iframe.setAttribute("src", "//player.vimeo.com/video/" + vimeoID + "?autoplay=1");
			iframe.setAttribute("frameborder", "0");
			iframe.setAttribute("class", "vimeo-iframe");
			iframe.setAttribute("width", "100%");
			iframe.setAttribute("height", vidHeight);
			iframe.setAttribute("webkitallowfullscreen", "");
			iframe.setAttribute("mozallowfullscreen", "");
			iframe.setAttribute("allowfullscreen", "");			
			
			$j(this).parent()
				.addClass("onloading")
				.removeAttr('style')
				.empty()
				.append(iframe);
		});
		
		
		
	/* HOMEPAGE INTRODUCTION - YOUTUBE PLAY */
		$j('.youtube_overlay').click(function(e) {
			e.preventDefault();
			var videoURL;
			
			videoURL = $j(this).attr('youtubeid');
			var heightEl = $j(this).attr('vidheight');	
			var vidHeight = "300px";
			
			if ( heightEl != "" ) {
				vidHeight = heightEl + "px";	
			}
				
			var iframe = document.createElement("iframe");
			iframe.setAttribute("src", "//www.youtube.com/embed/" + videoURL + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&controls=1&showinfo=0");
			iframe.setAttribute("frameborder", "0");
			iframe.setAttribute("class", "youtube-iframe");
			iframe.setAttribute("width", "100%");
			iframe.setAttribute("height", vidHeight);
			
			$j(this).parent()
				.addClass("onloading")
				.removeAttr('style')
				.empty()
				.append(iframe);		
		});
		
		
		
	/* PINTEREST */
		$j('.pinterest-overlay').click(function(e) {
			e.stopPropagation();
		});
		
	/* HOMEPAGE TESTIMONIAL */
		$j('.homepage_testimonial_inner .container').flexslider({
			animation: "fade",
			directionNav: false
		});
		
	/* ACCORDIONS */
				
		$j( 'body').on( 'click', '.panel-heading', function() {		
			var currentAccordion = $j( this );

			if ( currentAccordion.siblings(".panel-collapse").hasClass("in") ) {
				//panel currently opened and about to be closed
				currentAccordion.removeClass("active");
			}
			else {
				//panel currently closed and about to be opened
				currentAccordion.parents(".panel-group").find(".panel-heading").removeClass("active");
				currentAccordion.addClass("active");				
			}
		});
		
		$j( 'body').on( 'click', '.tdicon-switchplusmin', function() {		
			var currentParent = $j( this ).parent('.panel-heading');
			
			if ( currentParent.siblings(".panel-collapse").hasClass("in") ) {
				//panel currently opened and about to be closed
				currentParent.removeClass("active");
			}
			else {
				//panel currently closed and about to be opened
				currentParent.parents(".panel-group").find(".panel-heading").removeClass("active");
				currentParent.addClass("active");
			}
		});
		
	// SUBSCRIBE SHORTCODE 
		$j( '.shortcode_wrapper.subscribes' ).each(function() {
			var allSub = $j(this).find( '.subscribe_wrapper' );
			var maxHeight = 0;
			var thisHeight;
			
			allSub.each(function() {
				thisHeight = $j(this).height();
				if ( thisHeight > maxHeight ) {
					maxHeight = thisHeight	
				}
			});
			
			allSub.each(function() {
				$j(this).css({
					'height' : maxHeight + "px"
				});
			});
		});
		
	// GRID
		Grid.init();
		
	//Lightbox 
		var overlayAdvisorLightbox = $j('.overlay-advisorlightbox');
		var advisorLightbox = $j('.advisorlightbox');
	
		$j('body').on('click', '.findadvisor-inner a', function(e) {
			e.preventDefault();
			if ( overlayAdvisorLightbox.hasClass('shown') ) {
				// do nothing
			}
			else {				
				overlayAdvisorLightbox.fadeIn(200);
				advisorLightbox.fadeIn(200);
			}
		});
		
		$j('body').on('click', '.overlay-advisorlightbox', function(event) {		
			if ($j(event.target).is('.overlay-advisorlightbox')){
				closeLightbox();
			}			
		});
		
		$j('body').on('click', '.advisorlightbox-close', function() {
			closeLightbox();
		});
		
		function closeLightbox() {
			overlayAdvisorLightbox.fadeOut(200);
			advisorLightbox.fadeOut(200);	
		}
		
	// Advisor
		$j('.advisorlightbox-checkbox-wrapper > input').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});
		
	// Advisor Profile
		$j('.advisor-floater-close').click(function() {
			var parentAF = $j(this).parent('.advisor-floater-wrapper');
			
			if ( parentAF.hasClass('hideit') ) {
				$j(this).parent('.advisor-floater-wrapper').removeClass('hideit');	
				document.cookie="advisorbox=opened;path=/";
			}
			else {
				$j(this).parent('.advisor-floater-wrapper').addClass('hideit');	
				document.cookie="advisorbox=closed;path=/";
			}
		});		
		
	    var advisorbox=getCookie("advisorbox");
	    if (advisorbox == "closed") {
		    
	        $j('.advisor-floater-wrapper').addClass('hideit');
	    }
	    $j('.advisor-floater-wrapper').css('display', 'block');		
		
	// Advisor Search 
		var searchBy = 'name';
		var userSearchEl = $j( '#usersearch' );
		
		userSearchEl.autocomplete({
			source: this_url.template_url + '/findadvisor.php?searchby=name',		// local cache?
			html: true,  
			open: function(event, ui) {
				$j(".ui-autocomplete").css("z-index", 100000);
			},
			minLength: 4,
			appendTo: ".usersearch-result",
			select: function(event, ui) {
				var selectedAdvisorID = ui.item.id;
				$j("#submitFindAdvisor").show();
				$j("#submitFindAdvisor").attr("href", ui.item.link);
			}
			
		})
		.autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $j( "<li>" )
				.append('<div class="usersearch-autocomplete-wrapper">' + 
							'<div class="usersearch-autocomplete-avatar">' +
								item.avatar +
							'</div>' +
							'<div class="usersearch-autocomplete-info">' +
								'<div class="usersearch-autocomplete-name">' +
									item.label +
								'</div>' +
								'<div class="usersearch-autocomplete-email">' +
									item.loc +
								'</div>' +
							'</div>' +
						'</div>')
				.appendTo( ul );
		};
		
		userSearchEl.keyup(function(e) {
			
			var code = (e.keyCode || e.which);
			
   		   // do nothing if it's an arrow key
		    if(code == 13 || code == 37 || code == 38 || code == 39 || code == 40) {
		        return;
		    }
    
			$j("#submitFindAdvisor").removeAttr("href");
			$j("#submitFindAdvisor").hide();
		});
		
		$j('#submitFindAdvisor').on('click', function() {
			
			var attr = $j("#submitFindAdvisor").attr("href");

			if (typeof attr !== typeof undefined && attr !== false) {
			    window.location.href = attr;
			}
			
			
		});
				
		$j("#advisorbyname").on('ifChecked', function(event) {
			 userSearchEl.autocomplete( 'option', 'source', this_url.template_url + '/findadvisor.php?searchby=name' );
		});
		
		$j("#advisorbyzipcode").on('ifChecked', function(event) {
			 userSearchEl.autocomplete( 'option', 'source', this_url.template_url + '/findadvisor.php?searchby=zip' );
		});
		
	// Gift
		$j('.gift-close').click(function() {
			/*$j(this).parent('.gift-wrapper').stop().animate({
				'right' : '-108px'
			});*/
			var parentGift = $j(this).parent('.gift-wrapper');
			
			if ( parentGift.hasClass('hideit') ) {
				$j(this).parent('.gift-wrapper').removeClass('hideit');	
				document.cookie="gift=opened;path=/";
			}
			else {
				$j(this).parent('.gift-wrapper').addClass('hideit');	
				document.cookie="gift=closed;path=/";
			}
			
		});
		
	    var giftvar=getCookie("gift");
	    if (giftvar == "closed") {
		    
	        $j('.gift-wrapper').addClass('hideit');
	    }
	    $j('.gift-wrapper').css('display', 'block');
	    
	// Fix admin bar
	setTimeout(function(){
		$j("#wpadminbar").find('a').each(function() {
		    $j(this).attr("href", $j(this).attr("href").replace("https://", "http://"));
		});
	}, 5000);
	
	// Auto scroll tab on homepage
	var tabs = $j('.home .nav-tabs li');
	var tabcont = $j('.home .tab-content .tab-pane');
	
    var counter = 0;
    window.setInterval(activateTab, 10000);
    
    function activateTab(){
	    
      // remove active class from all the tabs
       tabs.removeClass('active');
       tabcont.removeClass('active');
       
       tabs.eq(counter).addClass('active');
       tabcont.eq(counter).addClass('active');
       
       if(counter < tabs.length-1)
         counter = counter + 1;
       else 
         counter = 0;
    }
	
	// iCheck - Cart
		$j('.cart_terms_conditions input').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});		
		
		$j('.cart_pay_now').click(function(){
						
			var checked1 = $j("#cart_term_1").parent('[class*="icheckbox"]').hasClass("checked");
			var checked2 = $j("#cart_term_2").parent('[class*="icheckbox"]').hasClass("checked");

			if (!(checked1 && checked2)) {
				alert("You must agree to both term of agreement.");
			} else {
				window.location.href = $j('.cart_pay_now').attr("href");
			}
			
		});
		
		$j('.go-regform').click(function(){
						
			var checked1 = $j("#cart_term_1").parent('[class*="icheckbox"]').hasClass("checked");
			var checked2 = $j("#cart_term_2").parent('[class*="icheckbox"]').hasClass("checked");

			if (!(checked1 && checked2)) {
				alert("You must agree to both term of agreement.");
				event.preventDefault();
				event.stopPropagation();
			} 
			
		});		
		
	// iCheck - Contact Advisor
		$j('.contactadvisor-form input[type=radio]').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});
		
		$j('.contactadvisor-form input[type=checkbox]').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});
		
	// Contact Advisor - Others
		$j('#contactadvisor-aboutother').attr('disabled', 'disabled');
	
		$j('.contactadvisor-about .wpcf7-list-item.last input').on('ifChecked', function() {
			$j('#contactadvisor-aboutother').removeAttr('disabled');			
		});
		
		$j('.contactadvisor-about .wpcf7-list-item.last input').on('ifUnchecked', function() {
			$j('#contactadvisor-aboutother').attr('disabled', 'disabled');
			$j('#contactadvisor-aboutother').val('');
		});
		
	// WLM Registration - Styling
		 $j( '#wlmreginstructions' ).next('h3').addClass( 'wlm-h3-title' );
		 
    // WLM Registration - iCheck
		$j('.fld_div input[type=checkbox]').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});
		
	//EDIT WORKSHOP - Load
	if (top.location.pathname === '/advisor-area/workshop-list/edit/')
	{	
		$j.ajax({
			type : "post",
			dataType : "json",
			url: "/wp-admin/admin-ajax.php",
			data : {action: "workshop_edit", workshopid : $_GET["workshopid"]},
			success: function(response) {
				
				if(response != 0) {
					
					$j("[name='workshop-name']").val(response['name']);
					$j("[name='workshop-hostedby']").val(response['host']);
					$j("[name='workshop-email']").val(response['email']);
					$j("[name='workshop-address1']").val(response['address1']);
					$j("[name='workshop-address2']").val(response['address2']);
					$j("[name='workshop-postalcode']").val(response['zip']);
					$j("[name='workshop-city']").val(response['city']);
					$j("[name='workshop-description']").val(response['description']);
					
					$j("[name='workshop-category']").val(response['category']);
					$j("[name='workshop-state']").val(response['state']);
															
					var date = response['datetime'];
					$j("[name='workshop-date']").datepicker('setDate', new Date(date.replace(/-/g,"/")));
					
					var hour = date.substr(11, 2);
					var timeDen = "AM";
					if(hour > 12) {
						hour = hour - 12;
						hour = "0" + hour;	
						timeDen = "PM" 
					}
					
					$j("[name='workshop-hour']").val(hour);
					$j("[name='workshop-minute']").val(date.substr(14, 2));
					$j("[name='workshop-ampm']").val(timeDen);
					$j("[name='actionfield']").val($_GET["workshopid"]);
				}
			}
		})   
	}
	
	// Woocommerce - Icheck
		$j('.woocommerce input[type=checkbox]').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'icheckbox_minimal',
			increaseArea: '0%'
		});
		
	// Backbutton
	$j( '.backbutton-wrapper' ).click(function() {
		goBack();
	});
	
	$j("#affwp-user-url").val('http://www.');
	$j("#affwp-user-url").keydown(function(e) {
		
		var oldvalue=$j(this).val();
		var field=this;
	
		setTimeout(function () {
		    if(field.value.indexOf('http://www.') !== 0) {
		        $j(field).val(oldvalue);
		    } 
		}, 1);
	});
	
	var checkExist = setInterval(function() {
	  
	   if ($j(".mtsnb-container-outer").length) {
	      
	      if($j("#wpadminbar").length)
	      	 if($j("#notification-second-bar").length)
 		        $j(".navbar-default").css("top", "107px"); 
 		     else
 		     	$j(".navbar-default").css("top", "73px");
	      else	
	         if($j("#notification-second-bar").length)
	         	$j(".navbar-default").css("top", "74px");
	         else
	         	$j(".navbar-default").css("top", "40px");
	         
	      clearInterval(checkExist);
	   
	   }
	   
	}, 100);
	
	// PMPRO - Form Makeover
	// 4523
	if ( $j( ".page-id-4523" ).length )
	{	
		// Section	
		var userFieldForm = $j( "#pmpro_user_fields" );		
		var checkoutForm = $j( "#pmpro_checkout_box-checkout_boxes" );
		var userFieldExtraForm = $j( "#pmpro_checkout_box-checkout_boxes" );
		var billingAddressForm = $j( "#pmpro_billing_address_fields" );
		var paymentMethod = $j( "#pmpro_payment_method" );
		var paymentInformationForm = $j( "#pmpro_payment_information_fields" );
		var pricingForm = $j( "#pmpro_pricing_fields" );
	
		// Placeholder		
		userFieldForm.find( "#username" ).attr( "placeholder", "Username" );		
		userFieldForm.find( "#password" ).attr( "placeholder", "Password" );		
		userFieldForm.find( "#password2" ).attr( "placeholder", "Confirm Password" );		
		userFieldForm.find( "#first_name" ).attr( "placeholder", "First Name" );		
		userFieldForm.find( "#last_name" ).attr( "placeholder", "Last Name" );		
		userFieldForm.find( "#bemail" ).attr( "placeholder", "E-mail Address" );		
		userFieldForm.find( "#bconfirmemail" ).attr( "placeholder", "Confirm E-mail Address" );		
		
		checkoutForm.find( "#phone" ).attr( "placeholder", "Phone" );
		
		billingAddressForm.find( "#bfirstname" ).attr( "placeholder", "First Name" );
		billingAddressForm.find( "#blastname" ).attr( "placeholder", "Last Name" );
		billingAddressForm.find( "#baddress1" ).attr( "placeholder", "Address 1" );
		billingAddressForm.find( "#baddress2" ).attr( "placeholder", "Address 2" );
		billingAddressForm.find( "#bcity" ).attr( "placeholder", "City" );
		billingAddressForm.find( "#bstate" ).attr( "placeholder", "State" );
		billingAddressForm.find( "#bzipcode" ).attr( "placeholder", "Postal Code" );
		billingAddressForm.find( "#bphone" ).attr( "placeholder", "Phone" );
		
		paymentInformationForm.find( "#AccountNumber" ).attr( "placeholder", "Card Number" );
		paymentInformationForm.find( "#CVV" ).attr( "placeholder", "CVV" );
		paymentInformationForm.find( "#discount_code" ).attr( "placeholder", "Discount Code" );		
		
		$j( '.pmpro_btn-submit-checkout' ).attr( "value", "Start Now" );		
		
		// iCheck - DELETE IF BROKEN
		$j('#pmpro_payment_method input, #pmpro_tos_fields input, #pmpro_mailing_lists input').iCheck({
			checkboxClass: 'icheckbox_minimal',
			radioClass: 'iradio_minimal'
		});
		
		$j('.gateway_authorizenet input[name=gateway]').on('ifChecked', function(event){
		
			$j("#pmpro_payment_information_fields").show();
		});
		
		$j('.gateway_paypalexpress input[name=gateway]').on('ifChecked', function(event){
		
			$j("#pmpro_payment_information_fields").hide();
		});
		
		// LAYOUT MEGIK - DELETE IF BROKEN
			// Create new columns
				var columnEl = $j( '<div class="row"><div class="col-lg-6 col-md-6 col-xs-12 pmpro-makeover-left"></div><div class="col-lg-6 col-md-6 col-xs-12 pmpro-makeover-right"></div><div>' );
				$j( "#pmpro_message" ).after( columnEl );
			
			// Move Thingie to the left
				userFieldForm.appendTo( $j( '.pmpro-makeover-left' ) );
				userFieldExtraForm.appendTo( $j( '.pmpro-makeover-left' ) );
				billingAddressForm.appendTo( $j( '.pmpro-makeover-left' ) );
				
			// Move Thingie to the right
				pricingForm.appendTo( $j( '.pmpro-makeover-right' ) );
				paymentMethod.appendTo( $j( '.pmpro-makeover-right' ) );
				paymentInformationForm.appendTo( $j( '.pmpro-makeover-right' ) );
				
			// Ugh create new div for expiration date
				var expirationEl = $j( '<div class="expiration-inner"></div>' );
				var expirationParent = $j( ".pmpro_payment-expiration ");
				var selectLeftEl = $j( '<div class="expiraton-select-left"></div>' );
				var selectRightEl = $j( '<div class="expiraton-select-right"></div>' );
				var expirationChildren = expirationParent.children( "*" );
				
				expirationParent.empty();
				expirationParent.prepend( expirationEl );
				expirationChildren.appendTo( expirationEl );
				
				expirationEl.prepend( selectLeftEl, selectRightEl );				
				
				expirationEl.find( "label" ).prependTo( expirationParent );
				expirationEl.find( "#ExpirationMonth" ).prependTo( selectLeftEl );
				expirationEl.find( "#ExpirationYear" ).prependTo( selectRightEl );
				
		// Comment ABOVE if broken		
	}
	
	// CAROUSEL - TESTIMONIAL
	var winWidth = $j(window).width();
	
	var CarouselWidth = 470;
	
	if ( winWidth >= 1200 ) {
		CarouselWidth = 470;	
	}
	else if ( winWidth >= 998 ) {
		CarouselWidth = 370;	
	}
	else if ( winWidth >= 768 ) {
		CarouselWidth = 320;	
	}
	else
	{
		CarouselWidth = 250;		
	}
	
	$j( '.shortcode-testimonial' ).flexslider({
		animation: "slide",
		animationLoop: false,
		itemWidth: CarouselWidth,
		itemMargin: 0,
		prevText: '',
		nextText: ''
	});	
	
	$j( '#bemail' ).attr('placeholder','Email*');
	$j( '#bconfirmemail' ).attr('placeholder','Confirm Email*');
	
	$j( '#username' ).attr('placeholder','Email');
	$j( ".page-id-4523 #username" ).keyup(function() {
	  
		var value = $j( ".page-id-4523 #username" ).val();
		
		$j("#bemail").val(value);
		$j("#bconfirmemail").val(value);
		
	});
	
	$j( ".page-id-4523 #bfirstname" ).keyup(function() {
	  
		var value = $j( ".page-id-4523 #bfirstname" ).val();
		
		$j("#first_name").val(value);
	});
	
	$j( ".page-id-4523 #blastname" ).keyup(function() {
	  
		var value = $j( ".page-id-4523 #blastname" ).val();
		
		$j("#last_name").val(value);
	});
	
	$j( ".page-id-4523 #bphone" ).keyup(function() {
	  
		var value = $j( ".page-id-4523 #bphone" ).val();
		
		$j("#phone").val(value);
	});
	
	$j( 'label[for="password"]' ).html("Create Your Password (Required)");
	$j( 'label[for="password2"]' ).html("Confirm Password");
	
	$j( ".page-id-4523 #pmpro_tos_fields th" ).html("<a href='https://20dishes.com/terms-of-service/' target='blank'>Terms of Service (Please Check. Required)</a>");
	
	$j( ".page-id-4523 #pmpro_tos_fields").appendTo(".pmpro-makeover-right");	
	
});


$j(window).resize(function() {
	setDefaultVar();	
});	

$j(document).on('scroll', function() {
	if ( initFloatingMenu == true ) {
		floatingMenu();
	}
});

function setDefaultVar(init) {
	navEl = $j('.navbar-default');
	
	if ( $j('.admin-bar').length ) {
		adminBar = 1;	
	}
	else {
		adminBar = 0;	
	}
	
	if ( jQuery(window).width() > 767 ) {
		windowSize = 1;
	}
	else {
		windowSize = 0;	
	}	
	
	topPoint = 0;
	
	if ( adminBar != 0 ) {
		if ( windowSize != 0 ) {
			topPoint = 32;	
		}
		else {
			topPOint = 46;	
		}
	}
	
	if ( init == 'init' ) {
		floatingMenu();		
		initFloatingMenu = true;
	}
}

function floatingMenu() {	
	scrollPoint = $j(document).scrollTop();	 

	if ( scrollPoint > topPoint ) {		
		if ( !navEl.hasClass('floating') ) {
			navEl.addClass('floating');	
		}
	}
	else {
		if ( navEl.hasClass('floating') ) {
			navEl.removeClass('floating');	
		}
	}	
}

function goBack() {
    window.history.back();
}