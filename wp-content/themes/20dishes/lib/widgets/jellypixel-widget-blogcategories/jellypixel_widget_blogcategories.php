<?php
	/*
	Plugin Name: Jellypixel Widget Blog Categories
	Description: Show Blog Categories
	Version: 1.0
	Author: Jellypixel 
	Author URI: http://www.jellypixelstudio.com
	License: GPLv2
	*/

	class jellypixel_widget_blogcategories extends WP_Widget 
	{
		/**
		 * Register widget with WordPress.
		 */
		function __construct() 
		{
			// Slug
			$this->plugin_slug = 'jellypixel-widget-blogcategories';
			
			// Constructor
			parent::__construct 
			(
				// ID
				$this->plugin_slug, 
				
				// Name
				__( 'Blog Categories', 'jellypixel' ),
				
				// Widget Options
				array
				( 
					'description' => __( 'Show blog category and its children.', 'jellypixel' ) 
				) 
			);
			
			/*add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
			add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
			add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );*/
		}
			
		function form( $instance ) 
		{	
			// Default
			$defaults = array(
				'title' => 'Categories'
			);
			
			$instance = wp_parse_args( (array) $instance, $defaults ); 			
			
			$title = esc_attr( $instance['title'] );
			?>
            	<p>
                	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'jellypixel' ); ?></label>
	                <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" class="widefat" type="text" value="<?php echo esc_attr( $title ); ?>" />
                </p>               
			<?php
		}
		
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
	
			$instance['title'] = strip_tags( $new_instance['title'] );			

			/*$this->flush_widget_cache();
	
			$alloptions = wp_cache_get( 'alloptions', 'options' );
			if ( isset( $alloptions[$this->plugin_slug] ) )
				delete_option( $this->plugin_slug );*/
	
			return $instance;
		}
	
		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		function widget( $args, $instance ) 
		{			
			/*$cache = wp_cache_get( $this->plugin_slug, 'widget' ); // ?
	
			if ( !is_array( $cache ) )
			{
				$cache = array();
			}
			
			if ( !isset( $args[ 'widget_id' ] ) ) {
				$args[ 'widget_id' ] = $this->id;
			}
	
			if ( isset( $cache[ $args[ 'widget_id' ] ] ) ) 
			{
				echo $cache[ $args[ 'widget_id' ] ];
				return;
			}
	
			ob_start();*/
			extract( $args );	
			
			// Title
			$title = $instance['title'];
						
			echo $before_widget;
			
			if ( $title ) 
			{
				echo $before_title . $title . $after_title; 
			}
			
			$blog_cat_ID = 18;
			
			//var_dump( get_category_link( $blog_cat_ID ) );			
			
			$childCategories = get_categories(
				array( 'parent' => $blog_cat_ID )
			);
			
			if ( !empty ( $childCategories ) ) {			
				foreach( $childCategories as $child ) {
					echo 
						'<a href="' . get_category_link( $child->cat_ID ) . '">' . $child->cat_name . '</a>';
				}
			}
			
			echo $after_widget;
	
			/*$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set( $this->plugin_slug, $cache, 'widget' );	*/
	
		}	
		/*
		function flush_widget_cache() {
			wp_cache_delete( $this->plugin_slug, 'widget' );
		}*/
	}	