<?php
	/*
	Plugin Name: Jellypixel Widget Product Categories
	Description: List of Product Categoriees
	Version: 1.0
	Author: Jellypixel 
	Author URI: http://www.jellypixelstudio.com
	License: GPLv2
	*/

	class jellypixel_widget_product_categories extends WP_Widget 
	{
		/**
		 * Register widget with WordPress.
		 */
		function __construct() 
		{
			// Slug
			$this->plugin_slug = 'jellypixel-product-categories';
			
			// Constructor
			parent::__construct 
			(
				// ID
				$this->plugin_slug, 
				
				// Name
				__( 'Product Categories', 'jellypixel' ),
				
				// Widget Options
				array
				( 
					'description' => __( 'Show product categories.', 'jellypixel' ) 
				) 
			);
			
			add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
			
			/*add_action( 'save_post', array( $this, 'flush_widget_cache' ) );
			add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
			add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );*/
		}
		
		function admin_scripts() {
			wp_enqueue_style( 'widget-product-categories-css', get_template_directory_uri() . '/lib/widgets/jellypixel-product-categories/css/style.css' );	
		}
	
		function form( $instance ) 
		{	
			// Default
			$defaults = array(
				'title' => 'Product Categories',				
				'image' => '',
				'description' => ''
			);
			
			$instance = wp_parse_args( (array) $instance, $defaults ); 			
			
			$title = esc_attr( $instance['title'] );
			
			?>
                <p>
                	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'jellypixel' ); ?></label>
	                <input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" class="widefat" type="text" value="<?php echo esc_attr( $title ); ?>" />
                </p>                
			<?php
		}
		
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
	
			$instance['title'] = strip_tags( $new_instance['title'] );			

			/*$this->flush_widget_cache();
	
			$alloptions = wp_cache_get( 'alloptions', 'options' );
			if ( isset( $alloptions[$this->plugin_slug] ) )
				delete_option( $this->plugin_slug );*/
	
			return $instance;
		}
	
		/**
		 * Front-end display of widget.
		 *
		 * @see WP_Widget::widget()
		 *
		 * @param array $args     Widget arguments.
		 * @param array $instance Saved values from database.
		 */
		function widget( $args, $instance ) 
		{			
			/*$cache = wp_cache_get( $this->plugin_slug, 'widget' ); // ?
	
			if ( !is_array( $cache ) )
			{
				$cache = array();
			}
			
			if ( !isset( $args[ 'widget_id' ] ) ) {
				$args[ 'widget_id' ] = $this->id;
			}
	
			if ( isset( $cache[ $args[ 'widget_id' ] ] ) ) 
			{
				echo $cache[ $args[ 'widget_id' ] ];
				return;
			}
	
			ob_start();*/
			extract( $args );	
			
			// Title
			$title = $instance['title'];
									
			echo $before_widget;
			
			if ( $title ) 
			{
				echo $before_title . $title . $after_title; 
			}
			
			/* Category
			 *
			 * Show Only Current Category and its children, but not Root Categories
			 *
			 */
			
				/*
				// Current Category
				$current_product_category = get_queried_object();
				$current_product_category_ID = $current_product_category->term_id;	
				$currentCategoryEl = '';
				
				if ( $current_product_category_ID != '8' && $current_product_category_ID != '9' )
				{
					// Show current category but not root categories
					$current_category_detail = get_term_by( 'id', $current_product_category_ID, 'product_cat' );
					
					$currentCategoryEl = 
						'<li>
							<a href="' .  get_term_link( $current_product_category_ID, 'product_cat' ) . '">' . $current_category_detail->name . '</a>
						</li>';
				}
				
				// Children Categories
				global $post;
				
				$product_cats = get_term_children( $current_product_category_ID, 'product_cat' );
				$childrenCategoriesEl = '';
					
				foreach ( $product_cats as $product_cat_id ) 
				{				
					$product_cat = get_term_by( 'id', $product_cat_id, 'product_cat' );				
					
					$childrenCategoriesEl .= '<li><a href="' . get_term_link( $product_cat_id, 'product_cat' ) . '" rel="recipe_category tag">' . $product_cat->name . '</a></li>';				
				}
				
				if ( !empty( $currentCategoryEl ) || !empty( $childrenCategoriesEl ) )
				{
					echo 
						'<ul class="product-categories">
							' .  $currentCategoryEl . $childrenCategoriesEl . '
						</ul>';
				}
				*/
				
				
				
				
			/* Category
			 *
			 * Show Every Categories, except membership and category that has children (children included, parent excluded)
			 *
			 */
			 
			$args = array(
				'hide_empty' => true,
			);
			
			$product_categories = get_terms( 'product_cat', $args );
			
			$currentCategoriesEl = '';
			
			// Excluded ID 
			// 939 - Membership
			// 940 - Parent (Utensils and Kitchen Gadgets)
			
			$excluded_id = array(939,940);			 
			
			foreach ( $product_categories as $product_category ) {
				if ( in_array( $product_category->term_id, $excluded_id ) )
				{
					
				}
				else
				{				
					$currentCategoriesEl .= '<li><a href="' . get_term_link( $product_category->term_id, 'product_cat' ) . '" rel="recipe_category tag">' . $product_category->name . '</a></li>';			
				}
			}
			
			if ( !empty( $currentCategoriesEl ) )
			{
				echo 
					'<ul class="product-categories">
						' .  $currentCategoriesEl . '
					</ul>';
			}

			
			
			
			
			
			
			
			
			
			
			
			
			echo $after_widget;
	
			/*$cache[$args['widget_id']] = ob_get_flush();
			wp_cache_set( $this->plugin_slug, $cache, 'widget' );	*/
	
		}	
		/*
		function flush_widget_cache() {
			wp_cache_delete( $this->plugin_slug, 'widget' );
		}*/
	}	