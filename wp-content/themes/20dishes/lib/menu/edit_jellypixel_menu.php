<?php

	

	class Walker_Nav_Menu_Edit_Custom extends Walker_Nav_Menu  {
		/**
		 * @see Walker_Nav_Menu::start_lvl()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference.
		 */
		function start_lvl(&$output, $depth = 0, $args = Array()) {	
		}
		
		/**
		 * @see Walker_Nav_Menu::end_lvl()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference.
		 */
		function end_lvl(&$output, $depth = 0, $args = Array()) {
		}
		
		/**
		 * @see Walker::start_el()
		 * @since 3.0.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param object $item Menu item data object.
		 * @param int $depth Depth of menu item. Used for padding.
		 * @param object $args
		 */
		function start_el(&$output, $item, $depth = 0, $args = Array(), $id = 0) {
			global $_wp_nav_menu_max_depth;
			
			$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;
		
			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		
			ob_start();
			$item_id = esc_attr( $item->ID );
			$removed_args = array(
				'action',
				'customlink-tab',
				'edit-menu-item',
				'menu-item',
				'page-tab',
				'_wpnonce',
			);
		
			$original_title = '';
			if ( 'taxonomy' == $item->type ) {
				$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
				if ( is_wp_error( $original_title ) )
					$original_title = false;
			} elseif ( 'post_type' == $item->type ) {
				$original_object = get_post( $item->object_id );
				$original_title = $original_object->post_title;
			}
		
			$classes = array(
				'menu-item menu-item-depth-' . $depth,
				'menu-item-' . esc_attr( $item->object ),
				'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
			);
		
			$title = $item->title;
		
			if ( ! empty( $item->_invalid ) ) {
				$classes[] = 'menu-item-invalid';
				/* translators: %s: title of menu item which is invalid */
				$title = sprintf( __( '%s (Invalid)' ), $item->title );
			} elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
				$classes[] = 'pending';
				/* translators: %s: title of menu item in draft status */
				$title = sprintf( __('%s (Pending)'), $item->title );
			}
		
			$title = empty( $item->label ) ? $title : $item->label;
						
			if ( $depth == 0 ) {
				$megamenuRootStyle = '';
				$widgetRootStyle = 'style="display:none;"';	
				$thumbnailRootStyle = 'style="display:none;"';
			}
			else if ( $depth == 1 || $depth == 2 ) {
				$megamenuRootStyle = 'style="display:none;"';
				$widgetRootStyle = '';
				$thumbnailRootStyle = '';
			}
			else {
				$megamenuRootStyle = 'style="display:none;"';
				$widgetRootStyle = 'style="display:none;"';
				$thumbnailRootStyle = 'style="display:none;"';
			}
		
			?>
			<li id="menu-item-<?php echo $item_id; ?>" class="<?php echo implode(' ', $classes ); ?>">
				<dl class="menu-item-bar">
					<dt class="menu-item-handle">
						<span class="item-title"><?php echo esc_html( $title ); ?></span>
						<span class="item-controls">
							<span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
							<span class="item-order hide-if-js">
								<a href="<?php
									echo wp_nonce_url(
										add_query_arg(
											array(
												'action' => 'move-up-menu-item',
												'menu-item' => $item_id,
											),
											remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
										),
										'move-menu_item'
									);
								?>" class="item-move-up"><abbr title="<?php esc_attr_e('Move up'); ?>">&#8593;</abbr></a>
								|
								<a href="<?php
									echo wp_nonce_url(
										add_query_arg(
											array(
												'action' => 'move-down-menu-item',
												'menu-item' => $item_id,
											),
											remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
										),
										'move-menu_item'
									);
								?>" class="item-move-down"><abbr title="<?php esc_attr_e('Move down'); ?>">&#8595;</abbr></a>
							</span>
							<a class="item-edit" id="edit-<?php echo $item_id; ?>" title="<?php esc_attr_e('Edit Menu Item'); ?>" href="<?php
								echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
							?>"><?php _e( 'Edit Menu Item' ); ?></a>
						</span>
					</dt>
				</dl>
		
				<div class="menu-item-settings" id="menu-item-settings-<?php echo $item_id; ?>">
					<?php if( 'custom' == $item->type ) : ?>
						<p class="field-url description description-wide">
							<label for="edit-menu-item-url-<?php echo $item_id; ?>">
								<?php _e( 'URL' ); ?><br />
								<input type="text" id="edit-menu-item-url-<?php echo $item_id; ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
							</label>
						</p>
					<?php endif; ?>
					<p class="description description-thin">
						<label for="edit-menu-item-title-<?php echo $item_id; ?>">
							<?php _e( 'Navigation Label' ); ?><br />
							<input type="text" id="edit-menu-item-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
						</label>
					</p>
					<p class="description description-thin">
						<label for="edit-menu-item-attr-title-<?php echo $item_id; ?>">
							<?php _e( 'Title Attribute' ); ?><br />
							<input type="text" id="edit-menu-item-attr-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
						</label>
					</p>
					<p class="field-link-target description">
						<label for="edit-menu-item-target-<?php echo $item_id; ?>">
							<input type="checkbox" id="edit-menu-item-target-<?php echo $item_id; ?>" value="_blank" name="menu-item-target[<?php echo $item_id; ?>]"<?php checked( $item->target, '_blank' ); ?> />
							<?php _e( 'Open link in a new window/tab' ); ?>
						</label>
					</p>
					<p class="field-css-classes description description-thin">
						<label for="edit-menu-item-classes-<?php echo $item_id; ?>">
							<?php _e( 'CSS Classes (optional)' ); ?><br />
							<input type="text" id="edit-menu-item-classes-<?php echo $item_id; ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo $item_id; ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
						</label>
					</p>
					<p class="field-xfn description description-thin">
						<label for="edit-menu-item-xfn-<?php echo $item_id; ?>">
							<?php _e( 'Link Relationship (XFN)' ); ?><br />
							<input type="text" id="edit-menu-item-xfn-<?php echo $item_id; ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
						</label>
					</p>
					<p class="field-description description description-wide">
						<label for="edit-menu-item-description-<?php echo $item_id; ?>">
							<?php _e( 'Description' ); ?><br />
							<textarea id="edit-menu-item-description-<?php echo $item_id; ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo $item_id; ?>]"><?php echo esc_html( $item->description ); // textarea_escaped ?></textarea>
							<span class="description"><?php _e('The description will be displayed in the menu if the current theme supports it.'); ?></span>
						</label>
					</p> 
                    
                    <?php
						// Nav_Menu_Roles plugin compatibility // PLUGCOMP
						do_action( 'wp_nav_menu_item_custom_fields', $item_id, $item, $depth, $args );
					?>
                     
                    <!-- Icon 
                    <p class="field-icon description description-wide">
						<label for="edit-menu-item-icon-<?php echo $item_id; ?>">							
                        	<?php _e( 'Menu Icon', 'jellypixel' ); ?><br />
							<input type="text" id="edit-menu-item-icon-<?php echo $item_id; ?>" class="widefat edit-menu-item-icon" name="menu-item-icon[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->icon ); ?>" />
                            <div class="normal">
                                <span class="bold">Example: </span>fa-smile-o <br>
                                Click <a href="http://fortawesome.github.io/Font-Awesome/icons/">here</a> for icon list. 
                            </div>
						</label>
					</p>
                    -->

					<!-- Megamenu    
                    <p class="megamenu-options-description description-section" <?php echo $megamenuRootStyle; ?>>
                        <?php _e( 'Megamenu Options', 'jellypixel' ); ?>
                    </p>
                    <div class="clear"></div>                
                    <div class="menu-item-megamenu-options" <?php echo $megamenuRootStyle; ?>>						
                        <div class="megamenu-options-header">
                            <p class="field-megamenu description description-wide">
                                <label for="edit-menu-item-megamenu-<?php echo $item_id; ?>">
                                    <?php 
                                        if ( esc_attr( $item->megamenu ) == '1' ) {
                                            $checkedTag = ' checked = "checked"';	
                                            $megamenuSectionStyle = "";
                                        }
                                        else {
                                            $checkedTag = '';	
                                            $megamenuSectionStyle = 'style="display:none;"';
                                        }
                                    ?>
                                    <input type="hidden" name="menu-item-megamenu[<?php echo $item_id; ?>]" value="0" />                       
                                    <input type="checkbox" id="edit-menu-item-megamenu-<?php echo $item_id; ?>" class="widefat code edit-menu-item-megamenu" name="menu-item-megamenu[<?php echo $item_id; ?>]" value="1" <?php echo $checkedTag; ?>>
                                    <?php echo sprintf( __( 'Enable %1$s Megamenu %2$s on this menu', 'jellypixel' ), '<span class="bold">', '</span>' ) ?>                             
                                </label>
                            </p>                                
                        </div>
                        <div class="megamenu-options-section" <?php echo $megamenuSectionStyle; ?>>
                            <p class="field-megamenu-type description description-thin">
                                <label for="edit-menu-item-megamenu-type-<?php echo $item_id; ?>">
                                    <?php _e( 'Megamenu Type', 'jellypixel' ); ?>
                                    <select id="edit-menu-item-megamenu-type-<?php echo $item_id; ?>" class="widefat code edit-menu-item-megamenu-type" name="menu-item-megamenu-type[<?php echo $item_id; ?>]">
	                                    <option value="column6" <?php echo ( esc_attr( $item->megamenu_type ) == 'column6' ? 'selected="selected"' : '' ) ?>>6 Columns (Default)</option>
                                        <option value="column4" <?php echo ( esc_attr( $item->megamenu_type ) == 'column4' ? 'selected="selected"' : '' ) ?>>4 Columns</option>
                                        <option value="column3" <?php echo ( esc_attr( $item->megamenu_type ) == 'column3' ? 'selected="selected"' : '' ) ?>>3 Columns</option>
                                        <option value="column2" <?php echo ( esc_attr( $item->megamenu_type ) == 'column2' ? 'selected="selected"' : '' ) ?>>2 Columns</option>
                                        <option value="column1" <?php echo ( esc_attr( $item->megamenu_type ) == 'column1' ? 'selected="selected"' : '' ) ?>>1 Column</option>
                                        <option value="verticaltab" <?php echo ( esc_attr( $item->megamenu_type ) == 'verticaltab' ? 'selected="selected"' : '' ) ?>>Vertical Tab</option>                                        
                                    </select>
                                </label>
                            </p>
                            <p class="megamenu-type-description"></p>
                            <div class="clear"></div>
                            <p class="megamenu-label description description-wide">										
                                <label for="edit-menu-item-megamenu-bg-<?php echo $item_id; ?>">
                                    <?php _e( 'Megamenu Background', 'jellypixel' ); ?>                             
                                </label>           
                            </p>
                            <div class="clear"></div>
                            <div class="field-megamenu-bg megamenu-bg-wrapper">       
                                <?php
									$megamenu_bg = esc_attr( $item->megamenu_bg );
								
                                    if ( !empty( $megamenu_bg ) ) {
                                        $megamenuBgPreviewStyle = '';
                                        $megamenuBgUploadStyle = 'style="display:none;"';
                                        $megamenuBgRemoveStyle = '';
                                    }
                                    else {
                                        $megamenuPreviewStyle = 'style="display:none;"';
                                        $megamenuBgUploadStyle = '';
                                        $megamenuBgRemoveStyle = 'style="display:none;"';
                                    }
                                ?>
                                <img id="megamenu-preview-<?php echo $item_id; ?>" class="megamenu-preview" src="<?php echo esc_attr( $item->megamenu_bg ); ?>" <?php echo $megamenuPreviewStyle; ?> />

                                <span id="megamenu-bg-upload-<?php echo $item_id; ?>" class="megamenu-bg-upload button button-primary" <?php echo $megamenuBgUploadStyle; ?>><?php _e( 'Upload', 'jellypixel' ); ?></span>
                                <span id="megamenu-bg-remove-<?php echo $item_id; ?>" class="megamenu-bg-remove button" <?php echo $megamenuBgRemoveStyle; ?>><?php _e( 'Remove', 'jellypixel' ); ?></span>  
                                                                
                                <input type="hidden" id="edit-menu-item-megamenu-bg-<?php echo $item_id; ?>" class="edit-menu-item-megamenu-bg" name="menu-item-megamenu-bg[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->megamenu_bg ); ?>" />
                            </div>
                        </div>						
                        <div class="clear"></div>
                    </div>
                    -->
                    
                    <!-- Thumbnail 
                    <?php
						if ( $item->type_label == "Post" || $item->type_label == "Page" || $item->type_label == "Category" ) {
							?>
                                <p class="field-widget description description-thin" <?php echo $thumbnailRootStyle; ?>>
                                    <label for="edit-menu-item-thumbnail-<?php echo $item_id; ?>">
                                        <?php _e( 'Post Thumbnail', 'jellypixel' ); ?>
                                        <select id="edit-menu-item-thumbnail-<?php echo $item_id; ?>" class="widefat code edit-menu-item-thumbnail" name="menu-item-thumbnail[<?php echo $item_id; ?>]">
                                        	<option value="yes" <?php echo ( esc_attr( $item->thumbnail ) == 'yes' ? 'selected="selected"' : '' ) ?>>Show Thumbnail</option>    
                                            <option value="no" <?php echo ( esc_attr( $item->thumbnail ) == 'no' ? 'selected="selected"' : '' ) ?>>Hide Thumbnail</option>                                            
                                        </select>
                                    </label>  
                                </p>
                    		<?php
						}
					?>
                    -->
	
                    <!-- Widget 
					<p class="field-widget description description-thin" <?php echo $widgetRootStyle; ?>>
                        <label for="edit-menu-item-widget-<?php echo $item_id; ?>">
                            <?php _e( 'Widget', 'jellypixel' ); ?>
                            <select id="edit-menu-item-widget-<?php echo $item_id; ?>" class="widefat code edit-menu-item-widget" name="menu-item-widget[<?php echo $item_id; ?>]">
                            	<option value="-1"><?php _e( 'No Widget' ); ?></option>
								<?php 
									foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) 
									{ 
										?>
											<option value="<?php echo ucwords( $sidebar['id'] ); ?>" <?php echo ( esc_attr( $item->widget ) == ucwords( $sidebar['id'] ) ? 'selected="selected"' : '' ) ?>>
												<?php echo ucwords( $sidebar['name'] ); ?>
											</option>
										<?php
                                    }
                                ?>
                            </select>
                        </label>
                    </p>
                    -->
                    
					<div class="menu-item-actions description-wide submitbox">
						<?php if( 'custom' != $item->type && $original_title !== false ) : ?>
							<p class="link-to-original">
								<?php printf( __('Original: %s'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
							</p>
						<?php endif; ?>
						<a class="item-delete submitdelete deletion" id="delete-<?php echo $item_id; ?>" href="<?php
						echo wp_nonce_url(
							add_query_arg(
								array(
									'action' => 'delete-menu-item',
									'menu-item' => $item_id,
								),
								remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
							),
							'delete-menu_item_' . $item_id
						); ?>"><?php _e('Remove'); ?></a> <span class="meta-sep"> | </span> <a class="item-cancel submitcancel" id="cancel-<?php echo $item_id; ?>" href="<?php echo esc_url( add_query_arg( array('edit-menu-item' => $item_id, 'cancel' => time()), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) ) ) );
							?>#menu-item-settings-<?php echo $item_id; ?>"><?php _e('Cancel'); ?></a>
					</div>
		
					<input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo $item_id; ?>]" value="<?php echo $item_id; ?>" />
					<input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
					<input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
					<input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
					<input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
					<input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
				</div><!-- .menu-item-settings-->
				<ul class="menu-item-transport"></ul>
			<?php
			
			$output .= ob_get_clean();	
		}
	}

?>