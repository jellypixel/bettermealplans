var $j = jQuery.noConflict();

/**
 * Callback function for the 'click' event of the 'Set Footer Image'
 * anchor in its meta box.
 *
 * Displays the media uploader for selecting an image.
 *
 * @param    object    $j    A reference to the jQuery object
 * @since    0.1.0
 */

function renderMediaUploader( previewWrapper ) {
	'use strict';

	var file_frame, image_data, json;

	/**
	 * If an instance of file_frame already exists, then we can open it
	 * rather than creating a new instance.
	 */
	
	if ( file_frame !== undefined ) {
		file_frame.open();
		
		return;
	}

	/**
	 * If we're this far, then an instance does not exist, so we need to
	 * create our own.
	 *
	 * Here, use the wp.media library to define the settings of the Media
	 * Uploader. We're opting to use the 'post' frame which is a template
	 * defined in WordPress core and are initializing the file frame
	 * with the 'insert' state.
	 *
	 * We're also not allowing the user to select more than one image.
	 */
	file_frame = wp.media.frames.file_frame = wp.media({
		frame: 'select', //post
//		state: 'insert',
		multiple: false,
		library: {
			type: 'image'	
		}
	});

	/**
	 * Setup an event handler for what to do when an image has been
	 * selected.
	 *
	 * Since we're using the 'view' state when initializing
	 * the file_frame, we need to make sure that the handler is attached
	 * to the insert event.
	 */
	file_frame.on( 'select', function() {
		// Read the JSON data returned from the Media Uploader
		json = file_frame.state().get( 'selection' ).first().toJSON();
		
		// First, make sure that we have the URL of an image to display
		if ( 0 > $j.trim( json.url.length ) ) {
			return;
		}

		// After that, set the properties of the image and display it

		previewWrapper			
				.attr( 'src', json.url )
				.show();
				
		previewWrapper
				.siblings(".megamenu-bg-upload")
				.hide()
				.siblings(".megamenu-bg-remove")
				.show();
//			.parent()
//			.removeClass( 'hidden' );

		// Next, hide the anchor responsible for allowing the user to select an image
		/*$j( '#featured-footer-image-container' )
			.prev()
			.hide();*/

		// Display the anchor for the removing the featured image
		/*$j( '#featured-footer-image-container' )
			.next()
			.show();*/

		// Store the image's information into the meta data fields
		/*$j( '#footer-thumbnail-src' ).val( json.url );
		$j( '#footer-thumbnail-title' ).val( json.title );
		$j( '#footer-thumbnail-alt' ).val( json.title );*/
		
		previewWrapper.siblings(".edit-menu-item-megamenu-bg").val( json.url );

	});

	// Now display the actual file_frame
	file_frame.open();

}

/**
 * Callback function for the 'click' event of the 'Remove Footer Image'
 * anchor in its meta box.
 *
 * Resets the meta box by hiding the image and by hiding the 'Remove
 * Footer Image' container.
 *
 * @param    object    $j    A reference to the jQuery object
 * @since    0.2.0
 */
function resetUploadForm( previewWrapper ) {
	
	previewWrapper
		.attr( 'src', '' )
		.hide();
		
	previewWrapper
				.siblings(".megamenu-bg-upload")
				.show()
				.siblings(".megamenu-bg-remove")
				.hide();
				
	previewWrapper.siblings(".edit-menu-item-megamenu-bg").val( '' );
	
	/*'use strict';

	// First, we'll hide the image
	$j( '#featured-footer-image-container' )
		.children( 'img' )
		.hide();

	// Then display the previous container
	$j( '#featured-footer-image-container' )
		.prev()
		.show();

	// We add the 'hidden' class back to this anchor's parent
	$j( '#featured-footer-image-container' )
		.next()
		.hide()
		.addClass( 'hidden' );

	// Finally, we reset the meta data input fields
	$j( '#featured-footer-image-info' )
		.children()
		.val( '' );
*/
}

/**
 * Checks to see if the input field for the thumbnail source has a value.
 * If so, then the image and the 'Remove featured image' anchor are displayed.
 *
 * Otherwise, the standard anchor is rendered.
 *
 * @param    object    $j    A reference to the jQuery object
 * @since    1.0.0
 */
function renderFeaturedImage( previewWrapper ) {

	/* If a thumbnail URL has been associated with this image
	 * Then we need to display the image and the reset link.
	 */
	 
	if ( '' !== $j.trim ( $j( '#footer-thumbnail-src' ).val() ) ) {

		$j( '#featured-footer-image-container' ).removeClass( 'hidden' );

		$j( '.megamenu-bg-upload' )
			.parent()
			.hide();

		$j( '.megamenu-bg-remove' )
			.parent()
			.removeClass( 'hidden' );

	}

}

$j(function() {
	// renderFeaturedImage( $j );
	
	$j( '.megamenu-bg-upload' ).on( 'click', function( e ) {
		// Stop the anchor's default behavior
		e.preventDefault();
		// Display the media uploader
		renderMediaUploader( $j( this ).siblings( ".megamenu-preview" ) );
	});

	$j( '.megamenu-bg-remove' ).on( 'click', function( e ) {
		// Stop the anchor's default behavior
		e.preventDefault();
		// Remove the image, toggle the anchors
		resetUploadForm( $j( this ).siblings( ".megamenu-preview" ) );
	});
	
	$j( '.edit-menu-item-megamenu' ).on( 'change', function() {
		if ( $j(this).prop("checked") == true ) {
			$j(this).closest(".megamenu-options-header").siblings(".megamenu-options-section").slideDown(400);
			$j(this).closest(".menu-item").nextUntil(".menu-item-depth-0").find(".field-widget").show();
		}
		else {
			$j(this).closest(".megamenu-options-header").siblings(".megamenu-options-section").slideUp(400);
			$j(this).closest(".menu-item").nextUntil(".menu-item-depth-0").find(".field-widget").hide();
		}
	});

	$j( 'body').on('mouseup', '.menu-item-bar', function() {	
//	$j( '.menu-item-bar' ).on( 'mouseup', function() {		
		var thisEl = $j(this);
		setTimeout( function() {
			if ( thisEl.parent(".menu-item").hasClass("menu-item-depth-0") ) {
				thisEl.siblings(".menu-item-settings").find(".menu-item-megamenu-options, .megamenu-options-description ").show();
				thisEl.siblings(".menu-item-settings").find(".edit-menu-item-widget").parents("p").hide();
				thisEl.siblings(".menu-item-settings").find(".edit-menu-item-thumbnail").parents("p").hide();
			}	
			else {
				thisEl.siblings(".menu-item-settings").find(".menu-item-megamenu-options, .megamenu-options-description ").hide();
				thisEl.siblings(".menu-item-settings").find(".edit-menu-item-widget").parents("p").show();
				thisEl.siblings(".menu-item-settings").find(".edit-menu-item-thumbnail").parents("p").show();
			}
		}, 200 );		
	});
	
	$j( '.edit-menu-item-megamenu-type' ).each(function() {
		if ( $j(this).val() == 'verticaltab' ) {			
			$j(this).closest(".megamenu-options-section").find(".megamenu-type-description").html("First level submenus of this menu will be act as tabs.<br>Second level submenus will be act as the content of the tab.").show();
		}
		else {
			$j(this).closest(".megamenu-options-section").find(".megamenu-type-description").html("").hide();
		}
	});
	
	$j( '.edit-menu-item-megamenu-type' ).on( 'change', function() {
		if ( $j(this).val() == 'verticaltab' ) {
			$j(this).closest(".megamenu-options-section").find(".megamenu-type-description").html("First level submenus of this menu will be act as tabs.<br>Second level submenus will be act as the content of the tab.").show();
		}
		else {
			$j(this).closest(".megamenu-options-section").find(".megamenu-type-description").html("").hide();
		}
	});
});
