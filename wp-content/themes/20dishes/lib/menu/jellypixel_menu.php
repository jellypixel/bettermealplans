<?php

	class jellypixel_menu {
			
		function __construct() {	
			add_filter( 'wp_setup_nav_menu_item', array( $this, 'jellypixel_add_nav' ) );	
			add_action( 'wp_update_nav_menu_item', array( $this, 'jellypixel_update_nav'), 10, 3 );			
			add_filter( 'wp_edit_nav_menu_walker', array( $this, 'jellypixel_edit_walker'), 10, 2 );	
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		}
		
		function jellypixel_add_nav( $menu_item ) {		
			$menu_item->icon = get_post_meta( $menu_item->ID, '_menu_item_icon', true );
			$menu_item->thumbnail = get_post_meta( $menu_item->ID, '_menu_item_thumbnail', true ); // NEW
			$menu_item->widget = get_post_meta( $menu_item->ID, '_menu_item_widget', true );			
			$menu_item->megamenu = get_post_meta( $menu_item->ID, '_menu_item_megamenu', true );
			$menu_item->megamenu_type = get_post_meta( $menu_item->ID, '_menu_item_megamenu_type', true );
			$menu_item->megamenu_bg = get_post_meta( $menu_item->ID, '_menu_item_megamenu_bg', true );					
			return $menu_item;			
		}
		
		function jellypixel_update_nav( $menu_id, $menu_item_db_id, $args ) {		
			// Icon
			if ( is_array( $_REQUEST['menu-item-icon'] ) ) {
				$icon_value = $_REQUEST['menu-item-icon'][$menu_item_db_id];
			
				update_post_meta( $menu_item_db_id, '_menu_item_icon', $icon_value );
			}
			
			// Thumbnail // NEW
			if ( is_array( $_REQUEST['menu-item-thumbnail'] ) ) {
				$thumbnail_value = $_REQUEST['menu-item-thumbnail'][$menu_item_db_id];			
					
				update_post_meta( $menu_item_db_id, '_menu_item_thumbnail', $thumbnail_value );
			}	
			
			// Widget
			if ( is_array( $_REQUEST['menu-item-widget'] ) ) {
				$widget_value = $_REQUEST['menu-item-widget'][$menu_item_db_id];			
					
				update_post_meta( $menu_item_db_id, '_menu_item_widget', $widget_value );
			}	
			
			// Megamenu Enabler
			if ( is_array( $_REQUEST['menu-item-megamenu'] ) ) {
				if ( !isset( $_REQUEST['menu-item-megamenu'][$menu_item_db_id] ) || $_REQUEST['menu-item-megamenu'][$menu_item_db_id] == "0" ) {
					$megamenu_value = '';	
				}
				else {
					$megamenu_value = $_REQUEST['menu-item-megamenu'][$menu_item_db_id];
				}
				
				update_post_meta( $menu_item_db_id, '_menu_item_megamenu', $megamenu_value );					
			}	
			
			// Megamenu Type
			if ( is_array( $_REQUEST['menu-item-megamenu-type'] ) ) {
				$megamenu_type_value = $_REQUEST['menu-item-megamenu-type'][$menu_item_db_id];			
					
				update_post_meta( $menu_item_db_id, '_menu_item_megamenu_type', $megamenu_type_value );
			}
			
			// Megamenu Bg
			if ( is_array( $_REQUEST['menu-item-megamenu-bg'] ) ) {
				$megamenu_bg_value = $_REQUEST['menu-item-megamenu-bg'][$menu_item_db_id];
				
				update_post_meta( $menu_item_db_id, '_menu_item_megamenu_bg', $megamenu_bg_value );
			}		
		}
		
		function jellypixel_edit_walker($walker,$menu_id) {		
			return 'Walker_Nav_Menu_Edit_Custom';			
		}	
		
		function enqueue_scripts() {
	
			wp_enqueue_media();
	
			wp_enqueue_script(
				'menu-js',
				get_template_directory_uri() . '/lib/menu/js/default.js',
				array( 'jquery' ),
				null,
				'all'
			);
	
		}
		
	}
	
	include_once( 'edit_jellypixel_menu.php' );	
	include_once( 'wp_bootstrap_navwalker.php' );
?>