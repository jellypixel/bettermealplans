<nav class="row nav-post">	
    <?php
		global $sidebarPosition;
		
		if ( $sidebarPosition == 'none' ) 
		{
			$heightClass = 'wider';	
		}
		else 
		{
			$heightClass = '';	
		}
		/*<div id="nav-previous-post" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?php
				previous_post_link( '<h4>' . __( 'PREVIOUS ARTICLE', 'jellypixel' ) . '</h4>%link', '%title' );
			?>
		</div>                            
		<div id="nav-next-post" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<?php
				next_post_link( '<h4>' . __( 'NEXT ARTICLE', 'jellypixel' ) . '</h4>%link', '%title' );
			?>
		</div>    
		<div class="clear"></div>*/
		
		// Check first / last post
		if ( get_previous_post( true ) == NULL || get_next_post( true ) == NULL ) 
		{
			$colClass = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';	
			$centerClass = 'center';
		}
		else 
		{
			$colClass = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';
			$centerClass = '';
		}
		
		// Previous
		if ( get_previous_post( true ) ) 
		{
			$prevPost = get_previous_post( true );
			$arrPrevThumbUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $prevPost->ID ), 'gallery-thumb' );
			$prevTitle = get_the_title( $prevPost->ID );			
			$prevLink = get_permalink( get_adjacent_post( false, '', true ) );
			
			if ( $prevLink == get_permalink() ) 
			{ 
				$prevLink = '#';
			}			
			
			if ( empty( $arrPrevThumbUrl[0] ) ) 
			{
				$prevThumbUrl = get_bloginfo('template_url') . '/img/noimage.png';	
			}
			else 			
			{
				$prevThumbUrl = $arrPrevThumbUrl[0];	
			}
			?>
            	<div id="nav-previous-post" class="<?php echo $colClass; ?>">
                    <!--<h4><?php  _e( 'PREVIOUS ARTICLE', 'jellypixel' ) ?></h4>-->
                    <div class="nav-post-wrapper <?php echo $heightClass; ?>">
                        <div class="nav-post-inner-wrapper">
                            <a href="<?php echo $prevLink; ?>" title="<?php echo $prevTitle; ?>">
                                <div class="nav-post-thumb" style="background-image:url(<?php echo $prevThumbUrl; ?>);">
                                    <img src="<?php echo $prevThumbUrl; ?>" title="<?php echo $prevTitle; ?>" />
                                </div>
                            </a>
                            <div class="nav-post-bottom">
                                <div class="nav-post-title">
                                    <a href="<?php echo $prevLink; ?>" title="<?php echo $prevTitle; ?>">
                                        <h4 class="post-title sans <?php echo $centerClass; ?>">
                                            <?php echo $prevTitle; ?>
                                        </h4>
                                    </a>
                                </div>
                            </div>	                                                                     
                        </div>	
                    </div>
                </div>
            <?php
		}
		
		// Next
		if ( get_next_post( true ) ) 
		{
			$nextPost = get_next_post( true );
			$arrNextThumbUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $nextPost->ID ), 'gallery-thumb' );			
			$nextTitle = get_the_title( $nextPost->ID );			
			$nextLink = get_permalink( get_adjacent_post( false, '', false ) );
			
			if ( $nextLink == get_permalink() ) 
			{ 
			 	$nextLink = '#';
			}
			
			if ( empty( $arrNextThumbUrl[0] ) ) 
			{
				$nextThumbUrl = get_bloginfo('template_url') . '/img/noimage.png';	
			}
			else 			
			{
				$nextThumbUrl = $arrNextThumbUrl[0];	
			}
			
			?>
            	<div id="nav-next-post" class="<?php echo $colClass; ?>">
                    <!--<h4><?php  _e( 'NEXT ARTICLE', 'jellypixel' ) ?></h4>-->
                    <div class="nav-post-wrapper <?php echo $heightClass; ?>">
                        <div class="nav-post-inner-wrapper">
                            <a href="<?php echo $nextLink; ?>" title="<?php echo $nextTitle; ?>">
                                <div class="nav-post-thumb" style="background-image:url(<?php echo $nextThumbUrl; ?>);">
                                    <img src="<?php echo $nextThumbUrl; ?>" title="<?php echo $nextTitle; ?>" />
                                </div>
                            </a>                                    
                            <div class="nav-post-bottom">
                                <div class="nav-post-title">
                                    <a href="<?php echo $nextLink; ?>" title="<?php echo $nextTitle; ?>">
                                        <h4 class="post-title sans <?php echo $centerClass; ?>">
                                            <?php echo $nextTitle; ?>
                                        </h4>
                                    </a>
                                </div>
                            </div>	                                                                     
                        </div>	
                    </div>
                </div>
            <?php
		}
	?>
    <div class="clear"></div>
</nav>