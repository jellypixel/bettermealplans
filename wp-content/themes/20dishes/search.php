<?php
	get_header();
?>

	<div class="beginpage container">
    	<?php get_template_part( 'section', 'subscribe' ); ?> 
        
        <div class="blog-cat-title">
        	Search result for: <?php the_search_query(); ?>
        </div>    
    
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <?php
                    // The Loop
                    if ( have_posts() ) 
                    {					
                        while ( have_posts() ) 
                        {
                           the_post();
						
							?>
                                <article id="post-<?php the_ID(); ?>">                                    
									<div class="search-meta-top">
										<?php 
											//$archiveDateLink = get_month_link( get_the_time('Y'), get_the_time('n') );
										?>                                    
                                    	<time class="entry-date" datetime="2014-01-01T11:11:11+00:00"><?php echo get_the_date(); ?></time>
                                    </div>
                                    <div class="search-wrapper">
                                    	<?php
											// Thumb 
											$attachment_image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-thumb' );
											$attachment_image_thumb_url = $attachment_image_thumb[0];
											
											if ( function_exists('has_post_thumbnail') ) {
												if ( has_post_thumbnail() ) {
													 $thumbnail =  wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
												}
											}
											
											echo 
												'<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 search-thumbnail-wrapper">' .
													'<a href="' . get_permalink() . '" title="' . get_the_title() . '">' .
														'<div class="search-thumbnail" style="background-image:url(' . $attachment_image_thumb_url . ')">' .
														'</div>' .
													'</a>' .												
												'</div>';													
										?>

                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 post-content post-search">
                                        	<header class="post-header">
                                                <h4 class="search-title">
                                                	<a rel="bookmark" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>		
                                                </h4>
                                            </header>
											<?php 
                                                $content =  trim(strip_tags(get_the_excerpt()));
                                                $content = get_the_excerpt();
                                                if ( empty( $content ) ) {
                                                    $content = '';	
                                                }                                                                                                                                                
                                            ?>
                                            <div class="post-excerpt post-search">
                                            	<?php echo $content; ?>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <footer><hr></footer>
                                </article>	
                            <?php	
                        }
                    } 
                    else 
                    {
                        // No posts found
						?>						
                            <h2 class="post-title sans"><?php _e( 'Nothing Found', 'jellypixel' ); ?></h2>
                            <div class="search post-content">               
								<?php _e( 'Sorry, please try to search with different keyword.', 'jellypixel' ); ?>
                                <?php //get_search_form(); ?>
                            </div>
                        <?php						
                    }
                    
                    // Reset
                    wp_reset_postdata();			
                ?>
                
                <?php
                    get_template_part( 'section', 'paging' );
                ?>
            </div>  
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            	<aside role="complementary">
                    <?php get_sidebar( 'blog' ); ?>
                </aside>
            </div>                  
        </div>
    </div>

<?php
	get_footer();
?>