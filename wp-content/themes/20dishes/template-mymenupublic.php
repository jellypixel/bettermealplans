<?php
	/* Template Name: My Menu Public
	 *
	 * The template for my menu in the MLM software
	 *
	 * @package WordPress
	 * @subpackage 20Dishes
	 * @since Antiquity
	 */   
	 
	get_header("mymenu"); 
	
	/* Prepare Variables 
	---------------------*/
	// Globals
	global $user_id;
	global $user_info;
	global $userServingSize;
	global $fontPrintSize;
	global $allDiets;
	global $checkedPrint;
	global $userDiet;
	global $excludeList;
	global $userExcludeList;
	
	global $userMenuRestriction;
	global $userRestrictCalories;
	global $userRestrictFat;
	global $userRestrictCarbs;		
	
	global $userRegistrationTime;		
	global $free_user;
	
	global $breakfast;

	$ads_supported = current_user_can('memberpress_product_authorized_25969') && !current_user_can('administrator');

	// Master/Slave
	$master = false;	
	if ( $user_id != get_current_user_id() )
	{
		$master = true;
	}
	
	function nullTrueFalseToCheck( $value ) 
	{
		if ( is_null( $value ) )
		{
			// New user, check all
			return 'checked';
		}
		else
		{
			if ( $value == 'false' )
			{
				return '';	
			}
			else
			{
				return 'checked';															
			}
		}
	}
?>
	<!-- CSS -->
	<link href='<?php bloginfo('template_url')?>/sys/css/jquery-ui.min.css' media='screen' rel='stylesheet' type='text/css' /> 
    <link href='<?php bloginfo('template_url')?>/sys/css/style.css?v=81' media='screen' rel='stylesheet' type='text/css' />
	<link href='<?php bloginfo('template_url')?>/sys/css/responsive.css?v=81' media='screen' rel='stylesheet' type='text/css' />
    <link href='<?php bloginfo('template_url')?>/sys/css/print.css?v=81' rel="stylesheet" type="text/css" media="print" />

	<!-- Changeable Styling -->
	<style>
		@media print {
			.addrecipe-topmeta, .notes-content, .ingredient_list_ingredient, .recipe-content, .textareaadditem, .addrecipe-topmeta, .addrecipe-rest, #shoppinglist,
			#shoppinglist span,
			#shoppinglist b,
			#shoppinglist input,
			#shoppinglist label,
			#shoppinglist textarea,
			#global-announcement-stepbystep p,
			#step_list th, #step_list td
			{
				font-size: <?php echo $fontPrintSize; ?>px;
				line-height: <?php echo $fontPrintSize + 2; ?>px;
			}	
			
			.print-bold, h4.tabtitle-print
			{
				font-size: <?php echo $fontPrintSize + 2; ?>px;
				line-height: <?php echo $fontPrintSize + 4; ?>px;
			}
			
			.addrecipe-title
			{
				font-size: <?php echo $fontPrintSize + 6; ?>px;
				line-height: <?php echo $fontPrintSize + 8; ?>px;
			}	

			.ingredientTitle span
			{
				font-size: <?php echo $fontPrintSize + 8; ?>px !important;
				line-height: <?php echo ($fontPrintSize + 8) * 2; ?>px !important;
			}					
		}
	</style>	
	
    <!-- JS -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/sys/js/jquery-ui.datepicker.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/sys/js/jquery.numberpicker.js"></script>
    <script type="text/javascript">
      var template_url = '<?php echo bloginfo('template_url'); ?>';
		var userRestrictCalories = '<?php echo $userRestrictCalories; ?>';
		var userRestrictFat = '<?php echo $userRestrictFat; ?>';
		var userRestrictCarbs = '<?php echo $userRestrictCarbs; ?>';
		
		jQuery(document).ready(function(){    
			//Number Picker
				var valNum = parseInt(jQuery("#options #number-picker").html());
				jQuery("#options #number-picker").dpNumberPicker({
					min: 1, // Minimum value.
					max: 25, // Maximum value.
					value: valNum, // Initial value
					step: 1, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//Number Picker
				var valNum = parseInt(jQuery("#wizard1 #number-picker").html());
				jQuery("#wizard1 #number-picker").dpNumberPicker({
					min: 1, // Minimum value.
					max: 25, // Maximum value.
					value: valNum, // Initial value
					step: 1, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//Number Font Picker
				var valNum = parseInt(jQuery("#options #fontprint-picker").html());
				jQuery("#options #fontprint-picker").dpNumberPicker({
					min: 10, // Minimum value.
					max: 20, // Maximum value.
					value: valNum, // Initial value
					step: 1, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//Number Font Picker
				var valNum = parseInt(jQuery("#wizard5 #fontprint-picker").html());
				jQuery("#wizard5 #fontprint-picker").dpNumberPicker({
					min: 10, // Minimum value.
					max: 20, // Maximum value.
					value: valNum, // Initial value
					step: 1, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//Calories Picker
				var valNum = parseInt(jQuery("#restrictcalories-picker").html());
				jQuery("#restrictcalories-picker").dpNumberPicker({
					value: valNum, // Initial value
					step: 10, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//Fat Picker
				var valNum = parseInt(jQuery("#restrictfat-picker").html());
				jQuery("#restrictfat-picker").dpNumberPicker({
					value: valNum, // Initial value
					step: 1, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//Carbs Picker
				var valNum = parseInt(jQuery("#restrictcarbs-picker").html());
				jQuery("#restrictcarbs-picker").dpNumberPicker({
					value: valNum, // Initial value
					step: 1, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//iCheck
				jQuery('#options input, #usersettings input').iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal',
				});

			// close bottom box
			jQuery(".bottombar-remove").click(function() {
				jQuery(".bottombar").fadeOut(400);
			});

			jQuery(".centerbar-remove").click(function() {
				jQuery(".centerbar").fadeOut(400);
			});
		});
    </script>    
<?php	

	include 'sys/loadweek.php';
	include 'sys/loadrecipe.php';
	include 'sys/addrecipe.php';

	?>
	
		<!--
		<form method="GET" action="https://www.amazon.com/AmazonFresh/aws/cart/add.html"> 
			<input type="hidden" name="AWSAccessKeyId" value="AKIAJKMGLVDW7JGGJKGQ" /><br/> 
	
			<input type="text" name="ASIN.B071J15YSQ"/>
			<input type="text" name="Quantity.20"/>
		 
			<input type="submit" name="add" value="add" /> 
		</form>
		-->
		
    	<div id="mymenu-stage-wrapper" class="container">
        	<div id="sys-tabpanel" role="tabpanel">
            
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                	<div id="extratab-wrapper">                          
                        <!--
                        <li id="startguide" class="extratab"><span class="hidden1199">Start </span>Guide</li>
                        -->
						
						<?php
							$day = date('w');
							
							$prevWeekStart = date('M d', strtotime('-'.($day+7).' days'));
							$prevWeekEnd = date('M d', strtotime('+'.(6-$day-7).' days'));
							
							$thisWeekStart = date('M d', strtotime('-'.$day.' days'));
							$thisWeekEnd = date('M d', strtotime('+'.(6-$day).' days'));
							
							$nextWeekStart = date('M d', strtotime('-'.($day-7).' days'));
							$nextWeekEnd = date('M d', strtotime('+'.(6-$day+7).' days'));	
																		
						?>							
						
						<div id="extratab-wrapper-icons">
							<li id="selectWeek" class="extratab" title="Select Week">
								<div id="selectedWeek" data-step="2" data-intro="Click Calendar to view all available weekly plans">
									<?php												
										if( !$free_user ) 
										{
											?>
												<?php echo $thisWeekStart .' - '. $thisWeekEnd . ' '; ?><div class="week-name">This Week</div>
											<?php
										} 
										else 
										{ 
											?>
												Free User<br>Demo Week</div>
											<?php
										} 
									?>
                                </div>

								<?php		
									/*
										<div class="icon tdicon-calendar"></div>
									*/
																		
									if( !$free_user ) {
										?>
                                            <div class="submenu-wrapper">
                                                <ul class="submenu">
                                                    <li id="prevweek" title="Previous Week"><?php echo $prevWeekStart .' - '. $prevWeekEnd . ' '; ?><div class="week-name">Previous Week</div></li>
                                                    <li id="thisweek" title="This Week"><?php echo $thisWeekStart .' - '. $thisWeekEnd . ' '; ?><div class="week-name">This Week</div></li>
                                                    <li id="nextweek" title="Next Week"><?php echo $nextWeekStart .' - '. $nextWeekEnd . ' '; ?><div class="week-name">Next Week</div></li>
                                                </ul>
                                            </div>
										<?php
									} 
								?>
							</li>						
						</div>
                        <div class="clear"></div>
                    </div>
                
                	<?php
						/*
						<div id="sys-title">My Menu</div>
						*/
                    ?>
						  <?php if($ads_supported) { ?>
						  <li role="presentation" class="adssupported">   
                        <a id="planAds" href="https://www.bettermealplans.com/upgrade/" target="_blank">
                            Upgrade to Remove Ads
                        </a>
                    </li> 
						  <?php } ?>   
                    <li role="presentation" class="active first">
                    	<div class="step-title">Step 1</div>                        
                        <a id ="planTab" href="#plan" aria-controls="plan" role="tab" data-toggle="tab">
                        	<div id="mealplan-icon" class="step-icon"></div>
                            Meal Plan
                        </a>
                    </li>     
					<li role="presentation">
                    	<div class="step-title">Step 2</div>                        
                        <a id ="shoppinglistTab" href="#shoppinglist" aria-controls="shoppinglist" role="tab" data-toggle="tab" data-step="8" data-intro="Click here for your customizable shopping list.">
                        	<div id="shoppinglist-icon" class="step-icon"></div>
                            Shopping List
                        </a>
                    </li>						
                    <li role="presentation" class="last">
                    	<div class="step-title">Step 3</div>                        
                        <a id ="stepTab" href="#stepbystep" aria-controls="stepbystep" role="tab" data-toggle="tab" data-step="9" data-intro="Click here for your simple food prep guide.">
                        	<div id="prepguide-icon" class="step-icon"></div>
                            Prep Guide
                        </a>
                    </li>
                    
                    <div id="extratab-footer">         
                    	<!-- <div id="resetMenu">Reset</div>   -->         	
                        <div id="massDelete" title="Hide Multiple. Enabling this will remove the yes/no confirmation when you delete a recipe allowing for faster deletion." data-step="3" data-intro='Click this button to bypass the "Are you sure?" warning message'>Hide Multiple</div>                        
                        
                        <?php							
							if ( $master || $userMenuRestriction['startfromscratch'] !== 'true' ) 
							{
								?>
                                	<div role="presentation" class="last" id="customMenuButton" aria-haspopup="true" data-step="7" data-intro="Click here to create and save custom menus">
										<?php
                                            require_once('sys/loadcustommenu.php');
                                            
                                            $current_custommenu = loadmenu( $user_id );								
                                        ?>                        
                                    
                                        <span>Start From Scratch</span>
                                        <div class="customMenu-child">
                                            <div class="customMenu-warning">
                                                You can only see the latest three menus saved. Please remember this when you are saving a new menu.
                                            </div>
                                            <div class="customMenu-stage">
                                                <div class="custommenu-wrapper">
                                                    <div class="newcustommenu custommenu">
                                                        New custom menu...
                                                    </div>
                                                </div>
                                                <?php
                                                    echo $current_custommenu;
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
							}							
						?>                        
                        
                        <a id ="recipesdetailTab" href="#recipesdetail" aria-controls="recipesdetail" role="tab" data-toggle="tab" title="Print" data-step="11" data-intro="Click here for to print your plan, shopping list and simple food prep guide. Thank you and happy prepping!">Print</a>
                        <a id ="optionsTab" href="#options" aria-controls="settings" role="tab" data-toggle="tab" title="Settings" data-step="1" data-intro="Change your settings here">Planner Settings</a>
                        <?php
							if ( $master )
							{
								?>
                                	<a id ="usersettingsTab" href="#usersettings" aria-controls="usersettings" role="tab" data-toggle="tab" title="User Settings">User Settings</a>
                                <?php
							}
						?>
                    </div>                    
                    
                    <?php
						/*
						<div id="loadCustomMenu" title="Load archived menu">Custom Menu</div>                        
						<li id="readme" class="extratab" title="Learn how to use">MEMBER FAQ</li>
                    	<li role="presentation" class="last" id="loadCustomMenu"><a><div class="icon glyphicon glyphicon-file" title="Load archived menu"></div></a></li>						                    	*/
					?>
                    
                    <?php
						/*
                    <li role="presentation" class="last" id="addcardfavorite"><a><div class="icon glyphicon glyphicon-heart" title="Favorite Recipes"></div></a></li>                    
                    <li role="presentation" class="last" id="massDelete"><a><div class="icon glyphicon glyphicon-eye-close" title="Batch delete. Enabling this will remove the yes/no confirmation when you delete a recipe allowing for faster deletion." data-step="3" data-intro='Click this button to bypass the "Are you sure?" warning message'></div></a></li>			
					<li role="presentation" class="last" id="printButton"><a id ="recipesdetailTab" href="#recipesdetail" aria-controls="recipesdetail" role="tab" data-toggle="tab"><div class="icon tdicon-print" title="Print" data-step="11" data-intro="Click here for to print your plan, shopping list and simple food prep guide. Thank you and happy prepping!"></div></a></li>						
					<li role="presentation" class="last" id="optionButton"><a id ="optionsTab" href="#options" aria-controls="settings" role="tab" data-toggle="tab"><div class="icon tdicon-gear-full" title="Settings" data-step="1" data-intro="Change your settings here"></div></a></li>   			
                    <li role="presentation" class="last" id="customMenuButton" aria-haspopup="true" data-step="7" data-intro="Click here to create and save custom menus">
                    	<?php
							require_once('sys/loadcustommenu.php');
							
							$current_custommenu = loadmenu( $user_id );
						?>
                    
                    
                    	<span>Custom Menu</span>
                        <div class="customMenu-child">
                        	<div class="customMenu-warning">
	                        	You can only see the latest three menus saved. Please remember this when you are saving a new menu.
                            </div>
                            <div class="customMenu-stage">
                            	<div class="custommenu-wrapper">
									<div class="newcustommenu custommenu">
										New custom menu...
									</div>
								</div>
								<?php
                                    echo $current_custommenu;
                                ?>
                            </div>
                        </div>
                    </li>  		
                    	*/
					?>		
                    
                </ul>
                
                
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="plan">                       	
						<img src="https://bettermealplans.com/wp-content/themes/20dishes/img/logo.png" class="img-print" alt="logo" />                        
						<h1 class="tabtitle-print">Meal Plan</h1>
                        <input id="user_id" type="hidden" value="<?php echo $user_id; ?>" />
                        
                        <div class="plan-action">
                        	<div class="savewrapper <?php echo $ads_supported? "ads" : "";?>">
											<?php
												if($ads_supported) {
											?>
												<div id="212216507">
													<script type="text/javascript">
														try {
																window._mNHandle.queue.push(function (){
																	window._mNDetails.loadTag("212216507", "728x90", "212216507");
																});
														}
														catch (error) {}
													</script>
												</div>
											<?php
												}
											?>
											<div class="singlebutton">
												<div id="saveweek" class="savebutton ready" data-step="6" data-intro="Click here to save your changes">								
														SAVE PLAN
												</div>
												<div id="saveasnewmenu" class="savebutton savebutton-green ready">								
														SAVE MENU TO ARCHIVE
												</div>      
											</div>                            
                            </div>
                        
                        	<div id="custommenu-title">
	                        	This Week
                            </div>
                        </div>
                        
						<div id="planpanel">                        	
							<?php
								$alltheweek = "";			
								if($free_user)	
									$alltheweek = loadWeek('1995-05-29', $user_id);	
								else			
									$alltheweek = loadWeek('', $user_id);
								
								if ( !empty( $alltheweek ) ) {
									echo $alltheweek;	
								}
							?>                            
						</div>					                    	
                        
                         
                    </div>
                    
                    
                    <div role="tabpanel" class="tab-pane tabInternal" id="stepbystep">
						<img src="https://bettermealplans.com/wp-content/themes/20dishes/img/logo.png" class="img-print" alt="logo" />
						<div class="insidePane">
							<h1 class="tabtitle-print">Simple Food Prep Guide</h1>
							
							<?php
								// Lookiehere
								$args = array (
									'p'                      => '1591',
								);
								
								// The Query
								$query = new WP_Query( $args );
								
								// The Loop
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										
										echo '<div id="global-announcement-stepbystep">';
										the_content();
										// do something
										echo '</div>';
									}
								} else {
									// no posts found
								}
								
								// Restore original Post Data
								wp_reset_postdata();
							?>

							<?php
								if($ads_supported) {
							?>
								<div id="692241469">
									<script type="text/javascript">
										try {
												window._mNHandle.queue.push(function (){
													window._mNDetails.loadTag("692241469", "728x90", "692241469");
												});
										}
										catch (error) {}
									</script>
								</div>
							<?php
								}
							?>
							
							<div id="step_list">

							</div>
						</div>
                    </div>
                    
                    <div role="tabpanel" class="tab-pane" id="shoppinglist">
                    	<div class="plan-action">
							  			<div class="savewrapper clearfix <?php echo $ads_supported? "ads" : "";?>">
											<?php
												if($ads_supported) {
											?>
												<div id="633766217">
													<script type="text/javascript">
														try {
																window._mNHandle.queue.push(function (){
																	window._mNDetails.loadTag("633766217", "728x90", "633766217");
																});
														}
														catch (error) {}
													</script>
												</div>
											<?php
												}
											?>
										  <div>
												<div id="saveshopping" class="savebutton ready">								
														SAVE
												</div>				
										
												<div id="reloadshopping" class="reloadbutton ready">								
														Refresh
												</div>     
										   </div>   
                            </div> 
                        </div>
                    
						<img src="https://bettermealplans.com/wp-content/themes/20dishes/img/logo.png" class="img-print" alt="logo" />
						<div class="insidePane">				
							<h1 class="tabtitle-print">Shopping List</h1>
							<div id="ingredient_list">

							</div>						  
						</div>
						  							
					</div>	
                    
                    <div role="tabpanel" class="tab-pane" id="recipesdetail">                        
                    	<div class="plan-action">
                        	<div class="savewrapper clearfix">
                                <div id="print" class="savebutton ready">								
                                    PRINT
                                </div>
                            </div>	
                        </div>
                    
						<img src="https://bettermealplans.com/wp-content/themes/20dishes/img/logo.png" class="img-print" alt="logo" />
                        
						<div class="insidePane">
						
							<input class="recipe-printcheck" id="recipePrintPlan" type="checkbox" name="recipePrintPlan" checked="checked" />
							<label for="recipePrintPlan" class="checkPrint-label2">Plan</label>
							<input class="recipe-printcheck" id="recipePrintShopping" type="checkbox" name="recipePrintShopping" checked="checked" />
							<label for="recipePrintShopping" class="checkPrint-label2">Shopping</label>

							<input class="recipe-printcheck" id="recipePrintStep" type="checkbox" name="recipePrintStep" checked="checked" />
							<label for="recipePrintStep" class="checkPrint-label2">Simple Food Prep Guide</label>								
							<input class="recipe-printcheck" id="recipePrintRecipe" type="checkbox" name="recipePrintRecipe" checked="checked" />
							<label for="recipePrintRecipe" class="checkPrint-label2">Recipes</label>
						
							<h1 class="tabtitle-print">Recipes</h1>
							
							<div id="recipesdetail-stage">
								<input class="recipe-check" id="recipeSelectAll" type="checkbox" name="recipeSelectAll" />
								<label for="recipeSelectAll" class="checkPrint-label">Select All</label>
								<div class="accordion" id="recipesdetail-accordion">
									
								</div>
							</div>
							
							<div id="recipesdetail-print">
							</div>
						</div>                    							
                    </div>
                    
                    <div role="tabpanel" class="tab-pane" id="options">
                    	<div class="plan-action">
                            <div class="savewrapper clearfix">
                                <div id="saveoptions" class="savebutton ready" title="Save Options">SAVE</div>
                            </div>	
                        </div>
						<div class="insidePane">
							<div class='error-warning'></div>
                            
                            <?php
								if ( $master || $userMenuRestriction['portion'] !== 'true' ) 
								{
									?>
                                        <h3>How many people are in your family?</h3>
                                        <div id="number-picker">
                                            <?php 
                                                echo $userServingSize; 
                                            ?>
                                        </div>
                                    <?php
								}
								
								if ( $master || $userMenuRestriction['diet'] !== 'true' ) 
								{
									?>
                                        <h3>Choose Your Plan</h3>
                                        <form id="dietOptions">
                                            <?php 
                                                foreach($allDiets as $diet) {
                                                        
                                                    if ( ($diet['name'] == 'BREAKFAST') )
                                                        continue;
                                                            
                                                    echo '<div style="float:left;"><input id="checkDiet-' . $diet['name'] . '" type="checkbox" name="dietCheck" '. (in_array($diet["id"], $userDiet)? "checked":"") .' value="'.$diet["id"].'" /><label for="checkDiet-' . $diet['name'] . '">' . $diet['name'] . '</label></div>';
                                                }
                                            ?>			
                                        </form>
                                    <?php
								}
								
								if ( $master || $userMenuRestriction['avoidfood'] !== 'true' ) 
								{
									?>								
                                        <h3>Are there food groups that you prefer to avoid? Check all that apply.</h3>
                                        <form id="excludedCategoriesOptions">
                                            <?php 
                                                foreach($excludeList as $exclude) {
                                                    
                                                    $checked = false;
                                                    foreach($userExcludeList as $userExclude){
                                                        
                                                        if($exclude["cat_id"] == $userExclude)		
                                                            $checked = true;																							
                                                    }
                                                    
                                                    if($checked)																							
                                                        echo '<div style="float:left;"><input id="checkExclude-' . $exclude['name'] . '" type="checkbox" name="excludeCheck" checked value="'.$exclude["cat_id"].'" /><label for="checkExclude-' . $exclude['name'] . '">' . $exclude['name'] . '</label></div>';	
                                                    else
                                                        echo '<div style="float:left;"><input id="checkExclude-' . $exclude['name'] . '" type="checkbox" name="excludeCheck" value="'.$exclude["cat_id"].'" /><label for="checkExclude-' . $exclude['name'] . '">' . $exclude['name'] . '</label></div>';		
                                                }
                                            ?>			
                                        </form>		
                                    <?php
								}
								
								if ( $master || $userMenuRestriction['excludeadditionalingredients'] !== 'true' ) 
								{
									?>
                                    	<h3>Search for any Additional Ingredients you would like to exclude.</h3>
                                        <!--
                                        This is a keyword search. That means if you exclude "beef" you will also exclude "beef bacon" and any kind of keyword with "beef" in it. However since this is a keyword search it does not understand context i.e excluding pork will not exclude bacon automatically. Use the excluded categories above for that.
                                        <br><br>
                                        -->
                                        Please note that by excluding ingredients you may not receive the complete provided meal plan each week. This is due to the fact that our meal plans do not take into account individual restrictions and recipes may contain some of these items. These recipes will no longer show on your weekly prepared meal plan.
                                        <br><br>
                                        <div class="ui-widget">
                                          <input id="excludedIngredientAdd" placeholder="Type Here" />
                                        </div>
                                         
                                        <div class="ui-widget">
                                            <div id="excludedIngredientList">
                                            <?php								
                                                $exingredients = get_user_meta($user_id, "exingredients", false); 
                                                $exingredients = $exingredients[0];
                                                                                    
                                                foreach($exingredients as $exingredient) {
                                                    echo "<div class='excludedIngredient'>" . $exingredient . "</div>";
                                                }									
                                            ?>
                                            </div>
                                        </div>	
                                    <?php	
								}
							?>
												
							<h3>Print Settings</h3>
							<?php
								if($checkedPrint == 'no')																					
									echo '<div style="float:left;"><input id="checkPrint" type="checkbox" name="checkPrint" value="no" /><label for="checkPrint">' . 'Print one recipe per page' . '</label></div><br><br>';	
								else
									echo '<div style="float:left;"><input id="checkPrint" type="checkbox" value="yes" name="checkPrint" checked /><label for="checkPrint">' . 'Print one recipe per page' . '</label></div><br><br>';
							?>
							<h4>Print Font Size</h4>
							<div id="fontprint-picker">
								<?php 
									echo $fontPrintSize; 
								?>
							</div>										
						</div>		                    							
                    </div>
                    
                    <?php
						if( $master )
						{
							?>
                                <div role="tabpanel" class="tab-pane" id="usersettings">    
                                	<div class="plan-action">
                                        <div class="savewrapper clearfix">
                                            <div id="saveusersettings" class="savebutton ready" title="Save Options">SAVE</div>
                                        </div>	
                                    </div>
                                	<div class="insidePane">                               
                                        <form id="restrictMealplan">                                                                            	
                                        	<div class="pane-block">
                                                <h3>Restrict meal planner access to:</h3>                                    
                                                <div class="pane-row">
                                                    <input id="checkRestrict-AddRecipe" type="checkbox" name="checkRestrict" <?php echo nullTrueFalseToCheck( $userMenuRestriction['addrecipe'] ); ?> value="addrecipe" />
                                                    <label for="checkRestrict-AddRecipe">Add Recipe</label>
                                                </div>
                                                <div class="pane-row">                                            
                                                    <input id="checkRestrict-AddNote" type="checkbox" name="checkRestrict" <?php echo nullTrueFalseToCheck( $userMenuRestriction['addnote'] ); ?> value="addnote" />
                                                    <label for="checkRestrict-AddNote">Add Note</label>
                                                </div>
                                                <div class="pane-row">                                           
                                                    <input id="checkRestrict-RemoveRecipe" type="checkbox" name="checkRestrict" <?php echo nullTrueFalseToCheck( $userMenuRestriction['removerecipe'] ); ?> value="removerecipe" />
                                                    <label for="checkRestrict-RemoveRecipe">Remove Recipe</label>
                                                </div>
                                            </div>                                           
                                        
                                        	<div class="pane-block">
                                                <h3>Restrict menu access to:</h3>                                        
                                                <div class="pane-row">
                                                    <input id="checkRestrict-StartFromScratch" type="checkbox" name="checkRestrict" <?php echo nullTrueFalseToCheck( $userMenuRestriction['startfromscratch'] ); ?> value="startfromscratch" />
                                                    <label for="checkRestrict-StartFromScratch">Start from scratch</label>
                                                </div>  
                                            </div>
                                            
                                            <div class="pane-block">
                                                <h3>Restrict option access to:</h3>                                    
                                                <div class="pane-row">
                                                    <input id="checkRestrict-Portion" type="checkbox" name="checkRestrict" <?php echo nullTrueFalseToCheck( $userMenuRestriction['portion'] ); ?> value="portion" />
                                                    <label for="checkRestrict-Portion">Portion</label>
                                                </div>
                                                <div class="pane-row">
                                                    <input id="checkRestrict-Diet" type="checkbox" name="checkRestrict" <?php echo nullTrueFalseToCheck( $userMenuRestriction['diet'] ); ?> value="diet" />
                                                    <label for="checkRestrict-Diet">Plan</label>
                                                </div>
                                                <div class="pane-row">
                                                    <input id="checkRestrict-AvoidFood" type="checkbox" name="checkRestrict" <?php echo nullTrueFalseToCheck( $userMenuRestriction['avoidfood'] ); ?> value="avoidfood" />
                                                    <label for="checkRestrict-AvoidFood">Food groups to avoid</label>
                                                </div>
                                                <div class="pane-row">
                                                    <input id="checkRestrict-ExcludeAdditionalIngredients" type="checkbox" name="checkRestrict" <?php echo nullTrueFalseToCheck( $userMenuRestriction['excludeadditionalingredients'] ); ?> value="excludeadditionalingredients" />
                                                    <label for="checkRestrict-ExcludeAdditionalIngredients">Additional ingredients to exclude</label>
                                                </div>
                                            </div>
                                            
                                            <div class="pane-block">
                                                <h3>Show warning when daily nutrition exceed:</h3>                                        
                                                <div class="pane-row pane-numberpicker">                                                
                                                    <label for="checkRestrict-Calories">Calories</label>
                                                    <div id="restrictcalories-picker">
                                                        <?php echo $userRestrictCalories; ?>
                                                    </div>
                                                </div>  
                                                <div class="pane-row pane-numberpicker">
                                                    <label for="checkRestrict-Fat">Fat</label>
                                                    <div id="restrictfat-picker">
                                                        <?php echo $userRestrictFat; ?>
                                                    </div>
                                                </div>  
                                                <div class="pane-row pane-numberpicker">
                                                    <label for="checkRestrict-Carbs">Carbs</label>
                                                    <div id="restrictcarbs-picker">
                                                        <?php echo $userRestrictCarbs; ?>
                                                    </div>
                                                </div>  
                                            </div>
                                        </form>	
                                    </div>
                                </div>
                    		<?php
						}
					?>
                                        
                </div>                
            </div>   
        </div>
        
        <div id="pdf-preview">
        </div>
        
        <div class="overlay">
            <div id="addcard-lightbox" class="lightbox">
            	<div id="addcard-tabs" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#add-recipe" aria-controls="add-recipe" role="tab" data-toggle="tab">Add Recipe</a></li>
                        <li role="presentation"><a href="#favorite-recipes" aria-controls="favorite-recipes" role="tab" data-toggle="tab">Your Favorited Recipes</a></li>
                        <li role="presentation"><a href="#add-note" aria-controls="add-note" role="tab" data-toggle="tab">Add a Note to Your Plan</a></li>                            
                        <li class="lightbox-close" title="Back to plan"><div class="icon tdicon-thin-close"></div></li>  
                    </ul>
                    
                    <div class="tab-content">
                    	<div role="tabpanel" class="tab-pane active" id="add-recipe">
                        	<div id="searchrecipe-form">
                            	<form action="" id="searchrecipe-formitself" name="searchrecipe-formitself">
                                    <input type="text" id="searchrecipe-input" name="searchrecipe-input" maxlength="255" placeholder="Search..." />
                                    <input type="submit" id="searchrecipe-submit" value="Search Recipe" />
                                    <div class="clear"></div>
                                </form>
                            </div>
                            <div id="searchrecipe-catbox-wrapper">
                            	<div class="searchrecipe-catbox" value="25">5 Ingredients or Less</div>
                                <div class="searchrecipe-catbox" value="87">Breakfast</div>
                                <div class="searchrecipe-catbox" value="696">Budget</div>
                                <div class="searchrecipe-catbox" value="601">Casserole</div>
                                <div class="searchrecipe-catbox" value="1066">Freezer Meals</div>
                                <div class="searchrecipe-catbox" value="948">Instant Pot</div>
                                <div class="searchrecipe-catbox" value="22">Kid Friendly</div>
                                <div class="searchrecipe-catbox" value="278">Side Dishes</div>
                                <div class="searchrecipe-catbox" value="336">Slow Cooker</div>
                                <div class="searchrecipe-catbox" value="21">Soups, Stews, Chili</div>                                
                            </div>
                            <div id="searchrecipe-message"></div>
                            <div id="searchrecipe-stage">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="favorite-recipes">
                        	<div id="favoriterecipes-message"></div>                                
                            <div id="favoriterecipes-stage">
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="add-note">                        
                        	<textarea id="addnotes-textarea" rows="8" cols="50" maxlength="500"></textarea>
                            <div id="addnotes-button" class="savebutton">Add Note</div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div id="dailymacros-lightbox" class="lightbox">
            	<div id="dailymacros-title-wrapper">
                	<div id="dailymacros-title">
                    </div>
                	<div class="lightbox-close" title="Back to plan"><div class="icon tdicon-thin-close"></div></div>  
                </div>
                <div id="dailymacros-content">
                </div>    	
            </div>
            
            <div id="addmenu-lightbox" class="lightbox">
            	<div id="addcard-tabs" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#add-menu" aria-controls="add-menu" role="tab" data-toggle="tab">Add Menu</a></li>
                        <li class="lightbox-close" title="Cancel"><div class="icon tdicon-thin-close"></div></li>  
                    </ul>
                    
                    <div class="tab-content">
                    	<div role="tabpanel" class="tab-pane active" id="add-menu">
                            <div id="addmenu-form">
                                <form action="" id="addmenu-formitself" name="addmenu-formitself">
                                    <input type="text" id="addmenu-input" name="addmenu-input" maxlength="255" />
                                    <input type="submit" id="addmenu-submit" value="Search Menu" />
                                    <div class="clear"></div>
                                </form>
                            </div>
                            <div id="addmenu-stage">
                            </div>
                        </div>
                    </div>
                </div>       	
            </div>
            
            <div id="loadcustommenu-lightbox" class="lightbox">
            	<div id="loadcustommenu-tabs" role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#loadcustommenu-tab" aria-controls="add-menu" role="tab" data-toggle="tab">Select Custom Menu...</a></li>
                        <li class="lightbox-close" title="Cancel"><div class="icon tdicon-thin-close"></div></li>  
                    </ul>
                    
                    <div class="tab-content">
                    	<div role="tabpanel" class="tab-pane active" id="loadcustommenu-tab">
                        	<div id="loadcustommenu-stage">
                            </div>
                        </div>
                    </div>
                </div>       	
            </div>
            
            <div id="textinputModal" class="modalbox">
            	<div class="modal-scrutiny">        
                	Save this menu as...            
                	<input type="text" id="textinputModal-input" maxlength="80" placeholder="Type a name" />
                    <div class="textinputModal-error"></div>
                </div>
                <div class="modal-action clearfix">
                    <div class="yesbtn modal-default modalbtn">OK</div>
                    <div class="nobtn modalbtn">Cancel</div>
                </div>
            </div>
            
            <div id="yesnoModal" class="modalbox">
            	<div class="modal-scrutiny">                    
                	Are you sure want to hide this recipe?
                </div>
                <div class="modal-action clearfix">
                    <div class="yesbtn modal-default modalbtn">Yes</div>
                    <div class="nobtn modalbtn">No</div>
                </div>
            </div>
            
            <div id="static-modal" class="modalbox">
            	<div class="modal-scrutiny">                    
                	
                </div>
                <div class="modal-action clearfix">
                    <div class="nobtn modal-default modalbtn">OK</div>
                </div>
            </div>
            
            <div id="wizard-lightbox" class="lightbox currentwizard0">
            	<div class="wizard-header">
                	Setup                 
                </div>       
                <div class="wizard-error error-warning"></div>     	
            	<div class="wizard-wrapper">
                	<div id="wizard0" class="wizard-panel">
                    	<div class="wizard-panel-inner">
                        	<div class="wizard-float">
                            	<h1>Welcome</h1>
                            	This is one time setup. 
                                <br>Don't worry, you can always change it later.
                                <br>If you're ready, press Next.
                            </div>
                        </div>
                    </div>
                	<div id="wizard1" class="wizard-panel">
                    	<div class="wizard-panel-inner">
                        	<div class="wizard-step">Step 1 of 5</div>
                            <h3>How many people are in your family?</h3>
                            <div id="number-picker">
                                <?php 
                                    echo $userServingSize; 
                                ?>
                            </div>
                        </div>
                    </div>
                    <div id="wizard2" class="wizard-panel">
                    	<div class="wizard-panel-inner">
                        	<div class="wizard-step">Step 2 of 5</div>
                            <h3>Choose Your Plan</h3>
                            <form id="dietOptions">
                                <?php 
                                    foreach($allDiets as $diet) {
                                            
                                        if ( ($diet['name'] == 'BREAKFAST') )
                                            continue;
                                                
                                        echo '<div style="float:left;"><input id="checkDiet-' . $diet['name'] . '" type="checkbox" name="dietCheck" value="'.$diet["id"].'" /><label for="checkDiet-' . $diet['name'] . '">' . $diet['name'] . '</label></div>';
                                    }
                                ?>			
                            </form>
                        </div>
                    </div>
                    <div id="wizard3" class="wizard-panel">
                    	<div class="wizard-panel-inner">
                        	<div class="wizard-step">Step 3 of 5</div>
                            <h3>Are there food groups that you prefer to avoid? Check all that apply.</h3>
                            <form id="excludedCategoriesOptions">
                                <?php 
                                    foreach($excludeList as $exclude) {
                                        
                                        $checked = false;
                                        foreach($userExcludeList as $userExclude){
                                            
                                            if($exclude["cat_id"] == $userExclude)		
                                                $checked = true;																							
                                        }
                                        
                                        if($checked)																							
                                            echo '<div style="float:left;"><input id="checkExclude-' . $exclude['name'] . '" type="checkbox" name="excludeCheck" checked value="'.$exclude["cat_id"].'" /><label for="checkExclude-' . $exclude['name'] . '">' . $exclude['name'] . '</label></div>';	
                                        else
                                            echo '<div style="float:left;"><input id="checkExclude-' . $exclude['name'] . '" type="checkbox" name="excludeCheck" value="'.$exclude["cat_id"].'" /><label for="checkExclude-' . $exclude['name'] . '">' . $exclude['name'] . '</label></div>';		
                                    }
                                ?>			
                            </form>	
                        </div>
                    </div>
                    <div id="wizard4" class="wizard-panel">
                    	<div class="wizard-panel-inner">
                        	<div class="wizard-step">Step 4 of 5</div>
                            <h3>Search for any Additional Ingredients you would like to exclude.</h3>
                            <!--
                            This is a keyword search. That means if you exclude "beef" you will also exclude "beef bacon" and any kind of keyword with "beef" in it. However since this is a keyword search it does not understand context i.e excluding pork will not exclude bacon automatically. Use the excluded categories above for that.
                            <br><br>
                            -->
                            Please note that by excluding ingredients you may not receive the complete provided meal plan each week. This is due to the fact that our meal plans do not take into account individual restrictions and recipes may contain some of these items. These recipes will no longer show on your weekly prepared meal plan.
                            <br><br>
                            <div class="ui-widget">
                              <input id="excludedIngredientAddFirst" placeholder="Type Here" />
                            </div>
                             
                            <div class="ui-widget">
                                <div id="excludedIngredientListFirst">
                                <?php                                
                                    $exingredients = get_user_meta($user_id, "exingredients", false); 
                                    $exingredients = $exingredients[0];
                                                                        
                                    foreach($exingredients as $exingredient) {
                                        echo "<div class='excludedIngredient'>" . $exingredient . "</div>";
                                    }									
                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="wizard5" class="wizard-panel">
                    	<div class="wizard-panel-inner">
                        	<div class="wizard-step">Step 5 of 5</div>
                            <h3>Print Settings</h3>
                            <?php
                                if($checkedPrint == 'no')																					
                                    echo '<div style="margin-bottom:40px;"><input id="checkPrint" type="checkbox" name="checkPrint" value="no" /><label for="checkPrint">' . 'Print one recipe per page' . '</label></div>';	
                                else
                                    echo '<div style="margin-bottom:40px;"><input id="checkPrint" type="checkbox" value="yes" name="checkPrint" checked /><label for="checkPrint">' . 'Print one recipe per page' . '</label></div>';
                            ?>                            
                            <h3>Printable Font Size</h3>
                            <div id="fontprint-picker">
                                <?php 
                                    echo $fontPrintSize; 
                                ?>
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="wizard-footer">
                	<div id="wizard-back" class="savebutton ready" title="Back">Back</div>
                    <div id="wizard-next" class="savebutton ready" title="Next">Next</div>
                    <div id="wizard-save" class="savebutton ready" title="Save & Finish">Save &amp; Finish</div>
                </div>
            </div>
        </div>
        
        <div class="overlay-2">
        	<div id="addrecipe-lightbox" class="lightbox-2" style="display:none;">
            	<div class="lightbox-2-header">
                	<div class="lightbox-2-title">                        
                    </div>
                	<div class="inner-lightbox-close"></div>    
                    <div class="clear"></div>                    
                </div>
                <div id="addrecipe-stage" class="lightbox-2-content">                    	
                </div>
                <div class="lightbox-2-input">
                	<div id="reallyaddrecipe" class="savebutton">Add to plan</div>
                </div>
            </div>  
            
            <div id="yesnoModal2" class="modalbox" style="display:none;">
            	<div class="modal-scrutiny">                    
                	Are you sure want to hide this recipe?
                </div>
                <div class="modal-action clearfix">
                    <div class="yesbtn modal-default modalbtn">Yes</div>
                    <div class="nobtn modalbtn">No</div>
                </div>
            </div> 
        </div>
        
        <div class="overlay-3">
        	<div id="freeuser-lightbox" class="lightbox-3" style="display:none;">
            	<div class="lightbox-3-header">
                	<div class="lightbox-3-title">                        
                    	Unlock <span class="lightbox-3-highlight">Better Meal Plans</span>
                    </div>
                	<div class="lightbox-3-close"></div>    
                    <div class="clear"></div> 
                </div>
                <div class="lightbox-3-content">  
                	<ul>
                    	<li><span class="bold">Enjoy scrumptious Weekly Dinner Meal Plans</span> for Keto, Paleo, Gluten-Free, AIP, Vegetarian and of course, Real Food WITH daily macros!</li>
                        <li><span class="bold">Gain private access</span> 1000+ tasty recipes!</li>
                        <li><span class="bold">Full access to our amazing step-by-step meal prep guide</span> - shut off your brain, follow the steps to “prep ahead” for the upcoming week of tasty meals</li>
                        <li class="bold">Get highly-optimized shopping lists </li>
                        <li class="bold">Enjoy being part of a community</li>
                        <li class="bold">Easily add your very own recipes to Better Meal Plans </li>
                    </ul>					
                </div>
                <div class="lightbox-3-input">
                	<a href="http://bettermealplans.com/subscribe" target="_blank">
	                	<div id="takemein" class="savebutton">GET PREMIUM</div>
                    </a>
                </div>
            </div>
        </div>
        
        <div class="underlay-help" data-step="12" data-intro="Contact our dedicated staff for more help" data-position="top">
		  </div>
		  
		  <?php if($ads_supported) { ?>
				<div class="bottombar">
					<div id="630437854">
						<script type="text/javascript">
							try {
									window._mNHandle.queue.push(function (){
										window._mNDetails.loadTag("630437854", "728x90", "630437854");
									});
							}
							catch (error) {}
						</script>
					</div>
					<div class="bottombar-remove" title="Remove">
						<div class="icon tdicon-thin-close"></div>										
					</div>
				</div>
			<?php } ?>

			<?php if($ads_supported) { ?>
				<div class="centerbar">
					<div class="centerbar-overflow">
						<div id="138656555">
							<script type="text/javascript">
								try {
										window._mNHandle.queue.push(function (){
											window._mNDetails.loadTag("138656555", "300x600", "138656555");
										});
								}
								catch (error) {}
							</script>
						</div>
					</div>
					<div class="centerbar-remove" title="Remove">
						<div class="icon tdicon-thin-close"></div>										
					</div>
				</div>
			<?php } ?>

			<?php if($ads_supported) { ?>
				<div class="sidebar">
					<div class="sidebar-inside">
						<div id="357584548" class="add">
							<script type="text/javascript">
								try {
										window._mNHandle.queue.push(function (){
											window._mNDetails.loadTag("357584548", "300x250", "357584548");
										});
								}
								catch (error) {}
							</script>
						</div>

						<div id="842422537" class="add">
							<script type="text/javascript">
								try {
										window._mNHandle.queue.push(function (){
											window._mNDetails.loadTag("842422537", "300x250", "842422537");
										});
								}
								catch (error) {}
							</script>
						</div>

						<div id="471114846" class="add">
							<script type="text/javascript">
								try {
										window._mNHandle.queue.push(function (){
											window._mNDetails.loadTag("471114846", "300x250", "471114846");
										});
								}
								catch (error) {}
							</script>
						</div>

						<div id="286411653" class="add">
							<script type="text/javascript">
								try {
										window._mNHandle.queue.push(function (){
											window._mNDetails.loadTag("286411653", "300x250", "286411653");
										});
								}
								catch (error) {}
							</script>
						</div>
					</div>
				</div>
			<?php } ?>

    <?php	
?>

<?php
	get_footer();
?>

<script src="<?php bloginfo('template_url'); ?>/sys/js/jquery-ui.touchpunch.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/sys/js/default.js?v=81" type="text/javascript"></script>

<script type="text/javascript">
	jQuery(document).ready(function(){    
		// on Refresh
		checkNextWeek(); 
		
		jQuery("#currentweek").val('thisweek');
	});
</script>