<?php
	get_header();
?>

	<div class="beginpage container">
    	<div class="blog-cat-title">
        	<h1>
	        	Category <?php echo single_cat_title( '', false ); ?>
            </h1>
        </div>
        
        <div class="blog-wrappers">
        	<?php
				// The Loop
				if ( have_posts() ) 
				{		
					?>
                    	<div class="row">
                        	<?php
								while ( have_posts() ) 
								{
									the_post();
									global $theme_options;
									
									// Get All Recipes
									$all_recipes = WPRM_Recipe_Manager::get_recipe_ids_from_content( get_the_content() );				
									
									// Get First Recipe
									if ( isset( $all_recipes[0] ) ) {
										$first_recipe_id = $all_recipes[0];
										$first_recipe = WPRM_Recipe_Manager::get_recipe( $first_recipe_id );
									}
									
									// Title
									$title = get_the_title();
									?>					
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-12 blog-outer">                                        	    
                                            <div class="blog-wrapper">
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> 
                                                    <div class="blog-thumbnail">
                                                    	<?php
															echo do_shortcode('[wprm-recipe-image id="' . $first_recipe_id .'"]');
														?>
                                                    </div>
                                                </a>
                                                <div class="blog-content">
                                                	<?php /*
                                                    <div class="blog-meta">
                                                        <?php 
															$all_cats = get_the_category(); 
															$all_cats_el = '';
															if ( !empty( $all_cats) )
															{
																foreach ( $all_cats as $cat )
																{
																	if ( empty( $all_cats_el ) )
																	{
																		$all_cats_el = $cat->name;
																	}
																}
															}
															
															echo $all_cats_el;
														?>
                                                    </div>
													*/ ?>
                                                    <div class="blog-title">
                                                        <h2 class="post-title">
                                                            <a rel="bookmark" href="<?php the_permalink(); ?>">
                                                                <?php echo $title; ?>
                                                            </a>
                                                        </h2> 
                                                    </div>
                                                    <div class="blog-readmore">
                                                        <a rel="bookmark" href="<?php the_permalink(); ?>">
                                                            Read more
                                                        </a>
                                                    </div>																								
                                                </div>                                               
                                            </div>
                                        </div>
									<?php
								}
							?>
                        </div>
                    <?php				
				} 
								
				// Reset
				wp_reset_postdata();
			?>
            
            <div class="col-12">
            	<div class="blog-nav">
					<?php
                        get_template_part( 'section', 'paging' );
                    ?>
                </div>
            </div>
        </div>
    
    	<?php
		/*
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <?php
					if ( have_posts() ) 
                    {	
						while ( have_posts() ) 
                        {
                            the_post();
							
							global $theme_options;
							
							// Thumb 
							$attachment_image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-thumb' );
							$attachment_image_thumb_url = $attachment_image_thumb[0];
							
							// Excerpt
							$excSource = get_the_excerpt();							
							
							// Title
							$title = get_the_title();
							?>					
    	                        <article id="post-<?php the_ID(); ?>" <?php post_class('blog-wrapper'); ?>>           
                                
                                	<a href="<?php the_permalink(); ?>">                                                
                                        <div class="blog-thumbnail" style="background-image:url(<?php echo $attachment_image_thumb_url; ?>);">
                                        </div>
                                    </a>
                                    
                                    <div class="blog-content">
                                        <div class="blog-title">
                                        	<h1 class="post-title">
                                                <a rel="bookmark" href="<?php the_permalink(); ?>">
                                                    <?php echo $title; ?>
                                                </a>
                                            </h1> 
                                        </div>
                                        <div class="blog-excerpt">
                                        	<?php echo $excSource; ?>
                                        </div>
                                        <div class="blog-meta clearfix">
                                        	<?php			
											 	$archiveDateLink = get_month_link( get_the_time('Y'), get_the_time('n') );
											?>
												 
                                           	<div class="blog-meta-left">
                                                <!--<a href="<?php echo $archiveDateLink; ?>" title="<?php the_date(); ?>" rel="bookmark">-->
                                                    <time class="entry-date" datetime="2014-01-01T11:11:11+00:00">
                                                        <?php echo get_the_date(); ?>
                                                    </time>
                                                <!--</a>-->
                                                
                                                
                                                <?php											 
                                                    _e( 'by ', 'jellypixel' );	
                                                    echo the_author_posts_link();
                                                ?>
                                            </div>
                                            <div class="blog-meta-right">
                                           		<?php
													edit_post_link( __( 'Edit', 'jellypixel' ), '<span class="edit-link">', '</span>' );
												?>
                                            </div>
                                        </div>
                                    </div>
                            	</article>
							<?php
						}
					}					
					else 
                    {
                        // No posts found
                        get_template_part( 'format','empty' );
                    }
                ?>
                
                <?php
                    get_template_part( 'section', 'paging' );
                ?>
            </div>        
            <div class="sidebar-right col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <aside role="complementary">
                	<div class="sidebar-inner">
	                    <?php get_sidebar( 'blog' ); ?>
                    </div>
                </aside>
            </div>        
        </div>
        
        */
        ?>
	</div>

<?php
	get_footer();
?>