<?php
	get_header();
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
        <div class="post-content">
            <?php
                // The Content					
                
                if ( have_posts() ) 
                {					
                    while ( have_posts() ) 
                    {
                        the_post();
                        the_content();
                    }
                } 
                
                // Reset
                wp_reset_query();			
            ?>                
        </div>
    </article>
    
<?php	
	get_footer();
?>