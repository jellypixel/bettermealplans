<?php
	/*
		Template Name: Start
	*/
	
	get_header();	
?>

	<div class="beginpage container">
    	<?php 
			$customTitle = "Welcome to Real Meal Plans!";
			include(locate_template('section-title.php'));
		?>    
        <article id="post-<?php the_ID(); ?>" <?php post_class('welcomepage'); ?>>     
            <div class="post-content">
                <?php
                    // The Content					
					
                    if ( have_posts() ) 
                    {					
                        while ( have_posts() ) 
                        {
                            the_post();
                            the_content();
                        }
                    } 
                    
                    // Reset
                    wp_reset_query();			
                ?>                
            </div>
        </article>        
	</div>

<?php
	get_footer();
?>