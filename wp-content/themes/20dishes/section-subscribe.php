<?php
	global $theme_options;
	
	$subscribe_title = get_opt( $theme_options['subscribe_title'], "It's Free" );		
	$subscribe_subtitle = get_opt( $theme_options['subscribe_subtitle'], 'Sign up for our prep ahead miracle masterclass.' );		
?>

<div class="subscribe-wrapper clearfix">
	<div class="subscribe-left col-lg-6 col-md-6 col-sm-12 col-xs-12">
    	<img src="<?php bloginfo('template_url'); ?>/img/webinar.png" alt="Subscribe" style="margin-top: -70px;"/>
    </div>
    <div class="subscribe-right col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center;">
        <div class="subscribe-subtitle">
            <?php echo $subscribe_subtitle; ?>
        </div>
        <div class="subscribe-title">
            <?php echo $subscribe_title; ?>
        </div>      
        <a href="https://bettermealplans.com/webinar/">
        <div class="section1-tagline" style="margin-left: auto; margin-right: auto; margin-bottom: 30px; margin-top: 20px;">
	        JOIN NOW!
	    </div>
	    </a>
    </div>
</div>