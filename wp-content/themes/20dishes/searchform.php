  
    <form role="search" id="searchform" method="get" action="<?php echo home_url( '/' ); ?>">
        <!--<label class="screen-reader-text" for="s"><?php _e( 'Search ', 'jellypixel' ); ?></label>		-->
        <input type="text" id="s" name="s" placeholder="<?php _e( 'Search Here', 'jellypixel' ); ?>" value="<?php the_search_query(); ?>" required>		
        <input type="submit" id="searchsubmit" class="tdicon-play-circle" value="<?php _e( 'Search', 'jellypixel' ); ?>">
    </form>