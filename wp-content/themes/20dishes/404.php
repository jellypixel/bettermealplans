<?php
	/* 
		404
	*/
?>

<?php
	get_header();
	
	$theme_options = get_option( 'theme_options' );
	$subscribe_title = get_opt( $theme_options['subscribe_title'], 'Get free email updates' );		
	$subscribe_subtitle = get_opt( $theme_options['subscribe_subtitle'], 'Subscribe today and get a Free copy of our report "Batch Cooking 101"' );
?>
	<div class="beginpage container">
    	<div class="subscribe-wrapper clearfix">
        	<div class="subscribe-title">
            	<?php echo $subscribe_title; ?>
            </div>
            <div class="subscribe-subtitle">
            	<?php echo $subscribe_subtitle; ?>
            </div>
            <form accept-charset="UTF-8" action="https://ov208.infusionsoft.com/app/form/redirectProcess/c983eeafc25890c5c95477aa2d65c5fc?referrer=http%3A%2F%2Fblog.20dishes.com%2F" target="_top" ?referrer="http%3A%2F%2Fblog.20dishes.com%2F&quot;" class="infusion-form" method="POST" name="Better Meal Plans Blog Optin" onsubmit="var form = document.forms[0];
                var resolution = document.createElement('input');
                resolution.setAttribute('id', 'screenResolution');
                resolution.setAttribute('type', 'hidden');
                resolution.setAttribute('name', 'screenResolution');
                var resolutionString = screen.width + 'x' + screen.height;
                resolution.setAttribute('value', resolutionString);
                form.appendChild(resolution);
                var pluginString = '';
                if (window.ActiveXObject) {
                    var activeXNames = {'AcroPDF.PDF':'Adobe Reader',
                        'ShockwaveFlash.ShockwaveFlash':'Flash',
                        'QuickTime.QuickTime':'Quick Time',
                        'SWCtl':'Shockwave',
                        'WMPLayer.OCX':'Windows Media Player',
                        'AgControl.AgControl':'Silverlight'};
                    var plugin = null;
                    for (var activeKey in activeXNames) {
                        try {
                            plugin = null;
                            plugin = new ActiveXObject(activeKey);
                        } catch (e) {
                            // do nothing, the plugin is not installed
                        }
                        pluginString += activeXNames[activeKey] + ',';
                    }
                    var realPlayerNames = ['rmockx.RealPlayer G2 Control',
                        'rmocx.RealPlayer G2 Control.1',
                        'RealPlayer.RealPlayer(tm) ActiveX Control (32-bit)',
                        'RealVideo.RealVideo(tm) ActiveX Control (32-bit)',
                        'RealPlayer'];
                    for (var index = 0; index < realPlayerNames.length; index++) {
                        try {
                            plugin = new ActiveXObject(realPlayerNames[index]);
                        } catch (e) {
                            continue;
                        }
                        if (plugin) {
                            break;
                        }
                    }
                    if (plugin) {
                        pluginString += 'RealPlayer,';
                    }
                } else {
                    for (var i = 0; i < navigator.plugins.length; i++) {
                        pluginString += navigator.plugins[i].name + ',';
                    }
                }
                pluginString = pluginString.substring(0, pluginString.lastIndexOf(','));
                var plugins = document.createElement('input');
                plugins.setAttribute('id', 'pluginList');
                plugins.setAttribute('type', 'hidden');
                plugins.setAttribute('name', 'pluginList');
                plugins.setAttribute('value', pluginString);
                form.appendChild(plugins);
                var java = navigator.javaEnabled();
                var javaEnabled = document.createElement('input');
                javaEnabled.setAttribute('id', 'javaEnabled');
                javaEnabled.setAttribute('type', 'hidden');
                javaEnabled.setAttribute('name', 'javaEnabled');
                javaEnabled.setAttribute('value', java);
                form.appendChild(javaEnabled);"
            >
                <input name="inf_form_xid" type="hidden" value="c983eeafc25890c5c95477aa2d65c5fc"><input name="inf_form_name" type="hidden" value="Better Meal Plans Blog Optin"><input name="infusionsoft_version" type="hidden" value="1.47.0.44">
                <div class="custom-31 beta-base beta-font-a" id="mainContent" style="height:100%">
                    <label for="inf_field_Email"></label>
                    <input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" type="text" placeholder="Email Address">
                    <div class="infusion-submit" style="text-align:center;">
                        <button type="submit" value="Subscribe">Subscribe</button>
                    </div>
                    <input type="text" name="inf_6tcVpS3suzrbysX3" class="inf_226723a11f13340302a9513a83ef939d">                
                </div>            
            </form>
    	</div> 
    
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 page-404">
            <h1 class="sans">
                <?php _e( '404 - Page not found', 'jellypixel' ); ?>
            </h1>
            
            <hr>
            
            <p class="notfound">
                <?php _e( 'You can go back to', 'jellypixel' ); ?>
                <a href="<?php bloginfo('url'); ?>">Home</a>
                <?php _e( 'or try to search.', 'jellypixel' ); ?>
            </p>
            
            <?php get_search_form(); ?>
        </div>
    </div>

<?php
	get_footer();
?>