<?php
	if ( is_singular( 'post' ) )
	{
		get_template_part( 'single', 'post' );	
	}
	else if ( is_singular( 'blog' ) )
	{
		get_template_part( 'single', 'blog' );	
	}
?>    