<?php
	/* Template Name: My Menu
	 *
	 * The template for my menu
	 *
	 * @package WordPress
	 * @subpackage 20Dishes
	 * @since Antiquity
	 */   
	 
	get_header();
		
	// Prepare data for User
	global $recipeIngredients; $current_user = wp_get_current_user();
	
	//Get serving size. Default to 4 if not exists.
	if(get_user_meta($current_user->ID, "servings", true) === "") 
		add_user_meta( $current_user->ID, "servings", 4, true ); 
	$userServingSize = get_user_meta($current_user->ID, "servings", true); 
	
	//Get font print size. Default to 10 if not exists.
	if(get_user_meta($current_user->ID, "fontprintsize", true) === "") 
		add_user_meta( $current_user->ID, "fontprintsize", 12, true ); 
	$fontPrintSize = get_user_meta($current_user->ID, "fontprintsize", true); 	
	
	//Get diet. Default to all if not exists.
	$allDiets = $wpdb->get_results('SELECT * FROM jp_diet', ARRAY_A);
	if(get_user_meta($current_user->ID, "diets", true) === "") 
		add_user_meta( $current_user->ID, "diets", array_column($allDiets, 'id'), true ); 
	$userDiet = get_user_meta($current_user->ID, "diets", false); 
	$userDiet = $userDiet[0];
	
	//Get ingredients exclude categories
	$excludeList = $wpdb->get_results('SELECT * FROM jp_excludelist', ARRAY_A);
	$userExcludeList = get_user_meta($current_user->ID, "excludeList", false); 	
	$userExcludeList = $userExcludeList[0];
	
	//Get and update shared recipe
	$userRegistrationTime = date( "Y-m-d h:i:s", strtotime( get_userdata(get_current_user_id())->user_registered . ' - 2 week'));
	$existingMenu = $wpdb->get_results( "SELECT GROUP_CONCAT(DISTINCT(menu_id)) as 'existing' FROM jp_userschedule WHERE user_id = " . $current_user->ID . " AND tanggal >= (NOW() - INTERVAL 30 DAY)");
	
	if(empty($existingMenu[0]->existing))
		$leftOverMenu = $wpdb->get_results( "INSERT INTO jp_userschedule(user_id, recipe_id, menu_id, tanggal, orders, notes, updates) 
											 SELECT ".$current_user->ID.", b.recipe_id, a.id, DATE_ADD(a.schedule,INTERVAL b.day-1 DAY), b.orders, b.notes, a.updates
											 FROM   jp_menu a, jp_menu_recipe b
											 WHERE  a.shared = 'Y' AND a.id = b.menu_id AND 
													a.schedule >= (NOW() - INTERVAL 30 DAY) AND
													a.schedule <= (NOW() + INTERVAL 3 DAY)
											");		
	else
		$leftOverMenu = $wpdb->get_results( "INSERT INTO jp_userschedule(user_id, recipe_id, menu_id, tanggal, orders, notes, updates) 
											 SELECT ".$current_user->ID.", b.recipe_id, a.id, DATE_ADD(a.schedule,INTERVAL b.day-1 DAY), b.orders, b.notes, a.updates
											 FROM   jp_menu a, jp_menu_recipe b
											 WHERE  a.shared = 'Y' AND a.id = b.menu_id AND 
													a.id NOT IN (".$existingMenu[0]->existing.") AND
													a.schedule >= (NOW() - INTERVAL 30 DAY) AND
													a.schedule <= (NOW() + INTERVAL 3 DAY)
											");
											
	//AND a.diet_id IN (".join(",", $userDiet).") 		
	//AND a.diet_id IN (".join(",", $userDiet).") 								
?>
	<!-- CSS -->
	<link href='<?php bloginfo('template_url')?>/sys/css/jquery-ui.min.css' media='screen' rel='stylesheet' type='text/css' /> 
    <link href='<?php bloginfo('template_url')?>/sys/css/style.css' media='screen' rel='stylesheet' type='text/css' />
	<link href='<?php bloginfo('template_url')?>/sys/css/responsive.css' media='screen' rel='stylesheet' type='text/css' />
    <link href="<?php bloginfo('template_url')?>/sys/css/print.css" rel="stylesheet" type="text/css" media="print" />

	<!-- Changeable Styling -->
	<style>
		@media print {
			.addrecipe-topmeta, .notes-content, .ingredient_list_ingredient, .recipe-content, .textareaadditem, .addrecipe-topmeta, .addrecipe-rest, #shoppinglist,
			#shoppinglist span,
			#shoppinglist b,
			#shoppinglist input,
			#shoppinglist label,
			#shoppinglist textarea,
			#global-announcement-stepbystep p,
			#step_list th, #step_list td
			{
				font-size: <?php echo $fontPrintSize; ?>px;
				line-height: <?php echo $fontPrintSize + 2; ?>px;
			}	
			
			.print-bold, h4.tabtitle-print
			{
				font-size: <?php echo $fontPrintSize + 2; ?>px;
				line-height: <?php echo $fontPrintSize + 4; ?>px;
			}
			
			.addrecipe-title
			{
				font-size: <?php echo $fontPrintSize + 6; ?>px;
				line-height: <?php echo $fontPrintSize + 8; ?>px;
			}	

			.ingredientTitle span
			{
				font-size: <?php echo $fontPrintSize + 8; ?>px !important;
				line-height: <?php echo ($fontPrintSize + 8) * 2; ?>px !important;
			}					
		}
	</style>	
	
    <!-- JS -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/sys/js/jquery-ui.datepicker.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/sys/js/jquery.numberpicker.js"></script>
    <script type="text/javascript">
        var template_url = '<?php echo bloginfo('template_url'); ?>';
		var userid ='<?php echo $current_user->ID; ?>';
				
		jQuery(document).ready(function(){    
			//Number Picker
				var valNum = parseInt(jQuery("#options #number-picker").html());
				jQuery("#options #number-picker").dpNumberPicker({
					min: 1, // Minimum value.
					max: 25, // Maximum value.
					value: valNum, // Initial value
					step: 1, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//Number Font Picker
				var valNum = parseInt(jQuery("#options #fontprint-picker").html());
				jQuery("#options #fontprint-picker").dpNumberPicker({
					min: 10, // Minimum value.
					max: 20, // Maximum value.
					value: valNum, // Initial value
					step: 1, // Incremental/decremental step on up/down change.
					format: false,
					editable: true,
					addText: "+",
					subText: "-",
					formatter: function(val){return val;},
					beforeIncrease: function(){},
					afterIncrease: function(){},
					beforeDecrease: function(){},
					afterDecrease: function(){},
					beforeChange: function(){},
					afterChange: function(){},
					onMin: function(){},
					onMax: function(){}
				});
				
			//iCheck
				jQuery('#options input').iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal',
				});
		});
    </script>    
<?php	

	/*I CHECKED THIS PORTION */
	if ( is_user_logged_in() )
	{	
		include 'sys/loadweek.php';
		include 'sys/loadrecipe.php';
		include 'sys/addrecipe.php';
	
		?>
        	<div id="mymenu-stage-wrapper" class="container mymenu-saveloop">
            	<div id="sys-tabpanel" role="tabpanel">
				
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
						<div id="sys-title"></div>
                        <li role="presentation" class="active first"><div class="step-title">Step 1</div><a id ="planTab" href="#plan" aria-controls="plan" role="tab" data-toggle="tab">MEAL PLAN</a></li>     
						<li role="presentation"><div class="step-title">Step 2</div><a id ="shoppinglistTab" href="#shoppinglist" aria-controls="shoppinglist" role="tab" data-toggle="tab">SHOPPING LIST</a></li>						
                        <li role="presentation" class="last"><div class="step-title">Step 3</div><a id ="stepTab" href="#stepbystep" aria-controls="stepbystep" role="tab" data-toggle="tab">PREP GUIDE</a></li>
                        <li role="presentation" class="last" id="massDelete"><a><div class="icon glyphicon glyphicon-eye-close"></div></a></li>						
						<li role="presentation" class="last" id="printButton"><a id ="recipesdetailTab" href="#recipesdetail" aria-controls="recipesdetail" role="tab" data-toggle="tab"><div class="icon tdicon-print"></div></a></li>						
						<li role="presentation" class="last" id="optionButton"><a id ="optionsTab" href="#options" aria-controls="settings" role="tab" data-toggle="tab"><div class="icon tdicon-gear-full"></div></a></li>   								
                        <div id="extratab-wrapper">  
                            <li id="readme" class="extratab" title="Learn how to use">READ ME</li>
							
							<?php
								$day = date('w');
								
								$prevWeekStart = date('M d', strtotime('-'.($day+7).' days'));
								$prevWeekEnd = date('M d', strtotime('+'.(6-$day-7).' days'));
								
								$thisWeekStart = date('M d', strtotime('-'.$day.' days'));
								$thisWeekEnd = date('M d', strtotime('+'.(6-$day).' days'));
								
								$nextWeekStart = date('M d', strtotime('-'.($day-7).' days'));
								$nextWeekEnd = date('M d', strtotime('+'.(6-$day+7).' days'));												
							?>							
							
							<div id="extratab-wrapper-icons">
								<li id="selectWeek" class="extratab" title="Select Week">
									<div id="selectedWeek"><?php echo $thisWeekStart .' - '. $thisWeekEnd . ' '; ?><br>This Week</div>
									<div class="icon tdicon-calendar"></div>
									<div class="submenu-wrapper">
										<ul class="submenu">
											<li id="prevweek"><?php echo $prevWeekStart .' - '. $prevWeekEnd . ' '; ?><br>Previous Week</li>
											<li id="thisweek"><?php echo $thisWeekStart .' - '. $thisWeekEnd . ' '; ?><br>This Week</li>
											<li id="nextweek"><?php echo $nextWeekStart .' - '. $nextWeekEnd . ' '; ?><br>Next Week</li>
										</ul>
									</div>
								</li>						
							</div>
                            <div class="clear"></div>
                        </div>
                    </ul>
                    
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="plan">   
							<img src="https://bettermealplans.com/wp-content/themes/20dishes/img/logo.png" class="img-print" alt="logo"></img>
							<h1 class="tabtitle-print">Meal Plan</h1>
							<div id="planpanel">
								<?php
									$alltheweek = loadWeek('');
									if ( !empty( $alltheweek ) ) {
										echo $alltheweek;	
									}
								?>
                                <script type="text/javascript">
									jQuery(document).ready(function(){    
										// on Refresh
										checkNextWeek(); 
										
										jQuery("#currentweek").val('thisweek');
									});
								</script>
							</div>					
                        	<div class="savewrapper clearfix">
                                <div id="saveweek" class="savebutton ready">								
                                    SAVE THIS PLAN
                                </div>
                            </div>
                        </div>
                        
                        <div role="tabpanel" class="tab-pane tabInternal" id="stepbystep">
							<img src="https://bettermealplans.com/wp-content/themes/20dishes/img/logo.png" class="img-print" alt="logo"></img>
							<div class="insidePane">
								<h1 class="tabtitle-print">Simple Food Prep Guide</h1>
								<div id="global-announcement-stepbystep">
									<?php
										// Lookiehere
										$args = array (
											'p'                      => '1591',
										);
										
										// The Query
										$query = new WP_Query( $args );
										
										// The Loop
										if ( $query->have_posts() ) {
											while ( $query->have_posts() ) {
												$query->the_post();
												the_content();
												// do something
											}
										} else {
											// no posts found
										}
										
										// Restore original Post Data
										wp_reset_postdata();
									?>
								</div>
								<div id="step_list">

								</div>
							</div>
                        </div>
                        
                        <div role="tabpanel" class="tab-pane" id="shoppinglist">
							<img src="https://bettermealplans.com/wp-content/themes/20dishes/img/logo.png" class="img-print" alt="logo"></img>	
							<div class="insidePane">				
								<h1 class="tabtitle-print">Shopping List</h1>
								<div id="ingredient_list">

								</div>						  
							</div>
							<div class="savewrapper clearfix">
								<div id="saveshopping" class="savebutton ready">								
									Save
								</div>				
						
								<div id="reloadshopping" class="reloadbutton ready">								
									Refresh
								</div>        
							</div>   							
						</div>	
                        
                        <div role="tabpanel" class="tab-pane" id="recipesdetail">
							<img src="https://bettermealplans.com/wp-content/themes/20dishes/img/logo.png" class="img-print" alt="logo"></img>
							<div class="insidePane">
							
								<input class="recipe-printcheck" id="recipePrintPlan" type="checkbox" name="recipePrintPlan" checked="checked" />
								<label for="recipePrintPlan" class="checkPrint-label2">Plan</label>
								<input class="recipe-printcheck" id="recipePrintShopping" type="checkbox" name="recipePrintShopping" checked="checked" />
								<label for="recipePrintShopping" class="checkPrint-label2">Shopping</label>

								<input class="recipe-printcheck" id="recipePrintStep" type="checkbox" name="recipePrintStep" checked="checked" />
								<label for="recipePrintStep" class="checkPrint-label2">Simple Food Prep Guide</label>								
								<input class="recipe-printcheck" id="recipePrintRecipe" type="checkbox" name="recipePrintRecipe" checked="checked" />
								<label for="recipePrintRecipe" class="checkPrint-label2">Recipes</label>
							
								<h1 class="tabtitle-print">Recipes</h1>
								
								<div id="recipesdetail-stage">
									<input class="recipe-check" id="recipeSelectAll" type="checkbox" name="recipeSelectAll" />
									<label for="recipeSelectAll" class="checkPrint-label">Select All</label>
									<div class="accordion" id="recipesdetail-accordion">
										
									</div>
								</div>
								
								<div id="recipesdetail-print">
								</div>
							</div>
                        	<div class="savewrapper clearfix">
                                <div id="print" class="savebutton ready">								
                                    PRINT
                                </div>
                            </div>							
                        </div>
                        
                        <div role="tabpanel" class="tab-pane" id="options">
							<div class="insidePane">
								<div class='error-warning'></div>
								<h3>Default Servings</h3>
								<div id="number-picker">
									<?php 
										echo $userServingSize; 
									?>
								</div>
								<h3>Categories</h3>
								<form id="dietOptions">
									<?php 
										foreach($allDiets as $diet) {
											echo '<div style="float:left;"><input id="checkDiet-' . $diet['name'] . '" type="checkbox" name="dietCheck" '. (in_array($diet["id"], $userDiet)? "checked":"") .' value="'.$diet["id"].'" /><label for="checkDiet-' . $diet['name'] . '">' . $diet['name'] . '</label></div>';
										}
									?>			
								</form>
								<h3>Excluded Categories</h3>
								<form id="excludedCategoriesOptions">
									<?php 
										foreach($excludeList as $exclude) {
											
											$checked = false;
											foreach($userExcludeList as $userExclude){
												
												if($exclude["cat_id"] == $userExclude)		
													$checked = true;																							
											}
											
											if($checked)																							
												echo '<div style="float:left;"><input id="checkExclude-' . $exclude['name'] . '" type="checkbox" name="excludeCheck" checked value="'.$exclude["cat_id"].'" /><label for="checkExclude-' . $exclude['name'] . '">' . $exclude['name'] . '</label></div>';	
											else
												echo '<div style="float:left;"><input id="checkExclude-' . $exclude['name'] . '" type="checkbox" name="excludeCheck" value="'.$exclude["cat_id"].'" /><label for="checkExclude-' . $exclude['name'] . '">' . $exclude['name'] . '</label></div>';		
										}
									?>			
								</form>					
								
								<h3>Excluded Ingredients</h3>
								This is a keyword search. That means if you exclude "beef" you will also exclude "beef bacon" and any kind of keyword with "beef" in it. However since this is a keyword search it does not understand context i.e excluding pork will not exclude bacon automatically. Use the excluded categories above for that.
								<br><br>
								Please note that by excluding ingredients you may not receive the complete provided meal plan each week. This is due to the fact that our meal plans do not take into account individual restrictions and recipes may contain some of these items. These recipes will no longer show on your weekly prepared meal plan.
								<br><br>
								<div class="ui-widget">
								  <input id="excludedIngredientAdd" placeholder="Search Ingredient">
								</div>
								 
								<div class="ui-widget">
									<div id="excludedIngredientList">
									<?php
									
										$exingredients = get_user_meta($current_user->ID, "exingredients", false); 
										$exingredients = $exingredients[0];
																			
										foreach($exingredients as $exingredient) {
											echo "<div class='excludedIngredient'>" . $exingredient . "</div>";
										}									
									?>
									</div>
								</div>						
								
								<?php
									$thisUser = new WP_User($current_user->ID);
									
									$levels = array_values(WLMAPI::GetUserLevels($current_user->ID));		
									$registime = strtotime($thisUser->user_registered);									
									$currenttime = time();
									$billedtime = "";
									$difference = "";
									$diff = calculateMonths($registime, $currenttime);
									$package = "";
									
									if(in_array('Monthly', $levels)) {
										$package = "Monthly";
										$difference = 1 - ($diff % 1) + $diff;
										
										if($difference == 0)
											$difference = 1;
									}
									if(in_array('Quarterly', $levels)){
										$difference = 3 - ($diff % 3) + $diff;	
										$package = "Quarterly";
									}

									if(in_array('6 Month', $levels)){
										$difference = 6 - ($diff % 6) + $diff;
										$package = "6 Month";
									}
									
									if(in_array('Yearly', $levels)){
										$difference = 12 - ($diff % 12) + $diff;
										$package = "Yearly";
									}
									
									$currenttime = strtotime("+".$diff." month", $registime);  	
									$billedtime = date("m-d-Y", strtotime("+".$difference." month", $registime));				
									
									if($difference === ""){
										$billedtime = "Unknown Membership Level";
									}									
									else if(count($levels) > 1 ) {
										$billedtime = "Multiple Membership Level";
									}
									else if(in_array('Expired', $levels)){
										$billedtime = 'Expired';	
									}
									else {
										echo "<br><br><br><div style='font-size:13px;'>You became a member on ".date("m-d-Y", $registime).". You selected our '". $package ."' membership and will be rebilled on ";
										echo $billedtime . "</div>";
									}
									
								?>
								<h3>Printable Font Size</h3>
								<div id="fontprint-picker">
									<?php 
										echo $fontPrintSize; 
									?>
								</div>										
							</div>					
                        	<div class="savewrapper clearfix">
                            	<div id="saveoptions" class="savebutton ready" title="Save Options">Save</div>
                            </div>							
                        </div>
                    </div>                
                </div>   
            </div>
            
            <div class="overlay">
                <div id="addcard-lightbox" class="lightbox">
                	<div id="addcard-tabs" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#add-recipe" aria-controls="add-recipe" role="tab" data-toggle="tab">Add Recipe</a></li>
                            <li role="presentation"><a href="#favorite-recipes" aria-controls="favorite-recipes" role="tab" data-toggle="tab">Favorite Recipes</a></li>
                            <li role="presentation"><a href="#add-note" aria-controls="add-note" role="tab" data-toggle="tab">Add Note</a></li>                            
                            <li class="lightbox-close" title="Cancel"><div class="icon tdicon-thin-close"></div></li>  
                        </ul>
                        
                        <div class="tab-content">
                        	<div role="tabpanel" class="tab-pane active" id="add-recipe">
                            	<div id="searchrecipe-form">
                                	<form action="" id="searchrecipe-formitself" name="searchrecipe-formitself">
                                    	<select id="searchrecipe-cat" name="searchrecipe-cat">
                                        	<option value="-1">All</option>
                                        	<option value="278">Side Dishes</option>
                                            <option value="336">Slow Cooker</option>
                                            <option value="591">Mexican</option>
											<option value="593">Asian Fusion</option>
											<option value="602">Indian</option>
											<option value="603">Italian</option>
											<option value="594">Mediterranean</option>
											<option value="592">American</option>
											<option value="615">Sugar Free</option>
                                        </select>
                                        <input type="text" id="searchrecipe-input" name="searchrecipe-input" maxlength="255" />
                                        <input type="submit" id="searchrecipe-submit" value="Search Recipe">
                                        <div class="clear"></div>
                                    </form>
                                </div>
                                <div id="searchrecipe-message"></div>
                                <div id="searchrecipe-stage">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="favorite-recipes">
                            	<div id="favoriterecipes-message"></div>                                
                                <div id="favoriterecipes-stage">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="add-note">                        
                            	<textarea id="addnotes-textarea" rows="8" cols="50" maxlength="500"></textarea>
                                <div id="addnotes-button" class="savebutton">Add Note</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="addmenu-lightbox" class="lightbox">
                	<div id="addcard-tabs" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#add-menu" aria-controls="add-menu" role="tab" data-toggle="tab">Add Menu</a></li>
                            <li class="lightbox-close" title="Cancel"></li>  
                        </ul>
                        
                        <div class="tab-content">
                        	<div role="tabpanel" class="tab-pane active" id="add-menu">
                                <div id="addmenu-form">
                                    <form action="" id="addmenu-formitself" name="addmenu-formitself">
                                        <input type="text" id="addmenu-input" name="addmenu-input" maxlength="255" />
                                        <input type="submit" id="addmenu-submit" value="Search Menu">
                                        <div class="clear"></div>
                                    </form>
                                </div>
                                <div id="addmenu-stage">
                                </div>
                            </div>
                        </div>
                    </div>       	
                </div>
                
                <div id="yesnoModal" class="modalbox">
                	<div class="modal-scrutiny">                    
	                	Are you sure want to hide this recipe?
                    </div>
                    <div class="modal-action clearfix">
                        <div class="yesbtn modal-default modalbtn">Yes</div>
                        <div class="nobtn modalbtn">No</div>
                    </div>
                </div>
                
                <div id="static-modal" class="modalbox">
                	<div class="modal-scrutiny">                    
	                	
                    </div>
                    <div class="modal-action clearfix">
                        <div class="nobtn modal-default modalbtn">OK</div>
                    </div>
                </div>
            </div>
            
            <div class="overlay-2">
            	<div id="addrecipe-lightbox" class="lightbox-2">
                	<div class="lightbox-2-header">
                    	<div class="lightbox-2-title">                        
                        </div>
                    	<div class="inner-lightbox-close"></div>    
                        <div class="clear"></div>                    
                    </div>
                    <div id="addrecipe-stage" class="lightbox-2-content">                    	
                    </div>
                    <div class="lightbox-2-input">
                    	<div id="reallyaddrecipe" class="savebutton">Add to plan</div>
                    </div>
                </div>            	
            </div>
        <?php	
	}
	else 
	{
		?>
        	<div class="container clear">
            	<div class="thick-col">
                	<p>
                    	You need to be registered and <a href="#" id="kitch_log">logged in</a> to use myKitchen.
                	</p>
				</div>
            </div>
			
        <?php	
	}
?>

<?php
	get_footer();
?>

<script src="<?php bloginfo('template_url'); ?>/sys/js/jquery-ui.touchpunch.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/sys/js/default.js" type="text/javascript"></script>

<script type="text/javascript">
	checkRecipe();
</script>