<?php 		
	$pinterestThumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); 
?>

<div class="post-share-wrapper">
    <div class="post-share-inner-wrapper">    	
        <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>" target="_blank" class="share-social facebook"><div class="tdicon-facebook-circle" title="Facebook"></div></a><a href="http://twitter.com/home?status=<?php the_title(); ?> <?php the_permalink(); ?>" target="_blank" class="share-social twitter"><div class="tdicon-twitterbird-circle" title="Twitter"></div></a><a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink()); ?>&amp;description=<?php echo urlencode($post->post_title); ?>&amp;media=<?php echo urlencode($pinterestThumb[0]); ?>" target="_blank" class="share-social pinterest" title="Pinterest"><div class="tdicon-pinterest-circle"></div></a>
    </div>    
    <div class="clear"></div>
</div>
