<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Recipes
 * @since Recipes 1.0
*/ 

header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if (is_user_logged_in() && is_front_page() && !current_user_can('administrator') ) {
	wp_redirect ( home_url("/my-menu-public") );
	exit;
}

/*
if (!is_user_logged_in() && is_singular('post') && !has_category('blog') ) {
	wp_redirect ( home_url() );
	exit;
}
*/

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	
	<?php
		$theme_options = get_option( 'theme_options' );
		$favicon = get_opt( $theme_options['branding_favicon'], '' );
		$logo = get_opt( $theme_options['branding_logo'], '' );	
		
		if ( !empty( $favicon ) && !empty( $favicon['url'] ) ) 
		{
			echo '<link rel="shortcut icon" href="' . preg_replace("/^http:/i", "https:", $favicon['url']) . '">';
		}
	?>
    
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6029024574441&cd[value]=0.00&cd[currency]=USD&noscript=1" /></noscript>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
    
	<?php 
		wp_head(); 
	?>  
      
    <script type="text/javascript">
		var $j = jQuery.noConflict();
		var freeUser = false;
		
		$j(window).load(function() { // makes sure the whole site is loaded
			$j('#status').fadeOut(); // will first fade out the loading animation
			$j('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
			$j('body').delay(350).css({'overflow':'visible'});
		})
	</script>	
		
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" />	
	
</head>
<body <?php body_class();?>>
	<?php
        if ( is_page_template('template-mymenu.php') ) 
		{
            ?>
                <!-- Preloader -->
                <div id="preloader">
                    <div id="status">Please wait while we create the perfect meal plan for you..</div>
                </div>
            <?php		
        }
		
		$bgClass = "page-bg";
		$logoClass = "page-logo";
		
		if( is_front_page() ) 
		{
			$bgClass = "home-bg";
			$logoClass = "home-logo";
		}			
		
		if ( $bgClass == 'home-bg' || is_page_template( 'template-subscribe.php' ) || is_page_template( 'template-webinar.php' ) || is_page_template( 'template-webinarsales.php' ) || is_page_template( 'template-webinarfinished.php' ) ) 
		{
			// Do Nothing - We do it via shortcode
		}
		else 
		{
		    /*
			?>
				<div class="<?php echo $bgClass; ?>"></div>
			<?php
			*/
		}
	?>    
    
    <!-- Menu -->
	<nav class="navbar yamm navbar-default" role="navigation">
		<div class="container">
			<div class="navbar-header">           	
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".primarymenu">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>                
			</div>          
			
			<div class="collapse navbar-collapse primarymenu">
				<div class="menu-logo">
					<div class="menu-logo-inner">
						<a href="<?php bloginfo( 'url' ); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo.png" class="img-responsive" alt="logo" />
                            <div class="menu-logo-subtitle">
                                Formerly 20 Dishes
                            </div>
                        </a> 
					</div>
				</div>  	
			
				<?php 					
					if ( is_user_logged_in() )
					{						
						if( $free_user ) 
						{							
							/* Free Member navigation */
							wp_nav_menu( array(
								'menu' => 'freemember_menu',
								'depth' => 5,
								'theme_location' => 'freemembermenu-location',
								'container' => false,
								'menu_class' => ' nav navbar-nav',
								'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 
								'walker' => new wp_bootstrap_navwalker())
							);
							
						} else {
							
							/* Member navigation */
							wp_nav_menu( array(
								'menu' => 'member_menu',
								'depth' => 5,
								'theme_location' => 'membermenu-location',
								'container' => false,
								'menu_class' => ' nav navbar-nav',
								'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 
								'walker' => new wp_bootstrap_navwalker())
							);							
						}						
					}
					else
					{
						if ( is_singular( 'post' ) ) 
						{
							/* Single Post Only 
							?>
								<div class="singlepostonly nav navbar-nav">
									<a href="<?php echo get_page_link(23031); ?>" title="Add Recipe to Your Meal Plan">
										Add Recipe to Your Meal Plan
									</a>
								</div>
							<?php
							*/

							/* Single Post Only */
							?>
								<div class="nav navbar-nav desktop addrecipenotloggedin">
									<a href="<?php echo get_page_link(23031); ?>" title="Add Recipe to Your Meal Plan">
										<img src="http://bettermealplans.com/wp-content/themes/20dishes/img/webstrip.png"/>
									</a>
								</div>

								<div class="nav navbar-nav mobile addrecipenotloggedin">
									<a href="<?php echo get_page_link(23031); ?>" title="Add Recipe to Your Meal Plan">
										<img src="http://bettermealplans.com/wp-content/themes/20dishes/img/webstrip-mobile.png"/>
									</a>
								</div>
							<?php
							
						}
						else
						{						
							/* Main Navigation */
							wp_nav_menu( array(
								'menu' => 'top_menu',
								'depth' => 5,
								'theme_location' => 'topmenu-location',
								'container' => false,
								'menu_class' => ' nav navbar-nav',
								'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 
								'walker' => new wp_bootstrap_navwalker())
							);
						}
					}

					if( is_user_logged_in() )
					{
						$user_id = get_current_user_id();
						?>
							<div class="avatar-wrapper-outer">
								<ul class="nav navbar-nav avatar-wrapper logout">
								    <!--
									<li class="dropdown">
										<a href="#"><?php echo get_avatar( $user_id ); ?></a>                                            
										<ul class="dropdown-menu">											
											<li>
												<a href="<?php echo home_url();?>/my-account/" >View my profile</a>
											</li>
                                            
										</ul>
									</li>
									-->
                                    <li>
                                        <a href="<?php echo home_url();?>/member-profile/" class="try_now" >Your Account</a>
                                    </li> 
									<li>                                    
										<a href="<?php echo wp_logout_url(); ?>" class="log_out" >Log out</a>
									</li>
								</ul>
							</div>
						<?php	
					} 
					else 
					{						
						?>
							<div class="avatar-wrapper-outer">
								<ul class="nav navbar-nav avatar-wrapper login">
									<li>                                    
										<a href="<?php echo home_url();?>/sign-in" class="log_in" >Log in</a>
									</li>
                                    <?php
										if ( !( is_singular( 'post' ) ) ) 
										{
											?>
                                                <li>                                    
                                                    <a href="https://www.bettermealplans.com/online-meal-planner/" class="try_now" >Start Meal Planning!<span class="trysmall"></span></a>
                                                </li>
                                    		<?php
										}
									?>
								</ul>
							</div>
						<?php
					}
				?>            
			</div>
		</div>
	</nav> 
    
    <?php 
        /*
		if ( !is_page_template( 'template-subscribe.php' ) && !is_page_template( 'template-webinar.php' ) && !is_page_template( 'template-webinarsales.php' ) && !is_page_template( 'template-webinarfinished.php' ) )
		{
			?>
                <div class="logo <?php echo $logoClass; ?>">
                    <?php 
                        if ( !empty( $logo ) && !empty( $logo['url'] ) ) {				
                            $logo_url = $logo['url'];
                        }
                        else {
                            $logo_url = get_bloginfo('template_url') . "/img/logo.png";
                        }
                    ?>
                    <a href="<?php bloginfo('url'); ?>"><img src="<?php echo $logo_url; ?>" class="img-responsive" alt="logo" /></a> 
                </div>
    		<?php
		}
		*/
	?>

	<?php
		if( !is_user_logged_in() )
		{
			?>
				<script>
					$j(function() 
					{
						$j("#menu-item-177").click(function()
						{
							loadPopup();
							$j(".tutorial").hide();
						});
						
						$j("div.close").hover(
							function() {
								$j('span.ecs_tooltip').show();
							},
							function () {
								$j('span.ecs_tooltip').hide();
							}
						);
		
						$j("div.close").click(function() {
							disablePopup();  // function close pop up
						});
		
						$j(this).keyup(function(event) {
							if (event.which == 27) { // 27 is 'Ecs' in the keyboard
								disablePopup();  // function close pop up
							}  	
						});
		
						$j("div#backgroundPopup").click(function() {
							disablePopup();  // function close pop up
						});
		
						$j('a.livebox').click(function() 
						{
							alert('Hello World!');
							return false;
						});
		
		
						/************** start: functions. **************/
						function loading() {
							$j("div.loader").show();  
						}
						function closeloading() {
							$j("div.loader").fadeOut('normal');  
						}
						
						var popupStatus = 0; // set value
						
						function loadPopup() { 
							if(popupStatus == 0) { // if value is 0, show popup
								closeloading(); // fadeout loading
								$j("#toPopup").fadeIn(0500); // fadein popup div
								$j("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
								$j("#backgroundPopup").fadeIn(0001); 
								popupStatus = 1; // and set value to 1
							}	
						}
							
						function disablePopup() {
							if(popupStatus == 1) { // if value is 1, close popup
								$j("#toPopup").fadeOut("normal");  
								$j("#backgroundPopup").fadeOut("normal");  
								popupStatus = 0;  // and set value to 0
							}
						}
					});
				</script>
		
			<?php
        }
    ?>

	<script>
        $j(document).ready(function()
        {
            $j(".log_out").click(function()
            {
                data = { action:'log_out'};
                $j.post( '<?php echo home_url();?>/wp-admin/admin-ajax.php',
                        data,
                        function(data)
                        {
                            window.location.href='<?php echo home_url();?>';
                        });
            });
        });	
    </script>
    
    <script type='text/javascript'>//<![CDATA[ 
        $j('body').on('touchstart.menu-main-menu-container', '.sub-menu', function (e) { 
            e.stopPropagation(); 
        });
    </script>
    
    <script>
        $j(function() {
            $j("#menu-item-177").click(function()
            {
                loadPopup();
                $j(".tutorial").hide();
            });
            
            $j("div.close").hover(
                function() {
                    $j('span.ecs_tooltip').show();
                },
                function () {
                    $j('span.ecs_tooltip').hide();
                }
            );
        
            $j("div.close").click(function() 
            {
                disablePopup();  // function close pop up
            });
            
            $j(this).keyup(function(event) 
            {
                if (event.which == 27) { // 27 is 'Ecs' in the keyboard
                    disablePopup();  // function close pop up
                }  	
            });
            
            $j("div#backgroundPopup").click(function() 
            {
                disablePopup();  // function close pop up
            });
            
            $j('a.livebox').click(function() 
            {
                alert('Hello World!');
                return false;
            });			
        
            /************** start: functions. **************/
            function loading() 
            {
                $j("div.loader").show();  
            }
            function closeloading() 
            {
                $j("div.loader").fadeOut('normal');  
            }
            
            var popupStatus = 0; // set value
            
            function loadPopup() 
            { 
                if(popupStatus == 0) 
                { // if value is 0, show popup
                    closeloading(); // fadeout loading
                    $j("#toPopup").fadeIn(0500); // fadein popup div
                    $j("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
                    $j("#backgroundPopup").fadeIn(0001); 
                    popupStatus = 1; // and set value to 1
                }	
            }
                
            function disablePopup() 
            {
                if ( popupStatus == 1) 
                { // if value is 1, close popup
                    $j("#toPopup").fadeOut("normal");  
                    $j("#backgroundPopup").fadeOut("normal");  
                    popupStatus = 0;  // and set value to 0
                }
            }
        });
    </script>
    
    
    
