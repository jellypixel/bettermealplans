<?php
	function get_distance($store_location, $user_location, $units = 'miles') {
		
	    if ( $store_location['longitude'] == $user_location['longitude'] && $store_location['latitude'] == $user_location['latitude'])
	        return 0;
	
	    $theta = ($store_location['longitude'] - $user_location['longitude']);
	    $distance = sin(deg2rad($store_location['latitude'])) * sin(deg2rad($user_location['latitude'])) + cos(deg2rad($store_location['latitude'])) * cos(deg2rad($user_location['latitude'])) * cos(deg2rad($theta));
	    $distance = acos($distance);
	    $distance = rad2deg($distance);
	    $distance = $distance * 60 * 1.1515;
	
	    if ( 'kilometers' == $units ) {
	        $distance = $distance * 1.609344;
	    }
	
	    return round($distance);
	}
	
	// prevent direct access
		$isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
		if( !$isAjax ) {
			$user_error = 'Go away...';
			trigger_error( $user_error, E_USER_ERROR );
		}
		else {
			define('WP_USE_THEMES', false);
			require('../../../.wordpress/wp-load.php');
		}

	// Get the search value
		$who = trim($_GET['term']);	
		$searchby = trim($_GET['searchby']);
		
		if ( empty ( $who ) ) {
			exit;	
		}
		
		if ( empty( $searchby ) ) {
			$searchby = 'name';
		}
	
	// Prevent fishy thingie
		$jsonInvalid = array(array("id" => "#", "value" => $term, "label" => "Only letters and digits are permitted..."));
		$invalidResult = json_encode($jsonInvalid);
		
		$term = preg_replace('/\s+/', ' ', $term); // replace multiple spaces with one 
		
		if(preg_match("/[^\040\pL\pN_-]/u", $term)) { // allow space, any unicode letter and digit, underscore and dash
			print $invalidResult;
			exit;
		}
	
	// Prep the holy grail		
		$searchResult = array();
		$searchResultByRow = array();
		
	// Quereh
	
		if($searchby == 'name'){
			$wp_query = new WP_User_Query( array ( 'orderby' => 'display_name')  );
			$wp_query->query_fields = ' SQL_CALC_FOUND_ROWS DISTINCT wp_users.* ';
			$wp_query->query_from .= ' INNER JOIN wp_usermeta ON ( wp_users.ID = wp_usermeta.user_id )';
			$wp_query->query_where .= ' AND  wp_usermeta.user_id IN (SELECT DISTINCT a.user_id FROM wp_wlm_userlevels a, wp_affiliate_wp_affiliates b WHERE (a.level_id = 1443825749 OR a.level_id = 1448335909) AND a.user_id = b.user_id AND b.status = \'active\') AND wp_users.display_name LIKE \'%'.$who.'%\' '; 
			$wp_query->query();
			
			// Get the results
			$authors = $wp_query->get_results();
								
			// Check for results
			if (!empty( $authors ) ) 
			{
				foreach ( $authors as $author )
				{
					// get all the user's data
					$author_id = $author->ID;
					$author_info = get_userdata( $author_id );
	
					$searchResultByRow['id'] = $author_id;
					$searchResultByRow['value'] = $author_info->first_name . ' ' . $author_info->last_name;
					$searchResultByRow['label'] = $author_info->first_name . ' ' . $author_info->last_name;					
					$searchResultByRow['email'] = $author_info->user_email;
					$searchResultByRow['avatar'] = get_avatar( $author->ID );
					
					// address
					$membershipDetails = wlmapi_get_member($author->ID);
					$membershipAddress = $membershipDetails['member'][0]['UserInfo']['wpm_useraddress'];				
					
					if($membershipAddress['city'] . ', ' . $membershipAddress['state']  != "City, State")
						$searchResultByRow['loc'] = $membershipAddress['city'] . ', ' . $membershipAddress['state'];
					else
						$searchResultByRow['loc'] = '';				
							
					// link
					$args = array(
						'affiliate_id' => affwp_get_affiliate_id($author->ID),
					);
			
					$searchResultByRow['link'] = affwp_get_affiliate_referral_url( $args );				
									
					array_push( $searchResult, $searchResultByRow);				
				}			
			} 
			else 
			{
				exit;
			}	
					
		} else if ($searchby == 'zip') {
			
			$sql = $wpdb->prepare( "SELECT zip, lat, lng FROM zips WHERE zip=%s ",$who );
			$results = $wpdb->get_results( $sql ); 
			$results = $results[0];
			
			$wp_query = new WP_User_Query( array ( 'orderby' => 'display_name')  );
			$wp_query->query_fields = ' SQL_CALC_FOUND_ROWS DISTINCT wp_users.* ';
			$wp_query->query_from .= ' INNER JOIN wp_usermeta ON ( wp_users.ID = wp_usermeta.user_id )';
			$wp_query->query_where .= ' AND wp_usermeta.user_id IN (SELECT DISTINCT user_id FROM wp_wlm_userlevels WHERE level_id = 1443825749 OR level_id = 1448335909) '; 
			$wp_query->query();
			
			// Get the results
			$authors = $wp_query->get_results();			
			
			// Check for results
			if (!empty( $authors ) ) 
			{
				foreach ( $authors as $author )
				{
					$author_id = $author->ID;
					$membershipDetails = wlmapi_get_member($author->ID);
					$membershipZip = $membershipDetails['member'][0]['UserInfo']['wpm_useraddress']['zip'];
							
					$sql = $wpdb->prepare( "SELECT zip, lat, lng FROM zips WHERE zip=%s ", $membershipZip );
					$resultAdvisor = $wpdb->get_results( $sql ); 
					$resultAdvisor = $resultAdvisor[0];					
					
					// Calculate distance
					$distance = get_distance(
									array(
								        'zip_code' => $resultAdvisor->zip, 
								        'latitude' => $resultAdvisor->lat, 
								        'longitude' => $resultAdvisor->lng, 										
									), 
									array (
								        'latitude' => $results->lat, 
								        'longitude' => $results->lng,										
									)
									, 'miles');
									
					if(($distance < 30) && ($distance != 7280)) {
						
						$author_info = get_userdata( $author_id );
		
						$searchResultByRow['id'] = $author_id;
						$searchResultByRow['value'] = $author_info->first_name . ' ' . $author_info->last_name;
						$searchResultByRow['label'] = $author_info->first_name . ' ' . $author_info->last_name;					
						$searchResultByRow['email'] = $author_info->user_email;
						$searchResultByRow['avatar'] = get_avatar( $author->ID );
						
						// address
						$membershipAddress = $membershipDetails['member'][0]['UserInfo']['wpm_useraddress'];				
						
						if($membershipAddress['city'] . ', ' . $membershipAddress['state']  != "City, State")
							$searchResultByRow['loc'] = $membershipAddress['city'] . ', ' . $membershipAddress['state'];
						else
							$searchResultByRow['loc'] = '';				
								
						// link
						$args = array(
							'affiliate_id' => affwp_get_affiliate_id($author->ID),
						);
				
						$searchResultByRow['link'] = affwp_get_affiliate_referral_url( $args );				
										
						array_push( $searchResult, $searchResultByRow);								
					}		
				}			
			} 
			else 
			{
				exit;
			}				
		}
		
		$jsonSearchResult = json_encode($searchResult);
		print $jsonSearchResult;
	
?>