<?php
	/*
		Template Name: Webinar Finished
	*/
	
	get_header();	
?>

	<div class="beginpage subscribepage">
        <div class="container">
        	<div class="section1">
            	<div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">
                        <div class="section1-tagline-wrapper">
                            <div class="section1-tagline">
                                <?php
                                    the_field( 'section1_tagline' );
                                ?>
                            </div>
                        </div>
                        <div class="section1-title">
                            <?php
                                the_field( 'section1_title' );
                            ?>
                        </div>
                        <div class="section1-subtitle">
                            <?php
                                the_field( 'section1_subtitle' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section5">
            	<div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">
                    	<div class="section5-title">
							<?php
                                the_field( 'section5_title' );
                            ?>
                        </div>  
                    </div>
                </div>
                <div class="section5-subscriptions">
                	<?php
						if( have_rows( 'section5_subscriptions' ) )
						{
							?>
								<div class="row">
									<?php
										$i = 0;
									
										while( have_rows( 'section5_subscriptions') ) 
										{
											the_row(); 
											
											$color = '';
											$mostpopular = 'no';
											if ( $i == 1 ) 
											{
												$color = '#fe424d';
												$mostpopular = 'yes';
											}											
											else
											{
												$color = '#1aa6b7';
											}
											
											$mostpopular_el = '';
											if ( $mostpopular == "yes" ) 
											{
												$mostpopular_el = 
													'<div class="subscribe_mostpopular_wrapper">						
														<div class="subscribe_mostpopular">
														</div>
													</div>';
											}
											else 
											{
												$mostpopular_el = '';
											}
											
											echo
												'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 subscribe_wrapper">
													<div class="subscribe_inner" style="border:3px solid ' . $color . ';">
														' . $mostpopular_el . '
														<div class="subscribe_subtitle">
															- ' . get_sub_field( 'description' ) . ' -
														</div>
														<div class="subscribe_title" style="color:' . $color .';">
															' . get_sub_field( 'name' ) . '
														</div>
														<div class="subscribe_price">
															<span class="subscribe_nominal">$ ' . get_sub_field( 'price' ) . '</span>
														</div>
														<div class="subscribe_note">
															' . get_sub_field( 'extra_description' ) . '
														</div>
														<a href="' . get_sub_field( 'link' ) . '" class="subscribe_select_link">
															<div class="subscribe_select" style="background-color:' . $color . ';">
																' . get_sub_field( 'join_text' ) . '						
															</div>
														</a>
													</div>
												</div>';
											
											$i++;				
										}
									?>
								</div>
							<?php
						}
					?>
                </div>  
                <div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">
                    	<div class="section5-footer">
							<?php
                                the_field( 'section5_footer' );
                            ?>
                        </div>  
                    </div>
                </div>                              
            </div>
            
            <div class="section6">
            	<div class="row row-subscribe">
                	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12">
                    	<div class="section6-title">
							<?php
                                the_field( 'section6_title' );
                            ?>
                        </div>                          
                    </div>
                </div>
                <div class="section6-testimonials">
					<?php
                        if( have_rows( 'section6_testimonials' ) )
                        {

							while( have_rows( 'section6_testimonials') ) 
							{
								the_row(); 
								
								echo 
									'<div class="testimonial-wrapper">
										<div class="testimonial-thumb" style="background-image:url(' . get_sub_field( 'photo' ) . ');">
										</div>
										<div class="testimonial-content">
											<h3 class="testimonial-title">' . get_sub_field( 'name' ) . '</h3>
											' . get_sub_field( 'content' ) . '
										</div>
									</div>';
							}

                        }
                    ?>
                 </div>
            </div>
            
        </div>
        
    </div>   
    
	<style>
		.navbar, .logo, .page-bg, .nc_socialPanel, .general-wrapper, .footer {
			display: none;
		}
	</style>
            
<?php
	get_footer();
?>