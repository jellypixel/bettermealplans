<?php
	/*
		Template Name: About
	*/
	
	get_header();
	
	$theme_options = get_option( 'theme_options' );
	$about_1_photo = get_opt( $theme_options['about_1_photo'], '' );
	$about_1_name = get_opt( $theme_options['about_1_name'], '' );
	$about_1_description = get_opt( $theme_options['about_1_description'], '' );
	
	$about_2_photo = get_opt( $theme_options['about_2_photo'], '' );
	$about_2_name = get_opt( $theme_options['about_2_name'], '' );
	$about_2_description = get_opt( $theme_options['about_2_description'], '' );
	
	$about_3_photo = get_opt( $theme_options['about_3_photo'], '' );
	$about_3_name = get_opt( $theme_options['about_3_name'], '' );
	$about_3_description = get_opt( $theme_options['about_3_description'], '' );	
?>

	<div class="beginpage container">
    	<?php 
			$customTitle = get_the_title();
			$customSubtitle = get_the_excerpt();
			include(locate_template('section-title.php'));
		?> 
		
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>     
            <div class="post-content default-page">
            	<div class="workshop-info-content">           
					<?php
                        // The Content					
                        
                        if ( have_posts() ) 
                        {					
                            while ( have_posts() ) 
                            {
                                the_post();
                                the_content();
                            }
                        } 
                        
                        // Reset
                        wp_reset_query();			
                    ?>                
                </div>
            </div>
	            		
			<div class="row">
	            <ul id="og-grid" class="og-grid">
	                <!--
	                <li>
	                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="<?php bloginfo('template_url'); ?>/images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
	                        <img src="<?php bloginfo('template_url'); ?>/images/thumbs/1.jpg" alt="img01"/>
	                    </a>
	                </li>
	                <li>
	                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="<?php bloginfo('template_url'); ?>/images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
	                        <img src="<?php bloginfo('template_url'); ?>/images/thumbs/1.jpg" alt="img01"/>
	                    </a>
	                </li>
	                <li>
	                    <a href="http://cargocollective.com/jaimemartinez/" data-largesrc="<?php bloginfo('template_url'); ?>/images/1.jpg" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
	                        <img src="<?php bloginfo('template_url'); ?>/images/thumbs/1.jpg" alt="img01"/>
	                    </a>
	                </li>
	                -->
	                
	                <?php 
	                    if ( !empty( $about_1_name ) )
	                    {					
	                ?>
	                        <li>
	                            <a 
	                                href="#" 
	                                data-largesrc="<?php echo preg_replace("/^http:/i", "https:", $about_1_photo['url']); ?>" 
	                                data-title="<?php echo $about_1_name; ?>"
	                                data-description="<?php echo rawurlencode($about_1_description); ?>"
	                            >
	                                <img src="<?php echo preg_replace("/^http:/i", "https:", $about_1_photo['url']); ?>" alt="<?php echo $about_1_name; ?>"/>
	                                <div class="og-name">
	                                    About <?php echo $about_1_name; ?>
	                                </div>
	                            </a>                        
	                        </li>
	                <?php
	                    }
	                    
	                    if ( !empty( $about_2_name ) )
	                    {					
	                        ?>
	                            <li>
	                                <a 
	                                    href="#" 
	                                    data-largesrc="<?php echo preg_replace("/^http:/i", "https:", $about_2_photo['url']); ?>" 
	                                    data-title="<?php echo $about_2_name; ?>"
	                                    data-description="<?php echo rawurlencode($about_2_description); ?>"
	                                >
	                                    <img src="<?php echo preg_replace("/^http:/i", "https:", $about_2_photo['url']); ?>" alt="<?php echo $about_2_name; ?>"/>
	                                    <div class="og-name">
	                                        About <?php echo $about_2_name; ?>
	                                    </div>
	                                </a>                            
	                            </li>
	                        <?php
	                    }
	                    
	                    if ( !empty( $about_3_name ) )
	                    {					
	                        ?>
	                            <li>
	                                <a 
	                                    href="#" 
	                                    data-largesrc="<?php echo preg_replace("/^http:/i", "https:", $about_3_photo['url']); ?>" 
	                                    data-title="<?php echo $about_3_name; ?>"
	                                    data-description="<?php echo rawurlencode($about_3_description); ?>"
	                                >
	                                    <img src="<?php echo preg_replace("/^http:/i", "https:", $about_3_photo['url']); ?>" alt="<?php echo $about_1_name; ?>"/>
	                                    <div class="og-name">
	                                        About <?php echo $about_3_name; ?>
	                                    </div>
	                                </a>
	                                
	                            </li>
	                        <?php
	                    }	
	                ?>            
	            </ul>
	        </div>
        </article>
    </div>
            
<?php
	get_footer();
?>