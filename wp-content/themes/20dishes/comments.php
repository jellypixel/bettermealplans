<section id="comments">
    <div class="comment-wrapper">
        <?php	
            // Comments allowed
            if ( true ) // should be comments_open(). But somehow it always return false.
            {
                // Title
                ?>			
                    <h1 class="comments-title sans">			
                    	Leave a Comment                        
                    </h1>	
                <?php
                
                // Password check
                if ( post_password_required() )
                {
                    ?>
                            <div class="comment-empty">
                                <?php echo __( 'Enter the password to view comments.', 'jellypixel' ); ?>
                            </div>
                        </div>
                    <?php
                    
                    return;
                }
                
                // Reply form - custom comment_form()
                ?>
                    <div id="respond" class="comment-form">
                        <?php 
                            
                            
                            // If required to login to post comment but not yet logged in
                            if ( get_option( 'comment_registration' ) && is_user_logged_in() == false )	
                            {
                                ?>
                                    <div class="comment-empty">
                                        <?php printf( __( 'You need to %slogin%s to post a comment.', 'jellypixel' ), '<a href="' . wp_login_url( get_permalink() ) . '">', '</a>' ); ?>
                                    </div>
                                <?php
                            }
                            // Not required to login to post comment
                            else 
                            {
                                // Logged in user
                                if ( is_user_logged_in() ) 
                                {
                                    ?>                            	
                                        <form action="<?php echo get_option( 'siteurl' ); ?>/wp-comments-post.php" method="post">
                                            
                                            <div class="comment-form-meta">
                                                <a href="<?php echo get_option( 'siteurl' ); ?>/wp-admin/profile.php" class="comment-form-username">
                                                    <?php 
                                                        // Username
                                                        echo $user_identity; 
                                                    ?>
                                                </a>
                                                <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php echo __('Log out', 'jellypixel'); ?>" class="comment-form-logout">
                                                    <?php 
                                                        // Logout button
                                                        echo __('Logout', 'jellypixel'); 
                                                    ?>
                                                </a>
                                            </div>
                                            
                                            <div class="textarea-comment col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <textarea id="comment" name="comment" rows="1" placeholder="<?php echo __('Join the discussion...', 'jellypixel'); ?>"></textarea>                                        
                                            </div>
                                            
                                            <div class="submit-comment col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input id="submit" name="submit" type="submit" value="<?php echo __('Post Comment', 'jellypixel'); ?>" />
                                                
                                                <?php
                                                    // Cancel reply button
                                                    cancel_comment_reply_link( 'Cancel Reply' ); 
                                                ?>
                                            </div>
                                            
                                            <?php
                                                comment_id_fields();
                                                do_action( 'comment_form', $post->ID );
                                            ?>
                                        </form>
                                    <?php
                                }
                                // Guest
                                else 
                                {
                                    ?>
                                        <form action="<?php echo get_option( 'siteurl' ); ?>/wp-comments-post.php" method="post">
                                        	<div class="row row-text-comments">
                                                <div class="text-comment col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <input type="text" id="author" name="author" value="<?php echo $comment_author; ?>" placeholder="<?php echo __('Your name*', 'jellypixel'); ?>" aria-required='true' />
                                                </div>                                        
                                                <div class="text-comment col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <input type="text" id="email" name="email"  value="<?php echo $comment_author_email; ?>" placeholder="<?php echo __('Your email*', 'jellypixel'); ?>" "aria-required='true'" />
                                                </div>
                                                <div class="text-comment col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                    <input type="text" id="url" name="url" value="<?php echo $comment_author_url; ?>" placeholder="<?php echo __('Website', 'jellypixel'); ?>" />                                
                                                </div>                                                
                                            </div>
                                            <div class="textarea-comment col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <textarea id="comment" name="comment" rows="1" placeholder="<?php echo __('Join the discussion...', 'jellypixel'); ?>"></textarea>                                        
                                            </div>
                                            
                                            <div class="submit-comment col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input id="submit" name="submit" type="submit" value="<?php echo __('Post Comment', 'jellypixel'); ?>" />
                                                
                                                <?php
                                                    // Cancel reply button
                                                    cancel_comment_reply_link( 'Cancel Reply' ); 
                                                ?>
                                            </div>
                                            
                                            <?php
                                                comment_id_fields();
                                                do_action( 'comment_form', $post->ID );
                                            ?>
                                        </form>
                                    <?php
                                }
                            }
                        ?>
                    </div>
					
                    <div class="comment_number_wrapper">                    
                    	<div class="comment_number_inner">
							<?php                                                
                                comments_number( __('No Comment', 'jellypixel'), __( 'One Comment', 'jellypixel' ), '% '.__( 'Comments', 'jellypixel' ) );
                            ?>
	                    </div>
                    </div>
                <?php
                            
                // Comment List
                if ( have_comments() ) 
                {
                    ?>	
                        <ul class="comment-list">
                            <?php
                                wp_list_comments( array( 'callback' => 'comment_format', 'style' => 'ul' ) );
                            ?>
                        </ul>                    
                        
                        <nav class="row nav-comment">
                            <div id="nav-previous-comment" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php													
                                    previous_comments_link( '<h5>' . __( 'Previous Comments', 'jellypixel' ) . '</h5>' );
                                ?>
                            </div>                            
                            <div id="nav-next-comment" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <?php
                                    next_comments_link( '<h5>' . __( 'Next Comments', 'jellypixel' ) . '</h5>' );			
                                ?>
                            </div>
                        </nav>
                    <?php
                }
            }
            else 
            {
                // Show total comments before closed
                if ( get_comments_number() > 0 ) 
                {
                    ?>                
                        <h4 class="comments-title">			
                            <?php                                                
                                comments_number( __('No Comment', 'jellypixel'), __( 'One Comment', 'jellypixel' ), '% '.__( 'Comments', 'jellypixel' ) );
                            ?>
                        </h4>	
                    <?php
                }
                
                // Comments are closed
                ?>			
                    <div class="comment-empty">
                        <?php echo __('Comments are currently closed.', 'jellypixel'); ?>
                    </div>
                <?php        
            }
        ?>
        <div class="clear"></div>
    </div>
</section>