<?php
	/*
		Template Name: Member Registration
	*/
	
	get_header();
?>
	
	<div class="container">
    	<?php 
			$customTitle = get_the_title();
			$customSubtitle = '';
			
			if ( $customTitle == 'Monthly' )
			{
				$customSubtitle = 'Save 2,000 minutes with our Monthly Plan';	
			}			
			else if ( $customTitle == 'Quarter (3 Months)' )
			{
				$customSubtitle = 'Save 6,000 minutes with our Monthly Plan';	
			}
			
			//include(locate_template('section-title.php'));
		?>    
        
        <div class="bmp-title-wrapper">
        	<div class="bmp-title">
            	<?php echo $customTitle; ?>
            </div>
            <div class="bmp-subtitle">
            	<?php echo '<p>' . $customSubtitle . '</p>'; ?>
            </div>
        </div>
        
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>     
        	<div class="registration-box">
				<?php			
                    // The Content					
                    
                    if ( have_posts() ) 
                    {					
                        while ( have_posts() ) 
                        {
                            the_post();
                            the_content();
                        }
                    } 
                    
                    // Reset
                    wp_reset_query();			
                ?>  
            </div>              
        </article>        
	</div>	
	
<?php	
	get_footer();	
?>