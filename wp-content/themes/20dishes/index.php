<?php 
	get_header();  
?>

<div class="beginpage container">
	<?php
		$customTitle = get_the_title();
		$customSubtitle = get_the_excerpt();
		
		include(locate_template('section-title.php'));
	?>    
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 docontact">
			<?php
				if ( have_posts() ) 
				{					
					while ( have_posts() ) 
					{
						the_post();
						the_content();
					}
				}
			?>
		</div>   		
	</div>
</div> 

<?php 
	get_footer(); 
?>
