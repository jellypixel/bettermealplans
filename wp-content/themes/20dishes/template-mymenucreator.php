<?php
	/* Template Name: My Menu Creator
	 *
	 * The template for CREATE my menu
	 *
	 * @package WordPress
	 * @subpackage 20Dishes
	 * @since Antiquity
	 */
	 
	get_header();	
	
?>
	<!-- CSS -->
    <link href='<?php bloginfo('template_url')?>/sys/css/style.css?v=25' media='screen' rel='stylesheet' type='text/css' />
    <link href='<?php bloginfo('template_url')?>/sys/css/responsive.css?v=25' media='screen' rel="stylesheet" type="text/css" />
    <link href='<?php bloginfo('template_url')?>/sys/css/jquery-ui.min.css' media='screen' rel='stylesheet' type='text/css' />         
	
    <!-- JS -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/sys/js/jquery-ui.datepicker.js"></script>
    <script type="text/javascript">
        var template_url = '<?php echo bloginfo('template_url'); ?>';
		var userid ='<?php echo $current_user->ID; ?>';
		
		jQuery(document).ready(function(){  
			//iCheck
				jQuery('#createmenu-saving-wrapper input').iCheck({
					checkboxClass: 'icheckbox_minimal',
					radioClass: 'iradio_minimal',
				});
		});
    </script>    
<?php	

	

	if ( is_user_logged_in() )
	{	
		include 'sys/createmenu_loadweek.php';
	
		?>
        	<div id="createmenu-menubar-wrapper" class="container">
            	<div id="createmenu-menubar">
					<div id="editor-title">My Menu Editor</div>
                    <div id="newmenu" class="roundbutton">
                        + New Menu
                    </div>
                    <div id="editmenu" class="roundbutton">
                        Edit Menu
                    </div>
                    <div id="deletemenu" class="roundbutton">
                        Delete This Menu
                    </div>                    
                    <div class="clear"></div>
                </div>
            </div>
        	
            <div id="createmenu-all-wrapper">
				<?php
                    $alltheweek = createmenu_loadWeek('');
                    if ( !empty( $alltheweek ) ) {
                        echo $alltheweek;	
                    }
                ?>  
            </div>
            
            <div id="createmenu-saving-wrapper" class="container">            	
                
				<div id="saveeditmenu" class="savebutton ready">Save</div>	
                <?php
					if ( current_user_can('manage_options') ) 
					{
				?>
						<div id="groupshare-input-wrapper">
							<label for="groupshare_selectDate" style="width:100%;">Group Sharing</label>
							<input id="groupshare_selectDate" type="text">
							
							<select id="selectShare" name="selectShare"/>
								<option value='...'>...</option>
							  <?php							  
								$userGroup = $wpdb->get_results( "
								SELECT DISTINCT data.value FROM wp_cimy_uef_data data, wp_cimy_uef_fields fields
								WHERE fields.Name = 'USER_GROUP' AND data.Field_ID = fields.ID");

								foreach($userGroup as $group) {
									if(!empty($group->value))
										echo "<option value='".$group->value."'>".$group->value."</option>";
								}
							  ?>
													
                            </select>
							<div id="checkShare-wrapper">
								<input id="checkShare" type="checkbox" name="checkShare" value="" /><label for="checkShare">Share to everyone</label>
							</div>							
						</div>
				<?php
					}
				?>
            </div>
            
            <div class="overlay">
                <div id="addcard-lightbox" class="lightbox">
                	<div id="addcard-tabs" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#add-recipe" aria-controls="add-recipe" role="tab" data-toggle="tab">Add Recipe</a></li>
                            <li role="presentation"><a href="#favorite-recipes" aria-controls="favorite-recipes" role="tab" data-toggle="tab">Your Favorited Recipes</a></li>
                            <li role="presentation"><a href="#add-note" aria-controls="add-note" role="tab" data-toggle="tab">Add a Note to Your Plan</a></li>                            
                            <li class="lightbox-close" title="Back to plan"><div class="icon tdicon-thin-close"></div></li>  
                        </ul>
                        
                        <div class="tab-content">
                        	<div role="tabpanel" class="tab-pane active" id="add-recipe">
                            	<div id="searchrecipe-form">
                                	<form action="" id="searchrecipe-formitself" name="searchrecipe-formitself">
                                        <input type="text" id="searchrecipe-input" name="searchrecipe-input" maxlength="255" placeholder="Search..." />
                                        <input type="submit" id="searchrecipe-submit" value="Search Recipe" />
                                        <div class="clear"></div>
                                    </form>
                                </div>
                                <div id="searchrecipe-catbox-wrapper">
                                	<div class="searchrecipe-catbox" value="25">5 Ingredients or Less</div>
                                    <div class="searchrecipe-catbox" value="87">Breakfast</div>
                                    <div class="searchrecipe-catbox" value="696">Budget</div>
                                    <div class="searchrecipe-catbox" value="601">Casserole</div>
                                    <div class="searchrecipe-catbox" value="1066">Freezer Meals</div>
                                    <div class="searchrecipe-catbox" value="948">Instant Pot</div>
                                    <div class="searchrecipe-catbox" value="22">Kid Friendly</div>
                                    <div class="searchrecipe-catbox" value="278">Side Dishes</div>
                                    <div class="searchrecipe-catbox" value="336">Slow Cooker</div>
                                    <div class="searchrecipe-catbox" value="21">Soups, Stews, Chili</div>                                
                                </div>
                                <div id="searchrecipe-message"></div>
                                <div id="searchrecipe-stage">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="favorite-recipes">
                            	<div id="favoriterecipes-message"></div>                                
                                <div id="favoriterecipes-stage">
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="add-note">                        
                            	<textarea id="addnotes-textarea" rows="8" cols="50" maxlength="500"></textarea>
                                <div id="addnotes-button" class="savebutton">Add Note</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="addmenu-lightbox" class="lightbox">
                	<div id="addcard-tabs" role="tabpanel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#edit-menu" aria-controls="edit-menu" role="tab" data-toggle="tab">Edit Menu</a></li>
                            <li class="lightbox-close" title="Cancel"><div class="icon tdicon-thin-close"></div></li>  
                        </ul>
                        
                        <div class="tab-content">
                        	<div role="tabpanel" class="tab-pane active" id="edit-menu">
                                <div id="editmenu-form">
                                    <form action="" id="editmenu-formitself" name="editmenu-formitself">
                                        <input type="text" id="editmenu-input" name="editmenu-input" maxlength="255" />
                                        <input type="submit" id="editmenu-submit" value="Search Menu">
                                        <div class="clear"></div>
                                    </form>
                                </div>
                                <div id="editmenu-stage">
                                </div>
                            </div>
                        </div>
                    </div>       	
                </div>
                
                <div id="yesnoModal" class="modalbox">
                	<div class="modal-scrutiny">                    
	                	Are you sure want to hide this recipe?
                    </div>
                    <div class="modal-action clearfix">
                        <div class="yesbtn modal-default modalbtn">Yes</div>
                        <div class="nobtn modalbtn">No</div>
                    </div>
                </div>
                
                <div id="static-modal" class="modalbox">
                	<div class="modal-scrutiny">                    
	                	
                    </div>
                    <div class="modal-action clearfix">
                        <div class="nobtn modal-default modalbtn">OK</div>
                    </div>
                </div>
                
            </div>    
            
            <div class="overlay-2">
            	<div id="addrecipe-lightbox" class="lightbox-2">
                	<div class="lightbox-2-header">
                    	<div class="lightbox-2-title">                        
                        </div>
                    	<div class="inner-lightbox-close"></div>    
                        <div class="clear"></div>                    
                    </div>
                    <div id="addrecipe-stage" class="lightbox-2-content">                    	
                    </div>
                    <div class="lightbox-2-input">
                    	<div id="reallyaddrecipe" class="savebutton">Add to plan</div>
                    </div>
                </div>            	
            </div>
        <?php	
	}
	else 
	{
		?>
        	<div class="container clear">
            	<div class="thick-col">
                	<p>
                    	You need to be registered and <a href="#" id="kitch_log">logged in</a> to use myKitchen.
                	</p>
				</div>
            </div>
			
        <?php	
	}
?>

<?php
	get_footer();
?>

<script src="<?php bloginfo('template_url'); ?>/sys/js/jquery-ui.touchpunch.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/sys/js/default.js" type="text/javascript"></script>