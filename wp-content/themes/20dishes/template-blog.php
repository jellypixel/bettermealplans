<?php
	/*
		Template Name: Blog Archive
	*/
	
	get_header();
?>

	<div class="beginpage container">
    	<div class="blog-cat-title">
        	<h1>
	        	Blog
            </h1>
        </div>
        
        <div class="blog-wrappers">
        	<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				
				$args = array (
					'ignore_sticky_posts' => true,
					'posts_per_page' 	=> get_option('posts_per_page'),
					'paged'				=> $paged,
					'post_type' 		=> 'blog',
				);	
				
				$wp_query = new WP_Query( $args );				
				$max = $wp_query->max_num_pages;
			
				// The Loop
				if ( $wp_query->have_posts() ) 
				{
					while ( $wp_query->have_posts() ) 
					{
						$wp_query->the_post();
						
						$postThumb = get_the_post_thumbnail_url( $post, 'full' );	
						$title = get_the_title();
						?>					
							<div class="cptblog-wrapper">                                        	    
								<div class="row">
                                	<div class="col-md-6 col-12 cptblog-left">
                                    	<div class="cptblog-description">
                                        	<div class="cptblog-title">
                                                <h2>
                                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> 
                                                        <?php echo $title; ?>
                                                    </a>
                                                </h2> 
                                            </div>
                                        	<div class="cptblog-meta">
												By <span class="red"><?php echo get_the_author_meta( 'display_name' ); ?></span>
                                            </div>
                                            <div class="cptblog-content">
                                            	<?php
													the_excerpt();
												?>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="col-md-6 col-12 cptblog-right">
                                    	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> 
                                            <div class="cptblog-thumb" style="background-image:url(<?php echo $postThumb; ?>);">
                                            </div>
                                        </a>
                                    </div>								                                              
								</div>
							</div>
						<?php
					}	
				} 
								
				// Reset
				wp_reset_postdata();
			?>
            
            <div class="col-12">
            	<div class="blog-nav">
					<?php
                        get_template_part( 'section', 'paging' );
                    ?>
                </div>
            </div>
        </div>
	</div>

<?php
	get_footer();
?>