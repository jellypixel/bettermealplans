<?php
	/*
        Template Name: Amazon Test
	*/	
	
	get_header();	
?>
	
	<div id="amazon-root"></div>
	<script type="text/javascript">
    
      window.onAmazonLoginReady = function() {
        amazon.Login.setClientId('amzn1.application-oa2-client.2f3137832c7f49a68f7cc9e1b214702f');
      };
      (function(d) {
        var a = d.createElement('script'); a.type = 'text/javascript';
        a.async = true; a.id = 'amazon-login-sdk';
        a.src = 'https://api-cdn.amazon.com/sdk/login1.js';
        d.getElementById('amazon-root').appendChild(a);
      })(document);
    
    </script>
    
    
    <div id="lwa">
    	<a href="#" id="LoginWithAmazon">
          <img border="0" alt="Login with Amazon"
            src="https://images-na.ssl-images-amazon.com/images/G/01/lwa/btnLWA_gold_156x32.png"
            width="156" height="32" />
        </a>
        <a id="Logout">Logout</a>
    </div>
    
    <script type="text/javascript">

	  document.getElementById('LoginWithAmazon').onclick = function() {
		options = { scope : 'profile' };
		amazon.Login.authorize(options, 'https://bettermealplans.com');
		return false;
	  };
	
	</script>
    
    <style>
		.navbar-default {
			position:static !important;
		}
	</style>
    
    
    <?php
		// verify that the access token belongs to us
		$c = curl_init('https://api.amazon.com/auth/o2/tokeninfo?access_token=' . urlencode($_REQUEST['access_token']));
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		 
		$r = curl_exec($c);
		curl_close($c);
		$d = json_decode($r);
		 
		if ($d->aud != 'amzn1.application-oa2-client.2f3137832c7f49a68f7cc9e1b214702f') {
		  // the access token does not belong to us
		  header('HTTP/1.1 404 Not Found');
		  echo 'Page not found';
		  //exit;
		}
		 
		// exchange the access token for user profile
		$c = curl_init('https://api.amazon.com/user/profile');
		curl_setopt($c, CURLOPT_HTTPHEADER, array('Authorization: bearer ' . $_REQUEST['access_token']));
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		 
		$r = curl_exec($c);
		curl_close($c);
		$d = json_decode($r);
		 
		echo sprintf('%s %s %s', $d->name, $d->email, $d->user_id);
	?>
    

<?php
	get_footer();
?>