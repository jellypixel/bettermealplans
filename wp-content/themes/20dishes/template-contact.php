<?php
	/*
        Template Name: Contact
	*/	
	
	get_header();	
?>

	<div class="beginpage container">
    	<?php 
	    	$customSubtitle = "Our Customer Service Department is open Monday through Friday from 9am to 5pm PST, excluding holidays. If you are sending an email in over the weekend, your message will be responded to on the next business day.";
			include(locate_template('section-title.php'));
		?>    
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>     
            <div class="post-content docontact">
                <?php
                    // The Content					
					
                    if ( have_posts() ) 
                    {					
                        while ( have_posts() ) 
                        {
                            the_post();
                            the_content();
                        }
                    } 
                    
                    // Reset
                    wp_reset_query();			
                ?>                
            </div>
        </article>        
	</div>	
            
<?php
	get_footer();
?>