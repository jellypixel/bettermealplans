<?php
	/*
		Template Name: Videos
	*/
	
	get_header();	
	
	$cpt_cat = 'video_category';
	$post_per_page = -1;
	
	$allEl = '';
	$restEl = '';
	
	// Get all categories from CPT
	$tax_terms = get_terms( $cpt_cat );
	$arrCPTCategoriesID = array();
	$arrCPTCategoriesName = array();
	$arrCPTCategoriesSlug = array();
	
	foreach ($tax_terms as $tax_term) {		
		array_push( $arrCPTCategoriesID, $tax_term->term_id );
		array_push( $arrCPTCategoriesName, $tax_term->name );
		array_push( $arrCPTCategoriesSlug, $tax_term->slug );
	}
	
	// ALL CATEGORY
		$args = array (
			'post_type'             => array( 'video' ),
			'posts_per_page'		=> $post_per_page
		);
		
		// The Query
		$query = new WP_Query( $args );
		
		// The Loop
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				
				global $post;
				
				$postMeta = get_post_custom($post->ID);	
				$video_link = isset( $postMeta['_video_post_embed_url'] ) ? esc_attr( $postMeta['_video_post_embed_url'][0] ) : '';							
				
				$vimeoID = $video_link;
				
				$sendingID = curl_init('http://vimeo.com/api/oembed.json?url=http://vimeo.com/' . $vimeoID);
				curl_setopt($sendingID, CURLOPT_RETURNTRANSFER, true);
				$json = '';
				
				if( ($json = curl_exec($sendingID) ) === false)
				{
					// Error
					$allEl .=
						'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 videopage_vimeo_wrapper">
							<div class="videopage_vimeo_error"><div class="vimeo_play" vimeoid="' . $vimeoID . '"></div>
							<div class="videopage_title">Video not found</div>
						</div>';
				}
				else
				{
					// Success
					$hash = json_decode(curl_exec($sendingID), true);
									
					$vimeoThumb = $hash['thumbnail_url'];  
					$vimeoTitle = $hash['title'];
					
					$pinterestLink = '<a href="http://pinterest.com/pin/create/button/?url=' . urlencode(get_permalink()) . '&amp;description=' . urlencode(get_the_title()) . '&amp;media=' . urlencode($vimeoThumb). '" target="_blank" class="pinterest-overlay" title="Pinterest"><span class="tdicon-pinterest-circle" title="Pinterest"></span></a>';
					
					$allEl .= 					
						'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 videopage_vimeo_wrapper">
							<div class="homepage_introduction_video" style="background-image:url(' . $vimeoThumb . ');"><div class="video_highlight_overlay vimeo_overlay" vimeoid="' . $vimeoID . '" vidheight="350">' . $pinterestLink . '<div class="tdicon-play-circle video_play video_play_vimeo"></div></div></div>
							<div class="videopage_title">' . get_the_title() . '</div>
						</div>';
				}
				
				// Close handle
				curl_close($sendingID);
			}		
		} else {
			
		}
		wp_reset_postdata();
	
	// INDIVIDUAL CATEGORY
		for ( $x = 0; $x < count( $arrCPTCategoriesName ); $x++ ) {
			$args = array (
				'post_type'             => array( 'video' ),
				'tax_query' => array(
					array(
					  'taxonomy' 		=> 'video_category',
					  'field' 			=> 'slug',
					  'terms' 			=> $arrCPTCategoriesSlug[$x]
					)
				),
				'posts_per_page'		=> $post_per_page
			);
			
			$query = new WP_Query( $args );			
			
			$restEl .= 
				'<div class="tab-pane" id="tab-' . $x . '">
					<div class="row">';
			
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();
					
					global $post;
					
					$postMeta = get_post_custom($post->ID);	
					$video_link = isset( $postMeta['_video_post_embed_url'] ) ? esc_attr( $postMeta['_video_post_embed_url'][0] ) : '';							
					
					$vimeoID = $video_link;
					
					$sendingID = curl_init('http://vimeo.com/api/oembed.json?url=http://vimeo.com/' . $vimeoID);
					curl_setopt($sendingID, CURLOPT_RETURNTRANSFER, true);
					$json = '';
					
					if( ($json = curl_exec($sendingID) ) === false)
					{
						// Error
						$restEl .=
							'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 videopage_vimeo_wrapper">
								<div class="videopage_vimeo_error"><div class="vimeo_play" vimeoid="' . $vimeoID . '"></div>
								<div class="videopage_title">Video not found</div>
							</div>';
					}
					else
					{
						// Success
						$hash = json_decode(curl_exec($sendingID), true);
										
						$vimeoThumb = $hash['thumbnail_url'];  
						$vimeoTitle = $hash['title'];
						
						$pinterestLink = '<a href="http://pinterest.com/pin/create/button/?url=' . urlencode(get_permalink()) . '&amp;description=' . urlencode(get_the_title()) . '&amp;media=' . urlencode($vimeoThumb). '" target="_blank" class="pinterest-overlay" title="Pinterest"><span class="tdicon-pinterest-circle" title="Pinterest"></span></a>';
						
						$restEl .= 					
							'<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 videopage_vimeo_wrapper">
								<div class="homepage_introduction_video" style="background-image:url(' . $vimeoThumb . ');"><div class="video_highlight_overlay vimeo_overlay" vimeoid="' . $vimeoID . '" vidheight="350">' . $pinterestLink . '<div class="tdicon-play-circle video_play video_play_vimeo"></div></div></div>
								<div class="videopage_title">' . get_the_title() . '</div>
							</div>';
					}
					
					// Close handle
					curl_close($sendingID);
				}		
			} else {
				
			}
			wp_reset_postdata();
			
			$restEl .= 
					'</div>
				</div>';
		}
	
	
?>

	<div class="beginpage container">
        <div class="videopage row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           		<div class="tabs-title video">
					<div class="tabs-title-1">
						Videos
					</div>				
				</div>
                
                <ul class="nav nav-tabs">
                	<li class="active">
                    	<a href="#tab-all" data-toggle="tab">All Categories</a>
                    </li>                	
                
                	<?php
						for ( $i = 0; $i < count( $arrCPTCategoriesName ); $i++ ) {
							echo 
								'<li>
									<a href="#tab-' . $i . '" data-toggle="tab">' . $arrCPTCategoriesName[$i] . '</a>
								</li>';
						}
					?>
                </ul>
                
                <div class="tab-content">
                	<div class="tab-pane active" id="tab-all">
                    	<div class="row">
	                        <?php echo $allEl; ?>
                        </div>
                    </div>
                
                	<?php echo $restEl; ?>
                </div>
                
            </div>                    
        </div>
	</div>

<?php
	get_footer();
?>