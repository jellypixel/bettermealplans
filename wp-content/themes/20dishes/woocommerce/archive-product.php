<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

	<div class="general-wrapper nomargin clearfix">
	    <div class="general-title ">
	    	<div class="bubble">                        
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		
					<?php woocommerce_page_title(); ?>
		
				<?php endif; ?>    
				
				<?php
					/**
					 * woocommerce_archive_description hook.
					 *
					 * @hooked woocommerce_taxonomy_archive_description - 10
					 * @hooked woocommerce_product_archive_description - 10
					 */
					do_action( 'woocommerce_archive_description' );
				?>			                
	    	</div>
	    </div>
	</div>
    
    <div class="archive-slider">
    	<?php echo do_shortcode('[rev_slider alias="shop_home"]'); ?>
    </div>
	
	<div class="post-content default-page default-page-wc clearfix">
    	<?php 
			if ( have_posts() ) 
			{ 
				?>    
                    <div class="row">
                        <div class="container">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-lg-offset-0 col-md-offset-0 col-sm-offset-0">
                            	<div class="archive-header">
									<?php
                                        /**
                                         * woocommerce_before_shop_loop hook.
                                         *
                                         * @hooked woocommerce_result_count - 20
                                         * @hooked woocommerce_catalog_ordering - 30
                                         */
                                        remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20);		 
                                        do_action( 'woocommerce_before_shop_loop' );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="container">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 col-lg-offset-0 col-md-offset-0 col-sm-offset-0">
                                <?php
									get_sidebar( 'woocommerce' );
								?>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            	<div class="row">
									<?php
                                        woocommerce_product_loop_start(); 
                                         
                                        woocommerce_product_subcategories(); 
                                         
                                        while ( have_posts() ) 
                                        {
                                            the_post(); 
                                            
                                            wc_get_template_part( 'content', 'product' );
                                        }  
                                        
                                        woocommerce_product_loop_end(); 
                                        
                                        /**
                                         * woocommerce_after_shop_loop hook.
                                         *
                                         * @hooked woocommerce_pagination - 10
                                         */
                                        do_action( 'woocommerce_after_shop_loop' );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class='woocommerce-main'>
                        
                    </div>
			
				<?php 
			}
			elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) 
			{
				wc_get_template( 'loop/no-products-found.php' ); 
			}
		?>	
	</div>
	
	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

<?php get_footer( 'shop' ); ?>
