<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div <?php post_class( 'productcard' ); ?>>
        <?php
        /**
         * woocommerce_before_shop_loop_item hook.
         *
         * @hooked woocommerce_template_loop_product_link_open - 10
         */
        do_action( 'woocommerce_before_shop_loop_item' );
    
        /**
         * woocommerce_before_shop_loop_item_title hook.
         *
         * @hooked woocommerce_show_product_loop_sale_flash - 10
         * @hooked woocommerce_template_loop_product_thumbnail - 10
         */
		remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);	
        do_action( 'woocommerce_before_shop_loop_item_title' );
				
		if ( has_post_thumbnail() ) // Manual Thumbnail
		{
			$attachment_image_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID, 'gallery-thumb' ) );					
			$attachment_image_thumb_url = $attachment_image_thumb[0];
		}
		else 
		{
			$attachment_image_thumb_url = '';
		}
		?>
			<a href="<?php the_permalink(); ?>">
				<div class="productcard-thumb" style="background-image:url(<?php echo $attachment_image_thumb_url; ?>);" title="<?php the_title; ?>">
				</div>                                
			</a>
		<?php
    
        /**
         * woocommerce_shop_loop_item_title hook.
         *
         * @hooked woocommerce_template_loop_product_title - 10
         */		
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);	
        do_action( 'woocommerce_shop_loop_item_title' );
		
		$titlecount = 6; // Manual Title						
		$getTitle = get_the_title();										
		if ( $getTitle != '' ) {
			if ( str_word_count( $getTitle, 0 ) > $titlecount ) {
				$words = str_word_count( $getTitle, 2 );
				$pos = array_keys( $words );
				$resultTitle = substr( $getTitle, 0, $pos[$titlecount] ) . '...';
			}
			else {
				$resultTitle = $getTitle;
			}
		}
		else {
			$resultTitle = '';	
		}
    
        /**
         * woocommerce_after_shop_loop_item_title hook.
         *
         * @hooked woocommerce_template_loop_rating - 5
         * @hooked woocommerce_template_loop_price - 10
         */
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);	
        //do_action( 'woocommerce_after_shop_loop_item_title' );
		
		?>
            <div class="productcard-content">
                <div class="productcard-title">
                    <div class="productcard-title-inner">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title; ?>">
                            <?php echo $resultTitle; ?>
                        </a>
                    </div>
                </div>
                <div class="productcard-price">
                    <?php 
                    	do_action( 'woocommerce_after_shop_loop_item_title' ); 
                    ?>
                </div>
                <div class="productcard-action">
                	<?php    
						/**
						 * woocommerce_after_shop_loop_item hook.
						 *
						 * @hooked woocommerce_template_loop_product_link_close - 5
						 * @hooked woocommerce_template_loop_add_to_cart - 10
						 */
						do_action( 'woocommerce_after_shop_loop_item' );
					?>
                </div>
            </div>        
    </div>
</div>