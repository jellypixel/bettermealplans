<?php

/**

 * The Template for displaying all single products

 *

 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.

 *

 * HOWEVER, on occasion WooCommerce will need to update template files and you

 * (the theme developer) will need to copy the new files to your theme to

 * maintain compatibility. We try to do this as little as possible, but it does

 * happen. When this occurs the version of the template file will be bumped and

 * the readme will list any important changes.

 *

 * @see 	    https://docs.woocommerce.com/document/template-structure/

 * @author 		WooThemes

 * @package 	WooCommerce/Templates

 * @version     1.6.4

 */



if ( ! defined( 'ABSPATH' ) ) {

	exit; // Exit if accessed directly

}



get_header( 'shop' ); ?>



	<?php

		/**

		 * woocommerce_before_main_content hook.

		 *

		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)

		 * @hooked woocommerce_breadcrumb - 20

		 */

		do_action( 'woocommerce_before_main_content' );

	?>

	

		<div class="general-wrapper nomargin clearfix">

		    <div class="general-title ">

		    	<div class="bubble">                        

					<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			

						<?php woocommerce_page_title(); ?>

			

					<?php endif; ?>    

					

					<?php

						/**

						 * woocommerce_archive_description hook.

						 *

						 * @hooked woocommerce_taxonomy_archive_description - 10

						 * @hooked woocommerce_product_archive_description - 10

						 */

						do_action( 'woocommerce_archive_description' );

					?>			                

		    	</div>

		    </div>

		</div>	



		<div class="post-content default-page default-page-wc default-single-product-wc">
        	<div class="backbutton-wrapper">
                <div class="backbutton">
                    Back
                </div>
            </div>
        

        	<div class="row">

                <div class="container">

                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0">

						<?php

                            // MAIN LOOP

                                while ( have_posts() )  

                                {

                                    the_post();

                                    

                                    wc_get_template_part( 'content', 'single-product' );        

                                }

                    	?>

            		</div>

                </div>

            </div>

		</div>

		

	<?php

		/**

		 * woocommerce_after_main_content hook.

		 *

		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)

		 */

		do_action( 'woocommerce_after_main_content' );

	?>



	<?php

		/**

		 * woocommerce_sidebar hook.

		 *

		 * @hooked woocommerce_get_sidebar - 10

		 

		do_action( 'woocommerce_sidebar' );

		*/

	?>



<?php get_footer( 'shop' ); ?>

