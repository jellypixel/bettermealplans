<div class="post-author-wrapper">
	<div class="row">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12  post-author-image">
            <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                <?php
                    echo get_avatar( get_the_author_meta( 'user_email' ), 170, "", get_the_author_meta( 'display_name' ) );
                ?>
            </a>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
            <h2 class="author-name">
            	<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
	                <?php the_author(); ?>
                </a>
            </h2>
            <p class="post-author-description">
                <?php	
                    echo get_the_author_meta( 'description' );
                ?>
            </p>
        </div>
    </div>
</div>
