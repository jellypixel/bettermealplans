<?php
	get_header();	
?>

	<div class="beginpage container">
    	<?php 
			include(locate_template('section-title.php'));
		?>    
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>     
            <div class="post-content default-page">
                <?php
                    // The Content					
					
                    if ( have_posts() ) 
                    {					
                        while ( have_posts() ) 
                        {
                            the_post();
                            the_content();
                        }
                    } 
                    
                    // Reset
                    wp_reset_query();			
                ?>                
            </div>
        </article>                
	</div>	
            
<?php
	get_footer();
?>