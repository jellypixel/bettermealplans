<?php
/*
Plugin Name: Affiliate Royale Math CAPTCHA
Plugin URI: http://www.affiliateroyale.com
Description: Shows a Math question on the affiliate signup form to prevent bots from signing up SPAM accounts.
Version: 1.0.0
Author: Caseproof, LLC
Author URI: http://caseproof.com
Text Domain: affiliate-royale
Copyright: 2004-2013, Caseproof, LLC
*/

if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.');}

include_once(ABSPATH . 'wp-admin/includes/plugin.php');

if(is_plugin_active('affiliate-royale/affiliate-royale.php')) {
  define('ARMATH_PLUGIN_SLUG', plugin_basename(__FILE__));
  define('ARMATH_PLUGIN_NAME', dirname(ARMATH_PLUGIN_SLUG));
  define('ARMATH_PATH', WP_PLUGIN_DIR.'/'.ARMATH_PLUGIN_NAME);
  define('ARMATH_URL', plugins_url('/'.ARMATH_PLUGIN_NAME));
  define('ARMATH_DB_KEY', 'wafpmath_unique_key');
  
  function wafpmath_gen_unique_key() {
    $key = get_option(ARMATH_DB_KEY, false);
    
    if($key === false)
      update_option(ARMATH_DB_KEY, time() . uniqid());
  }
  add_action('init', 'wafpmath_gen_unique_key');
  
  function wafpmath_encrypt_data($num1, $num2) {
    $key = get_option(ARMATH_DB_KEY, false);
    
    return base64_encode(md5(($num1 + $num2) . $key));
  }
  
  function wafpmath_compare_data($answer, $old_data) {
    $key = get_option(ARMATH_DB_KEY, false);
    $new_data = md5($answer . $key);
    $old_data = base64_decode($old_data);
    
    return ($new_data == $old_data);
  }
  
  function wafpmath_gen_random_number($size = 'small') {
    $int = 0;
    
    switch($size) {
      case 'large':
        $int = mt_rand(16, 30);
        break;
      case 'medium':
        $int = mt_rand(6, 15);
        break;
      default: //small
        $int = mt_rand(1, 5);
       break;
    }
    
    return $int;
  }
  
  //This field will be updated via JS -- further 
  function wafpmath_show_field() {
    //Don't show to logged in users
    if(WafpUtils::is_user_logged_in())
      return;
    
    $num1 = wafpmath_gen_random_number('medium');
    $num2 = wafpmath_gen_random_number('small');
    
    $data = wafpmath_encrypt_data($num1, $num2);
    ?>
    <tr>
      <td><label for="wafpmath_quiz"><span id="wafpmath_captcha"></span>*</label></td>
      <td>
        <input type="text" name="wafpmath_quiz" id="wafpmath_quiz" value="" />
        <input type="hidden" name="wafpmath_data" value="<?php echo $data; ?>" />
        <script>
          document.getElementById("wafpmath_captcha").innerHTML=base64.decode("<?php echo base64_encode(sprintf(__("%s equals?", "affiliate-royale"), "{$num1} + {$num2}")); ?>");
        </script>
      </td>
    </tr>
    <?php
  }
  add_action('wafp-user-signup-fields', 'wafpmath_show_field');
  
  function wafpmath_validate_answer($errors) {
    //Don't validate for already logged in users
    if(WafpUtils::is_user_logged_in())
      return $errors;
    
    if(!isset($_POST['wafpmath_quiz']) || empty($_POST['wafpmath_quiz'])) {
      $errors[] = __("You must fill out the Math Quiz correctly.", "affiliate-royale");
      return $errors;
    }
    
    if(!isset($_POST['wafpmath_data']) || empty($_POST['wafpmath_data'])) {
      $errors[] = __("You must fill out the Math Quiz correctly.", "affiliate-royale");
      return $errors;
    }
    
    $answer = (int)$_POST['wafpmath_quiz'];
    $data = $_POST['wafpmath_data'];
    
    if(!wafpmath_compare_data($answer, $data))
      $errors[] = __("You must fill out the Math Quiz correctly.", "affiliate-royale");
    
    return $errors;
  }
  add_filter('wafp-validate-signup', 'wafpmath_validate_answer');
  
  function wafpmath_load_scripts() {
    global $post, $wafp_options;
    
    if(is_page($wafp_options->signup_page_id)) {
      wp_enqueue_script('base64', ARMATH_URL.'/base64.js', array('jquery'), WAFP_VERSION);
    }
  }
  add_action('wp_enqueue_scripts', 'wafpmath_load_scripts');
} //End if (is plugin active)
