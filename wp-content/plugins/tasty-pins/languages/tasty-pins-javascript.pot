msgid ""
msgstr ""
"Content-Type: text/plain; charset=utf-8\n"
"X-Generator: babel-plugin-makepot\n"

#: assets/block-editor/image-block-fields.js:69
msgid "Pinterest Title"
msgstr ""

#: assets/block-editor/image-block-fields.js:76
msgid "Pinterest Text"
msgstr ""

#: assets/block-editor/image-block-fields.js:84
msgid "Pinterest Repin ID"
msgstr ""

#: assets/block-editor/image-block-fields.js:91
msgid "Disable Pinning"
msgstr ""

#: assets/settings/views/Settings.vue:195
msgid ""
"Pinterest recommends using images with an aspect ratio of 2:3. Consider "
"disabling pinning of horizontal images in your posts."
msgstr ""

#: assets/settings/views/Settings.vue:199
msgid ""
"The hover button will not appear on image smaller than the minimum image "
"width."
msgstr ""

#: assets/settings/views/Settings.vue:203
msgid ""
"The hover button will not appear on image smaller than the minimum image "
"height."
msgstr ""

#: assets/settings/views/Settings.vue:209
msgid "Tasty Pins hover button"
msgstr ""

#: assets/settings/views/Settings.vue:210
msgid "Hover button position"
msgstr ""

#: assets/settings/views/Settings.vue:214
msgid "Hover button shape"
msgstr ""

#: assets/settings/views/Settings.vue:215
msgid "Image overlay shadow"
msgstr ""

#: assets/settings/views/Settings.vue:216
msgid "Custom button label"
msgstr ""

#: assets/settings/views/Settings.vue:220
msgid "Custom button label text"
msgstr ""

#: assets/settings/views/Settings.vue:224
msgid "Minimum image width"
msgstr ""

#: assets/settings/views/Settings.vue:225
msgid "Minimum image height"
msgstr ""

#: assets/settings/views/Settings.vue:231
msgid "Custom"
msgstr ""

#: assets/settings/views/Settings.vue:235
msgid "Default"
msgstr ""

#: assets/settings/views/Settings.vue:239
msgid "Off"
msgstr ""

#: assets/settings/views/Settings.vue:245
msgid "Top Left"
msgstr ""

#: assets/settings/views/Settings.vue:249
msgid "Top Right"
msgstr ""

#: assets/settings/views/Settings.vue:253
msgid "Center"
msgstr ""

#: assets/settings/views/Settings.vue:257
msgid "Bottom Left"
msgstr ""

#: assets/settings/views/Settings.vue:261
msgid "Bottom Right"
msgstr ""

#: assets/settings/views/Settings.vue:267
msgid "Round"
msgstr ""

#: assets/settings/views/Settings.vue:271
msgid "Square"
msgstr ""

#: assets/settings/views/Settings.vue:275
msgid "Rounded Rectangle"
msgstr ""

#: assets/settings/views/Settings.vue:287
msgid "Yes"
msgstr ""

#: assets/settings/views/Settings.vue:291
msgid "No"
msgstr ""

#: assets/settings/views/Settings.vue:299
msgid "20 characters max."
msgstr ""

#: assets/settings/views/Settings.vue:303
#. Number of characters remaining.
msgid "%d characters remaining."
msgstr ""