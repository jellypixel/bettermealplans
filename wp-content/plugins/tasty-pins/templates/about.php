<?php
/**
 * Template for the about tab on the settings page.
 *
 * @package Tasty_Pins
 */

?>

<main class="tasty-pins-about">
	<div class="tasty-pins-about-content">
		<h1><?php esc_html_e( 'Welcome to Tasty Pins! 🎉', 'tasty-pins' ); ?></h1>
		<section>
			<p><?php esc_html_e( 'Tasty Pins is a Pinterest optimization plugin for WordPress websites. By purchasing Tasty Pins, you\'re getting access to amazing Pinterest tools for your images, superior code quality, and a helpful support team ready to answer all your questions.', 'tasty-pins' ); ?></p>

			<h2><?php esc_html_e( 'Getting Started', 'tasty-pins' ); ?></h2>

			<p><?php esc_html_e( 'Tasty Pins requires some very simple set up and then it just works! The magic happens in your posts, where you can add extra information to your images to help your content stand out on Pinterest.', 'tasty-pins' ); ?></p>
			<p><?php esc_html_e( 'You\'ll find most of what you need to get started here, but for more details, visit our Getting Started support document.', 'tasty-pins' ); ?></p>
			<p><a class="button" target="blank" href="http://support.wptasty.com/en/articles/4201547-getting-started-with-tasty-pins"><?php esc_html_e( 'Getting Started with Tasty Pins', 'tasty-pins' ); ?></a></p>
		</section>

		<section>
			<h2><?php esc_html_e( 'Tasty Pins Settings', 'tasty-pins' ); ?></h2>
			<img src="<?php echo esc_url( plugins_url( 'assets/images/tasty-pins-settings.jpg', __DIR__ ) ); ?>" data-pin-nopin="true" alt="Screenshot of the Tasty Pins fields in the block editor, including Pinterest Text, Disable Pinning, and Repin ID" />
			<p><?php esc_html_e( 'In the Tasty Pins settings, you’ll find everything you need to set the style and placement of your Pinterest hover button. Quickly and easily limit where your hover buttons appear with the minimum width and height settings.', 'tasty-pins' ); ?></p>
			<p><a class="button" href="<?php menu_page_url( Tasty_Pins\Admin::PAGE_SLUG, true ); ?>"><?php esc_html_e( 'Visit Settings', 'tasty-pins' ); ?></a>
		</section>

		<section>
			<h2><?php esc_html_e( 'Add Pinterest Descriptions', 'tasty-pins' ); ?></h2>
			<img src="<?php echo esc_url( plugins_url( 'assets/images/tasty-pins-post.jpg', __DIR__ ) ); ?>" data-pin-nopin="true" alt="Screenshot of the Tasty Pins fields in the block editor, including Pinterest Text, Disable Pinning, and Repin ID" />
			<p>
				<?php
				esc_html_e(
					'For each image in your post, Tasty Pins enables you to set a Pinterest Title and Description for your images, separate from the alt text. This means that you can use your alt text for what it\'s intended &mdash; describing the image for screen readers and search results &mdash; while still providing an optimized description for Pinterest.',
					'tasty-pins'
				);
				?>
			</p>
			<p>
				<?php
				esc_html_e(
					'Here you can also disable the pinning of images that might not be ideal for Pinterest. Pinterest recommends a vertical 2:3 image ratio for pins, so this is a great opportunity to disable those horizontal images.',
					'tasty-pins'
				);
				?>
			</p>
			<p>
			<?php
			printf(
				// translators: "adding Pinterest text to your images" and "writing great Pinterest descriptions".
				esc_html__( 'Learn more about %1$s, %2$s and %3$s.', 'tasty-pins' ),
				sprintf(
					'<a href="https://www.wptasty.com/blog/how-to-use-pinterest-titles" target="_blank">%s</a>',
					esc_html__( 'Pinterest titles', 'tasty-pins' )
				),
				sprintf(
					'<a href="https://support.wptasty.com/tasty-pins/how-do-i-add-a-pinterest-text-to-my-image" target="_blank">%s</a>',
					esc_html__( 'adding Pinterest Text to your images', 'tasty-pins' )
				),
				sprintf(
					'<a href="https://www.wptasty.com/blog/how-to-write-great-pinterest-image-descriptions" target="_blank">%s</a>',
					esc_html__( 'writing great Pinterest descriptions', 'tasty-pins' )
				)
			);
			?>
			</p>
			<h2><?php esc_html_e( 'Grow Your Pin Repin Count', 'tasty-pins' ); ?></h2>
			<p><?php esc_html_e( 'The number of Repins (or saves) an image has on Pinterest is a type of "ranking signal" for Pinterest searches. Pins with higher numbers of repins show up in search more frequently.', 'tasty-pins' ); ?></p>
			<p><?php esc_html_e( 'With Tasty Pins, you can add a Repin ID to your images to help boost the number of repins for images already on Pinterest.', 'tasty-pins' ); ?></p>
			<p>
			<?php
			printf(
				// translators: "Pinterest Repins".
				esc_html__( 'Learn more about %1$s and how to make them work for you.', 'tasty-pins' ),
				sprintf(
					'<a href="https://www.wptasty.com/blog/pinterest-repins" target="_blank">%s</a>',
					esc_html__( 'Pinterest Repins', 'tasty-pins' )
				)
			);
			?>
			</p>
		</section>

		<section>
			<h2><?php esc_html_e( 'Add Hidden Images', 'tasty-pins' ); ?></h2>
			<img src="<?php echo esc_url( plugins_url( 'assets/images/tasty-pins-hidden-images.jpg', __DIR__ ) ); ?>" data-pin-nopin="true" alt="Screenshot of the Tasty Pins meta area where you can add hidden images and force-pin the first hidden image" />
			<p><?php esc_html_e( 'Some images are great for Pinterest, but they don\'t look that great on your site. Add these as Hidden Images to your page and they\'ll appear as an option when readers go to save your content to Pinterest.', 'tasty-pins' ); ?></p>
			<p><?php esc_html_e( 'Better yet, check the "force-pin" box and any image in the content will automatically use this hidden image when it\'s saved to Pinterest.', 'tasty-pins' ); ?></p>
			<p>
			<?php
			printf(
				// translators: "adding hidden images" and "force-pinning hidden images".
				esc_html__( 'Learn more about %1$s and %2$s.', 'tasty-pins' ),
				sprintf(
					'<a href="https://support.wptasty.com/tasty-pins/how-do-i-add-a-hidden-pinterest-image" target="_blank">%s</a>',
					esc_html__( 'adding hidden images', 'tasty-pins' )
				),
				sprintf(
					'<a href="https://support.wptasty.com/tasty-pins/how-do-i-force-pinning-of-a-hidden-image" target="_blank">%s</a>',
					esc_html__( 'force-pinning hidden images', 'tasty-pins' )
				)
			);
			?>
			</p>
		</section>

		<section>
			<h2><?php esc_html_e( 'Visit Support and Documentation', 'tasty-pins' ); ?></h2>
			<p><?php esc_html_e( 'There\'s even more to Tasty Pins! To learn more, get answers to FAQs and see our documentation, visit Tasty Pins Support. If your questions aren\'t answered there, send us a quick chat and we\'ll be happy to help.', 'tasty-pins' ); ?></p>
			<p><a class="button" href="https://support.wptasty.com/tasty-pins" target="_blank"><?php esc_html_e( 'Visit Support and Documentation', 'tasty-pins' ); ?></a>
		</section>
	</div>
</main>
