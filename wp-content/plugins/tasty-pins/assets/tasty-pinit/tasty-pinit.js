import pinterestIcon from '../images/pinterest-badge.svg';

let hoverEl = null;
let overlayEl = null;
let isHoveringEl = false;

const configuration = window.tastyPinitSettings;

const getOffset = (el) => {
	let x = 0;
	let y = 0;
	while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
		x += el.offsetLeft - el.scrollLeft;
		y += el.offsetTop - el.scrollTop;
		el = el.offsetParent;
	}
	const html = document.querySelector('html');
	if (html) {
		x +=
			parseInt(
				window
					.getComputedStyle(html, null)
					.getPropertyValue('margin-left')
			) || 0;
		x +=
			parseInt(
				window
					.getComputedStyle(html, null)
					.getPropertyValue('padding-left')
			) || 0;
		y +=
			parseInt(
				window
					.getComputedStyle(html, null)
					.getPropertyValue('margin-top')
			) || 0;
		y +=
			parseInt(
				window
					.getComputedStyle(html, null)
					.getPropertyValue('padding-top')
			) || 0;
	}
	return { top: y, left: x };
};

export const getPinterestShareUrl = (img) => {
	const bits = [];
	let url = window.location.href;
	if (typeof img.dataset.pinUrl !== 'undefined') {
		url = img.dataset.pinUrl;
	}
	bits.push('url=' + encodeURIComponent(url));
	let media = img.getAttribute('src');
	if (typeof img.dataset.pinMedia !== 'undefined') {
		media = img.dataset.pinMedia;
	}
	bits.push('media=' + encodeURIComponent(media));
	if (
		typeof img.dataset.pinDescription !== 'undefined' ||
		typeof img.dataset.pinTitle !== 'undefined'
	) {
		let description = img.dataset.pinDescription || img.dataset.pinTitle;
		if (description.length > 500) {
			description = description.substring(0, 500);
		}
		bits.push('description=' + encodeURIComponent(description));
	}
	if (typeof img.dataset.pinId !== 'undefined') {
		bits.push('id=' + encodeURIComponent(img.dataset.pinId));
	}
	return 'https://www.pinterest.com/pin/create/button/?' + bits.join('&');
};

const getHoverClasses = (settings) => {
	let classes = 'tasty-pinit-button';
	if (settings.hover_button_shape) {
		classes += ' tasty-pinit-' + settings.hover_button_shape;
	}
	return classes;
};

const getHoverPosition = (offset, img, settings) => {
	let top = offset.top;
	let left = offset.left;
	const hoverElWidth = hoverEl.getBoundingClientRect().width || 0;
	const hoverElHeight = hoverEl.getBoundingClientRect().height || 0;
	switch (settings.hover_button_position) {
		case 'top-left':
			top += 10;
			left += 10;
			break;
		case 'top-right':
			top += 10;
			left = left + img.getBoundingClientRect().width - hoverElWidth - 10;
			break;
		case 'center':
			top = top + img.getBoundingClientRect().height / 2;
			left = left + img.getBoundingClientRect().width / 2;
			break;
		case 'bottom-left':
			top = top + img.getBoundingClientRect().height - hoverElHeight - 10;
			left += 10;
			break;
		case 'bottom-right':
			top = top + img.getBoundingClientRect().height - hoverElHeight - 10;
			left = left + img.getBoundingClientRect().width - hoverElWidth - 10;
			break;
	}
	return {
		top,
		left,
	};
};

const cleanUpExistingEl = () => {
	if (null !== hoverEl) {
		hoverEl.parentNode.removeChild(hoverEl);
		hoverEl = null;
	}
	if (null !== overlayEl) {
		overlayEl.parentNode.removeChild(overlayEl);
		overlayEl = null;
	}
};

const imgMouseOver = (img) => {
	if (isHoveringEl) {
		return;
	}
	if (-1 !== img.src.indexOf('.svg')) {
		return;
	}
	cleanUpExistingEl();
	const imgOffset = getOffset(img);
	if ('yes' === configuration.image_overlay_enabled) {
		overlayEl = document.createElement('div');
		overlayEl.setAttribute('class', 'tasty-pinit-overlay');
		const inlineStyle = [];
		inlineStyle.push('top: ' + imgOffset.top + 'px');
		inlineStyle.push('left: ' + imgOffset.left + 'px');
		inlineStyle.push(
			'height: ' + img.getBoundingClientRect().height + 'px'
		);
		inlineStyle.push('width: ' + img.getBoundingClientRect().width + 'px');
		overlayEl.setAttribute('style', inlineStyle.join(';'));
		document.body.appendChild(overlayEl);
	}
	hoverEl = document.createElement('a');
	hoverEl.setAttribute('href', getPinterestShareUrl(img));
	hoverEl.setAttribute('class', getHoverClasses(configuration));
	hoverEl.innerHTML = pinterestIcon;
	if ('yes' === configuration.custom_button_label_enabled) {
		const spanEl = document.createElement('span');
		spanEl.setAttribute('class', 'tasty-pinit-label');
		spanEl.innerText = configuration.custom_button_label_text;
		hoverEl.appendChild(spanEl);
	}
	document.body.appendChild(hoverEl);
	const inlineStyle = [];
	const hoverOffset = getHoverPosition(imgOffset, img, configuration);
	inlineStyle.push('top: ' + hoverOffset.top + 'px');
	inlineStyle.push('left: ' + hoverOffset.left + 'px');
	if ('center' === configuration.hover_button_position) {
		inlineStyle.push('transform: translate(-50%, -50%)');
	}
	hoverEl.setAttribute('style', inlineStyle.join(';'));
	hoverEl.addEventListener('mouseenter', () => {
		isHoveringEl = true;
	});
	hoverEl.addEventListener('mouseout', () => {
		isHoveringEl = false;
	});
	hoverEl.addEventListener('click', (event) => {
		event.preventDefault();
		window.open(
			hoverEl.getAttribute('href'),
			'tastypin' + new Date().getTime(),
			'status=no,resizable=yes,scrollbars=yes,personalbar=no,directories=no,location=no,toolbar=no,menubar=no,width=750,height=320,left=0,top=0'
		);
	});
};

const imgMouseOut = () => {
	if (isHoveringEl) {
		return;
	}
	cleanUpExistingEl();
};

const init = () => {
	document.querySelectorAll('img').forEach((img) => {
		if (
			typeof img.dataset.pinNopin !== 'undefined' ||
			img.getAttribute('nopin') !== null
		) {
			return;
		}
		img.addEventListener('mouseenter', () => {
			imgMouseOver(img);
		});
		img.addEventListener('mouseout', () => {
			setTimeout(() => {
				imgMouseOut(img);
			}, 10);
		});
	});
	// Stomp on existing elements added by Pinterest Chrome Extension.
	if (typeof MutationObserver !== 'undefined') {
		const observer = new MutationObserver(() => {
			let index, el;
			for (index in document.body.children) {
				el = document.body.children[index];
				if ('SPAN' !== el.tagName) {
					continue;
				}
				// Z-index isn't high enough to be Pinterest.
				if (parseInt(el.style.zIndex) < 10000) {
					continue;
				}
				// Probably the 'More Like This button'.
				if (
					'12px' === el.style.borderRadius &&
					'rgba(0, 0, 0, 0.4)' === el.style.backgroundColor &&
					-1 !==
						el.style.backgroundImage.indexOf('data:image/svg+xml')
				) {
					document.body.removeChild(el);
				}
				// Probably the 'Save' button.
				if (
					'3px' === el.style.borderRadius &&
					'rgb(230, 0, 35)' === el.style.backgroundColor &&
					-1 !==
						el.style.backgroundImage.indexOf('data:image/svg+xml')
				) {
					document.body.removeChild(el);
				}
			}
		});
		observer.observe(document, {
			attributes: false,
			childList: true,
			characterData: false,
			subtree: true,
		});
	}
};

if (/comp|inter/.test(document.readyState)) {
	init();
} else if ('addEventListener' in document) {
	document.addEventListener('DOMContentLoaded', () => {
		init();
	});
} else {
	document.attachEvent('onreadystatechange', () => {
		if (document.readyState === 'complete') {
			init();
		}
	});
}
