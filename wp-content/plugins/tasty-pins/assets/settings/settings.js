import Vue from 'vue';

// Entire views
Vue.component('tasty-pins-settings', require('./views/Settings.vue').default);

if (document.querySelector('.tasty-pins-vue')) {
	new Vue({
		el: '.tasty-pins-vue',
	});
}
