<?php
/**
 * Template to be used for the collections print page.
 *
 * @link       https://bootstrapped.ventures
 * @since      5.3.0
 *
 * @package    WP_Recipe_Maker_Premium/addons-elite/recipe-collections
 * @subpackage WP_Recipe_Maker_Premium/addons-elite/recipe-collections/templates/public
 */

?>
<!DOCTYPE html>
<html <?php echo get_language_attributes(); ?>>
	<head>
		<title><?php echo get_bloginfo( 'name' ); ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo get_bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta name="robots" content="noindex">
		<?php if ( WPRM_Settings::get( 'metadata_pinterest_disable_print_page' ) ) : ?>
			<meta name="pinterest" content="nopin" />
		<?php endif; ?>
		<?php wp_site_icon(); ?>

		<?php echo WPRM_Template_Manager::get_template_styles( false, 'print-collection' ); ?>
		<link rel="stylesheet" type="text/css" href="<?php echo WPRM_URL . 'dist/public-' . WPRM_Settings::get( 'recipe_template_mode' ) . '.css?ver=' . WPRM_VERSION; ?>"/>
		<link rel="stylesheet" type="text/css" href="<?php echo WPRMP_URL . 'dist/public-' . strtolower( WPRMP_BUNDLE ) . '.css?ver=' . WPRM_VERSION; ?>"/>
		<?php if ( ! WPRM_Settings::get( 'print_show_recipe_image' ) ) : ?>
			<style>.wprm-recipe-image { display: none !important }</style>
		<?php endif; ?>
		<?php if ( ! WPRM_Settings::get( 'print_show_instruction_images' ) ) : ?>
			<style>.wprm-recipe-instruction-image { display: none !important }</style>
		<?php endif; ?>
		<?php echo WPRM_Assets::custom_css( 'print' ); ?>
		<?php if ( WPRM_Settings::get( 'print_css' ) ) : ?>
			<style><?php echo WPRM_Settings::get( 'print_css' ); ?></style>
		<?php endif; ?>
	</head>
	<body class="wprm-print wprm-print-collection<?php echo is_rtl() ? ' rtl' : ''; ?>" data-recipe=" <?php echo esc_attr( 0 ); // TODO ?>">
		<?php
		$recipes = isset( $_POST['recipes'] ) && $_POST['recipes'] ? array_map( 'intval', explode( ';', $_POST['recipes'] ) ) : array();

		foreach( $recipes as $recipe_id ) {
			$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );

			if ( $recipe ) {
				echo WPRM_Template_Manager::get_template( $recipe, 'print-collection' );
				echo '<div class="wprm-print-collection-separator" style="margin-bottom: 40px"></div>';
			}
		}
		?>
		<script>
		setTimeout(function() {
			window.print();
		}, 250);
		</script>
	</body>
</html>