import React, { Component, Fragment } from 'react';

import { __wprm } from 'Shared/Translations';

import Ingredient from './Ingredient';

export default class Groups extends Component {
    render() {
        const groupNames = Object.keys(this.props.groups);

        return (
            <Fragment>
                {
                    0 < groupNames.length
                    ?
                    <Fragment>
                        {
                            groupNames.map((name, index) => {
                                const ingredients = this.props.groups[ name ];

                                // Order ingredients alphabetically.
                                const orderedIngredients = {};
                                Object.keys(ingredients).sort( (a, b) => a.toLowerCase().localeCompare( b.toLowerCase() ) ).forEach(function(key) {
                                    orderedIngredients[key] = ingredients[key];
                                });

                                const orderedIngredientNames = Object.keys(orderedIngredients);

                                return (
                                    <div className="wprmprc-shopping-list-list-ingredient-group-container" key={ index }>
                                        {
                                            '' !== name
                                            && <div className="wprmprc-shopping-list-list-ingredient-group">{ name }</div>
                                        }
                                        {
                                            orderedIngredientNames.map( ( ingredientName, ingredientIndex ) => (
                                                <Ingredient
                                                    name={ingredientName}
                                                    ingredient={orderedIngredients[ ingredientName ]}
                                                    key={ingredientIndex}
                                                />
                                            ))
                                        }
                                    </div>
                                )
                            })
                        }
                    </Fragment>
                    :
                    <div className="wprmprc-shopping-list-list-ingredients-none">{ __wprm( 'Your shopping list is empty.' ) }</div>
                }
            </Fragment>
        );
    }
}