import React, { Component, Fragment } from 'react';

import { __wprm } from 'Shared/Translations';

import Item from './Item';

export default class Collection extends Component {
    onChangeItem( columnId, groupId, index, field, value ) {
        let items = JSON.parse(JSON.stringify(this.props.collection.items));

        if ( items[`${columnId}-${groupId}`] && items[`${columnId}-${groupId}`][index] && 0 <= value ) {
            items[`${columnId}-${groupId}`][index][field] = value;

            this.props.onChangeCollection( this.props.type, this.props.collection.id, { items } );
        }
    }

    render() {
        const { collection } = this.props;

        if ( 0 === collection.nbrItems ) {
            return null;
        }

        return (
            <div className="wprmprc-shopping-list-collection">
                <div className="wprmprc-shopping-list-collection-header">
                    <div className="wprmprc-shopping-list-collection-name">
                        { __wprm( 'Collection' ) }
                    </div>
                </div>
                {
                    collection.columns.map( (column, columnIndex) => 
                        <div className="wprmprc-shopping-list-column" key={columnIndex}>
                            {
                                '' !== column.name
                                &&
                                <div className="wprmprc-shopping-list-column-header">
                                    <div className="wprmprc-shopping-list-column-name">
                                        {column.name}
                                    </div>
                                </div>
                            }
                            <div className="wprmprc-shopping-list-column-items">
                                {
                                    collection.groups.map( (group, groupIndex) => {
                                        const groupItems = collection.items[`${column.id}-${group.id}`] ? collection.items[`${column.id}-${group.id}`] : [];

                                        return (
                                            <Fragment key={groupIndex}>
                                                {
                                                    groupItems.map( (item, itemIndex) =>
                                                        <Item
                                                            item={item}
                                                            onChangeAmount={(amount) => this.onChangeItem( column.id, group.id, itemIndex, 'amount', amount )}
                                                            onChangeServings={(servings) => this.onChangeItem( column.id, group.id, itemIndex, 'servings', servings )}
                                                            key={item.id}
                                                        />
                                                    )
                                                }
                                            </Fragment>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}