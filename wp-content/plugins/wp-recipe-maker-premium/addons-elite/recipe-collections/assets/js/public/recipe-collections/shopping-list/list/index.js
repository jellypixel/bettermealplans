import React, { Component } from 'react';

import { __wprm } from 'Shared/Translations';

import Groups from './Groups';
import Api from '../../general/Api';
import Loader from '../../general/Loader';

import  { parseQuantity } from '../../../../../../../../assets/js/shared/quantities';

export default class List extends Component {

    constructor(props) {
        super(props);

        this.state = {
            recipes: {},
            ingredients: {},
            loading: this.loadRecipes(),
        }
    }

    loadRecipes() {
        let recipesMissing = [];
        let customIngredients = [];

        if ( this.props.collection.items ) {
            const items = Object.values(this.props.collection.items).reduce( (allItems, groupItems) => allItems.concat(groupItems), [] );

            for ( let item of items ) {
                if ( 'recipe' === item.type && item.recipeId && ! recipesMissing.includes( item.recipeId ) ) {
                    recipesMissing.push( item.recipeId );
                } else if ( 'ingredient' === item.type ) {
                    for ( let ingredient of item.ingredients ) {
                        if ( ingredient.name && ! customIngredients.includes( ingredient.name ) ) {
                            customIngredients.push( ingredient.name );
                        }
                    }
                } else if ( 'nutrition-ingredient' === item.type ) {
                    customIngredients.push( item.name );
                }
            }
        }

        if ( 0 < recipesMissing.length || 0 < customIngredients.length ) {
            Api.getIngredients(recipesMissing, customIngredients).then((data) => {
                if ( data ) {
                    this.setState({
                        recipes: data.recipes,
                        ingredients: data.ingredients,
                        loading: false,
                    });
                }
            });

            return true;
        }

        return false;
    }

    addIngredient( groups, ingredient, originalServings, servings ) {
        // Make sure group exists
        if ( ! groups.hasOwnProperty( ingredient.group ) ) {
            groups[ ingredient.group ] = {};
        }

        // Make sure ingredient exists.
        if ( ! groups[ ingredient.group ].hasOwnProperty( ingredient.name ) ) {
            groups[ ingredient.group ][ ingredient.name ] = {
                link: ingredient.hasOwnProperty( 'link' ) ? ingredient.link : false,
                variations: [],
            };
        }

        // Check if this unit already exists.
        const existingIndex = groups[ ingredient.group ][ ingredient.name ].variations.findIndex( (existingIngredient) => existingIngredient.unit === ingredient.unit.trim() );

        // Recalculate amount based on servings.
        let amount = parseQuantity( ingredient.amount );

        if ( amount && servings !== originalServings ) {
            amount = ( amount / originalServings ) * servings;
        }

        // Add to ingredients array.
        if ( -1 < existingIndex && false !== amount && false !== groups[ ingredient.group ][ ingredient.name ].variations[ existingIndex ].amount ) {
            groups[ ingredient.group ][ ingredient.name ].variations[ existingIndex ].amount += amount;
        } else {
            groups[ ingredient.group ][ ingredient.name ].variations.push({
                amount,
                unit: ingredient.unit.trim(),
            });
        }

        return groups;
    }

    getGroups() {
        let groups = {};
        const items = Object.values(this.props.collection.items).reduce( (allItems, groupItems) => allItems.concat(groupItems), [] );

        for ( let item of items ) {
            if ( item.servings > 0 ) {
                if ( 'recipe' === item.type && this.state.recipes.hasOwnProperty( item.recipeId ) ) {
                    const recipe = this.state.recipes[ item.recipeId ];
                    for ( let ingredient of recipe.ingredients ) {
                        groups = this.addIngredient( groups, ingredient, recipe.servings, item.servings );
                    }
                } else if ( 'ingredient' === item.type ) {
                    for ( let ingredient of item.ingredients ) {
                        if ( ingredient.name && this.state.ingredients.hasOwnProperty( ingredient.name ) ) {
                            const enhancedIngredient = {
                                ...ingredient,
                                ...this.state.ingredients[ ingredient.name ],
                            };

                            groups = this.addIngredient( groups, enhancedIngredient, 1, item.servings );
                        }
                    }
                } else if ( 'nutrition-ingredient' === item.type ) {
                    if ( item.name && this.state.ingredients.hasOwnProperty( item.name ) ) {
                        const enhancedIngredient = {
                            ...item,
                            ...this.state.ingredients[ item.name ],
                        };

                        groups = this.addIngredient( groups, enhancedIngredient, 1, item.servings );
                    }
                }
            }
        }

        const orderedGroups = {};
        Object.keys(groups).sort( (a, b) => a.toLowerCase().localeCompare( b.toLowerCase() ) ).forEach(function(key) {
            orderedGroups[key] = groups[key];
        });

        return orderedGroups;
    }

    render() {
        return (
            <div className="wprmprc-shopping-list-list">
                <div className="wprmprc-shopping-list-list-header">
                    <div className="wprmprc-shopping-list-list-name">
                        { __wprm( 'Shopping List' ) }
                    </div>
                </div>
                <div className="wprmprc-shopping-list-list-ingredients">
                    {
                        this.state.loading
                        ?
                        <Loader />
                        :
                        <Groups
                            groups={this.getGroups()}
                        />
                    }
                </div>
            </div>
        );
    }
}