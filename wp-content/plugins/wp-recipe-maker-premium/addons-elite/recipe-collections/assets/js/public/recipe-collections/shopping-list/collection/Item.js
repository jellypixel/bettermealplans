import React, { Component } from 'react';

import { __wprm } from 'Shared/Translations';

import Icon from '../../general/Icon';

export default class Item extends Component {
    render() {
        const { item } = this.props;

        const changeServings = (e, plus) => {
            e.preventDefault();

            const servings = plus ? item.servings + 1 : item.servings - 1;
            this.props.onChangeServings(servings);

            return false;
        }

        // Item classes.
        let itemClass = 'wprmprc-shopping-list-item';
        itemClass += ` wprmprc-shopping-list-item-${item.type}`;
        
        if ( item.servings <= 0 ) {
            itemClass += ' wprmprc-shopping-list-item-unused';
        }

        if ( item.hasOwnProperty( 'color' ) ) {
            itemClass += ` wprmprc-shopping-list-item-color-${item.color}`;
        }

        // Item text.
        let itemText = item.name;
        if ( ! itemText && 'ingredient' === item.type ) {
            itemText = '';
            item.ingredients.map((ingredient, index) => {
                if ( 0 < index ) { itemText += '\n'; }
                if ( ingredient.amount ) { itemText += `${ ingredient.amount } `; }
                if ( ingredient.unit ) { itemText += `${ ingredient.unit } `; }
                if ( ingredient.name ) { itemText += `${ ingredient.name }`; }

                itemText = itemText.trim();
            });
        } else if ( 'nutrition-ingredient' === item.type ) {
            const prefix = `${item.amount} ${item.unit}`.trim();
            itemText = `${prefix} ${itemText}`; 
        }

        return (
            <div className={ itemClass }>
                {
                    'note' !== item.type
                    &&
                    <div className="wprmprc-shopping-list-item-servings-adjust">
                        <div className="wprmprc-shopping-list-item-servings-adjust-minus" onClick={(e) => changeServings(e, false)}><Icon type="minus" /></div>
                        <div className="wprmprc-shopping-list-item-servings-adjust-servings-container">
                            <div
                                className="wprmprc-shopping-list-item-servings-adjust-servings"
                                onClick={(e) => {
                                    e.preventDefault();
                                    let servings = prompt( __wprm( 'Set the number of servings' ), item.servings ).trim();
                                    if( servings ) {
                                        this.props.onChangeServings( parseFloat( servings ) );
                                    }
                                }}
                            >{item.servings}</div>
                            {
                                item.servingsUnit
                                && <div className="wprmprc-shopping-list-item-servings-adjust-servings-unit">{item.servingsUnit}</div>
                            }
                        </div>
                        <div className="wprmprc-shopping-list-item-servings-adjust-plus" onClick={(e) => changeServings(e, true)}><Icon type="plus" /></div>
                    </div>
                }
                <div className="wprmprc-shopping-list-item-details">
                    <div className="wprmprc-shopping-list-item-name">{ itemText }</div>
                    {
                        item.image
                        &&
                        <div className="wprmprc-shopping-list-item-image">
                            <img className="wprmprc-shopping-list-item-image" width="50" src={item.image} />
                        </div>
                    }
                </div>
            </div>
        );
    }
}