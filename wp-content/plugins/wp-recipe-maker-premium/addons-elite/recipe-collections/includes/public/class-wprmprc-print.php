<?php
/**
 * Handle the Recipe Collections printing.
 *
 * @link       http://bootstrapped.ventures
 * @since      6.1.0
 *
 * @package    WP_Recipe_Maker_Premium/addons-elite/recipe-collections
 * @subpackage WP_Recipe_Maker_Premium/addons-elite/recipe-collections/includes/public
 */

/**
 * Handle the Recipe Collections printing.
 *
 * @since      6.1.0
 * @package    WP_Recipe_Maker_Premium/addons-elite/recipe-collections
 * @subpackage WP_Recipe_Maker_Premium/addons-elite/recipe-collections/includes/public
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMPRC_Print {

	/**
	 * Register actions and filters.
	 *
	 * @since    6.1.0
	 */
	public static function init() {
		add_filter( 'wprm_print_output', array( __CLASS__, 'output' ), 10, 2 );
	}

	/**
	 * Get output for the collections print page.
	 *
	 * @since    6.1.0
	 * @param	array $output 	Current output for the print page.
	 * @param	array $args	 	Arguments for the print page.
	 */
	public static function output( $output, $args ) {
		// TODO for /collection/ or /shopping-list/.
		return $output;
	}
}

WPRMPRC_Print::init();
