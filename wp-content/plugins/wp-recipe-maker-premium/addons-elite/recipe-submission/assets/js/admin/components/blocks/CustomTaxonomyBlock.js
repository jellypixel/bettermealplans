import React from 'react';
import Select from 'react-select';

import InputBlock from './InputBlock';

const CustomTaxonomyBlock = (props) => {
    // Message if no custom taxonomies set.
    if ( ! wprm_admin_modal.categories || ! Object.keys( wprm_admin_modal.categories ).length ) {
        return (
            <div className={`wprmprs-layout-block-input wprmprs-layout-block-${props.block.type}`}>
                <label className="wprmprs-form-label">Custom Taxonomy</label>
                <p>You need to create a custom taxonomy through the WP Recipe Maker > Manage page first.</p>
            </div>
        )
    }

    // Dropdown if no custom field selected yet.
    if ( ! props.block.field || ! wprm_admin_modal.categories.hasOwnProperty( props.block.field ) ) {
        return (
            <div className={`wprmprs-layout-block-input wprmprs-layout-block-${props.block.type}`}>
                <label className="wprmprs-form-label">Custom Taxonomy</label>
                <Select
                    className="wprmprs-layout-block-select"
                    placeholder="Select the custom taxonomy to display..."
                    value={false}
                    onChange={(option) => {
                        props.onEdit('field', option.value);
                        props.onEdit('label', option.label);
                    }}
                    options={
                        Object.keys( wprm_admin_modal.categories ).map((taxonomy) => {
                            return {
                                value: taxonomy,
                                label: wprm_admin_modal.categories[ taxonomy ].label,
                            }
                        })
                    }
                    clearable={false}
                />
            </div>
        );
    }

    return (
        <InputBlock {...props } />
    );
}

export default CustomTaxonomyBlock;