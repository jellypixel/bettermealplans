import 'ezdz';
import 'ezdz/src/jquery.ezdz.css';

import '../../css/public/form.scss';
import '../../css/public/blocks.scss';

jQuery('.wprmprs-layout-block-recipe_image').each(function() {
    var input = jQuery(this).find('input');
    var placeholder = input.data('placeholder');

    input.ezdz({
        text: placeholder,
        validators: {
            maxNumber: 1,
            maxSize: wprmp_public.recipe_submission.max_file_size,
        },
        reject: function(file, errors) {
            if (errors.maxSize) {
                alert( wprmp_public.recipe_submission.text.image_size + ' (> ' + formatBytes( wprmp_public.recipe_submission.max_file_size ) + ')' );
            }
        }
    });
});

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}