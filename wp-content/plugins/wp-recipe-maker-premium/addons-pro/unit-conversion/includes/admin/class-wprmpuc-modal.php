<?php
/**
 * Handle the Recipe Modal.
 *
 * @link       http://bootstrapped.ventures
 * @since      5.5.0
 *
 * @package    WP_Recipe_Maker_Premium/addons-pro/unit-conversion
 * @subpackage WP_Recipe_Maker_Premium/addons-pro/unit-conversion/includes/admin
 */

/**
 * Handle the Recipe Modal.
 *
 * @since      5.5.0
 * @package    WP_Recipe_Maker_Premium/addons-pro/unit-conversion
 * @subpackage WP_Recipe_Maker_Premium/addons-pro/unit-conversion/includes/admin
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMPUC_Modal {

	/**
	 * Register actions and filters.
	 *
	 * @since    5.5.0
	 */
	public static function init() {
		add_filter( 'wprm_admin_modal_localize', array( __CLASS__, 'localize' ) );
	}

	/**
	 * Localize data for the recipe modal.
	 *
	 * @since   5.5.0
	 * @param	array $data Localized data.
	 */
	public static function localize( $data ) {
		if ( WPRM_Settings::get( 'unit_conversion_enabled' ) ) {
			$data['unit_conversion'] = array(
				'systems' => array(
					1 => WPRM_Settings::get( 'unit_conversion_system_1' ),
					2 => WPRM_Settings::get( 'unit_conversion_system_2' ),
				),
				'units' => array(
					'data' => WPRM_Settings::get( 'unit_conversion_units' ),
					'weight' => WPRM_Settings::get( 'unit_conversion_system_2_weight_units' ),
					'volume' => WPRM_Settings::get( 'unit_conversion_system_2_volume_units' ),
				),
			);
		}

		return $data;
	}
}
WPRMPUC_Modal::init();
