import 'tooltipster';
import 'tooltipster/dist/css/tooltipster.bundle.css';

import  { parseQuantity, formatQuantity } from '../shared/quantities';

export function analyze_quantities(recipe) {
	var servings = parseInt(recipe.find('.wprm-recipe-servings').data('original_servings'));
	if(servings > 0) {
		recipe.find('.wprm-recipe-ingredient-amount, .wprm-dynamic-quantity').each(function() {
			// Only do this once.
			if(0 === jQuery(this).find('.wprm-adjustable').length) {
				// Surround all the number blocks
				var quantity = jQuery(this),
					quantity_text = quantity.text();

				// Special case: .5
				if ( /^\.\d+\s*$/.test(quantity_text) ) {
					quantity.html('<span class="wprm-adjustable">' + quantity_text + '</span>');
				} else {
					var fractions = '\u00BC\u00BD\u00BE\u2150\u2151\u2152\u2153\u2154\u2155\u2156\u2157\u2158\u2159\u215A\u215B\u215C\u215D\u215E';
					var number_regex = '[\\d'+fractions+']([\\d'+fractions+'.,\\/\\s]*[\\d'+fractions+'])?';
					var substitution = '<span class="wprm-adjustable">$&</span>';

					quantity_text = quantity_text.replace(new RegExp(number_regex, 'g'), substitution);
					quantity.html(quantity_text);
				}
			}
		});

		recipe.find('.wpurp-adjustable-quantity').each(function() {
			jQuery(this).addClass('wprm-adjustable');
		});

		recipe.find('.wprm-adjustable').each(function() {
			// Only do this once.
			if('undefined' == typeof jQuery(this).data('original_quantity')) {
				var quantity = parseQuantity(jQuery(this).text());
				quantity /= servings;

				jQuery(this)
					.data('original_quantity', jQuery(this).text())
					.data('unit_quantity', quantity);
			}
		});
	}
};

export function update_serving_size(recipe) {
	var servings_element = recipe.find('.wprm-recipe-servings'),
		servings = parseInt(servings_element.data('servings')),
		original_servings = servings_element.data('original_servings');

	var adjustable_quantities = recipe.find('.wprm-adjustable');
	
	if(adjustable_quantities.length == 0) {
		analyze_quantities(recipe);
		adjustable_quantities = recipe.find('.wprm-adjustable');
	}

	adjustable_quantities.each(function() {
		var quantity_element = jQuery(this);

		if(servings == original_servings) {
			quantity_element.text(quantity_element.data('original_quantity'));
		} else {
			var quantity = parseFloat(quantity_element.data('unit_quantity')) * servings;

			if(!isNaN(quantity)) {
				quantity_element.text(formatQuantity(quantity, wprmp_public.settings.adjustable_servings_round_to_decimals));
			}
		}
	});
};

export function set_print_servings(servings, elem = false) {
	if(servings > 0) {
		var recipe = elem ? jQuery(elem) : jQuery('body');

		recipe.find('.wprm-recipe-servings').each(function() {
			jQuery(this).text(servings);
			jQuery(this).data('servings', servings);
		});

		update_serving_size(recipe);
	}
};

function init_tooltip_slider(servings_element) {
	// Get the recipe ID.
	var recipe_id = servings_element.data('recipe'),
		aria_label = servings_element.attr('aria-label');

	servings_element.attr('aria-label', '');

	// Backwards compatibility.
	if (!recipe_id) {
		recipe_id = servings_element.parents('.wprm-recipe-container').data('recipe-id');
		servings_element.data('recipe', recipe_id);
	}

	// Make the servings a link
	servings_element.wrap('<a href="#" class="wprm-recipe-servings-link" aria-label="' + aria_label + '"></a>');

	// Add tooltip
	servings_element.tooltipster({
		content: '<input type="range" min="1" max="1" value="1" class="wprm-recipe-servings-slider" aria-label="' + aria_label + '">',
		contentAsHTML: true,
		functionBefore: function() {
			var instances = jQuery.tooltipster.instances();
			jQuery.each(instances, function(i, instance){
				instance.close();
			});
		},
		functionReady: function(instance, helper) {
			var max = 20,
				value = parseInt(jQuery(helper.origin).text());

			if( max < 2*value ) {
				max = 2*value;
			}

			// Set reference to correct servings changer.
			var uid = Date.now();
			jQuery(helper.origin).attr('id', 'wprm-tooltip-' + uid);

			jQuery(helper.tooltip)
				.find('.wprm-recipe-servings-slider').attr('max', max)
				.data('origin', 'wprm-tooltip-' + uid)
				.val(value);
		},
		interactive: true,
		delay: 0,
		trigger: 'custom',
		triggerOpen: {
			mouseenter: true,
			touchstart: true
		},
		triggerClose: {
			click: true,
			tap: true
		},
	});

	jQuery(document).on('input change', '.wprm-recipe-servings-slider', function() {
		var servings = jQuery(this).val(),
			origin = jQuery(this).data('origin'),
			servings_element = jQuery('#' + origin),
			recipe_id = servings_element.data('recipe'),
			recipe = jQuery('#wprm-recipe-container-' + recipe_id);

		if ( ! recipe.length ) {
			recipe = servings_element.parents('.wprm-recipe');
		}

		// Update this servings element.
		servings_element.text(servings);
		servings_element.data('servings', servings);

		// Make sure all serving elements for this recipe are changed
		jQuery('.wprm-recipe-servings-' + recipe_id).each(function() {
			jQuery(this).text(servings);
			jQuery(this).val(servings);
			jQuery(this).data('servings', servings);

			if ( jQuery(this).hasClass('wprm-recipe-adjustable-servings-input') ) {
				jQuery(this).change();
			}
		});
		update_serving_size(recipe);
	});

	jQuery(document).on('click', '.wprm-recipe-servings-link', function(e) {
		e.preventDefault();
		e.stopPropagation();
	});
};

function init_text_field(servings_element) {
	var servings = servings_element.data('servings'),
		recipe_id = servings_element.data('recipe'),
		aria_label = servings_element.attr('aria-label');

	// Backwards compatibility.
	if (!recipe_id) {
		recipe_id = servings_element.parents('.wprm-recipe-container').data('recipe-id');
	}

	var input = '<input type="number" class="wprm-recipe-servings wprm-recipe-servings-' + recipe_id + '" min="1" value="' + servings + '" data-servings="' + servings + '" data-original_servings="' + servings + '" data-recipe="' + recipe_id + '" aria-label="' + aria_label + '" />';

	servings_element.replaceWith(input);

	jQuery(document).on('input change', 'input.wprm-recipe-servings:not(.wprm-recipe-adjustable-servings-input)', function() {
		var servings_element = jQuery(this),
			servings = servings_element.val(),
			recipe_id = servings_element.data('recipe'),
			recipe = jQuery('#wprm-recipe-container-' + recipe_id);

		if ( ! recipe.length ) {
			recipe = servings_element.parents('.wprm-recipe');
		}

		// Make sure all serving elements for this recipe are changed.
		jQuery('.wprm-recipe-servings-' + recipe_id).each(function() {
			jQuery(this).text(servings);
			jQuery(this).val(servings);
			jQuery(this).data('servings', servings);

			if ( jQuery(this).hasClass('wprm-recipe-adjustable-servings-input') ) {
				jQuery(this).change();
			}
		});
		update_serving_size(recipe);
	});
};

function clicked_multiplier(elem) {
	if ( ! elem.hasClass( 'wprm-toggle-active' ) ) {
		const multiplier = elem.data('multiplier'),
			recipe_id = elem.data('recipe'),
			servings = elem.data('servings');
		
		let recipe = jQuery('#wprm-recipe-container-' + recipe_id);

		if ( ! recipe.length ) {
			recipe = elem.parents('.wprm-recipe');
		}

		let newServings = false;

		if ( '?' === multiplier ) {
			// TODO later?
		} else {
			newServings = parseInt( servings ) * parseInt( multiplier );
		}

		if ( newServings ) {
			// Make sure all serving elements for this recipe are changed.
			jQuery('.wprm-recipe-servings-' + recipe_id).each(function() {
				jQuery(this).text(newServings);
				jQuery(this).val(newServings);
				jQuery(this).data('servings', newServings);

				if ( jQuery(this).hasClass('wprm-recipe-adjustable-servings-input') ) {
					jQuery(this).change();
				}
			});
			update_serving_size(recipe);
		}
	}
};

function check_multipliers(elem) {
	const servings = elem.val(),
		originalServings = elem.data('original_servings'),
		container = elem.parent('.wprm-recipe-adjustable-servings-container');

	const multiplier = parseFloat( servings ) / parseFloat( originalServings );
	let matchFound = false;

	container.find('.wprm-recipe-adjustable-servings').each(function() {
		const button = jQuery(this);

		// Switch classes.
		button.removeClass('wprm-toggle-active');
		if ( multiplier === parseFloat( button.data('multiplier') ) ) {
			matchFound = true;
			button.addClass('wprm-toggle-active');
		} else if ( '?' === button.data('multiplier') && ! matchFound ) {
			button.addClass('wprm-toggle-active');
		}
	});
}

window.wprm_init_adjustable_servings = () => {
	jQuery('.wprm-recipe-servings').each(function() {
		var servings_element = jQuery(this),
			servings = parseInt(servings_element.text());

		// Only if has servings and no original servings set yet.
		if( servings > 0 && ! servings_element.data('original_servings') ) {
			// Save original servings
			servings_element.data('servings', servings);
			servings_element.data('original_servings', servings);

			if( !jQuery('body').hasClass('wprm-print')) {
				if ( 'modern' === wprmp_public.settings.recipe_template_mode ) {
					if ( servings_element.hasClass('wprm-recipe-servings-adjustable-tooltip') ) {
						init_tooltip_slider(servings_element);
					} else if ( servings_element.hasClass('wprm-recipe-servings-adjustable-text') ) {
						init_text_field(servings_element);
					}
				} else if ( wprmp_public.settings.features_adjustable_servings) {
					if ( wprmp_public.settings.servings_changer_display == 'text_field' ) {
						init_text_field(servings_element);
					} else { // Default = Tooltip Slider
						init_tooltip_slider(servings_element);
					}
				}
			}
		}
	});
}

jQuery(document).ready(function($) {
	wprm_init_adjustable_servings();

	jQuery(document).on('click', '.wprm-recipe-adjustable-servings', function(e) {
		e.preventDefault();
		clicked_multiplier( jQuery(this) );
	});
	jQuery(document).on('change', '.wprm-recipe-adjustable-servings-input', function() {
		check_multipliers( jQuery(this) );
	});
});
