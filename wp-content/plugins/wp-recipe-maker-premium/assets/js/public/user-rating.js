import animateScrollTo from 'animated-scroll-to';
import '../../css/public/user-rating.scss';

if (wprmp_public.settings.features_user_ratings) {
	jQuery(document).ready(function($) {
		var color = wprmp_public.settings.template_color_icon;

		jQuery(document).on('mouseenter focus', '.wprm-user-rating-allowed .wprm-rating-star', function() {		
			if ( 'modern' === wprmp_public.settings.recipe_template_mode && jQuery(this).data('color') ) {
				color = jQuery(this).data('color');
			}
			jQuery(this).prevAll().andSelf().each(function() {
				jQuery(this)
					.addClass('wprm-rating-star-selecting-filled')
					.find('polygon')
					.css('fill', color);
			});
			jQuery(this).nextAll().each(function() {
				jQuery(this)
					.addClass('wprm-rating-star-selecting-empty')
					.find('polygon')
					.css('fill', 'none');
			});
		});
		jQuery(document).on('mouseleave blur', '.wprm-user-rating-allowed .wprm-rating-star', function() {
			jQuery(this).siblings().andSelf().each(function() {
				jQuery(this)
					.removeClass('wprm-rating-star-selecting-filled wprm-rating-star-selecting-empty')
					.find('polygon')
					.css('fill', '');
			});
		});

		jQuery(document).on('click keypress', '.wprm-user-rating-allowed .wprm-rating-star', function(e) {
			const key = e.which || e.keyCode || 0;
			
			// Rate recipe on click, ENTER or SPACE.
			if ( 'click' === e.type || ( 'keypress' === e.type && 13 === key || 32 === key ) ) {
				e.preventDefault();

				var star = jQuery(this),
				rating_container = star.parents('.wprm-recipe-rating'),
				rating = star.data('rating'),
				recipe_id = rating_container.data('recipe'),
				allow_user_rating = true;

				// Backwards compatibility.
				if (!recipe_id) {
					recipe_id = star.parents('.wprm-recipe-container').data('recipe-id');
				}

				// Check if we allow a user rating.
				if ( wprmp_public.settings.features_comment_ratings && 'never' !== wprmp_public.settings.user_ratings_force_comment ) {
					let check_stars = {
						'1_star': 1,
						'2_star': 2,
						'3_star': 3,
						'4_star': 4,
						'always': 5,
					}

					if ( check_stars.hasOwnProperty( wprmp_public.settings.user_ratings_force_comment ) ) {
						if ( rating <= check_stars[ wprmp_public.settings.user_ratings_force_comment ] ) {
							allow_user_rating = false;
						}
					}
				}

				if ( allow_user_rating ) {
					// Update current view.
					if(rating_container.length > 0) {
						var count = rating_container.data('count'),
							total = rating_container.data('total'),
							user = rating_container.data('user');

						if(user > 0) {
							total -= user;
						} else {
							count++;
						}

						total += rating;

						var average = Math.ceil(total/count * 100) / 100;

						// Upate details.
						rating_container.find('.wprm-recipe-rating-average').text(average);
						rating_container.find('.wprm-recipe-rating-count').text(count);

						// Update stars.
						var stars = Math.ceil(average);

						for(var i = 1; i <= 5; i++) {
							var star = rating_container.find('.wprm-rating-star-' + i);
							star.removeClass('wprm-rating-star-full').removeClass('wprm-rating-star-empty');

							if(i <= stars) {
								star.addClass('wprm-rating-star-full');
							} else {
								star.addClass('wprm-rating-star-empty');
							}
						}
					}

					// Update rating via AJAX.
					var data = {
						action: 'wprm_user_rate_recipe',
						security: wprm_public.nonce,
						recipe_id: recipe_id,
						rating: rating
					};
			
					jQuery.post(wprm_public.ajax_url, data);
				} else {
					// User rating not allowed, redirect to comment rating.
					jQuery('.comment-form-wprm-rating').find('.wprm-rating-star').eq( rating - 1 ).click();

					let scrollToElement = document.querySelector( '.comment-form-wprm-rating' );

					if ( wprmp_public.settings.user_ratings_force_comment_scroll_to && ! ! document.querySelector( wprmp_public.settings.user_ratings_force_comment_scroll_to ) ) {
						scrollToElement = document.querySelector( wprmp_public.settings.user_ratings_force_comment_scroll_to );
					}

					if ( scrollToElement ) {
						animateScrollTo( scrollToElement, {
							verticalOffset: -100,
							speed: 250,
						} ).then(() => {
							jQuery('#comment').focus();
						});
					}
				}
			}
		});
	});
}
