import { set_print_servings } from '../public/servings-changer';
import { set_print_system } from '../../../addons-pro/unit-conversion/assets/js/public/unit-conversion';

window.WPRMPremiumPrint = {
	init() {
        this.servingsInput = document.querySelector( '#wprm-print-servings' );
        this.initServingsInput();
        
        // Check if initial servings passed along.
        if ( window.hasOwnProperty( 'wprmp_print_recipes' ) ) {
            this.setInitialServings( window.wprmp_print_recipes );
        }

        // On args change.
        document.addEventListener( 'wprmPrintArgs', () => {
            this.onArgsChange();
        });
    },
    onArgsChange(  ) {
        const args = window.WPRMPrint.args;

        if ( args.hasOwnProperty( 'servings' ) ) {
            this.setServings( args.servings );
        }
        if ( args.hasOwnProperty( 'system' ) ) {
            this.setSystem( args.system );
        }
    },
    servingsInput: false,
    initServingsInput() {
        if ( this.servingsInput ) {
            // On input change.
            this.servingsInput.addEventListener( 'change', () => {
                this.setServings( this.servingsInput.value );
            });

            // On click servings change.
            const servingsChangers = [ ...document.querySelectorAll( '.wprm-print-servings-change' )];

            for ( let servingsChanger of servingsChangers ) {
                // Event listener.
                servingsChanger.addEventListener( 'click', () => {
                    this.onClickServingsChange( servingsChanger );
                });
            }

            // Find servings unit in recipe.
            const recipeServingsUnitElem = document.querySelector( '.wprm-recipe-servings-unit' );

            if ( recipeServingsUnitElem ) {
                const recipeServingsUnit = recipeServingsUnitElem.innerText.trim();
                
                if ( recipeServingsUnit ) {
                    document.querySelector( '#wprm-print-servings-unit' ).innerText = recipeServingsUnit;
                }
            }
        }
    },
    onClickServingsChange( button ) {
        if ( this.servingsInput ) {
            let servingsValue = parseInt( this.servingsInput.value );

            if ( button.classList.contains( 'wprm-print-servings-increment' ) ) {
                servingsValue++;
            } else {
                servingsValue--;
            }
            this.setServings( servingsValue );
        }
    },
    setServings( servings ) {
        // Make sure it's valid.
        servings = parseInt( servings );
        servings = isNaN( servings ) || servings <= 0 ? false : servings;

        if ( false !== servings ) {
            set_print_servings( servings );
            if ( this.servingsInput ) {
                this.servingsInput.value = servings;
            }
        }
    },
    setSystem( system ) {
        // Make sure it's valid.
        system = parseInt( system );
        system = isNaN( system ) || system < 0 ? false : system;

        if ( false !== system ) {
            set_print_system( system );
        }
    },
    setInitialServings( recipes ) {
        // Need to do after timeout to make sure the servings have been initialized.
        setTimeout( () => {
            for ( let i = 0; i < recipes.length; i++ ) {
                let recipe = recipes[i];
    
                if ( recipe.original_servings && recipe.servings !== recipe.original_servings ) {
                    set_print_servings( recipe.servings, '#wprm-print-recipe-' + i );
                }
            }
        }, 100 );
    },
};
document.addEventListener( 'wprmPrintInit', () => {
    window.WPRMPremiumPrint.init();
} );