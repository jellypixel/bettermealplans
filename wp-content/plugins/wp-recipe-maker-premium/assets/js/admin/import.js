function import_json_recipes( page, total ) {
	var data = {
		action: 'wprm_import_json',
		security: wprm_admin.nonce,
		page: page,
	};

	jQuery.post(wprm_admin.ajax_url, data, function(out) {
		if (out.success) {
			page++;
			update_progress_bar( page, total );

			if ( page < total ) {
				import_json_recipes( page, total );
			} else {
				jQuery('#wprm-tools-finished').show();
			}
		} else {
			// alert( 'Something went wrong. Please contact support.' );
		}
	}, 'json');
}

function update_progress_bar( page, total ) {
	var percentage = page / total * 100;
	jQuery('#wprm-tools-progress-bar').css('width', percentage + '%');
};

jQuery(document).ready(function($) {
	// Import Process
	if( typeof window.wprm_import_json === 'object' && wprm_import_json.hasOwnProperty( 'pages' ) ) {
		import_json_recipes( 0, parseInt( wprm_import_json.pages ) );
	}
});
