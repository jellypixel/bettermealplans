<?php
/**
 * Handle the custom taxonomies API.
 *
 * @link       http://bootstrapped.ventures
 * @since      5.0.0
 *
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public/api
 */

/**
 * Handle the custom taxonomies API.
 *
 * @since      5.0.0
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public/api
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMP_Api_Custom_Taxonomies {

	/**
	 * Register actions and filters.
	 *
	 * @since    5.0.0
	 */
	public static function init() {
		add_action( 'rest_api_init', array( __CLASS__, 'api_register_data' ) );
	}

	/**
	 * Register data for the REST API.
	 *
	 * @since    5.0.0
	 */
	public static function api_register_data() {
		if ( function_exists( 'register_rest_field' ) ) { // Prevent issue with Jetpack.
			register_rest_route( 'wp-recipe-maker/v1', '/manage/taxonomies', array(
				'callback' => array( __CLASS__, 'api_manage_taxonomies' ),
				'methods' => 'POST',
				'permission_callback' => array( __CLASS__, 'api_required_permissions' ),
			) );
			register_rest_route( 'wp-recipe-maker/v1', '/custom-taxonomies', array(
				'callback' => array( __CLASS__, 'api_create_taxonomy' ),
				'methods' => 'POST',
				'permission_callback' => array( __CLASS__, 'api_required_permissions' ),
			) );
			register_rest_route( 'wp-recipe-maker/v1', '/custom-taxonomies', array(
				'callback' => array( __CLASS__, 'api_update_taxonomy' ),
				'methods' => 'PUT',
				'permission_callback' => array( __CLASS__, 'api_required_permissions' ),
			) );
			register_rest_route( 'wp-recipe-maker/v1', '/custom-taxonomies', array(
				'callback' => array( __CLASS__, 'api_delete_taxonomy' ),
				'methods' => 'DELETE',
				'permission_callback' => array( __CLASS__, 'api_required_permissions' ),
			) );
		}
	}

	/**
	 * Required permissions for the API.
	 *
	 * @since 5.0.0
	 */
	public static function api_required_permissions() {
		return current_user_can( WPRM_Settings::get( 'features_manage_access' ) );
	}

	/**
	 * Handle manage taxonomies call to the REST API.
	 *
	 * @since    5.0.0
	 * @param    WP_REST_Request $request Current request.
	 */
	public static function api_manage_taxonomies( $request ) {
		// Parameters.
		$params = $request->get_params();

		$page = isset( $params['page'] ) ? intval( $params['page'] ) : 0;
		$page_size = isset( $params['pageSize'] ) ? intval( $params['pageSize'] ) : 25;

		$starting_index = $page * $page_size;
		$ending_index = $starting_index + $page_size;
		
		$rows = array();
		$taxonomies = get_option( 'wprm_custom_taxonomies', array() );

		$counter = 0;
		foreach ( $taxonomies as $key => $label ) {
			if ( $starting_index <= $counter && $counter < $ending_index ) {
				$rows[] = array(
					'key' => substr( $key, 5 ),
					'singular_name' => $label['singular_name'],
					'name' => $label['name'],
				);
			}
			$counter++;
		}

		return array(
			'rows' => $rows,
			'total' => count( $taxonomies ),
			'filtered' => count( $taxonomies ),
			'pages' => ceil( count( $taxonomies ) / $page_size ),
		);
	}

	/**
	 * Handle create taxonomy call to the REST API.
	 *
	 * @since    5.0.0
	 * @param    WP_REST_Request $request Current request.
	 */
	public static function api_create_taxonomy( $request ) {
		// Parameters.
		$params = $request->get_params();

		$key = isset( $params['key'] ) ? sanitize_key( $params['key'] ) : '';
		$singular_name = isset( $params['singular_name'] ) ? sanitize_text_field( $params['singular_name'] ) : '';
		$name = isset( $params['name'] ) ? sanitize_text_field( $params['name'] ) : '';

		if ( $key && $singular_name && $name ) {
			$key = 'wprm_' . $key;
			$key = substr( $key, 0, 32 ); // Max length for taxonomies.

			if ( ! taxonomy_exists( $key ) ) {
				$taxonomies = get_option( 'wprm_custom_taxonomies', array() );
				$taxonomies[ $key ] = array(
					'name' => $name,
					'singular_name' => $singular_name,
				);
				update_option( 'wprm_custom_taxonomies', $taxonomies );

				return array(
					'key' => $key,
					'singular_name' => $singular_name,
					'name' => $name,
				);
			}
		}

		return false;
	}

	/**
	 * Handle update taxonomy call to the REST API.
	 *
	 * @since    5.0.0
	 * @param    WP_REST_Request $request Current request.
	 */
	public static function api_update_taxonomy( $request ) {
		// Parameters.
		$params = $request->get_params();

		$key = isset( $params['key'] ) ? sanitize_key( $params['key'] ) : '';
		$singular_name = isset( $params['singular_name'] ) ? sanitize_text_field( $params['singular_name'] ) : '';
		$name = isset( $params['name'] ) ? sanitize_text_field( $params['name'] ) : '';

		if ( $key && $singular_name && $name ) {
			$key = 'wprm_' . $key;
			$taxonomies = get_option( 'wprm_custom_taxonomies', array() );

			if ( array_key_exists( $key, $taxonomies ) ) {
				$taxonomies[ $key ] = array(
					'name' => $name,
					'singular_name' => $singular_name,
				);
				update_option( 'wprm_custom_taxonomies', $taxonomies );

				return array(
					'key' => $key,
					'singular_name' => $singular_name,
					'name' => $name,
				);
			}
		}

		return false;
	}

	/**
	 * Handle delete taxonomy call to the REST API.
	 *
	 * @since    5.0.0
	 * @param    WP_REST_Request $request Current request.
	 */
	public static function api_delete_taxonomy( $request ) {
		// Parameters.
		$params = $request->get_params();

		$key = isset( $params['key'] ) ? sanitize_key( $params['key'] ) : '';

		if ( $key ) {
			$key = 'wprm_' . $key;
			$taxonomies = get_option( 'wprm_custom_taxonomies', array() );

			if ( array_key_exists( $key, $taxonomies ) ) {
				unset( $taxonomies[ $key ] );
				update_option( 'wprm_custom_taxonomies', $taxonomies );

				return true;
			}
		}

		return false;
	}
}

WPRMP_Api_Custom_Taxonomies::init();
