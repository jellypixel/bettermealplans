<?php
/**
 * Handle the export of recipes to JSON.
 *
 * @link       http://bootstrapped.ventures
 * @since      5.2.0
 *
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public
 */

/**
 * Handle the export of recipes to JSON.
 *
 * @since      5.2.0
 * @package    WP_Recipe_Maker_Premium
 * @subpackage WP_Recipe_Maker_Premium/includes/public
 * @author     Brecht Vandersmissen <brecht@bootstrapped.ventures>
 */
class WPRMP_Export_JSON {

	/**
	 * Export recipes to JSON.
	 *
	 * @since	5.2.0
	 * @param	int $recipe_ids IDs of the recipes to export.
	 */
	public static function bulk_edit_export( $recipe_ids ) {
		$export = array();

		foreach ( $recipe_ids as $recipe_id ) {
			$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );

			if ( $recipe ) {
				$data = $recipe->get_data();
				$export[] = self::clean_up_recipe_for_export( $data );
			}
		}

		$json = json_encode( $export, JSON_PRETTY_PRINT );
		
		// Create file.
		$upload_dir = wp_upload_dir();
		$slug = 'wprm';
		$dir = trailingslashit( trailingslashit( $upload_dir['basedir'] ) . $slug );
		$url = $upload_dir['baseurl'] . '/' . $slug . '/';

		wp_mkdir_p( $dir );

		$filename = 'WPRM Recipe Export.json';
		$filepath = $dir . $filename;

		$f = fopen( $filepath, 'wb' );
		if ( ! $f ) {
			wp_die( 'Unable to create recipe export file. Check file permissions' );
		}

		fwrite( $f, $json );
		fclose( $f );

		return array(
			'result' => __( 'Your recipes have been exported to:', 'wp-recipe-maker' ) . '<br/><a href="' . esc_url( $url . $filename ) . '" target="_blank">' . $url . $filename . '</a>',
		);
	}

	/**
	 * Clean up recipe data for export.
	 *
	 * @since	5.2.0
	 * @param	mixed $data Recipe data to export.
	 */
	public static function clean_up_recipe_for_export( $data ) {
		unset( $data[ 'image_id' ] );
		unset( $data[ 'pin_image_id' ] );
		unset( $data[ 'video_id' ] );
		unset( $data[ 'video_thumb_url' ] );
		unset( $data[ 'ingredients' ] ); // Use ingredients_flat for easier editing.
		unset( $data[ 'instructions' ] ); // Use instructions_flat for easier editing.

		foreach ( $data['tags'] as $tag => $terms ) {
			$term_names = array();

			foreach ( $terms as $term ) {
				$term_names[] = $term->name;
			}

			$data['tags'][ $tag ] = $term_names;
		}

		foreach ( $data['equipment'] as $index => $equipment ) {
			unset( $data['equipment'][ $index ]['id'] );
			unset( $data['equipment'][ $index ]['uid'] );
		}

		foreach ( $data['ingredients_flat'] as $index => $ingredient ) {
			unset( $data['ingredients_flat'][ $index ]['id'] );
			unset( $data['ingredients_flat'][ $index ]['uid'] );
		}

		foreach ( $data['instructions_flat'] as $index => $ingredient ) {
			unset( $data['instructions_flat'][ $index ]['image'] );
			unset( $data['instructions_flat'][ $index ]['uid'] );
		}

		if ( isset( $data['custom_fields'] ) ) {
			foreach ( $data['custom_fields'] as $index => $custom_field ) {
				if ( is_array( $custom_field ) && isset( $custom_field['id'] ) ) {
					unset( $data['custom_fields'][ $index ]['id'] );
				}
			}
		}

		return $data;
	}
}
