<?php

namespace Mediavine\Create;

use Mediavine\MV_DBI;

// Don't load just in case the autoload file gets removed
if ( ! file_exists( plugin_dir_path( __FILE__ ) . 'vendor/autoload.php' ) ) {
	return;
}

require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

class Plugin {

	const VERSION = '1.6.2';

	const DB_VERSION = '1.6.2';

	const TEXT_DOMAIN = 'mediavine';

	const PLUGIN_DOMAIN = 'mv_create';

	const PREFIX = '_mv_';

	const PLUGIN_FILE_PATH = __FILE__;

	const PLUGIN_ACTIVATION_FILE = 'mediavine-create.php';

	const REQUIRED_IMPORTER_VERSION = '0.10.3';

	public $api_route = 'mv-create';

	public $api_version = 'v1';

	public static $db_interface = null;

	public static $views = null;

	public static $api_services = null;

	public static $models = null;

	public static $models_v2 = null;

	public static $custom_content = null;

	public static $settings = null;

	public static $settings_group = 'mv_create';

	public static $shapes = null;

	public static $mcp_enabled = false;

	public static $create_settings_slugs = [
		'mv_create_affiliate_message',
		'mv_create_copyright_attribution',
	];

	public $rest_response = null;

	private static $instance = null;

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$models   = new \Mediavine\Models();
			self::$instance = new self;
		}
		return self::$instance;
	}

	public static function assets_url() {
		return plugin_dir_url( __FILE__ );
	}

	/**
	 * Get MCP site id if it exists
	 *
	 * @return  string|false  Site id if it exists and MCP is active, or false
	 */
	public static function get_mcp_data() {
		$mcp_data = false;
		if ( self::$mcp_enabled ) {
			// TODO: Check if video support exists and is authorized with identity service
			$mcp_data = [ 'site_id' => get_option( 'MVCP_site_id' ) ];
		}

		return $mcp_data;
	}

	/**
	 * Return image size value and label
	 *
	 * @return array
	 */
	public static function get_image_size_values() {
		$image_sizes                   = Creations::get_image_sizes();
		$image_sizes_values_and_labels = [];

		// @TODO add a filter for adding size names to exclude from disable list?
		$exclude = [
			'mv_create_no_ratio',
			'mv_create_vert',
		];

		foreach ( $image_sizes as $size => $size_data ) {
			if ( strpos( $size, '_high_res' ) || in_array( $size, $exclude, true ) ) {
				continue;
			}

			$image_sizes_values_and_labels[ $size ] = $size_data['name'];
		}

		return $image_sizes_values_and_labels;
	}

	public static function get_activation_path() {
		return dirname( __FILE__ ) . '/' . self::PLUGIN_ACTIVATION_FILE;
	}

	public function load_models() {
		$models_loader = new \Mediavine\Models();

		return $models_loader;
	}

	public function plugin_activation() {
		// This runs after all plugins are loaded so it can run after update
		if ( get_option( 'mv_create_version' ) === self::VERSION ) {
			return;
		}

		do_action( self::PLUGIN_DOMAIN . '_plugin_activated' );
		update_option( 'mv_create_version', self::VERSION );
		flush_rewrite_rules();
	}

	/**
	 * Flushes rewrite rules on deactivation
	 * @return void
	 */
	public function plugin_deactivation() {
		do_action( self::PLUGIN_DOMAIN . '_plugin_deactivated' );
		flush_rewrite_rules();
	}

	public function generate_tables() {
		\Mediavine\MV_DBI::upgrade_database_check( self::PLUGIN_DOMAIN, self::DB_VERSION );
	}

	public function init() {
		if (
			(
				class_exists( 'MV_Control_Panel' ) ||
				class_exists( 'MVCP' )
			) && get_option( 'MVCP_site_id' )
		) {
			self::$mcp_enabled = true;
		}

		// initialize Sentry debug log catcher -- only if `mv_create_enable_debugging` setting is true
		MV_Sentry::init_debugging();
		self::$views        = \Mediavine\View_Loader::get_instance( plugin_dir_path( __FILE__ ) );
		self::$api_services = \Mediavine\API_Services::get_instance();
		self::$models_v2    = \Mediavine\MV_DBI::get_models(
			[
				'mv_images',
				'mv_nutrition',
				'mv_products',
				'mv_products_map',
				'mv_reviews',
				'mv_creations',
				'mv_supplies',
				'mv_relations',
				'mv_revisions',
				'posts',
			]
		);

		self::$custom_content = Custom_Content::make( 'mv-create', __( 'Create', 'mediavine' ) );

		self::$settings = [
			[
				'slug'  => self::$settings_group . '_default_access_role',
				'value' => 'manage_options',
				'group' => self::$settings_group . '_advanced',
				'order' => 10,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Default Access Role', 'mediavine' ),
					'instructions' => __( 'Select what user roles have access to edit Create Cards.', 'mediavine' ),
					'default'      => __( 'Administrators', 'mediavine' ),
					// QUESTION: Should these settings be created programmatically?
					'options'      => [
						[
							'label' => __( 'Administrators', 'mediavine' ),
							'value' => 'manage_options',
						],
						[
							'label' => __( 'Editors', 'mediavine' ),
							'value' => 'edit_others_posts',
						],
						[
							'label' => __( 'Authors', 'mediavine' ),
							'value' => 'edit_posts',
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_copyright_attribution',
				'value' => null,
				'group' => self::$settings_group . '_advanced',
				'order' => 15,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Default Copyright Attribution', 'mediavine' ),
					'instructions' => __( 'If left blank, the Create Card author will be displayed.', 'mediavine' ),
					'default'      => null,
				],
			],
			[
				'slug'  => self::$settings_group . '_copyright_override',
				'value' => false,
				'group' => self::$settings_group . '_advanced',
				'order' => 16,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Override author', 'mediavine' ),
					'instructions' => __( 'Enabling this setting will cause the default copyright attribution to display instead of the author.', 'mediavine' ),
					'default'      => 'false',
					'dependent_on' => self::$settings_group . '_copyright_attribution',
				],
			],
			[
				'slug'  => self::$settings_group . '_measurement_system',
				'value' => null,
				'group' => self::$settings_group,
				'order' => 20,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Measurement System', 'mediavine' ),
					'instructions' => __( 'Force a default measurement system (or choose "Any" to allow either).', 'mediavine' ),
					'default'      => null,
					'options'      => [
						[
							'label' => __( 'Metric', 'mediavine' ),
							'value' => 'metric',
						],
						[
							'label' => __( 'Imperial', 'mediavine' ),
							'value' => 'imperial',
						],
						[
							'label' => __( 'Any', 'mediavine' ),
							'value' => 'any',
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_photo_ratio',
				'value' => 'mv_create_no_ratio',
				'group' => self::$settings_group . '_display',
				'order' => 30,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Photo Ratio', 'mediavine' ),
					'instructions' => __( 'Select an aspect ratio for photo display. Some card styles, such as Classy Circle, will ignore this setting.', 'mediavine' ),
					'default'      => __( 'No fixed ratio', 'mediavine' ),
					'options'      => [
						[
							'label' => __( 'No fixed ratio', 'mediavine' ),
							'value' => 'mv_create_no_ratio',
						],
						[
							'label' => '1x1',
							'value' => 'mv_create_1x1',
						],
						[
							'label' => '4x3',
							'value' => 'mv_create_4x3',
						],
						[
							'label' => '16x9',
							'value' => 'mv_create_16x9',
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_enable_print_thumbnails',
				'value' => true,
				'group' => self::$settings_group . '_display',
				'order' => 35,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Enable Print Thumbnails', 'mediavine' ),
					'instructions' => __( 'By default, card thumbnails will display in the print view. This can be disabled.', 'mediavine' ),
					'default'      => __( 'Enabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_pinterest_location',
				'value' => 'mv-pinterest-btn-right',
				'group' => self::$settings_group . '_display',
				'order' => 40,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Pinterest Button Location', 'mediavine' ),
					'instructions' => __( 'Select location for Pinterest button. Note: On the list card styles Numbered and Circles, the Pinterest button will still display to the right.', 'mediavine' ),
					'default'      => __( 'Top Right', 'mediavine' ),
					'options'      => [
						[
							'label' => __( 'Off', 'mediavine' ),
							'value' => 'off',
						],
						[
							'label' => __( 'Top Left', 'mediavine' ),
							'value' => 'mv-pinterest-btn-left',
						],
						[
							'label' => __( 'Inside Top Left', 'mediavine' ),
							'value' => 'mv-pinterest-btn-left-inside',
						],
						[
							'label' => __( 'Inside Top Right', 'mediavine' ),
							'value' => 'mv-pinterest-btn-right-inside',
						],
						[
							'label' => __( 'Top Right', 'mediavine' ),
							'value' => 'mv-pinterest-btn-right',
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_force_uppercase',
				'value' => true,
				'group' => self::$settings_group . '_display',
				'order' => 50,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Force Uppercase', 'mediavine' ),
					'instructions' => __( 'By default, recipe cards show some pieces of text as all-uppercase, which for certain typefaces may not be desired.', 'mediavine' ),
					'default'      => __( 'Enabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_aggressive_lists',
				'value' => false,
				'group' => self::$settings_group . '_display',
				'order' => 55,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Aggressive List CSS', 'mediavine' ),
					'instructions' => __( 'Some themes may remove bullets and numbers from lists. This forces them to display in Create by Mediavine Cards.', 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_aggressive_buttons',
				'value' => false,
				'group' => self::$settings_group . '_display',
				'order' => 60,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Aggressive Buttons CSS', 'mediavine' ),
					'instructions' => __( "Some themes may not have button styles, or they won't look good with your theme. This forces a generic button style.", 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_center_cards',
				'value' => true,
				'group' => self::$settings_group . '_display',
				'order' => 65,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Center Full Width Cards', 'mediavine' ),
					'instructions' => __( 'When a card reaches its max width of 700px, center the card within the content area.', 'mediavine' ),
					'default'      => __( 'Enabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_primary_headings',
				'value' => 'h2',
				'group' => self::$settings_group . '_advanced',
				'order' => 18,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Primary Heading Tag', 'mediavine' ),
					'instructions' => sprintf(
						// translators: Link tags
						__( 'While having %1$smultiple H1s on a page is approved by Google%2$s, many still recommend maintaining the page to only a single H1. This allows you to choose what tag you want for the primary heading, properly adjusting the heading hierarchy throughout the card.', 'mediavine' ),
						'<a href="https://www.youtube.com/watch?v=WsgrSxCmMbM" target="_blank">',
						'</a>'
					),
					'default'      => __( 'H2', 'mediavine' ),
					'options'      => [
						[
							'label' => 'H1',
							'value' => 'h1',
						],
						[
							'label' => 'H2',
							'value' => 'h2',
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_enhanced_search',
				'value' => false,
				'group' => self::$settings_group . '_advanced',
				'order' => 80,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Use Enhanced Search', 'mediavine' ),
					'instructions' => __( 'Create by Mediavine has a search feature that allows users to match posts based on the content of the recipe cards included in the post. If you notice that this feature is causing an issue with other themes or plugins that modify the search query, you can disable this feature.', 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_autosave',
				'value' => true,
				'group' => self::$settings_group . '_advanced',
				'order' => 85,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Autosave', 'mediavine' ),
					'instructions' => __( 'By default, we\'ll save your work as you edit, even if you haven\'t published your changes. If you disable this setting, we\'ll only save draft content if you specifically click the \'Save Draft\' button.', 'mediavine' ),
					'default'      => __( 'Enabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_card_style',
				'value' => 'square',
				'group' => self::$settings_group . '_display',
				'order' => 90,
				'data'  => [
					'type'    => 'image_select',
					'label'   => __( 'Card Style', 'mediavine' ),
					'default' => __( 'Simple Square', 'mediavine' ),
					'options' => [
						[
							'label' => __( 'Simple Square by Purr Design', 'mediavine' ),
							'value' => 'square',
							'image' => plugin_dir_url( __FILE__ ) . 'admin/img/card-style-default.png',
							/* translators: credit name and url */
							'title' => sprintf( __( 'Simple Square<br>by %s', 'mediavine' ), '<a href="https://www.purrdesign.com/" target="_blank">Purr Design<span class="dashicons dashicons-external"></span></a>' ),
						],
						[
							'label' => __( 'Dark Simple Square by Purr Design', 'mediavine' ),
							'value' => 'dark',
							'image' => plugin_dir_url( __FILE__ ) . 'admin/img/card-style-dark.png',
							/* translators: credit name and url */
							'title' => sprintf( __( 'Dark Simple Square<br>by %s', 'mediavine' ), '<a href="https://www.purrdesign.com/" target="_blank">Purr Design<span class="dashicons dashicons-external"></span></a>' ),
						],
						[
							'label' => __( 'Classy Circle by Purr Design', 'mediavine' ),
							'value' => 'centered',
							'image' => plugin_dir_url( __FILE__ ) . 'admin/img/card-style-centered.png',
							/* translators: credit name and url */
							'title' => sprintf( __( 'Classy Circle<br>by %s', 'mediavine' ), '<a href="https://www.purrdesign.com/" target="_blank">Purr Design<span class="dashicons dashicons-external"></span></a>' ),
						],
						[
							'label' => __( 'Dark Classy Circle by Purr Design', 'mediavine' ),
							'value' => 'centered-dark',
							'image' => plugin_dir_url( __FILE__ ) . 'admin/img/card-style-centered-dark.png',
							/* translators: credit name and url */
							'title' => sprintf( __( 'Dark Classy Circle<br>by %s', 'mediavine' ), '<a href="https://www.purrdesign.com/" target="_blank">Purr Design<span class="dashicons dashicons-external"></span></a>' ),
						],
						[
							'label' => __( 'Hero Image by Purr Design', 'mediavine' ),
							'value' => 'big-image',
							'image' => plugin_dir_url( __FILE__ ) . 'admin/img/card-style-big-image.png',
							/* translators: credit name and url */
							'title' => sprintf( __( 'Hero Image<br>by %s', 'mediavine' ), '<a href="https://www.purrdesign.com/" target="_blank">Purr Design<span class="dashicons dashicons-external"></span></a>' ),
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_color',
				'value' => null,
				'group' => self::$settings_group . '_display',
				'order' => '0',
				'data'  => [
					'type'         => 'color_picker',
					'label'        => __( 'Theme Colors' ),
					'instructions' => null,
				],
			],
			[
				'slug'  => self::$settings_group . '_secondary_color',
				'value' => null,
				'group' => 'mv_create_hidden',
				'order' => '0',
				'data'  => [],
			],
			[
				'slug'  => self::$settings_group . '_enable_high_contrast',
				'value' => false,
				'group' => self::$settings_group . '_advanced',
				'order' => 10,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Enable High Contrast', 'mediavine' ),
					'instructions' => __( 'By default, high contrast mode is disabled.', 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_lists_rounded_corners',
				'value' => '0',
				'group' => self::$settings_group . '_lists',
				'order' => 95,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Rounded Corners', 'mediavine' ),
					'instructions' => __( 'This value is used for buttons and other card elements.', 'mediavine' ),
					'default'      => __( 'None', 'mediavine' ),
					'options'      => [
						[
							'label' => __( 'High', 'mediavine' ),
							'value' => '1rem',
						],
						[
							'label' => __( 'Low', 'mediavine' ),
							'value' => '3px',
						],
						[
							'label' => __( 'None', 'mediavine' ),
							'value' => '0',
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_external_link_tab',
				'value' => true,
				'group' => self::$settings_group . '_lists',
				'order' => 100,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Open external list items in new tab', 'mediavine' ),
					'instructions' => __( 'Checking the box will open external list items in a new tab', 'mediavine' ),
					'default'      => __( 'Enabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_internal_link_tab',
				'value' => false,
				'group' => self::$settings_group . '_lists',
				'order' => 100,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Open internal list items in new tab', 'mediavine' ),
					'instructions' => __( 'Checking the box will open internal list items in a new tab', 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_enable_nutrition',
				'value' => true,
				'group' => self::$settings_group . '_recipes',
				'order' => 95,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Use Nutrition', 'mediavine' ),
					'instructions' => __( 'Unchecking the box will remove nutrition inputs from the recipe card interface and hide nutrition data for all recipes.', 'mediavine' ),
					'default'      => __( 'Enabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_allow_reviews',
				'value' => true,
				'group' => self::$settings_group . '_advanced',
				'order' => 100,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Allow Reviews', 'mediavine' ),
					'instructions' => __( 'Unchecking this box will prevent users from being able to leave reviews on your recipe cards.', 'mediavine' ),
					'default'      => __( 'Enabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_use_realistic_nutrition_display',
				'value' => false,
				'group' => self::$settings_group . '_recipes',
				'order' => 98,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Use Traditional Nutrition Display', 'mediavine' ),
					'instructions' => __( 'Checking the box will add a traditional nutrition display.', 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_nutrition_disclaimer',
				'value' => '',
				'group' => self::$settings_group . '_recipes',
				'order' => 99,
				'data'  => [
					'type'         => 'textarea',
					'label'        => __( 'Calculated Nutrition Disclaimer', 'mediavine' ),
					'instructions' => __( 'If provided, this disclaimer will be automatically added to each recipe upon nutrition calculation.', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_api_token',
				],
			],
			[
				'slug'  => self::$settings_group . '_enable_logging',
				'value' => false,
				'group' => self::$settings_group . '_advanced',
				'order' => 105,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Enable Error Reporting', 'mediavine' ),
					'instructions' => __( 'Checking this box allows the plugin to automatically send useful error reports to the development team. (You may still be prompted to manually send error reports, even if this box is unchecked.)', 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_affiliate_message',
				'value' => 'As an Amazon Associate and member of other affiliate programs, I earn from qualifying purchases.',
				'group' => self::$settings_group . '_advanced',
				'order' => 80,
				'data'  => [
					'type'         => 'textarea',
					'label'        => __( 'Global Affiliate Message', 'mediavine' ),
					'instructions' => __( 'Set the default affiliate disclaimer message with this text. Affiliate messaging can be overridden in individual posts.', 'mediavine' ),
					// No localization because the default value does not get translated.
					'default'      => 'As an Amazon Associate and member of other affiliate programs, I earn from qualifying purchases.',
				],
			],
			[
				'slug'  => self::$settings_group . '_allowed_types',
				'value' => '[]',
				'group' => self::$settings_group . '_advanced',
				'order' => 0,
				'data'  => [
					'type'         => 'allowed_types',
					'label'        => __( 'Allowed Types', 'mediavine' ),
					'instructions' => null,
					'default'      => '[]',
				],
			],
			[
				'slug'  => self::$settings_group . '_enable_review_prompt_always',
				'value' => true,
				'group' => self::$settings_group . '_advanced',
				'order' => 110,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Always Enable Review Popup', 'mediavine' ),
					'instructions' => __( 'If enabled, users leaving a star rating will see a popup modal prompting them to leave an optional review. Disabling this will not display the prompt if the left rating is 4 or higher.', 'mediavine' ),
					'default'      => 'Enabled',
				],
			],
			[
				'slug'  => self::$settings_group . '_enable_public_reviews',
				'value' => false,
				'group' => self::$settings_group . '_advanced',
				'order' => 120,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Enable Public Reviews', 'mediavine' ),
					'instructions' => __( 'If enabled, card reviews will be publicly visible, displayed in a tab alongside comments. You must specify a DOM selector for your comments section.' ),
					'default'      => 'Disabled',
				],
			],
			[
				'slug'  => self::$settings_group . '_public_reviews_el',
				'value' => '#comments',
				'group' => self::$settings_group . '_advanced',
				'order' => 125,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Comments Section', 'mediavine' ),
					// TODO: Add a link to help.mediavine.com
					'instructions' => __( 'Add the DOM selector of your comments section. (In most themes, this will be "#comments".)' ),
					'default'      => '#comments',
					'dependent_on' => self::$settings_group . '_enable_public_reviews',
				],
			],
			[
				'slug'  => self::$settings_group . '_disable_image_sizes',
				'value' => '[]',
				'group' => self::$settings_group . '_advanced',
				'order' => 130,
				'data'  => [
					'type'         => 'multiselect',
					'label'        => __( 'Prevent Image Size Generation', 'mediavine' ),
					'instructions' => __( 'If enabled, will disable specific image sizes created by Create', 'mediavine' ),
					'default'      => 'Disabled',
					'options'      => self::get_image_size_values(),
				],
			],
			[
				'slug'  => self::$settings_group . '_custom_buttons',
				'value' => 'Read More\nGet Recipe',
				'group' => self::$settings_group . '_lists',
				'order' => 126,
				'data'  => [
					'type'         => 'custom_buttons',
					'label'        => __( 'Button Action Defaults', 'mediavine' ),
					'instructions' => __( 'Add options for the Button Action dropdown in list items. Add a new line between items.' ),
					'default'      => '',
				],
			],
			[
				'slug'  => self::$settings_group . '_api_token',
				'value' => null,
				'group' => self::$settings_group . '_api',
				'order' => 105,
				'data'  => [
					'type'         => 'api_authentication',
					'label'        => __( 'Product Registration', 'mediavine' ),
					'instructions' => __( 'In order to use services like nutrition calculation or link scraping, you must register an account. This is a free, one-time action that will grant access to all of our external APIs.', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_api_email_confirmed',
				'value' => false,
				'group' => 'hidden',
				'order' => 105,
				'data'  => [],
			],
			[
				'slug'  => self::$settings_group . '_api_user_id',
				'value' => false,
				'group' => 'hidden',
				'order' => 105,
				'data'  => [],
			],
			[
				'slug'  => self::$settings_group . '_enable_debugging',
				'value' => false,
				'group' => 'mv_secret_do_not_share_or_you_will_be_fired',
				'order' => 10,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Enabled Debugging', 'mediavine' ),
					'instructions' => __( 'Enable this setting to send all error log data to Sentry for the Publisher Engineering team to debug. This reduces the need for FTP and should be used in lieu of debug log/error log files.', 'mediavine' ),
					'default'      => 'Disabled',
				],
			],
			[
				'slug'  => self::$settings_group . '_enable_jump_to_recipe',
				'value' => false,
				'group' => self::$settings_group . '_pro',
				'order' => 10,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Enable Jump To Recipe Button', 'mediavine' ),
					'instructions' => __(
						'When enabled, use of a Jump Button means that readers will be able to bypass
						the content of your blog post, including any in-content ads that would have
						earned income.

						To mitigate some of this potential loss, when the button
						is pressed, our script will automatically optimize the Create card ad placements
						for Mediavine publishers.',
						'mediavine'
					),
					'default'      => 'Disabled',
				],
			],
			[
				'slug'  => self::$settings_group . '_jump_to_recipe_text',
				'value' => __( 'Jump to Recipe', 'mediavine' ),
				'group' => self::$settings_group . '_pro',
				'order' => 15,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Jump To Recipe Button Text', 'mediavine' ),
					'instructions' => __( 'The text of the Jump To Recipe Button' ),
					'default'      => __( 'Jump to Recipe', 'mediavine' ),
					'dependent_on' => self::$settings_group . '_enable_jump_to_recipe',
				],
			],
			[
				'slug'  => self::$settings_group . '_jump_to_howto_text',
				'value' => __( 'Jump to How-To', 'mediavine' ),
				'group' => self::$settings_group . '_pro',
				'order' => 20,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Jump To How-To Button Text', 'mediavine' ),
					'instructions' => __( 'The text of the Jump To How-To Button' ),
					'default'      => __( 'Jump to How-To', 'mediavine' ),
					'dependent_on' => self::$settings_group . '_enable_jump_to_recipe',
				],
			],
			[
				'slug'  => self::$settings_group . '_jump_to_btn_color',
				'value' => 'gray',
				'group' => self::$settings_group . '_pro',
				'order' => 25,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Jump to Recipe Color', 'mediavine' ),
					'instructions' => __( 'Style for Jump to Recipe Button', ' mediavine' ),
					'default'      => __( 'Gray', 'mediavine' ),
					'dependent_on' => self::$settings_group . '_enable_jump_to_recipe',
					'options'      => [
						[
							'label' => __( 'Gray', 'mediavine' ),
							'value' => 'gray',
						],
						[
							'label' => __( 'Custom Colors', 'mediavine' ),
							'value' => 'custom',
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_jump_to_btn_style',
				'value' => 'mv-create-jtr-link',
				'group' => self::$settings_group . '_pro',
				'order' => 30,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Jump to Recipe Button Style', 'mediavine' ),
					'instructions' => __( 'Style for Jump to Recipe Button', ' mediavine' ),
					'default'      => __( 'Link', 'mediavine' ),
					'dependent_on' => self::$settings_group . '_enable_jump_to_recipe',
					'options'      => [
						[
							'label' => __( 'Link', 'mediavine' ),
							'value' => 'mv-create-jtr-link',
						],
						[
							'label' => __( 'Hollow Button', 'mediavine' ),
							'value' => 'mv-create-jtr-button-hollow',
						],
						[
							'label' => __( 'Solid Button', 'mediavine' ),
							'value' => 'mv-create-jtr-button',
						],
					],
				],
			],
			[
				'slug'  => self::$settings_group . '_social_footer',
				'value' => false,
				'group' => self::$settings_group . '_pro',
				'order' => 35,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Enable Social Footer', 'mediavine' ),
					'instructions' => __( 'Adds a call to action to the bottom of each card encouraging social sharing.', 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
			[
				'slug'  => self::$settings_group . '_social_service',
				'value' => 'instagram',
				'group' => self::$settings_group . '_pro',
				'order' => 40,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Social Sharing Service', 'mediavine' ),
					'instructions' => __( 'Select the social service to encourage.', 'mediavine' ),
					'default'      => __( 'Instagram', 'mediavine' ),
					'options'      => [
						[
							'label' => __( 'Facebook', 'mediavine' ),
							'value' => 'facebook',
						],
						[
							'label' => __( 'Instagram', 'mediavine' ),
							'value' => 'instagram',
						],
						[
							'label' => __( 'Pinterest', 'mediavine' ),
							'value' => 'pinterest',
						],
					],
					'dependent_on' => self::$settings_group . '_social_footer',
				],
			],
			[
				'slug'  => self::$settings_group . '_social_cta_facebook_user',
				'value' => '',
				'group' => self::$settings_group . '_pro',
				'order' => 42,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Facebook Username', 'mediavine' ),
					'instructions' => __( 'Enter your Facebook username to link the Facebook icon on Facebook social footer cards.', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_social_footer',
				],
			],
			[
				'slug'  => self::$settings_group . '_social_cta_instagram_user',
				'value' => '',
				'group' => self::$settings_group . '_pro',
				'order' => 44,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Instagram Username', 'mediavine' ),
					'instructions' => __( 'Enter your Instagram username to link the Instagram icon on Instagram social footer cards.', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_social_footer',
				],
			],
			[
				'slug'  => self::$settings_group . '_social_cta_pinterest_user',
				'value' => '',
				'group' => self::$settings_group . '_pro',
				'order' => 46,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Pinterest Username', 'mediavine' ),
					'instructions' => __( 'Enter your Pinterest username to link the Pinterest icon on Pinterest social footer cards.', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_social_footer',
				],
			],
			[
				'slug'  => self::$settings_group . '_social_cta_title_recipe',
				'value' => __( 'Did you make this recipe?', 'mediavine' ),
				'group' => self::$settings_group . '_pro',
				'order' => 48,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Social Footer Heading - Recipe', 'mediavine' ),
					'instructions' => __( 'The title for the social footer on recipe cards.', 'mediavine' ),
					'instructions' => __( 'The title for the social footer on recipe cards. If left blank, "Did you make this recipe?" will display.', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_social_footer',
				],
			],
			[
				'slug'  => self::$settings_group . '_social_cta_body_recipe',
				'value' => '',
				'group' => self::$settings_group . '_pro',
				'order' => 50,
				'data'  => [
					'type'         => 'wysiwyg',
					'label'        => __( 'Social Footer Content - Recipe', 'mediavine' ),
					'instructions' => __( 'The content for the social footer on recipe cards. If left blank, "Please leave a comment on the blog or share a photo on {service_name}" will display.', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_social_footer',
				],
			],
			[
				'slug'  => self::$settings_group . '_social_cta_title_diy',
				'value' => __( 'Did you make this project?', 'mediavine' ),
				'group' => self::$settings_group . '_pro',
				'order' => 52,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Social Footer Heading - How-To', 'mediavine' ),
					'instructions' => __( 'The title for the social footer on how-to cards. If left blank, "Did you make this project?" will display.', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_social_footer',
				],
			],
			[
				'slug'  => self::$settings_group . '_social_cta_body_diy',
				'value' => '',
				'group' => self::$settings_group . '_pro',
				'order' => 54,
				'data'  => [
					'type'         => 'wysiwyg',
					'label'        => __( 'Social Footer Content - How-To', 'mediavine' ),
					'instructions' => __( 'The content for the social footer on how-to cards. If left blank, "Please leave a comment on the blog or share a photo on {service_name}" will display.', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_social_footer',
				],
			],
			[
				'slug'  => self::$settings_group . '_enable_amazon',
				'value' => false,
				'group' => self::$settings_group . '_affiliates',
				'order' => 10,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Enable Amazon Affiliates', 'mediavine' ),
					'instructions' => __(
						'When enabled, recommended products in Create cards have the ability to pull data from Amazon, using the Product Advertising API Version 5.0 (PA API 5).

						You will need to register with Amazon as an affiliate and use your own Product Advertising/Amazon Affiliates Access Key, Secret, and Store ID.

						Images will be pulled directly from Amazon and refreshed every 24 hours per their terms and conditions.

						Checking this box also acknowledges that a valid SSL certificate is required to use this feature.',
						'mediavine'
					),
					'default'      => 'Disabled',
				],
			],
			[
				'slug'  => self::$settings_group . '_paapi_access_key',
				'value' => '',
				'group' => self::$settings_group . '_affiliates',
				'order' => 15,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Amazon Affiliates Access Key', 'mediavine' ),
					'instructions' => __( 'The Amazon Affiliates Access Key', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_enable_amazon',
				],
			],
			[
				'slug'  => self::$settings_group . '_paapi_secret_key',
				'value' => '',
				'group' => self::$settings_group . '_affiliates',
				'order' => 20,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Amazon Affiliates Secret Access Key', 'mediavine' ),
					'instructions' => __( 'The Amazon Affiliates Secret Access Key. Please note that Amazon generally takes 48 hours to provision the secret access key for use.' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_enable_amazon',
				],
			],
			[
				'slug'  => self::$settings_group . '_paapi_tag',
				'value' => '',
				'group' => self::$settings_group . '_affiliates',
				'order' => 25,
				'data'  => [
					'type'         => 'text',
					'label'        => __( 'Amazon Associate Tag', 'mediavine' ),
					'instructions' => __( 'The Amazon Associate Tag/Store ID for the US Marketplace (other countries will be supported in the future)', 'mediavine' ),
					'default'      => '',
					'dependent_on' => self::$settings_group . '_enable_amazon',
				],
			],
			[
				'slug'  => self::$settings_group . '_display_nutrition_zeros',
				'value' => false,
				'group' => self::$settings_group . '_recipes',
				'order' => 100,
				'data'  => [
					'type'         => 'checkbox',
					'label'        => __( 'Display Zero Values For Net Carbs And Sugar Alcohols', 'mediavine' ),
					'instructions' => __( 'Checking this box will display the Net Carbohydrate and Sugar Alcohols fields on recipe nutrition when they have a value of "0", which are hidden by default. The display of zero values can be overridden for individual recipes.', 'mediavine' ),
					'default'      => __( 'Disabled', 'mediavine' ),
				],
			],
		];

		if ( class_exists( 'MV_Control_Panel' ) || class_exists( 'MVCP' ) ) {
			self::$settings[] = [
				'slug'  => self::$settings_group . '_ad_density',
				'value' => '583',
				'group' => self::$settings_group . '_mvp',
				'order' => 100,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'Ad Density', 'mediavine' ),
					'instructions' => __( 'Choose the density of ads in a recipe card.', 'mediavine' ),
					'default'      => __( 'Normal', 'mediavine' ),
					'options'      => [
						[
							'label' => __( 'Single Ad', 'mediavine' ),
							'value' => '0',
						],
						[
							'label' => __( 'Normal', 'mediavine' ),
							'value' => '583',
						],
						[
							'label' => __( 'Medium', 'mediavine' ),
							'value' => '750',
						],
						[
							'label' => __( 'Low', 'mediavine' ),
							'value' => '1000',
						],
					],
				],
			];
			self::$settings[] = [
				'slug'  => self::$settings_group . '_list_items_between_ads',
				'value' => '3',
				'group' => self::$settings_group . '_mvp',
				'order' => 110,
				'data'  => [
					'type'         => 'select',
					'label'        => __( 'List Items Between Ads', 'mediavine' ),
					'instructions' => __( 'Choose the number of card between ads in a list.', 'mediavine' ),
					'options'      => [
						[
							'label' => __( 'Disable ads in lists', 'mediavine' ),
							'value' => 0,
						],
						[
							'label' => __( '2', 'mediavine' ),
							'value' => '2',
						],
						[
							'label' => __( '3', 'mediavine' ),
							'value' => '3',
						],
						[
							'label' => __( '4', 'mediavine' ),
							'value' => '4',
						],
						[
							'label' => __( '5', 'mediavine' ),
							'value' => '5',
						],
					],
				],
			];
		}

		// Shape implementation is very, very temp
		self::$shapes = [
			[
				'name'   => 'Recipe',
				'plural' => 'Recipes',
				'slug'   => 'recipe',
				'icon'   => 'carrot',
				'shape'  => file_get_contents( __DIR__ . '/shapes/recipe.json' ),
			],
			[
				'name'   => 'How-To',
				'plural' => 'How-Tos',
				'slug'   => 'diy',
				'icon'   => 'lightbulb',
				'shape'  => file_get_contents( __DIR__ . '/shapes/how-to.json' ),
			],
			[
				'name'   => 'List',
				'plural' => 'Lists',
				'slug'   => 'list',
				'icon'   => '',
				'shape'  => file_get_contents( __DIR__ . '/shapes/list.json' ),
			],
		];

		register_activation_hook( self::get_activation_path(), [ $this, 'plugin_activation' ] );
		add_action( 'setup_theme', [ $this, 'plugin_activation' ], 10, 2 );
		register_deactivation_hook( self::get_activation_path(), [ $this, 'plugin_deactivation' ] );

		// Activations hooks, forcing order
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'generate_tables' ], 20 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'create_settings' ], 30 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'create_shapes' ], 35 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'republish_queue' ], 40 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'update_queue' ], 45 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'update_reviews_table' ], 50 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'importer_admin_notice' ], 60 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'fix_cloned_ratings' ], 70 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'fix_cookbook_canonical_post_ids' ], 80 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'add_initial_revision_to_cards' ], 85 );
		add_action( self::PLUGIN_DOMAIN . '_plugin_activated', [ $this, 'queue_existing_amazon_products' ], 90 );

		// fixes
		// ex: add_action( 'mv_fix_video_description_queue_action', [ $this, 'fix_video_description' ] );
		add_action( 'mv_fix_video_description_queue_action', [ $this, 'fix_video_description' ] );

		// Shortcodes
		add_shortcode( 'mv_img', [ $this, 'mv_img_shortcode' ] );
		add_shortcode( 'mvc_ad', [ $this, 'mvc_ad_shortcode' ] );
		add_shortcode( 'mv_schema_meta', [ $this, 'mv_schema_meta_shortcode' ] );

		add_filter( 'rest_prepare_post', [ $this, 'rest_prepare_post' ], 10, 3 );

		add_filter( 'mv_create_paapi_access_key_settings_value', 'trim', 10 );
		add_filter( 'mv_create_paapi_secret_key_settings_value', 'trim', 10 );
		add_filter( 'mv_create_paapi_tag_settings_value', 'trim', 10 );
	}

	/**
	 * Updates Create Services Site ID with php, create and wp versions
	 *
	 * @return void
	 */
	function update_services_api() {
		global $wp_version;
		$php_version       = PHP_VERSION;
		$create_version    = Plugin::VERSION;
		$api_token_setting = \Mediavine\Settings::get_settings( 'mv_create_api_token' );

		if ( ! $api_token_setting ) {
			return;
		}

		$token_values = explode( '.', $api_token_setting->value );

		if ( empty( $token_values[1] ) ) {
			return;
		}

		$token_data = json_decode( base64_decode( $token_values[1] ) );

		if ( ! isset( $token_data->site_id ) ) {
			return;
		}

		$data = [];

		if ( isset( $php_version ) ) {
			$data['php_version'] = PHP_VERSION;
		}

		if ( isset( $wp_version ) ) {
			$data['wp_version'] = $wp_version;
		}

		if ( isset( $create_version ) ) {
			$data['create_version'] = $create_version;
		}

		$result = wp_remote_post(
			'https://create-api.mediavine.com/api/v1/sites/' . $token_data->site_id, [
				'headers' => [
					'Content-Type'  => 'application/json; charset=utf-8',
					'Authorization' => 'bearer ' . $api_token_setting->value,
				],
				'body'    => wp_json_encode( $data ),
				'method'  => 'POST',
			]
		);
		return;
	}

	public function mv_schema_meta_shortcode( $atts ) {
		if ( isset( $atts['name'] ) ) {
			return '<span data-schema-name="' . $atts['name'] . '" style="display: none;"></span>';
		}
		return '';
	}

	public function mv_img_shortcode( $atts ) {
		$a = shortcode_atts(
			[
				'id'      => null,
				'options' => null,
				'no-pin'  => null, // @todo check for option to turn pinning on or off
			], $atts
		);

		if ( isset( $a['id'] ) ) {
			$attr = [];
			if ( isset( $a['no-pin'] ) ) {
				$attr['data-pin-nopin'] = $a['no-pin'];
			}

			if ( isset( $a['options'] ) ) {
				$meta    = wp_prepare_attachment_for_js( $a['id'] );
				$alt     = $meta['alt'];
				$title   = $meta['title'];
				$options = json_decode( $a['options'] );

				$class = 'align' . esc_attr( $options->alignment ) . ' size-' . esc_attr( $options->size ) . ' wp-image-' . $a['id'];
				$class = apply_filters( 'get_image_tag_class', $class, $a['id'], $options->alignment, $options->size );

				$attr = [
					'alt'   => $alt,
					'title' => $title,
					'class' => $class,
				];
			}

			$img = wp_get_attachment_image( $a['id'], '', false, $attr );

			return $img;
		}
		return '';
	}

	/**
	 * In 1.4.12, we moved ad insertion logic from the admin UI to the client, see #2860.
	 * This shortcode output is intentionally left empty to provide backwards compatibility
	 * with content that includes the old ad target shortcode.
	 */
	public function mvc_ad_shortcode() {
		return '';
	}

	public function create_settings() {
		$settings = $this->update_settings( self::$settings );
		\Mediavine\Settings::create_settings_filter( $settings );
	}

	public function create_shapes() {
		$shape_dbi = new \Mediavine\MV_DBI( 'mv_shapes' );

		foreach ( self::$shapes as $shape ) {
			$shape_dbi->upsert( $shape );
		}
	}

	/**
	 * Migrates old settings to newer versions within settings table
	 *
	 * Always check for less than current version as this is run before the version is updated
	 * Add estimated removal date (6 months) so we don't clutter code with future publishes
	 * Remove code within this function, but don't remove this function
	 *
	 * Example usage:
	 * ```
	 * if ( version_compare( $last_plugin_version, '1.0.0', '<' ) ) {
	 *     $settings = \Mediavine\Settings::migrate_setting_value( $settings, self::$settings_group . '_slug', 'old_value', 'new_value' );
	 *     $settings = \Mediavine\Settings::migrate_setting_slug( $settings, self::$settings_group . '_old_slug', self::$settings_group . '_new_slug' );
	 * }
	 * ```
	 *
	 * @param   array  $settings  Current list of settings before running create settings
	 * @return  array             List of settings after migrated changes made
	 */
	public function update_settings( $settings ) {
		$last_plugin_version = get_option( 'mv_create_version', Plugin::VERSION );

		// Update incorrect card style slug of mv_create to square (Remove Jan 2020)
		if ( version_compare( $last_plugin_version, '1.4.8', '<' ) ) {
			$settings = \Mediavine\Settings::migrate_setting_value( $settings, self::$settings_group . '_card_style', 'mv_create', 'square' );
		}

		return $settings;
	}

	public function fix_video_description( $id ) {
		// fix the video description
		$creations = new \Mediavine\MV_DBI( 'mv_creations' );
		$creation  = $creations->find_one_by_id( $id );

		if ( ! empty( $creation->video ) ) {
			$video_data         = json_decode( $creation->video );
			$make_the_call      = false;
			$video_data_changed = false;
			$update_data        = [
				'id' => $creation->id,
			];

			if ( empty( $video_data->description ) ) {
				if (
					! empty( $video_data->rawData ) &&
					! empty( $video_data->rawData->description )
				) {
					$video_data->description = $video_data->rawData->description;
					$video_data_changed      = true;
				} else {
					$make_the_call = true;
				}
			}

			if ( empty( $video_data->duration ) ) {
				if (
					! empty( $video_data->rawData ) &&
					! empty( $video_data->rawData->duration )
				) {
					$video_data->duration = 'PT' . $video_data->rawData->duration . 'S';
					$video_data_changed   = true;
				} else {
					$make_the_call = true;
				}
			}

			if ( $make_the_call && $video_data->slug ) {
				$api_data = file_get_contents( 'https://embed.mediavine.com/oembed/?url=https%3A%2F%2Fvideo.mediavine.com%2Fvideos%2F' . $video_data->slug );
				if ( $api_data ) {
					$new_video_data = json_decode( $api_data );

					if ( ! empty( $new_video_data->duration ) ) {
						$video_data->duration = 'PT' . $new_video_data->duration . 'S';
						$video_data_changed   = true;
					}

					if ( ! empty( $new_video_data->description ) ) {
						$video_data->description = $new_video_data->description;
						$video_data_changed      = true;
					}

					if ( ! empty( $new_video_data->keywords ) ) {
						$video_data->keywords = $new_video_data->keywords;
						$video_data_changed   = true;
					}
				}
			}

			if ( $video_data_changed ) {
				$creation->video      = wp_json_encode( $video_data );
				$update_data['video'] = $creation->video;
				if ( ! empty( $creation->json_ld ) ) {
					$json_ld     = json_decode( $creation->json_ld );
					$upload_date = $json_ld->video->uploadDate;
					if ( ! empty( $video_data->rawData->uploadDate ) ) {
						$upload_date = $video_data->rawData->uploadDate;
					}

					$json_ld->video         = [
						'@type'        => 'VideoObject',
						'name'         => $json_ld->video->name,
						'description'  => $video_data->description,
						'thumbnailUrl' => $json_ld->video->thumbnailUrl,
						'contentUrl'   => $json_ld->video->contentUrl,
						'duration'     => $video_data->duration,
						'uploadDate'   => $upload_date,
					];
					$creation->json_ld      = wp_json_encode( $json_ld );
					$update_data['json_ld'] = $creation->json_ld;

					if ( ! empty( $creation->published ) ) {
						$published_data           = json_decode( $creation->published );
						$published_data->video    = $creation->json_ld;
						$update_data['published'] = wp_json_encode( $published_data );
					}
				}

				$creations->update( $update_data );

			}
		}
	}

	/**
	 * Updates cards based on various queues and actions.
	 *
	 * Always check for less than current version as this is run before the version is updated.
	 * Add estimated removal date (around 6 months).
	 *
	 * @return void
	 */
	public function update_queue() {
		$creations           = new MV_DBI( 'mv_creations' );
		$last_plugin_version = get_option( 'mv_create_version', Plugin::VERSION );

		// add version compares here
		// use `Publish::selective_update_queue( $creation_ids, 'fix_name' );` to selectively update
		// add an action in the plugin `init` method under `fixes` where the action name is `mv_[fix_name]_queue_action`

		// FIX VIDEO DESCRIPTIONS -- Remove May 2020
		if ( version_compare( $last_plugin_version, '1.5.4', '<' ) ) {
			// get creation IDS
			$args = [
				'select' => [ 'id' ],
				'limit'  => 10000,
			];
			$ids  = array_values( wp_list_pluck( $creations->find( $args ), 'id' ) );
			if ( ! empty( $ids ) ) {
				Publish::selective_update_queue( $ids, 'fix_video_description' );
			}
		}
	}

	/**
	 * Republishes create cards depending on plugin version
	 *
	 * Always check for less than current version as this is run before the version is updated
	 * Add estimated removal date (6 months) so we don't clutter code with future publishes
	 * Remove code within this function, but don't remove this function
	 *
	 * @return  void
	 */
	public function republish_queue() {
		global $wpdb;
		$creations           = new \Mediavine\MV_DBI( 'mv_creations' );
		$last_plugin_version = get_option( 'mv_create_version', Plugin::VERSION );

		// Update JSON-LD with correct contentUrl data for videos in cards (Remove November 2019)
		if ( version_compare( $last_plugin_version, '1.3.22', '<' ) ) {
			$args = [
				'select' => [ 'id' ],
				'limit'  => 10000,
			];
			$ids  = array_values( wp_list_pluck( $creations->find( $args ), 'id' ) );
			if ( ! empty( $ids ) ) {
				\Mediavine\Create\Publish::update_publish_queue( $ids );
			}
		}

		// Add associated_posts key to creations that are embedded in posts as `mv_recipe` shortcodes (remove September 2019)
		if ( version_compare( $last_plugin_version, '1.3.13', '<' ) ) {
			$statement = "SELECT ID, post_content
				FROM {$wpdb->posts}
				WHERE post_content LIKE '%[mv_recipe%'";
			$posts     = $wpdb->get_results( $statement );
			$recipes   = [];
			if ( $posts ) {
				foreach ( $posts as $post ) {
					$re = '/\[mv_recipe[\s\S]post_id="(\d+)"/';
					if ( preg_match( $re, $post->post_content, $match ) ) {
						if ( ! empty( $match[1] ) ) {
							$recipes[ $match[1] ][] = $post->ID;
						}
					}
				}
			}
			if ( $recipes ) {
				foreach ( $recipes as $recipe_id => $post_ids ) {
					$creation            = $creations->find_one(
						[
							'col' => 'original_object_id',
							'key' => $recipe_id,
						]
					);
					$original_associated = json_decode( $creation->associated_posts, true );
					$original_associated = ! empty( $original_associated ) && is_array( $original_associated ) ? $original_associated : [];
					$associated_posts    = array_merge( $post_ids, $original_associated );
					$creations->update(
						[
							'id'               => $creation->id,
							'associated_posts' => wp_json_encode( $associated_posts ),
						]
					);
				}
			}
		}
	}

	/**
	 * Display importer download admin notice
	 *
	 * @return void
	 */
	public function importer_admin_notice_display() {
		printf(
			'<div class="notice notice-info"><p><strong>%1$s</strong></p><p>%2$s</p></div>',
			wp_kses_post( __( 'Thanks for installing Create by Mediavine!', 'mediavine' ) ),
			wp_kses_post(
				sprintf(
					/* translators: %1$s: linked importer plugin */
					__( 'If you\'re moving from another recipe plugin, you can also download and install our %1$s.', 'mediavine' ),
					'<a href="https://www.mediavine.com/mediavine-recipe-importers-download" target="_blank">' . __( 'importer plugin', 'mediavine' ) . '</a>'
				)
			)
		);
	}

	/**
	 * Display importer download admin notice if plugin not active
	 *
	 * @return void
	 */
	public function importer_admin_notice() {
		if ( ! class_exists( 'Mediavine\Create\Importer\Plugin' ) ) {
			add_action( 'admin_notices', [ $this, 'importer_admin_notice_display' ] );
		}
	}

	/**
	 * Fixes reviews that were imported from other plugins.
	 *
	 * Importers were assigning a `recipe_id` to imported reviews instead of `creation`.
	 * This caused reviews to not show up, even though they'd been imported.
	 * This method fixes that by reassigning imported reviews.
	 *
	 * Remove Apr 2019
	 *
	 * @since 1.1.1
	 *
	 * @return {void}
	 */
	public function update_reviews_table() {
		global $wpdb;
		$last_plugin_version = get_option( 'mv_create_version', Plugin::VERSION );

		if ( version_compare( $last_plugin_version, '1.2.0', '<' ) ) {
			// Not all users had the plugin when `recipe_id` was a column in the `mv_reviews` table.
			// Check for this column before trying to update it.
			$has_recipe_id_column_statement = "SHOW COLUMNS FROM {$wpdb->prefix}mv_reviews LIKE 'recipe_id'";
			$has_recipe_id_column           = $wpdb->get_row( $has_recipe_id_column_statement );
			if ( ! $has_recipe_id_column ) {
				return;
			}

			$statement = "UPDATE {$wpdb->prefix}mv_reviews a
							INNER JOIN {$wpdb->prefix}mv_reviews b on a.id = b.id
							SET a.creation = b.recipe_id
							WHERE b.recipe_id";
			$wpdb->query( $statement );
		}
	}

	/**
	 * Fixes cloned cards' ratings.
	 *
	 * Previously, cloned cards retained the originating card's `rating` and `rating_count`
	 * attributes, giving the client-facing card the appearance of its ratings having been
	 * duplicated. Resetting the count resolves this issue.
	 *
	 * Remove November 2019
	 *
	 * @since 1.3.20
	 *
	 * @return void
	 */
	public function fix_cloned_ratings() {
		global $wpdb;
		$last_plugin_version = get_option( 'mv_create_version', Plugin::VERSION );

		if ( version_compare( $last_plugin_version, '1.3.20', '<' ) ) {
			$creations_with_ratings = $wpdb->get_results(
				"SELECT id as creation FROM {$wpdb->prefix}mv_creations WHERE rating AND rating_count;"
			);
			$model                  = new Reviews_Models();
			foreach ( $creations_with_ratings as $review ) {
				$model->update_creation_rating( $review );
			}
		}
	}

	/**
	 * Fixes canonical post ids of imported Cookbook recipes.
	 *
	 * Recipes imported from Cookbook were using the Cookbook recipe id as the canonical_post_id.
	 * Obviously, this was not good, so we need to fix that.
	 *
	 * Remove December 2019
	 *
	 * @since 1.4.6
	 *
	 * @return void
	 */
	public function fix_cookbook_canonical_post_ids() {
		global $wpdb;
		$last_plugin_version = get_option( 'mv_create_version', Plugin::VERSION );

		if ( version_compare( $last_plugin_version, '1.4.6', '<' ) ) {
			$creations = $wpdb->get_results(
				"SELECT * FROM {$wpdb->prefix}mv_creations WHERE type='recipe' AND metadata LIKE '%cookbook%' AND metadata NOT LIKE '%fixed_canonical_post_id%'",
				ARRAY_A
			);
			$ids       = [];
			foreach ( $creations as $creation ) {
				$post     = get_post( $creation['canonical_post_id'] );
				$metadata = json_decode( $creation['metadata'] );
				$posts    = json_decode( $creation['associated_posts'] );
				if ( 'cookbook_recipe' === $post->post_type && ! empty( $posts ) ) {
					$creation['canonical_post_id'] = $posts[0];
				}
				$metadata->fixed_canonical_post_id = true;
				$creation['metadata']              = wp_json_encode( $metadata );
				self::$models_v2->mv_creations->update( $creation );
				$ids[] = $creation->id;
			}
			\Mediavine\Create\Publish::update_publish_queue( $ids );
		}
	}

	/**
	 * Ensures all current versions of create cards store a revision.
	 *
	 * Remove January 2020
	 *
	 * @since 1.4.11
	 *
	 * @return void
	 */
	public function add_initial_revision_to_cards() {
		$last_plugin_version = get_option( 'mv_create_version', Plugin::VERSION );

		if ( version_compare( $last_plugin_version, '1.4.11', '<' ) ) {
			Publish::add_all_to_publish_queue();
		}
	}

	/**
	 * Queues up all currently existing Amazon products after the API changes
	 *
	 * Remove May 2020
	 *
	 * @since 1.5.1
	 *
	 * @return void
	 */
	public function queue_existing_amazon_products() {
		$last_plugin_version = get_option( 'mv_create_version', Plugin::VERSION );

		if ( version_compare( $last_plugin_version, '1.5.4', '<' ) ) {
			$Products = Products::get_instance();
			$Products->initial_queue_products();
		}
	}

	/**
	 * Extend default REST API with useful data.
	 *
	 * @param [object] $data the current object outbound to rest response
	 * @param [object] $post post object for use in the outbound response
	 * @param [object] $request the wp rest request object.
	 * @return [object] update $data object
	 */
	public function rest_prepare_post( $data, $post, $request ) {
		$_data                        = $data->data;
		$_data['mv']                  = [];
		$_data['mv']['thumbnail_id']  = null;
		$_data['mv']['thumbnail_uri'] = null;

		$thumbnail_id = get_post_thumbnail_id( $post->ID );

		if ( empty( $thumbnail_id ) ) {
			$data->data = $_data;
			return $data;
		}

		$thumbnail                   = wp_get_attachment_image_src( $thumbnail_id, 'medium' );
		$_data['mv']['thumbnail_id'] = $thumbnail_id;

		if ( isset( $thumbnail[0] ) ) {
			$_data['mv']['thumbnail_uri'] = $thumbnail[0];
		}

		$data->data = $_data;
		return $data;
	}
}

$Plugin = Plugin::get_instance();
$Plugin->init();

$Images = new Images();
$Images->init();

$Settings = new \Mediavine\Settings();
$Settings->init();

$Nutrition = new Nutrition();
$Nutrition->init();

$Products = new Products();
$Products->init();

$Products_Map = new Products_Map();
$Products_Map->init();

$Relations = new Relations();
$Relations->init();

$Reviews_Models = new Reviews_Models();
$Reviews_Models->init();

$Reviews_API = new Reviews_API();
$Reviews_API->init();

$Reviews = new Reviews();
$Reviews->init();

$Shapes    = Shapes::get_instance();
$Creations = Creations::get_instance();
$Supplies  = Supplies::get_instance();

$Images->step_queue();

$Revisions = Revisions::get_instance();

$Admin_Init = new Admin_Init();
$Admin_Init->init();

$Plugin_Checker = Plugin_Checker::get_instance();

$Theme_Checker = Theme_Checker::get_instance();

