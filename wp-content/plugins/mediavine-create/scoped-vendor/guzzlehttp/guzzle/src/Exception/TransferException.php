<?php

namespace Mediavine\Create\GuzzleHttp\Exception;

class TransferException extends \RuntimeException implements \Mediavine\Create\GuzzleHttp\Exception\GuzzleException
{
}
