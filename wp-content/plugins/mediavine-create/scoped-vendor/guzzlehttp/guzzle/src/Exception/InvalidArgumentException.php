<?php

namespace Mediavine\Create\GuzzleHttp\Exception;

final class InvalidArgumentException extends \InvalidArgumentException implements \Mediavine\Create\GuzzleHttp\Exception\GuzzleException
{
}
