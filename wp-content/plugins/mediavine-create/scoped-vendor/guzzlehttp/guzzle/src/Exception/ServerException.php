<?php

namespace Mediavine\Create\GuzzleHttp\Exception;

/**
 * Exception when a server error is encountered (5xx codes)
 */
class ServerException extends \Mediavine\Create\GuzzleHttp\Exception\BadResponseException
{
}
