<?php

namespace Mediavine\Create\GuzzleHttp\Exception;

class TooManyRedirectsException extends \Mediavine\Create\GuzzleHttp\Exception\RequestException
{
}
