<?php
/**
 * The plugin bootstrap file
 *
 * @link              https://www.mediavine.com/
 * @since             1.0.0
 *
 * @wordpress-plugin
 * Plugin Name:       Create by Mediavine
 * Plugin URI:        https://www.mediavine.com/mediavine-create/
 * Description:       Create custom recipe cards to be displayed in posts.
 * Version:           1.6.2

 * Author:            Mediavine
 * Author URI:        https://www.mediavine.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mediavine
 * Domain Path:       /languages
 */

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'This plugin requires WordPress' );
}

/**
 * Checks for a minimum version
 *
 * @param int|string $minimum Minimum version to check
 * @param int|string $compare 'php' to check against PHP, 'wp' to check against WP, or a specific
 *                            value to check against
 * @return boolean True if the version is compatible
 */
function mv_create_is_compatible_check( $minimum, $compare = 0 ) {
	if ( 'php' === $compare ) {
		$compare = PHP_VERSION;
	}
	if ( 'wp' === $compare ) {
		global $wp_version;
		$compare = $wp_version;
	}

	if ( version_compare( $compare, $minimum, '<' ) ) {
		return false;
	}

	return true;
}

/**
 * Checks if Create is compatible
 *
 * @param boolean $return_errors Should the errors found be returned instead of false
 * @return boolean|array True if compatible. False or array of errors if not compatible
 */
function mv_create_is_compatible( $return_errors = false ) {
	$minimum_wp      = '4.7';
	$minimum_php     = '5.5.38';
	$deprecated_php  = '5.6.40'; // WP requires 5.6.20, but that's not the last version of EOL PHP 5.6
	$recommended_php = '7.3';
	$errors          = array();
	if ( ! mv_create_is_compatible_check( $minimum_php, 'php' ) ) {
		$errors['php']             = $minimum_php;
		$errors['recommended_php'] = $recommended_php;
	}
	if ( ! mv_create_is_compatible_check( $minimum_wp, 'wp' ) ) {
		$errors['wp'] = $minimum_wp;
	}
	if ( $return_errors ) {
		if ( ! mv_create_is_compatible_check( $deprecated_php, 'php' ) ) {
			$errors['deprecated_php']  = $deprecated_php;
			$errors['recommended_php'] = $recommended_php;
		}
	}

	if ( ! empty( $errors ) ) {
		if ( $return_errors ) {
			return $errors;
		}
		return false;
	}

	return true;
}

/**
 * Displays a WordPress admin error notice
 *
 * @param string $message Message to display in notice
 * @return void
 */
function mv_create_admin_error_notice( $message ) {
	printf(
		'<div class="notice notice-error"><p>%1$s</p></div>',
		wp_kses(
			$message,
			array(
				'strong' => array(),
				'code'   => array(),
				'br'     => array(),
				'a'      => array(
					'href'   => true,
					'target' => true,
				),
			)
		)
	);
}

/**
 * Adds incompatibility notices to admin if WP or PHP needs to be updated
 *
 * @return void
 */
function mv_create_incompatible_notice() {
	$compatible_errors = mv_create_is_compatible( true );
	$deactivate_plugin = false;
	if ( is_array( $compatible_errors ) ) {
		if ( isset( $compatible_errors['php'] ) ) {
			$notice = sprintf(
				// translators: Required PHP version number; Recommended PHP version number; Current PHP version number; Link to learn about updating PHP
				__( '<strong>Create by Mediavine</strong> requires PHP version %1$s or higher, but recommends %2$s or higher. This site is running PHP version %3$s. %4$s.', 'mediavine' ),
				$compatible_errors['php'],
				$compatible_errors['recommended_php'],
				PHP_VERSION,
				'<a href="https://wordpress.org/support/update-php/" target="_blank">' . __( 'Learn about updating PHP', 'mediavine' ) . '</a>'
			);
			mv_create_admin_error_notice( $notice );
			$deactivate_plugin = true;
		}
		if ( isset( $compatible_errors['wp'] ) ) {
			global $wp_version;
			$notice = sprintf(
				// translators: Required WP version number; Current WP version number
				__( '<strong>Create by Mediavine</strong> requires WordPress %1$s or higher. This site is running WordPress %2$s. Please update WordPress to activate <strong>Create by Mediavine</strong>.', 'mediavine' ),
				$compatible_errors['wp'],
				$wp_version
			);
			mv_create_admin_error_notice( $notice );
			$deactivate_plugin = true;
		}
		if ( isset( $compatible_errors['deprecated_php'] ) ) {
			$notice = sprintf(
				// translators: Required PHP version number; Recommended PHP version number; Current PHP version number; Link to learn about updating PHP
				__( 'A future version of <strong>Create by Mediavine</strong> will require PHP version %1$s, but recommends %2$s or higher. This site is running PHP version %3$s. To maintain compatibility with <strong>Create by Mediavine</strong>, please upgrade your PHP version. %4$s.', 'mediavine' ),
				$compatible_errors['deprecated_php'],
				$compatible_errors['recommended_php'],
				PHP_VERSION,
				'<a href="https://wordpress.org/support/update-php/" target="_blank">' . __( 'Learn about updating PHP', 'mediavine' ) . '</a>'
			);
			mv_create_admin_error_notice( $notice );
		}

		// Should we deactivate the plugin?
		if ( $deactivate_plugin ) {
			mv_create_admin_error_notice( __( '<strong>Create by Mediavine</strong> has been deactivated.', 'mediavine' ) );
			deactivate_plugins( plugin_basename( __FILE__ ) );
			return;
		}
	}
}

function mv_create_throw_warnings() {
	$compatible    = true;
	$missing_items = array();
	if ( ! extension_loaded( 'mbstring' ) ) {
		$missing_items[] = 'php-mbstring';
		$compatible      = false;
	}
	if ( ! extension_loaded( 'xml' ) ) {
		$missing_items[] = 'php-xml';
		$compatible      = false;
	}
	if ( $compatible || empty( $missing_items ) ) {
		return;
	}

	$message = trim( implode( ', ', $missing_items ), ', ' );

	$notice = sprintf(
		// translators: a list of disabled PHP extensions
		__( '<strong>Create by Mediavine</strong> requires the following disabled PHP extensions in order to function properly: <code>%1$s</code>.<br/><br/>Your hosting environment does not currently have these enabled.<br/><br/>Please contact your hosting provider and ask them to ensure these extensions are enabled.', 'mediavine' ),
		$message
	);

	mv_create_admin_error_notice( $notice );
	return;
}

function mv_create_add_action_links( $links ) {
	$create_links = array(
		'<a href="' . admin_url( 'options-general.php?page=mv_settings' ) . '">Settings</a>',
	);
	if ( class_exists( 'MV_Control_Panel' ) || class_exists( 'MVCP' ) ) {
		$create_links[] = '<a href="https://help.mediavine.com">Support</a>';
	}

	return array_merge( $links, $create_links );
}

function mv_create_plugin_info_links( $links, $file ) {
	if ( strpos( $file, 'mediavine-create.php' ) !== false ) {
		$new_links = array(
			'importers' => '<a href="https://www.mediavine.com/mediavine-recipe-importers-download" target="_blank">Download Mediavine Recipe Importers Plugin</a>',
		);
		$links     = array_merge( $links, $new_links );
	}

	return $links;
}

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'mv_create_add_action_links' );
add_filter( 'plugin_row_meta', 'mv_create_plugin_info_links', 10, 2 );

add_action( 'admin_notices', 'mv_create_incompatible_notice' );
add_action( 'admin_head', 'mv_create_throw_warnings' );

if ( mv_create_is_compatible() ) {
	require_once( __DIR__ . '/lib/functions.php' );
	require_once( __DIR__ . '/class-plugin.php' );
}
