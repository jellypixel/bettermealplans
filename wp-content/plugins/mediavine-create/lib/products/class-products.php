<?php

namespace Mediavine\Create;

use Mediavine\Settings;

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'This plugin requires WordPress' );
}

if ( class_exists( 'Mediavine\Create\Plugin' ) ) {

	class Products extends Plugin {

		public static $instance = null;

		public $api_root = 'mv-create';

		public $api = null;

		public $api_version = 'v1';

		private $table_name = 'mv_products';

		public $amazon_queue;

		public $schema = [
			'title'                  => 'text',
			'link'                   => 'text',
			'thumbnail_id'           => 'bigint(20)',
			'asin'                   => 'varchar(10)',
			'external_thumbnail_url' => 'text',
			'expires'                => 'datetime',
		];

		public $singular = 'product';

		public $plural = 'product';
		private $amazon;

		public static function get_instance() {
			if ( null === self::$instance ) {
				self::$instance = new self;
				self::$instance->init();
			}
			return self::$instance;
		}

		public static function prepare_product_thumbnail( $product ) {
			if ( empty( $product['thumbnail_id'] ) && isset( $product['external_thumbnail_url'] ) ) {
				return $product;
			}

			// If the type is the product map, we need to get the true product's asin
			if ( ! empty( $product['type'] ) && 'product_map' === $product['type'] ) {
				$true_product = self::$models_v2->mv_products->select_one( $product['product_id'] );

				if ( property_exists( $true_product, 'asin' ) ) {
					$product['asin'] = $true_product->asin;
				}
			}

			// Attempt to create a new thumbnail, but only if no ASIN
			if ( isset( $product['remote_thumbnail_uri'] ) && empty( $product['asin'] ) ) {
				// Some results won't include protocol -or- use relative URLs, so we coerce these to absolute URLs.
				if ( strpos( $product['remote_thumbnail_uri'], 'http' ) === false ) {
					if ( strpos( $product['remote_thumbnail_uri'], '//' ) === 0 ) { // Only catch at beginning
						$product['remote_thumbnail_uri'] = 'http:' . $product['remote_thumbnail_uri'];
					} else {
						$parsed_url                      = parse_url( $product['remote_thumbnail_uri'] );
						$product['remote_thumbnail_uri'] = 'http://' . $parsed_url['host'] . $product['remote_thumbnail_uri'];
					}
				}

				$thumbnail_id            = Images::get_attachment_id_from_url( $product['remote_thumbnail_uri'] );
				$product['thumbnail_id'] = $thumbnail_id;
			}

			return $product;
		}

		public static function restore_product_images( $creation ) {
			if ( empty( $creation ) ) {
				return $creation;
			}

			$metadata = json_decode( $creation->metadata, true );
			if ( empty( $metadata ) ) {
				$metadata = [];
			}

			if ( isset( $metadata['product_images_restored'] ) && $metadata['product_images_restored'] ) {
				return $creation;
			}

			$products = self::$models_v2->mv_products_map->find(
				[
					'where' => [
						'creation' => $creation->id,
					],
				]
			);

			$scraper = new \Mediavine\Create\LinkScraper();
			$changed = false;
			foreach ( $products as $product ) {
				if ( $product->thumbnail_id ) {
					continue;
				}

				if ( ! isset( $product->link ) ) {
					continue;
				}

				$data = $scraper->scrape( $product->link );
				if ( ! isset( $data['remote_thumbnail_uri'] ) ) {
					continue;
				}
				$product->remote_thumbnail_uri = $data['remote_thumbnail_uri'];
				unset( $product->thumbnail_id );

				$product = self::prepare_product_thumbnail( (array) $product );
				$updated = self::$models_v2->mv_products_map->update( (array) $product );
				if ( $updated ) {
					$changed = true;
				}
			}
			if ( $changed ) {
				$metadata['product_images_restored'] = true;
				$creation->metadata                  = wp_json_encode( $metadata );
				$creation                            = self::$models_v2->mv_creations->update( (array) $creation );
				return \Mediavine\Create\Creations::publish_creation( $creation->id );
			}
			return $creation;
		}

		function init() {
			$this->amazon_queue = new Queue(
				[
					'transient_name' => 'mv_create_amazon_queue',
					'queue_name'     => 'mv_create_amazon_queue',
					'lock_timeout'   => 600,
					'auto_unlock'    => false,
				]
			);
			$this->amazon       = Amazon::get_instance();
			$this->api          = new Products_API();

			add_filter( 'mv_custom_schema', [ $this, 'custom_schema' ] );
			add_action( 'rest_api_init', [ $this, 'routes' ] );
			add_filter( 'mv_dbi_after_update_' . $this->table_name, [ $this, 'cascade_after_update' ] );
			add_action( 'init', [ $this, 'refresh_product_images' ] );
			add_action( 'init', [ $this, 'step_amazon_queue' ] );
			add_action( 'mv_create_setting_updated_mv_create_paapi_secret_key', [ $this, 'lock_amazon_queue' ] );
		}

		public function refresh_product_images() {
			remove_action( 'mv_dbi_after_update_mv_products', [ Products::get_instance(), 'cascade_after_update' ] );
			$transient = 'mv_amazon_expiring_products';
			if ( get_transient( $transient ) ) {
				return false;
			}
			$THREE_HOURS       = 3 * 60 * 60;
			$amazon_rate_limit = apply_filters( 'mv_create_amazon_rate_limit', $THREE_HOURS );
			$expiring          = $this->get_expiring_products( $amazon_rate_limit );
			if ( empty( $expiring ) ) {
				return false;
			}
			$expiring = array_column( $expiring, 'id' );
			$this->amazon_queue->push_many( $expiring );

			set_transient( $transient, time(), $amazon_rate_limit );
		}

		public function lock_amazon_queue() {
			$timeout = 2 * DAY_IN_SECONDS;
			$this->amazon_queue->lock( $timeout );

			// Also set provision transient
			$transient = 'mv_create_amazon_provision';
			set_transient( $transient, true, $timeout );

			// Clear transient complete setting
			Settings::delete_setting( $transient . '_complete' );
		}

		public function step_amazon_queue() {
			// Only run the queue if Amazon is setup
			if ( $this->amazon->amazon_affiliates_setup() ) {
				return $this->amazon_queue->step( [ $this, 'build_amazon_data' ] );
			}
		}

		public function initial_queue_products() {
			global $wpdb;
			$table       = self::$models_v2->mv_products->table_name;
			$query       = "SELECT id FROM $table WHERE link LIKE '%amazon.%'";
			$product_ids = $wpdb->get_col( $query );
			if ( empty( $product_ids ) ) {
				return;
			}

			$this->amazon_queue->push_many( $product_ids );
		}

		public function build_amazon_data( $product_id ) {
			$product = (array) self::$models_v2->mv_products->select_one_by_id( $product_id );
			if ( empty( $product ) ) {
				return false;
			}
			if ( is_wp_error( $product ) ) {
				return false;
			}
			if ( empty( $product['asin'] ) ) {
				$product['asin'] = $this->amazon->get_asin_from_link( $product['link'] );
			}
			$result = $this->amazon->get_products_by_asin( $product['asin'] );

			// Move on if empty or is an error
			if ( empty( $result ) || is_wp_error( $result ) ) {
				return false;
			}

			$product['external_thumbnail_url'] = $result[ $product['asin'] ]['external_thumbnail_url'];
			$product['expires']                = $result[ $product['asin'] ]['expires'];
			self::$models_v2->mv_products->update( $product );
		}

		public function cascade_after_update( $product ) {
			global $wpdb;

			$update_values = [];

			if ( isset( $product->title ) ) {
				$update_values['title'] = $product->title;
			}

			// We want to update null values as well, so checking if property exists
			if ( property_exists( $product, 'thumbnail_id' ) ) {
				$update_values['thumbnail_id'] = $product->thumbnail_id;
			}

			if ( isset( $product->link ) ) {
				$update_values['link'] = $product->link;
			}

			add_filter( 'query', [ self::$models_v2->mv_products, 'allow_null' ] );
			$updated = $wpdb->update(
				$wpdb->prefix . 'mv_products_map',
				$update_values,
				[ 'product_id' => $product->id ]
			);
			remove_filter( 'query', [ self::$models_v2->mv_products, 'allow_null' ] );

			$result = self::$models_v2->mv_products_map->find(
				[
					'select' => [ 'creation' ],
					'where'  => [
						'product_id' => $product->id,
					],
				]
			);

			$ids = [];

			foreach ( $result as $item ) {
				$ids[] = $item->creation;
			}

			\Mediavine\Create\Publish::update_publish_queue( $ids );

			return $product;
		}

		public function custom_schema( $tables ) {
			$tables[] = [
				'version'    => self::DB_VERSION,
				'table_name' => $this->table_name,
				'schema'     => $this->schema,
			];
			return $tables;
		}

		/**
		 * Given a product id, return array of creations using that product
		 * @param  int   $product_id
		 * @return array             Array of [id, title] arrays
		 */
		public static function get_product_creations( $product_id ) {
			global $wpdb;
			$creations    = $wpdb->prefix . 'mv_creations';
			$products_map = $wpdb->prefix . 'mv_products_map';
			$sql          = "SELECT $creations.type, $creations.object_id, $creations.id, $creations.title FROM $creations JOIN $products_map ON $creations.id = $products_map.creation WHERE $products_map.product_id = %d;";
			$prepared     = $wpdb->prepare( $sql, $product_id );
			$creations    = $wpdb->get_results( $prepared );
			return count( $creations ) ? $creations : [];
		}

		/**
		 * Get products whose Amazon images are expiring within a certain timeframe.
		 *
		 * @param integer $within time in seconds from now--default is 3 hours
		 * @param integer $limit how many products to query at one time
		 * @return array of products expiring
		 */
		function get_expiring_products( $within = 10800, $limit = 50 ) {
			$timestamp = date( 'Y-m-d H:i:s', strtotime( "+{$within} seconds" ) );
			$products  = self::$models_v2->mv_products->where(
				[
					// make sure the product is an Amazon link and has an expiration
					[ 'asin', 'IS NOT', 'NULL' ],
					[ 'expires', 'IS NOT', 'NULL' ],
					// and that the expiration is $within the $timestamp
					[ 'expires', '<', $timestamp ],
					'ORDER BY expires ASC LIMIT ' . $limit,
				]
			);
			return $products;
		}

		function routes() {
			$namespace = $this->api_root . '/' . $this->api_version;

			register_rest_route(
				$namespace, '/products', [
					[
						'methods'             => \WP_REST_Server::READABLE,
						'callback'            => function( \WP_REST_Request $request ) {
							return \Mediavine\API_Services::middleware(
								[
									[ self::$api_services, 'process_pagination' ],
									[ $this->api, 'find' ],
								], $request
							);
						},
						'permission_callback' => [ self::$api_services, 'permitted' ],
					],
					[
						'methods'             => \WP_REST_Server::EDITABLE,
						'callback'            => function( \WP_REST_Request $request ) {
							return \Mediavine\API_Services::middleware(
								[
									[ $this->api, 'upsert' ],
								], $request
							);
						},
						'permission_callback' => [ self::$api_services, 'permitted' ],
					],
				]
			);

			register_rest_route(
				$namespace, '/products/scrape', [
					[
						'methods'             => \WP_REST_Server::EDITABLE,
						'callback'            => function ( \WP_REST_Request $request ) {
							return \Mediavine\API_Services::middleware(
								[
									[ $this->api, 'scrape' ],
								],
								$request
							);
						},
						'permission_callback' => [ self::$api_services, 'permitted' ],
					],
				]
			);

			register_rest_route(
				$namespace, '/products/reset-amazon-thumbnails', [
					[
						'methods'             => \WP_REST_Server::EDITABLE,
						'callback'            => function ( \WP_REST_Request $request ) {
							return \Mediavine\API_Services::middleware(
								[
									[ $this->api, 'reset_amazon_thumbnails' ],
								],
								$request
							);
						},
						'permission_callback' => [ self::$api_services, 'permitted' ],
					],
				]
			);

			register_rest_route(
				$namespace, '/products/reset-amazon-provision', [
					[
						'methods'             => \WP_REST_Server::EDITABLE,
						'callback'            => function ( \WP_REST_Request $request ) {
							return \Mediavine\API_Services::middleware(
								[
									[ $this->api, 'reset_amazon_provision' ],
								],
								$request
							);
						},
						'permission_callback' => [ self::$api_services, 'permitted' ],
					],
				]
			);

			register_rest_route(
				$namespace, '/products/(?P<id>\d+)', [
					[
						'methods'             => \WP_REST_Server::READABLE,
						'callback'            => function( \WP_REST_Request $request ) {
							return \Mediavine\API_Services::middleware(
								[
									[ $this->api, 'find_one' ],
									[ $this->api, 'get_pagination_links' ],
								], $request
							);
						},
						'permission_callback' => [ self::$api_services, 'permitted' ],
					],
					[
						'methods'             => \WP_REST_Server::DELETABLE,
						'callback'            => function( \WP_REST_Request $request ) {
							return \Mediavine\API_Services::middleware(
								[
									[ $this->api, 'destroy' ],
								], $request
							);
						},
						'permission_callback' => [ self::$api_services, 'permitted' ],
					],
					[
						'methods'             => \WP_REST_Server::EDITABLE,
						'callback'            => function ( \WP_REST_Request $request ) {
							return \Mediavine\API_Services::middleware(
								[
									[ $this->api, 'upsert' ],
								],
								$request
							);
						},
						'permission_callback' => [ self::$api_services, 'permitted' ],
					],
				]
			);

		}
	}
}
