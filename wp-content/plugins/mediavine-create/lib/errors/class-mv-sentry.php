<?php

namespace Mediavine\Create;

use Mediavine\Settings;

// Prevent direct access
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'This plugin requires WordPress' );
}

if ( class_exists( 'Mediavine\Create\Plugin' ) ) {
	class MV_Sentry {

		public $client;

		function __construct() {
			$this->client = $this->get_client();
		}

		private function get_client() {
			$dsn     = 'https://4df8f478a4c747d69b827c939337a0ca@sentry.io/1287304';
			$options = [
				'release' => \Mediavine\Create\Plugin::VERSION,
			];

			return new \Raven_Client( $dsn, $options );
		}

		public static function init_debugging() {
			if ( ! mv_create_table_exists( 'mv_settings' ) ) {
				return;
			}
			$debugging_enabled = Settings::get_setting( 'mv_create_enable_debugging', false );

			if ( ! $debugging_enabled ) {
				add_option( 'mv_create_enable_debugging', false );
				return;
			}

			return ( new static )->client->install();
		}

		public static function log_exception( \Exception $e, $context = [] ) {
			if ( ! mv_create_table_exists( 'mv_settings' ) ) {
				return;
			}
			$logging_enabled = Settings::get_setting( 'mv_create_enable_logging', false );

			if ( ! $logging_enabled ) {
				return;
			}

			if ( \wp_is_numeric_array( $context ) ) {
				$context = array_flip( $context );
			}

			$data = [
				'extra' => $context,
			];

			return ( new static )->client->captureException( $e, $data );
		}

		public static function log( $message, $params = [], $context = [], $level = 'info' ) {
			if ( ! mv_create_table_exists( 'mv_settings' ) ) {
				return;
			}
			$logging_enabled = Settings::get_setting( 'mv_create_enable_logging', false );

			if ( ! $logging_enabled ) {
				return;
			}

			$stack = 'debug' !== $level && 'info' !== $level;

			if ( \wp_is_numeric_array( $context ) ) {
				$context = array_flip( $context );
			}

			$data = [
				'level' => $level,
				'extra' => $context,
			];

			return ( new static )->client->captureMessage( $message, $params, $data, $stack );
		}
	}
}
