<?php
// Load i18n
function mv_create_load_plugin_textdomain() {
	load_plugin_textdomain( 'mediavine', false, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'mv_create_load_plugin_textdomain' );

/**
 * Determine whether or not a table exists.
 *
 * @param string $table_name
 * @param string $prefix
 * @return bool
 */
function mv_create_table_exists( $table_name, $prefix = '' ) {
	global $wpdb;
	$table_name = ( $prefix ? $prefix : $wpdb->prefix ) . $table_name;
	$statement  = "SHOW TABLES LIKE '%{$table_name}%'";

	return ! empty( $wpdb->get_results( $statement ) );
}

/**
 * Manually log an error in Sentry.
 *
 * @param string $message the message we want to log (can be formatted with `print_f` style placeholders)
 * @param array $message_params if `$message` is formatted, this is an array of values to replace format markers
 * @param array $data an array of contextual data -- must be associative, not numeric
 * @param string $level the log level (debug, info, warning, error, fatal)
 *
 * Example: mv_create_log( 'this is a %', ['serious problem'], ['someVar' => 'had an issue'], $level = 'error');
 * This will produce a sentry report with the message "this is a serious problem" and a full stack trace.
 *
 * @return string uuid of Sentry event
 */
function mv_create_log( $message, $message_params = [], $data = [], $level = 'info' ) {
	// ensure this function can be used anywhere in the plugin
	if ( ! class_exists( 'Mediavine\Create\MV_Sentry' ) ) {
		require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
	}

	return \Mediavine\Create\MV_Sentry::log( $message, $message_params, $data, $level );
}

/**
 * Manually log an exception in Sentry.
 *
 * @param \Exception $e
 * @param array $data contextual data -- must be associative, not numeric
 * @return string uuid of Sentry event
 */
function mv_create_log_exception( \Exception $e, $data = [] ) {
	// ensure this function can be used anywhere in the plugin
	if ( ! class_exists( 'Mediavine\Create\MV_Sentry' ) ) {
		require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
	}

	return \Mediavine\Create\MV_Sentry::log_exception( $e, $data );
}

/**
 * Gets a list of reviews by creation_id
 * @param integer $creation_id ID of Creation from which you want reviews.
 * @param array $args limit and offset to get max number or paginate (default 50, 0)
 * @return array Returns an array of objects
 */
function mv_create_get_reviews( $creation_id, $args = [] ) {
	return \Mediavine\Create\Reviews::get_reviews( $creation_id, $args );
}

/**
 * Gets a list of Creation IDs associated with a Post ID
 * @param integer $post_id ID of WP Post from which you want a list of Associated Creations.
 * @return array Returns an array of objects
 */
function mv_get_post_creations( $post_id ) {
	return \Mediavine\Create\Creations::get_creation_ids_by_post( $post_id );
}

/**
 * Gets a single creation by ID
 * @param  {number}  $id        Creation ID
 * @param  {boolean} $published Return published data
 * @return {object}             Card data
 */
function mv_create_get_creation( $id, $published = false ) {
	$creations_dbi = new \Mediavine\MV_DBI( 'mv_creations' );
	$creation      = $creations_dbi->find_one_by_id( $id );
	if ( $published ) {
		$published_content = '[]';
		if ( is_array( $creation ) && isset( $creation['published'] ) ) {
			$published_content = $creation['published'];
		}
		if ( is_object( $creation ) && isset( $creation->published ) ) {
			$published_content = $creation->published;
		}
		return json_decode( $published_content );
	}
	return $creation;
}

/**
 * Get a custom field registered to a creation
 *
 * @since 1.1.0
 * @param {string} $slug   Custom field slug
 * @param {number} $id     Creation ID
 * @param {mixed}          Value of field
 */
function mv_create_get_field( $id, $slug ) {
	$creation      = mv_create_get_creation( $id );
	$custom_fields = $creation->custom_fields;
	$parsed_data   = json_decode( $custom_fields );
	if ( empty( $parsed_data ) || empty( $parsed_data[ $slug ] ) ) {
		return null;
	}
	return $parsed_data[ $slug ];
}

/**
 * Declares that a theme supports integration with a particular version of Create skins.
 * (For now, if a theme integrates, just pass 'v1')
 *
 * If this is _not_ called in the theme's functions.php file, custom skins will _not_ override defaults.
 *
 * @since 1.1.0
 * @param  {string} $version  Compatible version
 * @return {void}
 */
function mv_create_theme_support( $version ) {
	add_filter(
		'mv_create_style_version',
		function() use ( $version ) {
			return $version;
		}
	);
}

/**
 * Register a custom field.
 *
 * A helper function to quickly register a custom field to the Custom Fields section of Create Cards.
 *
 * @since 1.1.0
 * @param array $field Refer to CustomFields.md for acceptable params
 * @return void
 */
function mv_create_register_custom_field( $field ) {
	add_filter(
		'mv_create_fields', function( $arr ) use ( $field ) {
			$arr[] = $field;
			return $arr;
		}
	);
}

// We go ahead and register custom fields for users
add_filter(
	'mv_create_fields', function( $arr ) {
		$arr[] = [
			'slug'         => 'class',
			'label'        => __( 'CSS Class', 'mediavine' ),
			'instructions' => __( 'Add an additional CSS class to this card.', 'mediavine' ),
			'type'         => 'text',
		];
		$arr[] = [
			'slug'         => 'mv_create_nutrition_disclaimer',
			'label'        => __( 'Custom Nutrition Disclaimer', 'mediavine' ),
			'instructions' => __( 'Example: Nutrition information isn’t always accurate.', 'mediavine' ),
			'type'         => 'textarea',
			'card'         => 'recipe',
		];
		$arr[] = [
			'slug'         => 'mv_create_affiliate_message',
			'label'        => __( 'Custom Affiliate Message', 'mediavine' ),
			'instructions' => __( 'Override the default affiliate message for this card.', 'mediavine' ),
			'type'         => 'textarea',
			'card'         => [ 'recipe', 'diy', 'list' ],
		];
		$arr[] = [
			'slug'         => 'mv_create_show_list_affiliate_message',
			'label'        => __( 'Show Custom Affiliate Message', 'mediavine' ),
			'instructions' => __( 'Check this box to display an affiliate message on this List.', 'mediavine' ),
			'type'         => 'checkbox',
			'card'         => 'list',
		];

		// Social footer overrides
		if ( \Mediavine\Settings::get_setting( 'mv_create_social_footer', false ) ) {
			$arr[] = [
				'slug'         => 'mv_create_social_footer_icon',
				'label'        => __( 'Social Footer Icon', 'mediavine' ),
				'instructions' => __( 'Override the default social footer icon for this card.', 'mediavine' ),
				'type'         => 'select',
				'defaultValue' => 'default',
				'options'      => [
					'default'   => 'Use Default',
					'facebook'  => 'Facebook',
					'instagram' => 'Instagram',
					'pinterest' => 'Pinterest',
				],
				'card'         => [ 'recipe', 'diy' ],
			];
			$arr[] = [
				'slug'         => 'mv_create_social_footer_header',
				'label'        => __( 'Social Footer Heading', 'mediavine' ),
				'instructions' => __( 'Override the default social footer heading for this card.', 'mediavine' ),
				'type'         => 'text',
				'card'         => [ 'recipe', 'diy' ],
			];
			$arr[] = [
				'slug'         => 'mv_create_social_footer_content',
				'label'        => __( 'Social Footer Content', 'mediavine' ),
				'instructions' => __( 'Override the default social footer content for this card.', 'mediavine' ),
				'type'         => 'wysiwyg',
				'card'         => [ 'recipe', 'diy' ],
			];
		}

		return $arr;
	}
);

// Register default Create settings for Creation published data
add_filter(
	'mv_publish_create_settings', function ( $arr ) {
		// Get the authenticated user to assign the copyright attribution if none has been set in settings.
		$user = wp_get_current_user();
		$arr[ \Mediavine\Create\Plugin::$settings_group . '_copyright_attribution' ] = $user->display_name;

		// Assign the default settings. These can be overwritten by using this filter.
		foreach ( \Mediavine\Create\Plugin::$create_settings_slugs as $slug ) {
			$setting = \Mediavine\Settings::get_setting( $slug );
			if ( $setting ) {
				$arr[ $slug ] = $setting;
			}
		}
		return $arr;
	}
);
