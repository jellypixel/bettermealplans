<?php

namespace Mediavine\Create;

use Mediavine\WordPress\Support\Str;
use Mediavine\WordPress\Support\Arr;

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

class Theme_Checker {

	public static $instance;

	private static $active_theme;

	private static $parent_theme = null;

	/**
	 * Makes sure class is only instantiated once and runs init
	 *
	 * @return object Instantiated class
	 */
	static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
			self::$instance->init();
		}

		return self::$instance;
	}

	/**
	 * Hooks to be run on class instantiation
	 *
	 * @return void
	 */
	public function init() {
		$this->get_active_theme();
		$this->load_rascal_theme_classes();
		add_action( 'plugins_loaded', [ $this, 'register_rascals' ] );

	}

	public function register_rascals() {
		do_action( 'mv_helpers_register_rascal_themes' );
	}

	private function get_active_theme() {
		self::$active_theme = get_option( 'stylesheet' );
		$template_directory = get_template_directory();

		if ( get_stylesheet_directory() === $template_directory ) {
			return;
		}

		$directory_parts    = explode( DIRECTORY_SEPARATOR, $template_directory );
		self::$parent_theme = array_pop( $directory_parts );

	}


	private function load_rascal_theme_classes() {
		$rascal_dir = dirname( __FILE__ ) . '/rapscallion_themes';
		foreach ( scandir( $rascal_dir ) as $rascal ) {
			if ( substr( $rascal, -4 ) !== '.php' ) {
				continue;
			}

			$rascal_class = Str::replace( '.php', '', $rascal );
			$rascal_class = Str::replace( 'class-', '', $rascal_class );
			$rascal_class = explode( '-', $rascal_class );
			$rascal_class = implode( '_', Arr::map( $rascal_class, 'ucwords' ) );

			if ( 'Rascal_Theme' === $rascal_class ) {
				continue;
			}

			$rascal_class = __NAMESPACE__ . '\\' . $rascal_class;

			if ( ! class_exists( $rascal_class ) ) {
				continue;
			}

			$rascal_instance = new $rascal_class;
			$rascal_instance->init();

		}
	}

	/**
	 * Checks based on a slug to see if a theme is active
	 *
	 * @param string $theme_slug Slug of theme to check
	 * @return bool True if theme is active, false if not found
	 */
	public static function is_theme_active( $theme_slug ) {
		if ( $theme_slug === self::$active_theme || $theme_slug === self::$parent_theme ) {
			return true;
		}

		// No evidence of theme found
		return false;
	}

}
