<?php
function twentydishes_createstepbystep () {

	$todo=$_POST["todo"];
	$priority=$_POST["priority"];
	
	//insert
	if(isset($_POST['insert'])){
		global $wpdb;
		$wpdb->insert(
			'jp_stepbystep', //table
			array('todo' => $todo, 'priority' => $priority), //data
			array('%s', '%f') //data format		
		);
		$message.="Step by Step inserted";
	}
	?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
	<h2>Add New Step by Step<a href="<?php echo admin_url('admin.php?page=twentydishes_allstepbystep')?>" class="add-new-h2">&laquo; Back to step by step list</a></h2><br>
	<?php if (isset($message)): ?><div class="updated"><p><?php echo $message;?></p></div><?php endif;?>
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class='wp-list-table widefat fixed'>
	<tr><th>To Do</th><td><input type="text" name="todo" value="<?php echo $todo;?>"/></td></tr>
	<tr><th>Priority</th><td><input type="text" name="priority" value="<?php echo $priority;?>"/></td></tr>
	</table> <br>
	<input type='submit' name="insert" value='Save' class='button'>
	</form>
	</div>
<?php
}