<?php
function twentydishes_createexcludelist () {

	$name=$_POST["name"];
	$cat_id=$_POST["cat_id"];
	
	//insert
	if(isset($_POST['insert'])){
		global $wpdb;
		$wpdb->insert(
			'jp_excludelist', //table
			array('name' => $name, 'cat_id' => $cat_id), //data
			array('%s', '%d') //data format		
		);
		$message.="Category inserted";
	}
	?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
	<h2>Add New Category to Exclude List<a href="<?php echo admin_url('admin.php?page=twentydishes_allexcludelist')?>" class="add-new-h2">&laquo; Back to exclude category list </a></h2><br>
	<?php if (isset($message)): ?><div class="updated"><p><?php echo $message;?></p></div><?php endif;?>
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class='wp-list-table widefat fixed'>
	<tr><th>Name</th><td><input type="text" name="name" value="<?php echo $name;?>"/></td></tr>
	<tr><th>Category ID</th><td><input type="text" name="cat_id" value="<?php echo $cat_id;?>"/></td></tr>
	</table> <br>
	<input type='submit' name="insert" value='Save' class='button'>
	</form>
	</div>
<?php
}