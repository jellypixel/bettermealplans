<?php
function twentydishes_generalreport () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>General Stat</h2>
		
		<?php
			global $wpdb;

			$inactive = 0;
			$active = 0;				

			// Get all diet
			$allDiets = $wpdb->get_results('SELECT * FROM jp_diet', ARRAY_A);
			
			$allDietsCount = array();
			for($i = 0; $i < count($allDiets); $i++ ){

				$allDietsCount[] = 0;
			}
				
			// Get user diet	
			$sql = "SELECT b.*, a.user_registered, IF(ISNULL(c.pricing),0,c.pricing) as 'pricing' FROM wp_users a JOIN wp_usermeta b ON a.ID = b.user_id LEFT JOIN jp_billing c ON a.id = c.user_id WHERE a.user_pass != '' and b.meta_key = 'diets'";
			$users = $wpdb->get_results($sql);	

			$currentmonth = 0; $onemonth = 0; $twomonth = 0; $threemonth = 0;
			$currentmonthprice = 0; $onemonthprice = 0; $twomonthprice = 0; $threemonthprice = 0;
			
			foreach ($users as $user ){
				
				$levels = array_values(WLMAPI::GetUserLevels($user->user_id));
				
				if(in_array('Expired', $levels)){
					$inactive++;
					continue;
				}
					
				$active++;
				$userDiets = get_user_meta($user->user_id, "diets", false); 
				$userDiets = $userDiets[0];
				
				for($i = 0; $i < count($allDiets); $i++ ){

					if( in_array($allDiets[$i]['id'], $userDiets) ) {
						
						$allDietsCount[$i]++;
						
					}
				}
				
				// Calculate Estimate
				$levels = array_values(WLMAPI::GetUserLevels($user->user_id));		
				$registime = strtotime($user->user_registered);									
				$currenttime = time();
				$billedtime = "";
				$difference = "";
				$diff = calculateMonths($registime, $currenttime);
				
				if(in_array('Monthly', $levels)) {
					$difference = 1 - ($diff % 1) + $diff;
					
					if($difference == 0)
						$difference = 1;
				}
				if(in_array('Quarterly', $levels))
					$difference = 3 - ($diff % 3) + $diff;				

				if(in_array('6 Month', $levels))
					$difference = 6 - ($diff % 6) + $diff;
					
				if(in_array('Yearly', $levels))
					$difference = 12 - ($diff % 12) + $diff;
				 	
				$billedtime = strtotime("+".$difference." month", $registime);
				$earningMonths = calculateEarningMonths($currenttime, $billedtime);
				
				if(!(($difference === "") || (count($levels) > 1 ) || (in_array('Expired', $levels)))){
					if($earningMonths >= 0)	
					{
						if($earningMonths == 0)	{
							$currentmonth++;
							$currentmonthprice += $user->pricing;
							
							if(in_array('Monthly', $levels)) {
								$onemonth++;
								$onemonthprice += $user->pricing;	

								$twomonth++;
								$twomonthprice += $user->pricing;			

								$threemonth++;
								$threemonthprice += $user->pricing;										
							}	

							if(in_array('Quarterly', $levels))	{
								$threemonth++;
								$threemonthprice += $user->pricing;					
							}						
						}				

						if($earningMonths == 1)	{
							$onemonth++;
							$onemonthprice += $user->pricing;
							
							if(in_array('Monthly', $levels)) {
								$twomonth++;
								$twomonthprice += $user->pricing;	

								$threemonth++;
								$threemonthprice += $user->pricing;									
							}								
						}	

						if($earningMonths == 2)	{
							$twomonth++;
							$twomonthprice += $user->pricing;
							
							if(in_array('Monthly', $levels)) {
								$threemonth++;
								$threemonthprice += $user->pricing;									
							}									
						}	

						if($earningMonths == 3)	{
							$threemonth++;
							$threemonthprice += $user->pricing;
						}							
					}	
				}				
			}
			
			// General Stat
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>Info</th><th>Descriptions</th></tr></thead>";
			
			echo "<tr>";
			echo "<td>Active Users</td>";
			echo "<td>$active</td>";	
			echo "</tr>";
			
			echo "<tr>";
			echo "<td>Inactive Users</td>";
			echo "<td>$inactive</td>";	
			echo "</tr>";					
			
			for($i = 0; $i < count($allDiets); $i++ ){

				echo "<tr>";
				echo "<td>".$allDiets[$i]['name']."</td>";
				echo "<td>".$allDietsCount[$i]."</td>";	
				echo "</tr>";
			}
			
			echo "</table>";
			
			// Income Prediction
			echo "<h3>Earnings Forecast</h3>";
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>Period</th><th>Estimate</th></tr></thead>";
			
			echo "<tr>";
			echo "<td>Current Month</td>";
			echo "<td>$currentmonth / $currentmonthprice</td>";	
			echo "</tr>";
			
			echo "<tr>";
			echo "<td>Next Month</td>";
			echo "<td>$onemonth / $onemonthprice</td>";	
			echo "</tr>";		

			echo "<tr>";
			echo "<td>2 Months</td>";
			echo "<td>$twomonth / $twomonthprice</td>";	
			echo "</tr>";

			echo "<tr>";
			echo "<td>3 Months</td>";
			echo "<td>$threemonth / $threemonthprice</td>";	
			echo "</tr>";			
			
			echo "</table>";			
		?>
	</div>
<?php } ?>