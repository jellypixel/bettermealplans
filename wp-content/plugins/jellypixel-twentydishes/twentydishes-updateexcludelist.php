<?php
function twentydishes_updateexcludelist () {
	global $wpdb;

	$id = $_GET["id"];
	$name=$_POST["name"];
	$cat_id=$_POST["cat_id"];
	
	$rows = $wpdb->get_results("SELECT id,name,cat_id from jp_excludelist ORDER BY name asc");
	
	if(isset($_POST['update'])){ //update	
		$wpdb->update(
			'jp_excludelist', //table
			array('name' => $name, 'cat_id' => $cat_id), //data
			array( 'id' => $id ), //where
			array('%s', '%d'), //data format
			array('%d') //where format
		);	
	}
	else if(isset($_POST['delete'])){ //delete	
		$wpdb->query($wpdb->prepare("DELETE FROM jp_excludelist WHERE id = %s", $id));
	}
	else{//selecting value to update	
		$diets = $wpdb->get_results($wpdb->prepare("SELECT id,name,cat_id from jp_excludelist where id=%s",$id));
		foreach ($diets as $s ){
			$name=$s->name;
			$cat_id=$s->cat_id;
		}
	}
?>
<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
<div class="wrap">
<h2>Update/Delete Category in Exclude List <a href="<?php echo admin_url('admin.php?page=twentydishes_allexcludelist')?>" class="add-new-h2">&laquo; Back to exclude category list</a></h2><br>

<?php if($_POST['delete']){?>
<div class="updated"><p>Category deleted</p></div>

<?php } else if($_POST['update']) {?>
<div class="updated"><p>Category updated</p></div>

<?php } else {?>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
<table class='wp-list-table widefat fixed'>
<tr><th>Name</th><td><input type="text" name="name" value="<?php echo $name;?>"/></td></tr>
<tr><th>Category ID</th><td><input type="text" name="cat_id" value="<?php echo $cat_id;?>"/></td></tr>
</table><br>
<input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
<input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Are you sure you want to delete this category?')">
</form>
<?php }?>

</div>
<?php
}