<?php
function twentydishes_reports () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>General Stat
		
		<?php
			global $wpdb;
			$inactive = $wpdb->get_results("SELECT count(*) as 'count' FROM wp_users WHERE user_pass = ''");
			$inactive = $inactive[0]->count;
			
			$active = $wpdb->get_results("SELECT count(*) as 'count' FROM wp_users WHERE user_pass != ''");
			$active = $active[0]->count;			
			
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>Info</th><th>Descriptions</th></tr></thead>";
			
			echo "<tr>";
			echo "<td>Active Users</td>";
			echo "<td>$active</td>";	
			echo "</tr>";
			
			echo "<tr>";
			echo "<td>Inactive Users</td>";
			echo "<td>$inactive</td>";	
			echo "</tr>";			

			// Get all diet
			$allDiets = $wpdb->get_results('SELECT * FROM jp_diet', ARRAY_A);
			
			$allDietsCount = array();
			for($i = 0; $i < count($allDiets); $i++ ){

				$allDietsCount[] = 0;
			}
				
			// Get user diet	
			$sql = "SELECT b.* FROM wp_users a, wp_usermeta b WHERE a.user_pass != '' and a.ID = b.user_id and b.meta_key = 'diets'";
			$users = $wpdb->get_results($sql);			
			
			foreach ($users as $user ){
				 
				$userDiets = get_user_meta($user->user_id, "diets", false); 
				$userDiets = $userDiets[0];
				
				for($i = 0; $i < count($allDiets); $i++ ){

					if( in_array($allDiets[$i]['id'], $userDiets) ) {
						
						$allDietsCount[$i]++;
						
					}
				}
			}
			
			for($i = 0; $i < count($allDiets); $i++ ){

				echo "<tr>";
				echo "<td>".$allDiets[$i]['name']."</td>";
				echo "<td>".$allDietsCount[$i]."</td>";	
				echo "</tr>";
			}
			
			echo "</table>";
		?>
	</div>
<?php } ?>