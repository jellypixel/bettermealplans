<?php
	/*
	Plugin Name: 20 Dishes
	Plugin URI: http://jelly-pixel.com
	Description: 20 Dishes system customization by Jelly-Pixel (Fei)
	Version: 1.0
	Author: Jelly-Pixel (Fei)
	Author URI: http://jelly-pixel.com
	License: GPL3
	*/

	//add colorpicker
	add_action( 'admin_enqueue_scripts', 'add_color_picker' );
	function add_color_picker( $hook ) {
	 
		if( is_admin() ) { 
			wp_enqueue_style( 'wp-color-picker' );  
		}
	}	
	
	//add ingredients taxonomy
	add_action( 'init', 'create_ingredients_taxonomy', 0 );
	function create_ingredients_taxonomy() {
 
		  $labels = array(
			 'name' => _x( 'Ingredients', 'taxonomy general name' ),
			 'singular_name' => _x( 'Ingredient', 'taxonomy singular name' ),
			 'search_items' =>  __( 'Search Ingredients' ),
			 'all_items' => __( 'All Ingredients' ),
			 'parent_item' => __( 'Parent Ingredient' ),
			 'parent_item_colon' => __( 'Parent Ingredient:' ),
			 'edit_item' => __( 'Edit Ingredient' ), 
			 'update_item' => __( 'Update Ingredient' ),
			 'add_new_item' => __( 'Add New Ingredient' ),
			 'new_item_name' => __( 'New Ingredient Name' ),
			 'menu_name' => __( 'Ingredients' ),
		  );    
		 
		  register_taxonomy('ingredient',array('post'), array(
			 'hierarchical' => true,
			 'labels' => $labels,
			 'show_ui' => true,
			 'show_admin_column' => true,
			 'query_var' => true,
			 'rewrite' => array( 'slug' => 'topic' ),
		  ));
		 
		}

	// Other function
	function calculateMonths($fromStamp, $toStamp) {
		$from = explode("-",date("Y-m-d",$fromStamp));
		$to = explode("-",date("Y-m-d",$toStamp));
		$months = ($to[0]-$from[0])*12+$to[1]-$from[1];
		if( $to[1] == $from[1] && $to[2] > $from[2]) $months--; // incomplete month
		if( $to[1] == ($from[1]+1)%12 && $to[2] < $from[2]) $months--; // other side
		
		if($months == 0)
			return 1;
		else
			return $months;
	}
	
	function calculateEarningMonths($fromStamp, $toStamp) {
		
		// First day
		$fromStamp = strtotime( 'first day of ' . date('F Y', $fromStamp));
		$toStamp = strtotime( 'first day of ' . date('F Y', $toStamp));		

		$from = explode("-",date("Y-m-d",$fromStamp));
		$to = explode("-",date("Y-m-d",$toStamp));
		$months = ($to[0]-$from[0])*12+$to[1]-$from[1];
		if( $to[1] == $from[1] && $to[2] > $from[2]) $months--; // incomplete month
		if( $to[1] == ($from[1]+1)%12 && $to[2] < $from[2]) $months--; // other side
		
		return $months;
	}	

	function sortDescending($a, $b) {
		
		return ($a["sort"] - $b["sort"]);	
	}
	

	//add scripts
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-widget');
		wp_enqueue_script('jquery-ui-autocomplete');
		wp_enqueue_script('jquery-ui-datepicker');
		
		wp_enqueue_script('plugin-script', plugins_url('script.js', __FILE__ ), array( 'wp-color-picker' ), false, true);	
		wp_enqueue_style('jquery-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
		
	//other action hooks
	add_action('wishlistmember_add_user_levels','addBilling');
	add_action('wishlistmember_remove_user_levels','removeBilling');
	
	function addBilling ($id, $levels) {
		
		global $wpdb;
		
		// Get Pricing
		$pricing = 0;
		$levels = array_values(WLMAPI::GetUserLevels($id));	
		
		$rows = $wpdb->get_results("SELECT id,name,price from jp_billingpackage ORDER BY id asc");
		foreach ($rows as $row ){
			
			if(in_array($row->name, $levels)) {
				$pricing = $row->price;
			}
		}
		
		// Save Data
		$wpdb->query($wpdb->prepare("DELETE FROM jp_billing WHERE user_id = %s", $id));
		$wpdb->insert(
			'jp_billing', //table
			array('user_id' => $id, 'pricing' => $pricing), //data
			array('%s', '%f') //data format		
		);		
	}
	
	function removeBilling ($id, $levels) {
		
		global $wpdb;
		$wpdb->query($wpdb->prepare("DELETE FROM jp_billing WHERE user_id = %s", $id));
	}	
	
	//menu items
	add_action('admin_menu','twentydishes_menu');

	function twentydishes_menu() {
		
		//this is the main item for the menu
		add_menu_page(null, //page title
		'20 Dishes', //menu title
		'nocapabilities', //capabilities
		'twentydishes_mainmenu' //menu slug
		);	
		
		//this is a submenu
		add_submenu_page('twentydishes_mainmenu', //parent slug,
		'Diets', //page title
		'Diets', //menu title
		'manage_options', //capabilities
		'twentydishes_alldiets', //menu slug
		'twentydishes_alldiets' //function
		);			
		
		add_submenu_page('twentydishes_mainmenu', //parent slug
		'Step by Step', //page title
		'Step by Step', //menu title
		'manage_options', //capability
		'twentydishes_allstepbystep', //menu slug
		'twentydishes_allstepbystep'); //function			
		
		add_submenu_page('twentydishes_mainmenu', //parent slug
		'Recipe - Step by Step', //page title
		'Recipe - Step by Step', //menu title
		'manage_options', //capability
		'twentydishes_allrecipestepbystep', //menu slug
		'twentydishes_allrecipestepbystep'); //function			

		add_submenu_page('twentydishes_mainmenu', //parent slug,
		'Billing', //page title
		'Billing', //menu title
		'manage_options', //capabilities
		'twentydishes_allbillings', //menu slug
		'twentydishes_allbillings' //function
		);				
		
		add_submenu_page('twentydishes_mainmenu', //parent slug
		'General Report', //page title
		'General Report', //menu title
		'manage_options', //capability
		'twentydishes_generalreport', //menu slug
		'twentydishes_generalreport'); //function			

		add_submenu_page('twentydishes_mainmenu', //parent slug
		'User Report', //page title
		'User Report', //menu title
		'manage_options', //capability
		'twentydishes_userreport', //menu slug
		'twentydishes_userreport'); //function			

		add_submenu_page('twentydishes_mainmenu', //parent slug
		'Import Recipe', //page title
		'Import Recipe', //menu title
		'manage_options', //capability
		'twentydishes_importrecipe', //menu slug
		'twentydishes_importrecipe'); //function		
		
		add_submenu_page('twentydishes_mainmenu', //parent slug
		'Exclude List', //page title
		'Exclude List', //menu title
		'manage_options', //capability
		'twentydishes_allexcludelist', //menu slug
		'twentydishes_allexcludelist'); //function							
		
		//this is a submenu HIDDEN
		add_submenu_page(null, //parent slug
		'Add New Diets', //page title
		'Add New Diets', //menu title
		'manage_options', //capability
		'twentydishes_creatediet', //menu slug
		'twentydishes_creatediet'); //function
		
		//this submenu is HIDDEN
		add_submenu_page(null, //parent slug
		'Update Diets', //page title
		'Update Diets', //menu title
		'manage_options', //capability
		'twentydishes_updatediet', //menu slug
		'twentydishes_updatediet'); //function	
		
		//this is a submenu HIDDEN
		add_submenu_page(null, //parent slug
		'Add New Billings', //page title
		'Add New Billings', //menu title
		'manage_options', //capability
		'twentydishes_createbilling', //menu slug
		'twentydishes_createbilling'); //function
		
		//this submenu is HIDDEN
		add_submenu_page(null, //parent slug
		'Update Billings', //page title
		'Update Billings', //menu title
		'manage_options', //capability
		'twentydishes_updatebilling', //menu slug
		'twentydishes_updatebilling'); //function			
		
		//this is a submenu HIDDEN
		add_submenu_page(null, //parent slug
		'Add New Step by Step', //page title
		'Add New Step by Step', //menu title
		'manage_options', //capability
		'twentydishes_createstepbystep', //menu slug
		'twentydishes_createstepbystep'); //function

		//this is a submenu HIDDEN
		add_submenu_page(null, //parent slug
		'Update Step by Step', //page title
		'Update Step by Step', //menu title
		'manage_options', //capability
		'twentydishes_updatestepbystep', //menu slug
		'twentydishes_updatestepbystep'); //function		
		
		//this is a submenu HIDDEN
		add_submenu_page(null, //parent slug
		'Add New Recipe-Step by Step', //page title
		'Add New Recipe-Step by Step', //menu title
		'manage_options', //capability
		'twentydishes_createrecipestepbystep', //menu slug
		'twentydishes_createrecipestepbystep'); //function

		//this is a submenu HIDDEN
		add_submenu_page(null, //parent slug
		'Update Recipe-Step by Step', //page title
		'Update Recipe-Step by Step', //menu title
		'manage_options', //capability
		'twentydishes_updaterecipestepbystep', //menu slug
		'twentydishes_updaterecipestepbystep'); //function

		//this is a submenu HIDDEN just for ajax function
		add_submenu_page(null, //parent slug
		'', //page title
		'', //menu title
		'manage_options', //capability
		'twentydishes_usersave', //menu slug
		'twentydishes_usersave'); //function		
		
		//this is a submenu HIDDEN
		add_submenu_page(null, //parent slug
		'Add New Category to Exclude List', //page title
		'Add New Category to Exclude List', //menu title
		'manage_options', //capability
		'twentydishes_createexcludelist', //menu slug
		'twentydishes_createexcludelist'); //function		
		
		//this is a submenu HIDDEN
		add_submenu_page(null, //parent slug
		'Update Category in Exclude List', //page title
		'Update Category in Exclude List', //menu title
		'manage_options', //capability
		'twentydishes_updateexcludelist', //menu slug
		'twentydishes_updateexcludelist'); //function				
	}

	define('ROOTDIR', plugin_dir_path(__FILE__));
	require_once(ROOTDIR . 'twentydishes-alldiets.php');
	require_once(ROOTDIR . 'twentydishes-creatediet.php');
	require_once(ROOTDIR . 'twentydishes-updatediet.php');
	require_once(ROOTDIR . 'twentydishes-allbillings.php');
	require_once(ROOTDIR . 'twentydishes-createbilling.php');
	require_once(ROOTDIR . 'twentydishes-updatebilling.php');	
	require_once(ROOTDIR . 'twentydishes-allstepbystep.php');
	require_once(ROOTDIR . 'twentydishes-createstepbystep.php');
	require_once(ROOTDIR . 'twentydishes-updatestepbystep.php');
	require_once(ROOTDIR . 'twentydishes-allrecipestepbystep.php');
	require_once(ROOTDIR . 'twentydishes-createrecipestepbystep.php');
	require_once(ROOTDIR . 'twentydishes-updaterecipestepbystep.php');	    
	require_once(ROOTDIR . 'twentydishes-generalreport.php');	
	require_once(ROOTDIR . 'twentydishes-userreport.php');	
	require_once(ROOTDIR . 'twentydishes-usersave.php');		
	require_once(ROOTDIR . 'twentydishes-importrecipe.php');	
	require_once(ROOTDIR . 'twentydishes-allexcludelist.php');	
	require_once(ROOTDIR . 'twentydishes-createexcludelist.php');	
	require_once(ROOTDIR . 'twentydishes-updateexcludelist.php');	
?>