<?php

function twentydishes_userreport () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>User Report</h2>
		
		<?php
			global $wpdb;
			
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>No</th><th>Display Name</th><th>Email</th><th>Registration Date</th><th>Levels</th><th>Expiring Date</th></tr></thead>";

			// Get user
			$sql = "SELECT a.id, a.display_name, a.user_email, a.user_registered, IF(ISNULL(c.pricing),0,c.pricing) as 'pricing' FROM wp_users a JOIN wp_usermeta b ON a.ID = b.user_id LEFT JOIN jp_billing c ON a.id = c.user_id WHERE a.user_pass != '' and b.meta_key = 'diets'";
			$users = $wpdb->get_results($sql);			
		
			$unsortedResult= array();
			foreach ($users as $user ){
				
				$levels = array_values(WLMAPI::GetUserLevels($user->id));		
				$registime = strtotime($user->user_registered);									
				$currenttime = time();
				$billedtime = "";
				$difference = "";
				$diff = calculateMonths($registime, $currenttime);
				
				if(in_array('Monthly', $levels)) {
					$difference = 1 - ($diff % 1) + $diff;
					
					if($difference == 0)
						$difference = 1;
				}
				if(in_array('Quarterly', $levels))
					$difference = 3 - ($diff % 3) + $diff;	
					
				if(in_array('3 Month', $levels))
					$difference = 3 - ($diff % 3) + $diff;									

				if(in_array('6 Month', $levels))
					$difference = 6 - ($diff % 6) + $diff;
					
				if(in_array('Yearly', $levels))
					$difference = 12 - ($diff % 12) + $diff;
				
				$currenttime = strtotime("+".$diff." month", $registime);  	
				$billedtime = date("m-d-Y", strtotime("+".$difference." month", $registime));				
				$billedtimesort = strtotime("+".$difference." month", $registime);	
				
				if($difference === ""){
					$billedtime = "Unknown Membership Level";	
					$billedtimesort = 9999999999;
				}
				
				if(count($levels) > 1 ){
					$billedtime = "Multiple Membership Level";
					$billedtimesort = 9999999998;
				}
				
				if(count($levels) == 0 ){
					continue;
				}				
				
				if(in_array('Expired', $levels)){
					$billedtime = 'Expired';
					$billedtimesort = 9999999997;
					continue;
				}						
				
				$unsortedResult[] = array("id"=>$user->id, "name"=>$user->display_name, "user_email"=>$user->user_email, "regisdate"=>date("m-d-Y", $registime), "level"=>implode(",", $levels), "expiringdate"=>$billedtime, "pricing"=>$user->pricing, "sort"=>$billedtimesort);
			}
			
			usort($unsortedResult, 'sortDescending'); $i = 1; 
			foreach ($unsortedResult as $result ){
				
				echo "<tr>";
				echo "<td>".$i."</td>";
				echo "<td>".$result['name']."</td>";
				echo "<td>".$result['user_email']."</td>";
				echo "<td>".$result['regisdate']."</td>";
				echo "<td>".$result['level']."</td>";
				echo "<td>".$result['expiringdate']."</td>";				
				
				echo "</tr>";
				
				$i++;
			}			
			
			echo "</table>";
		?>
	</div>
<?php } ?>