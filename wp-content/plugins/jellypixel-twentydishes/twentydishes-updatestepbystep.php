<?php
function twentydishes_updatestepbystep () {
	global $wpdb;

	$id = $_GET["id"];
	$todo=$_POST["todo"];
	$priority=$_POST["priority"];
	
	if(isset($_POST['update'])){ //update	
		$wpdb->update(
			'jp_stepbystep', //table
			array('todo' => $todo, 'priority' => $priority), //data
			array( 'id' => $id ), //where
			array('%s', '%f'), //data format
			array('%s') //where format
		);	
	}
	else if(isset($_POST['delete'])){ //delete	
		$wpdb->query($wpdb->prepare("DELETE FROM jp_stepbystep WHERE id = %s", $id));
	}
	else{//selecting value to update	
		
		$stepbystep = $wpdb->get_results($wpdb->prepare("SELECT id, todo, priority from jp_stepbystep where id=%s",$id));
		foreach ($stepbystep as $s ){
			$todo=$s->todo;
			$priority=$s->priority;
		}
	}
?>
<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
<div class="wrap">
<h2>Update/Delete Step by Step <a href="<?php echo admin_url('admin.php?page=twentydishes_allstepbystep')?>" class="add-new-h2">&laquo; Back to step by step list</a></h2><br>

<?php if($_POST['delete']){?>
<div class="updated"><p>Step by step deleted</p></div>

<?php } else if($_POST['update']) {?>
<div class="updated"><p>Step by step updated</p></div>

<?php } else {?>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
<table class='wp-list-table widefat fixed'>
<tr><th>To Do</th><td><input type="text" name="todo" value="<?php echo $todo;?>"/></td></tr>
<tr><th>Priority</th><td><input type="text" name="priority" value="<?php echo $priority;?>"/></td></tr>
</table><br>
<input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
<input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Are you sure you want to delete this step by step')">
</form>
<?php }?>

</div>
<?php
}