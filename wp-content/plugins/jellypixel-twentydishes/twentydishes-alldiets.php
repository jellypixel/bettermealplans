<?php
function twentydishes_alldiets () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>All Diets
		<a href="<?php echo admin_url('admin.php?page=twentydishes_creatediet'); ?>" class="add-new-h2">Add New Diet</a></h2><br>

		<?php
			global $wpdb;
			$rows = $wpdb->get_results("SELECT id,name,colordark,colorlight,orders from jp_diet ORDER BY orders asc");
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>ID</th><th>Name</th><th>Background Color</th><th>Text Color</th><th>Order</th><th>&nbsp;</th></tr></thead>";
			foreach ($rows as $row ){
				echo "<tr>";
				echo "<td>$row->id</td>";
				echo "<td>$row->name</td>";	
				echo "<td style='background-color:".$row->colorlight."; color:#000;'>$row->colorlight</td>";
				echo "<td style='background-color:".$row->colordark."; color:#000;'>$row->colordark</td>";
				echo "<td>$row->orders</td>";				
				echo "<td><a href='".admin_url('admin.php?page=twentydishes_updatediet&id='.$row->id)."'>Update</a></td>";
				echo "</tr>";}
			echo "</table>";
		?>
	</div>
<?php } ?>