<?php
function twentydishes_updatediet () {
	global $wpdb;

	$id = $_GET["id"];
	$name=$_POST["name"];
	$colordark=$_POST["colordark"];
	$colorlight=$_POST["colorlight"];
	$orders=$_POST["orders"];
	
	$rows = $wpdb->get_results("SELECT id,name,colordark,colorlight,orders from jp_diet ORDER BY orders asc");
	
	if(isset($_POST['update'])){ //update	
		$wpdb->update(
			'jp_diet', //table
			array('name' => $name, 'colordark' => $colordark, 'colorlight' => $colorlight, 'orders' => $orders), //data
			array( 'id' => $id ), //where
			array('%s', '%s', '%s', '%d'), //data format
			array('%s') //where format
		);	
	}
	else if(isset($_POST['delete'])){ //delete	
		$wpdb->query($wpdb->prepare("DELETE FROM jp_diet WHERE id = %s", $id));
	}
	else{//selecting value to update	
		$diets = $wpdb->get_results($wpdb->prepare("SELECT id,name,colordark,colorlight,orders from jp_diet where id=%s",$id));
		foreach ($diets as $s ){
			$name=$s->name;
			$colordark=$s->colordark;
			$colorlight=$s->colorlight;
			$orders=$s->orders;
		}
	}
?>
<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
<div class="wrap">
<h2>Update/Delete Diets <a href="<?php echo admin_url('admin.php?page=twentydishes_alldiets')?>" class="add-new-h2">&laquo; Back to diet list</a></h2><br>

<?php if($_POST['delete']){?>
<div class="updated"><p>Diet deleted</p></div>

<?php } else if($_POST['update']) {?>
<div class="updated"><p>Diet updated</p></div>

<?php } else {?>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
<table class='wp-list-table widefat fixed'>
<tr><th>Name</th><td><input type="text" name="name" value="<?php echo $name;?>"/></td></tr>
<tr><th>Background Color</th><td><input type="text" name="colorlight" class="color-field" value="<?php echo $colorlight;?>"/></td></tr>
<tr><th>Text Color</th><td><input type="text" name="colordark" class="color-field" value="<?php echo $colordark;?>"/></td></tr>
<tr><th>Order</th><td><input type="text" name="orders" value="<?php echo $orders;?>"/></td></tr>
</table><br>
<input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
<input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Are you sure you want to delete this diet?')">
</form>
<?php }?>

</div>
<?php
}