<?php
function twentydishes_allexcludelist () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>All Categories on Exclude List
		<a href="<?php echo admin_url('admin.php?page=twentydishes_createexcludelist'); ?>" class="add-new-h2">Add New Category to Exclude List</a></h2><br>

		<?php
			global $wpdb;
			$rows = $wpdb->get_results("SELECT id,cat_id,name from jp_excludelist ORDER BY name asc");
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>Category ID</th><th>Name</th></tr></thead>";
			foreach ($rows as $row ){
				echo "<tr>";
				echo "<td>$row->cat_id</td>";
				echo "<td>$row->name</td>";				
				echo "<td><a href='".admin_url('admin.php?page=twentydishes_updateexcludelist&id='.$row->id)."'>Update</a></td>";
				echo "</tr>";}
			echo "</table>";
		?>
	</div>
<?php } ?>