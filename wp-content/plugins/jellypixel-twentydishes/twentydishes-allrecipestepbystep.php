<?php
function twentydishes_allrecipestepbystep () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>All Recipe - Step by Step
		<a href="<?php echo admin_url('admin.php?page=twentydishes_createrecipestepbystep'); ?>" class="add-new-h2">Add Recipe-Step by Step</a></h2><br>

		<?php
			global $wpdb;
			$rows = $wpdb->get_results("SELECT a.id as 'recipeid', a.post_title as 'recipetitle', GROUP_CONCAT(c.todo) as todo FROM wp_posts a, jp_stepbystep_recipe b, jp_stepbystep c where c.id >= 122 and a.post_type = 'post' AND a.id = b.recipe AND b.stepbystep = c.id group by a.id, a.post_title order by a.post_title, c.priority asc");
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>Recipe</th><th>To Do</th></tr></thead>";
			foreach ($rows as $row ){
				echo "<tr>";
				echo "<td>$row->recipetitle</td>";	
				echo "<td>$row->todo</td>";					
				echo "<td><a href='".admin_url('admin.php?page=twentydishes_updaterecipestepbystep&id='.$row->recipeid.'&recipetitle='.$row->recipetitle."'>Update</a></td>");
				echo "</tr>";}
			echo "</table>";
		?>
	</div>
<?php } ?>