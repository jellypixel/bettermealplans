<?php
function twentydishes_updaterecipestepbystep () {

	global $wpdb;
	$recipeid = $_GET["id"];
	$recipetitle = $_GET["recipetitle"];
	
	//delete
	if(isset($_POST['delete'])){
		
		$recipeselectedid=$_POST["recipeselectedid"];
		$wpdb->query($wpdb->prepare("DELETE FROM jp_stepbystep_recipe WHERE recipe = %s", $recipeselectedid));
	}	
	else if(isset($_POST['update'])){
		
		$todolist=$_POST["todolist"];
		$todolist=str_replace("<div>", "", $todolist);
		$todolist=explode("</div>", $todolist);
		
		$recipeselectedid=$_POST["recipeselectedid"];
		
		$wpdb->query($wpdb->prepare("DELETE FROM jp_stepbystep_recipe WHERE recipe = %s", $recipeselectedid));
		foreach($todolist as $eachtodo) {
			
			if(!empty($eachtodo)) {
				
				$stepselectedid = $wpdb->get_results("SELECT a.id FROM jp_stepbystep a where a.id >= 122 and a.todo = '$eachtodo'"); $stepselectedid = $stepselectedid[0]->id;
				
				$wpdb->insert(
							'jp_stepbystep_recipe', //table
							array('recipe' => $recipeselectedid, 'stepbystep' => $stepselectedid), //data
							array('%d', '%d') //data format		
						);				
			}
		}
	}
	
	$steps = $wpdb->get_results("SELECT * FROM jp_stepbystep a where a.id >= 122 order by a.priority asc");
	$stepsfirstvalue = $wpdb->get_results("SELECT a.todo FROM jp_stepbystep a where a.id in (select stepbystep from jp_stepbystep_recipe where recipe=$recipeid) order by a.priority asc");
?>

	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<script>
		jQuery(document).ready(function() {

			function log( message ) {
				var stepcurrent = jQuery( "#stepplugin-steptextarea" ).html();
				
				if(stepcurrent.indexOf(message) > -1) {
					var tmpStep = jQuery( "#stepplugin-steptextarea" ).html();
					
					jQuery( "#stepplugin-steptextarea" ).html(stepcurrent.replace("<div>"+message+"</div>", ""));
					jQuery( "#stepplugin-stephidden" ).val( jQuery( "#stepplugin-steptextarea" ).html() );	
					
					if(tmpStep == jQuery( "#stepplugin-steptextarea" ).html()) {
					
						jQuery( "<div>" ).text( message ).prependTo( "#stepplugin-steptextarea" );
						jQuery( "#stepplugin-steptextarea" ).scrollTop( 0 );
						jQuery( "#stepplugin-stephidden" ).val( jQuery( "#stepplugin-steptextarea" ).html() );
					}				
				} else {
					jQuery( "<div>" ).text( message ).prependTo( "#stepplugin-steptextarea" );
					jQuery( "#stepplugin-steptextarea" ).scrollTop( 0 );
					jQuery( "#stepplugin-stephidden" ).val( jQuery( "#stepplugin-steptextarea" ).html() );
				}
			}		
			
			var availableSteps = [
			  <?php
				foreach($steps as $step) {
					echo '"'.addslashes($step->todo).'"';
					
					if(end($steps)->id !== $step->id){
						echo ','; // not the last element
					}
				}
			  ?>
			];			
			
			<?php
			foreach($stepsfirstvalue as $step) {
				echo "log('$step->todo');";
			}
			?>	

			jQuery( "#stepplugin-steptext" ).autocomplete({
			  source: availableSteps,
			  minLength: 2,
			  select: function( event, ui ) {
				  
				  if(ui.item) {
					  log(ui.item.value);
				  }
			  }			  
			});	
			
			jQuery(document).ready(function() {
			   jQuery("form").bind("keypress", function(e) {
				  if (e.keyCode == 13) {
					 return false;
				  }
			   });
			});					
		});
	</script>
  
	<div class="wrap">
	<h2>Update Recipe-Step by Step<a href="<?php echo admin_url('admin.php?page=twentydishes_allrecipestepbystep')?>" class="add-new-h2">&laquo; Back to recipe-step by step list</a></h2><br>
	
	<?php if($_POST['delete']){?>
	<div class="updated"><p>Recipe-Step by step deleted</p></div>

	<?php } else if($_POST['update']) {?>
	<div class="updated"><p>Recipe-Step by step updated</p></div>

	<?php } else {?>
	
		<form method="post" name="stepform" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<table class='wp-list-table widefat fixed'>	
		<tr>
			<th>Recipe</th>
			<td>
				<input type="hidden" name="recipeselectedid" value="<?php echo $recipeid; ?>">
				<input id="stepplugin-recipetext" name="recipeselected" disabled value="<?php echo $recipetitle; ?>"/>
			</td>
		</tr>
		<tr>
			<th>To Do</th>	
			<td>
				<input id="stepplugin-steptext" value=""/>
			</td>
			<td>
				<input type="hidden" id="stepplugin-stephidden" name="todolist" value="">
				<div id="stepplugin-steptextarea" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content" />
				</div>
			</td>			
		</tr>
		</table> <br>
		<input type='submit' name="update" value='Save' class='button'>
		<input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Are you sure you want to delete this step by step')">
		</form>
		
	<?php }?>
	</div>
<?php
}