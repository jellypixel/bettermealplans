<?php

require("PHPExcel/PHPExcel.php");

class Excel {

    protected $objPHPExcel;
    public function __construct($file){
        if($file instanceof \SplFileInfo){
            $filename = $file->getRealPath();
        }else{
            $filename = $file;
        }

        $this->objPHPExcel = PHPExcel_IOFactory::load($filename);
        $this->objPHPExcel->setActiveSheetIndex(0);
    }

    public function setActiveSheetIndex($index){
        $this->objPHPExcel->setActiveSheetIndex($index);
    }

    /**
     * Create array from worksheet
     *
     * @param mixed $nullValue Value returned in the array entry if a cell doesn't exist
     * @param boolean $calculateFormulas Should formulas be calculated?
     * @param boolean $formatData  Should formatting be applied to cell values?
     * @param boolean $returnCellRef False - Return a simple array of rows and columns indexed by number counting from zero
     *                               True - Return rows and columns indexed by their actual row and column IDs
     * @return array
     */
    public function toArray($nullValue = null, $calculateFormulas = true, $formatData = false){
    	
    	$rows = $this->objPHPExcel->getActiveSheet()->toArray($nullValue,$calculateFormulas,$formatData,false);
		$headers = array_shift($rows);
		
		array_walk($rows, function(&$values) use($headers){
			$values = array_combine($headers, $values);
		});

       return $rows;
    }
	
 
    public function toJson($options = 0, $nullValue = null, $calculateFormulas = true, $formatData = false){
        return json_encode($this->toArray($nullValue,$calculateFormulas,$formatData), $options);
    }
}

function twentydishes_importrecipe () {

	$file=$_POST["upload_file"];
	
	//insert
	if(isset($_POST['insert'])){
		
		$file = get_attached_file($file);		
		$rows = new Excel($file); $rows = $rows->toArray();

		foreach($rows as $row) {
			
			// Get Category
			$categories = explode("|", $row['categories']);
			$categoriesSaved = array();
			foreach($categories as $category) {
			
				$tag = get_term_by('name', $category, 'category'); $tagID = "";
				if ($tag) {
					$tagID = $tag->term_id;
				} 			
				
				$categoriesSaved[] = $tagID;
			}
				
			$post_id = wp_insert_post( array(
			  'post_title'     => $row['title'],
			  'post_status'    => 'publish',
			  'post_type'      => 'post',
			  'post_category'  => $categoriesSaved
			));  	

			if ($post_id) {
				add_post_meta($post_id, 'recipe_notes', $row['notes']);
				add_post_meta($post_id, 'recipe_difficulty', $row['diff']);
				add_post_meta($post_id, 'recipe_servings', $row['yields']);
				add_post_meta($post_id, 'recipe_servings_normalized', $row['yields']);
				add_post_meta($post_id, 'recipe_servings_type', 'people');
				add_post_meta($post_id, 'recipe_prep_time', $row['preptime']);
				add_post_meta($post_id, 'recipe_prep_time_text', 'minutes');
				add_post_meta($post_id, 'recipe_cook_time', $row['cooktime']);
				add_post_meta($post_id, 'recipe_cook_time_text', 'minutes');
				
				// Create Instructions
				$instructions = explode("|", $row['instructions']);
				$instructionsSaved = array();
				foreach($instructions as $instruction) {
				
					$instructionSaved = array("description"=>$instruction, "group"=>"");
					$instructionsSaved[] = $instructionSaved;
				}
				
				add_post_meta($post_id, 'recipe_instructions', $instructionsSaved);
				
				// Create Ingredients
				$ingredients = explode("|", $row['ingredients']);
				$ingredientsSaved = array();
				
				foreach($ingredients as $ingredient) {
				
					$ingredientComponent = explode("-", $ingredient);
				
					if($ingredientComponent[0] == "none") $ingredientComponent[0] = "";
					if($ingredientComponent[1] == "none") $ingredientComponent[1] = "";
					if($ingredientComponent[2] == "none") $ingredientComponent[2] = "";
					if($ingredientComponent[3] == "none") $ingredientComponent[3] = "";
					
					$tag = get_term_by('name', $ingredientComponent[2], 'ingredient'); $tagID = "";
					if ($tag) {
						$tagID = $tag->term_id;  
					} 

					$ingredientSaved = array("amount"=>$ingredientComponent[0], "unit"=>$ingredientComponent[1], "ingredient"=>$ingredientComponent[2], "notes"=>$ingredientComponent[3], "group"=>"", "ingredient_id"=>$tagID, "amount_normalized"=>$ingredientComponent[0]);
					$ingredientsSaved[] = $ingredientSaved;
				}				
				
				add_post_meta($post_id, 'recipe_ingredients', $ingredientsSaved);
			}
		}
		
		$message.="Recipe imported";
	}
	?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
	<h2>Import Recipe</h2><br>
	<?php if (isset($message)): ?><div class="updated"><p><?php echo $message;?></p></div><?php endif;?>
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
		<input type="text" name="upload_file" id="upload_file" value="" size='40' />
		<input type="button" class='button-secondary' id="upload_file_button" value="Upload File" />
		<input type='submit' name="insert" value='Process' class='button'>
	</form>
	</div>
<?php
}
?>