<?php
function twentydishes_allbillings () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>All Billings
		<a href="<?php echo admin_url('admin.php?page=twentydishes_createbilling'); ?>" class="add-new-h2">Add New Billing</a></h2><br>

		<?php
			global $wpdb;
			$rows = $wpdb->get_results("SELECT id,name,price from jp_billingpackage ORDER BY id asc");
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>ID</th><th>Name</th><th>Price</th><th></th></tr></thead>";
			foreach ($rows as $row ){
				echo "<tr>";
				echo "<td>$row->id</td>";
				echo "<td>$row->name</td>";	
				echo "<td>$row->price</td>";			
				echo "<td><a href='".admin_url('admin.php?page=twentydishes_updatebilling&id='.$row->id)."'>Update</a></td>";
				echo "</tr>";}
			echo "</table>";
		?>
	</div>
<?php } ?>