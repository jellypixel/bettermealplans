<?php
function twentydishes_creatediet () {

	$name=$_POST["name"];
	$colordark=$_POST["colordark"];
	$colorlight=$_POST["colorlight"];
	$orders=$_POST["orders"];
	
	//insert
	if(isset($_POST['insert'])){
		global $wpdb;
		$wpdb->insert(
			'jp_diet', //table
			array('name' => $name, 'colordark' => $colordark, 'colorlight' => $colorlight, 'orders' => $orders), //data
			array('%s', '%s', '%s', '%d') //data format		
		);
		$message.="Diet inserted";
	}
	?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
	<h2>Add New Diet<a href="<?php echo admin_url('admin.php?page=twentydishes_alldiets')?>" class="add-new-h2">&laquo; Back to diet list</a></h2><br>
	<?php if (isset($message)): ?><div class="updated"><p><?php echo $message;?></p></div><?php endif;?>
	<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class='wp-list-table widefat fixed'>
	<tr><th>Name</th><td><input type="text" name="name" value="<?php echo $name;?>"/></td></tr>
	<tr><th>Background Color</th><td><input type="text" class="color-field" name="colorlight" value="<?php echo $colorlight;?>"/></td></tr>
	<tr><th>Text Color</th><td><input type="text" class="color-field" name="colordark" value="<?php echo $colordark;?>"/></td></tr>
	<tr><th>Order</th><td><input type="text" name="orders" value="<?php echo $orders;?>"/></td></tr>
	</table> <br>
	<input type='submit' name="insert" value='Save' class='button'>
	</form>
	</div>
<?php
}