jQuery(document).ready(function() {

	var file_frame;
	
	jQuery('#upload_file_button').live('click', function( event ){
	
		event.preventDefault();
		
		// If the media frame already exists, reopen it.
		if ( file_frame ) {
		  file_frame.open();
		  return;
		}
		
		// Create the media frame.
		file_frame = wp.media.frames.file_frame = wp.media({
		  title: jQuery( this ).data( 'uploader_title' ),
		  button: {
		    text: jQuery( this ).data( 'uploader_button_text' ),
		  },
		  multiple: false  // Set to true to allow multiple files to be selected
		});
		
		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {
		  // We set multiple to false so only get one image from the uploader
		  attachment = file_frame.state().get('selection').first().toJSON();
		
		  // Do something with attachment.id and/or attachment.url here
		  	jQuery('#upload_file').val(attachment.id);
		});
		
		// Finally, open the modal
		file_frame.open();
	});
  
    jQuery('.itcc-date-picker').datepicker({
        dateFormat : 'yy/mm/dd',
		defaultDate: new Date()
    });
	
	jQuery('.color-field').wpColorPicker();
	
	jQuery( ".save" ).click(function(event) {
		
		jQuery.post( "http://20dishes.com/wp-admin/admin.php?page=twentydishes_usersave", 
		{ id: event.target.id, pricing: jQuery(this).parents("tr").find("input").val()} );
	});
	
    jQuery("input[id*='txtQty']").keydown(function (event) {


        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) || 
            (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 110 || 
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

        } else {
            event.preventDefault();
        }

        if(jQuery(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault(); 
        //if a decimal has been added, disable the "."-button

    });	
	
});