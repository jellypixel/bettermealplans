<?php
function twentydishes_createrecipestepbystep () {

	global $wpdb;
		
	//insert
	if(isset($_POST['insert'])){
		
		$todolist=$_POST["todolist"];
		$recipeselected=$_POST["recipeselected"];
		
		$todolist=str_replace("<div>", "", $todolist);
		$todolist=explode("</div>", $todolist);
		
		$recipeselectedid = $wpdb->get_results("SELECT a.id FROM wp_posts a where a.post_title = '$recipeselected' and a.post_type='post'"); $recipeselectedid = $recipeselectedid[0]->id;
		
		foreach($todolist as $eachtodo) {
			
			if(!empty($eachtodo)) {
				
				$stepselectedid = $wpdb->get_results("SELECT a.id FROM jp_stepbystep a where a.id >= 122 and a.todo = '$eachtodo'"); $stepselectedid = $stepselectedid[0]->id;
				
				$wpdb->insert(
							'jp_stepbystep_recipe', //table
							array('recipe' => $recipeselectedid, 'stepbystep' => $stepselectedid), //data
							array('%d', '%d') //data format		
						);				
			}
		}
		$message.="Recipe-Step bt Step Inserted!";
	}
	
	$recipes = $wpdb->get_results("SELECT * FROM wp_posts a where a.post_type = 'post' and a.id not in (select a.recipe from jp_stepbystep_recipe a where a.stepbystep >= 122) order by a.post_title asc");
	$steps = $wpdb->get_results("SELECT * FROM jp_stepbystep a where a.id >= 122 order by a.priority asc");
?>

	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<script>
		jQuery(document).ready(function() {
			var availableRecipes = [
			  <?php
				foreach($recipes as $recipe) {
					echo '"'.addslashes($recipe->post_title).'"';
					
					if(end($recipes)->ID !== $recipe->ID){
						echo ','; // not the last element
					}
				}
			  ?>
			];
			
			var availableSteps = [
			  <?php
				foreach($steps as $step) {
					echo '"'.addslashes($step->todo).'"';
					
					if(end($steps)->id !== $step->id){
						echo ','; // not the last element
					}
				}
			  ?>
			];			

			jQuery( "#stepplugin-recipetext" ).autocomplete({
			  source: availableRecipes
			});	
			
			jQuery( "#stepplugin-steptext" ).autocomplete({
			  source: availableSteps,
			  minLength: 2,
			  select: function( event, ui ) {
				  
				  if(ui.item) {
					  log(ui.item.value);
				  }
			  }			  
			});	
			
			jQuery(document).ready(function() {
			   jQuery("form").bind("keypress", function(e) {
				  if (e.keyCode == 13) {
					 return false;
				  }
			   });
			});			
		
			function log( message ) {
				var stepcurrent = jQuery( "#stepplugin-steptextarea" ).html();
				
				if(stepcurrent.indexOf(message) > -1) {
					
					var tmpStep = jQuery( "#stepplugin-steptextarea" ).html();
					
					jQuery( "#stepplugin-steptextarea" ).html(stepcurrent.replace("<div>"+message+"</div>", ""));
					jQuery( "#stepplugin-stephidden" ).val( jQuery( "#stepplugin-steptextarea" ).html() );	
					
					if(tmpStep == jQuery( "#stepplugin-steptextarea" ).html()) {
					
						jQuery( "<div>" ).text( message ).prependTo( "#stepplugin-steptextarea" );
						jQuery( "#stepplugin-steptextarea" ).scrollTop( 0 );
						jQuery( "#stepplugin-stephidden" ).val( jQuery( "#stepplugin-steptextarea" ).html() );
					}
				} else {
					jQuery( "<div>" ).text( message ).prependTo( "#stepplugin-steptextarea" );
					jQuery( "#stepplugin-steptextarea" ).scrollTop( 0 );
					jQuery( "#stepplugin-stephidden" ).val( jQuery( "#stepplugin-steptextarea" ).html() );
				}
			}				
		});
	</script>
  
	<div class="wrap">
	<h2>Add New Recipe-Step by Step<a href="<?php echo admin_url('admin.php?page=twentydishes_allrecipestepbystep')?>" class="add-new-h2">&laquo; Back to recipe-step by step list</a></h2><br>
	<?php if (isset($message)): ?><div class="updated"><p><?php echo $message;?></p></div><?php endif;?>
	<form method="post" name="stepform" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<table class='wp-list-table widefat fixed'>	
	<tr>
		<th>Recipe</th>
		<td>
			<input id="stepplugin-recipetext" name="recipeselected" value=""/>
		</td>
	</tr>
	<tr>
		<th>To Do</th>	
		<td>
			<input id="stepplugin-steptext" value=""/>
		</td>
		<td>
			<input type="hidden" id="stepplugin-stephidden" name="todolist" value="">
			<div id="stepplugin-steptextarea" style="height: 200px; width: 300px; overflow: auto;" class="ui-widget-content" />
			</div>
		</td>			
	</tr>
	</table> <br>
	<input type='submit' name="insert" value='Save' class='button'>
	</form>
	</div>
<?php
}