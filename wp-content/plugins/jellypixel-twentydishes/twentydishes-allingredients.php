<?php
function twentydishes_allingredients () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>All Ingredients</h2>
		<?php
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>ID</th><th>#of Recipe</th></tr></thead>";
			
			$terms = get_terms('ingredient');

			 if ( !empty( $terms ) && !is_wp_error( $terms ) ){
				 
				 foreach ( $terms as $term ) {
				   echo '<tr>';
				   echo '<td>' . $term->name . '</td>';
				   echo '<td>' . $term->count . '</td>';
				   echo '</tr>';
				 }
				 
			 } 
		 
		?>
	</div>
<?php } ?>