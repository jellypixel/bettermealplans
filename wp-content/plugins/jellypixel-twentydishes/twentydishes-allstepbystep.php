<?php
function twentydishes_allstepbystep () {
?>
	<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
	<div class="wrap">
		<h2>All Step by Step
		<a href="<?php echo admin_url('admin.php?page=twentydishes_createstepbystep'); ?>" class="add-new-h2">Add Step by Step</a></h2><br>

		<?php
			global $wpdb;
			$rows = $wpdb->get_results("SELECT id, todo, priority from jp_stepbystep where id >= 122 ORDER BY priority asc");
			echo "<table class='wp-list-table widefat fixed'>";
			echo "<thead><tr><th>To Do</th><th>Priority</th></tr></thead>";
			foreach ($rows as $row ){
				echo "<tr>";
				echo "<td>$row->todo</td>";	
				echo "<td>$row->priority</td>";				
				echo "<td><a href='".admin_url('admin.php?page=twentydishes_updatestepbystep&id='.$row->id)."'>Update</a></td>";
				echo "</tr>";}
			echo "</table>";
		?>
	</div>
<?php } ?>