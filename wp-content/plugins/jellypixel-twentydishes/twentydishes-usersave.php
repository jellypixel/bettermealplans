<?php
function twentydishes_usersave () {
	global $wpdb;

	$id = $_POST["id"];
	$pricing = $_POST["pricing"];
	
	$wpdb->query($wpdb->prepare("DELETE FROM jp_billing WHERE user_id = %s", $id));
	
	$wpdb->insert(
		'jp_billing', //table
		array('user_id' => $id, 'pricing' => $pricing), //data
		array('%s', '%f') //data format		
	);
		
}
?>