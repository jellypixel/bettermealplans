<?php
function twentydishes_updatebilling () {
	global $wpdb;

	$id = $_GET["id"];
	$name=$_POST["name"];
	$price=$_POST["price"];
	
	$rows = $wpdb->get_results("SELECT id,name,price from jp_billingpackage");
	
	if(isset($_POST['update'])){ //update	
		$wpdb->update(
			'jp_billingpackage', //table
			array('name' => $name, 'price' => $price), //data
			array( 'id' => $id ), //where
			array('%s', '%f'), //data format
			array('%s') //where format
		);	
	}
	else if(isset($_POST['delete'])){ //delete	
		$wpdb->query($wpdb->prepare("DELETE FROM jp_billingpackage WHERE id = %s", $id));
	}
	else{//selecting value to update	
		$billings = $wpdb->get_results($wpdb->prepare("SELECT id,name,price from jp_billingpackage where id=%s",$id));
		foreach ($billings as $s ){
			$name=$s->name;
			$price=$s->price;
		}
	}
?>
<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/jellypixel-twentydishes/style-admin.css" rel="stylesheet" />
<div class="wrap">
<h2>Update/Delete Billing <a href="<?php echo admin_url('admin.php?page=twentydishes_allbillings')?>" class="add-new-h2">&laquo; Back to billings list</a></h2><br>

<?php if($_POST['delete']){?>
<div class="updated"><p>Billing deleted</p></div>

<?php } else if($_POST['update']) {?>
<div class="updated"><p>Billing updated</p></div>

<?php } else {?>
<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
<table class='wp-list-table widefat fixed'>
<tr><th>Name</th><td><input type="text" name="name" value="<?php echo $name;?>"/></td></tr>
<tr><th>Price</th><td><input type="text" name="price" value="<?php echo $price;?>"/></td></tr>
</table><br>
<input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
<input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Are you sure you want to delete this billing?')">
</form>
<?php }?>

</div>
<?php
}