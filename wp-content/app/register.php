<?php 
    /*  
        TODO:
        1. Detect payment from this user
        2. Add tags
	*/
	require_once('../../.wordpress/wp-load.php');
	 
	$response = array(
		'data'		=> array(),
		'msg'		=> 'Unknown error',
		'status'	=> false
	);

	/* Sanitize all received posts */
	foreach($_POST as $k => $value){
		$_POST[$k] = sanitize_text_field($value);
	}
	
	/**
	 * Register Method
	 *
	 */
	 
	 /* Get user data */
	if( isset( $_POST['firstname'] ) && isset( $_POST['email'] ) &&  isset( $_POST['password'] ))
	{
	 
		/* Get user data */
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$response['status'] = 5;
			$response['msg'] = 'Invalid Email';
			
			echo json_encode( $response );
		}
		
		$user = get_user_by( 'email', $email );

		if ( !$user ) {
			$userdata = array(
				'user_login' =>  $email,
				'user_email'   =>  $email,
				'user_pass'  =>  $password, 
				'first_name'  =>  $firstname, 
				'last_name'  =>  $lastname 
		  	);
		   
			$user_id = wp_insert_user( $userdata ) ;
		} else {
			$user_id = $user->ID;
		}

		if (is_wp_error($user_id)) {
			$response['status'] = 2;
			$response['msg'] = 'Unknown error';
			
			echo json_encode($response);	
		} else {
				
				try {
				// Register the user and add them to iOS Members Group
				$txn = new MeprTransaction();
				$txn->user_id = $user_id;
				$txn->product_id = 25456; // TODO: Make sure you change this value to whatever the product_id will be
				$txn->trans_num  = uniqid();
				$txn->status     = MeprTransaction::$complete_str;
				$txn->gateway    = MeprTransaction::$free_gateway_str;
				$txn->expires_at = 0; 
				$txn->store();
					
				$response['status'] = 1;
				$response['msg'] = 'Success';
				
				/* Return generated token and user ID*/
				$user = get_user_by( 'email', $email );
				$token = md5( $email . $password . rand() );
				
				$response['status'] = 1;
				$response['data'] = array(
					'auth_token' 	=>	$token,
					'user_id'		=>	$user->ID,
					'user_login'	=>	$user->user_login
				);
				
				$response['msg'] = 'Successfully Registered and Authenticated';
				
				echo json_encode($response);
					
				} catch (Exception $ex) {
					$response['status'] = 2;
				$response['msg'] = 'Unknown error';
				
				echo json_encode($response);
					
				}
				
		}
	} 
	else 
	{			
		$response['status'] = 4;
		$response['msg'] = 'Empty Fields';
		
		echo json_encode( $response );	
	}
	
	exit();
?>