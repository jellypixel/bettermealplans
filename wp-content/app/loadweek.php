<?php 
	$response = array(
		'result'	=> ''
	);	

	if ( isset( $_POST['targetDate'] )  && isset( $_POST['userid'] ) ) {
		define('WP_USE_THEMES', false);
		require_once('../../.wordpress/wp-load.php');
		require_once( 'previewrecipe.php' );
		
		// Log them in
		wp_set_current_user( $_POST['userid'] );

		// Check and give default values if not exist
		
		// Print Option
		if(get_user_meta($_POST['userid'], "printoption", true)  === "") 
			add_user_meta( $_POST['userid'], "printoption", 'no', true ); 
		
		// Serving Size
		if(get_user_meta($_POST['userid'], "servings", true) === "") 
			add_user_meta( $_POST['userid'], "servings", 4, true ); 
		
		// Print Size
		if(get_user_meta($_POST['userid'], "fontprintsize", true) === "") 
			add_user_meta( $_POST['userid'], "fontprintsize", 12, true ); 
		
		// Diets
		$allDiets = $wpdb->get_results('SELECT * FROM jp_diet', ARRAY_A);
		if(get_user_meta($_POST['userid'], "diets", true) === "") 
			add_user_meta( $_POST['userid'], "diets", array_column($allDiets, 'id'), true ); 

		// Check their menu and sync
		$tmp_registrationTime = get_userdata($_POST['userid'])->user_registered;
		$tmp_thirtydays = 'NOW() - INTERVAL 30 DAY';
		$tmp_threedays = 'NOW() + INTERVAL 3 DAY';

		$existingMenu = $wpdb->get_results( "SELECT DISTINCT GROUP_CONCAT(id) as 'existing' FROM jp_menu WHERE schedule >= (".$tmp_thirtydays.") AND schedule <= (".$tmp_threedays.") AND id NOT IN (SELECT menu_id FROM jp_usermenu WHERE user_id = " . $_POST['userid'] . " AND insertdate >= (".$tmp_thirtydays.")) AND shared = 'Y'");
		if(!empty($existingMenu[0]->existing)) {
			$leftOverMenu = $wpdb->get_results( "INSERT INTO jp_userschedule(user_id, recipe_id, menu_id, tanggal, orders, notes, updates) 
												 SELECT ".$_POST['userid'].", b.recipe_id, a.id, DATE_ADD(a.schedule,INTERVAL b.day-1 DAY), b.orders, b.notes, a.updates
												 FROM   jp_menu a, jp_menu_recipe b
												 WHERE  a.shared = 'Y' AND a.id = b.menu_id AND 
														a.id IN (".$existingMenu[0]->existing.") AND
														a.schedule >= (".$tmp_thirtydays.") AND
														a.schedule <= (".$tmp_threedays.")
												");	
												
			$menuUpdate = $wpdb->get_results ("DELETE FROM jp_usermenu WHERE user_id = " . $_POST['userid'] . " AND insertdate < (".$tmp_thirtydays.")");	
			
			$menuUpdate = $wpdb->get_results ("INSERT INTO jp_usermenu(user_id, menu_id, insertdate)
											   SELECT DISTINCT ". $_POST['userid'] ." as 'user_id', menu_id, now() as 'insertdate' FROM jp_userschedule WHERE user_id = " . $_POST['userid'] . " AND tanggal >= (".$tmp_thirtydays.") AND tanggal <= (".$tmp_threedays.")");							
		}	
		
		// Load their menu
		$ajaxResult = loadweek($_POST['targetDate']);
	
		// Main Return // echo $ajaxResult;
		$response['result'] = $ajaxResult;
		echo json_encode($response);

	}	
	
	function loadWeek($targetDate) {		
		global $wpdb;
		        
		// Query Diets 
		$queryDiets = '';
		$arrDiet = get_user_meta(wp_get_current_user()->ID, "diets", false);
		$arrDiet = $arrDiet[0];
		
		foreach ($arrDiet as $value) {
			if ( $queryDiets == '' ) {
				$queryDiets = $value;		
			}
			else {
				$queryDiets .= ',' . $value;	
			}			
		}
		
		if ( !empty( $queryDiets ) ) {
			$queryDiets = 'WHERE z.id IN (' . $queryDiets . ')';	
		} else {
			$queryDiets = 'WHERE z.id IN (-10000)';	
		}
		
		// User ID
		$user_id = get_current_user_id();
		
		// Target Date and Date Formatting
		if ( empty( $targetDate ) ) {
			$targetDateQuery = 'CURDATE()';	
			$targetDate = date( 'Y-m-d' );
		}
		else {
			$targetDateQuery = '"' . $targetDate . '"';
		}
		
		$response['result'] = $targetDate;
					
		$targetDateFormatted = date( 'm/d/Y', strtotime( $targetDate ) );
		$targetDay = date( 'w', strtotime( $targetDate ) );
		
		$targetSunday = date( 'Y-m-d', strtotime( $targetDateFormatted . '-' . $targetDay . ' days' ) );
		$targetMonday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 1 - $targetDay ) . ' days') );
		$targetTuesday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 2 - $targetDay ) . ' days') );
		$targetWednesday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 3 - $targetDay ) . ' days') );
		$targetThursday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 4 - $targetDay ) . ' days') );
		$targetFriday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 5 - $targetDay ) . ' days') );
		$targetSaturday = date( 'Y-m-d', strtotime( $targetDateFormatted . '+' . ( 6 - $targetDay ) . ' days') );
		
		$targetSimpleSunday = date( 'F j', strtotime( $targetDateFormatted . '-' . $targetDay . ' days' ) );
		$targetSimpleMonday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 1 - $targetDay ) . ' days') );
		$targetSimpleTuesday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 2 - $targetDay ) . ' days') );
		$targetSimpleWednesday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 3 - $targetDay ) . ' days') );
		$targetSimpleThursday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 4 - $targetDay ) . ' days') );
		$targetSimpleFriday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 5 - $targetDay ) . ' days') );
		$targetSimpleSaturday = date( 'F j', strtotime( $targetDateFormatted . '+' . ( 6 - $targetDay ) . ' days') );
		
		$sundayDay = 'Day 1'; //'Sunday';
		$sundayDate = $targetSimpleSunday;
		
		$mondayDay = 'Day 2'; //'Monday';
		$mondayDate = $targetSimpleMonday;
		
		$tuesdayDay = 'Day 3'; //'Tuesday';
		$tuesdayDate = $targetSimpleTuesday;
		
		$wednesdayDay = 'Day 4'; //'Wednesday';
		$wednesdayDate = $targetSimpleWednesday;
		
		$thursdayDay = 'Day 5'; //'Thursday';
		$thursdayDate = $targetSimpleThursday;
		
		$fridayDay = 'Day 6'; //'Friday';
		$fridayDate = $targetSimpleFriday;
		
		$saturdayDay = 'Day 7'; //'Saturday';
		$saturdayDate = $targetSimpleSaturday;
		
		// Get the word exclusion from database
		$current_user = wp_get_current_user();
		$exingredients = get_user_meta($current_user->ID, "exingredients", false); 
		$exingredients = $exingredients[0];				
		
		$exWhere = " ";
		if(count($exingredients) >= 1) {
			
			$exWhere .= " WHERE a.recipe_id NOT IN (SELECT DISTINCT a.meta_value as 'ID' from wp_postmeta a, wp_postmeta b where a.post_id = b.post_id AND a.meta_key= 'wprm_parent_post_id' AND (";
			foreach($exingredients as $exingredient) 
				$exWhere .= " ( b.meta_key = 'wprm_ingredients' AND CAST(b.meta_value AS CHAR) LIKE '%" . $exingredient . "%' ) OR ";			
			
			$exWhere = rtrim($exWhere, 'OR ');
			$exWhere .= ")) ";
			
		}							
		// Execution
		$sql = ( "
				SELECT * FROM (
					SELECT * FROM (
						SELECT x.*, z.name AS menu_name, z.id AS diet_id, z.colordark as diet_colordark, z.colorlight as diet_colorlight
						FROM
						(
							SELECT user_id, recipe_id, menu_id, tanggal, MIN(orders) as 'orders', notes, DAYOFWEEK(tanggal) AS date_order
							FROM jp_userschedule 
							WHERE 
								user_id = " . $user_id . " AND
								YEARWEEK(tanggal, 0) = YEARWEEK(" . $targetDateQuery . ", 0)	
							GROUP BY user_id, recipe_id, menu_id, tanggal, notes	
						) x
						LEFT JOIN jp_menu y
						ON x.menu_id = y.id
						LEFT JOIN jp_diet z
						ON y.diet_id = z.id
						" . $queryDiets . "
					) a
					UNION
					SELECT * FROM (
						SELECT x.*, z.name AS menu_name, z.id AS diet_id, z.colordark as diet_colordark, z.colorlight as diet_colorlight
						FROM
						(
							SELECT user_id, recipe_id, menu_id, tanggal, MIN(orders) as 'orders', notes, DAYOFWEEK(tanggal) AS date_order
							FROM jp_userschedule 
							WHERE 
								user_id = " . $user_id . " AND
								YEARWEEK(tanggal, 0) = YEARWEEK(" . $targetDateQuery . ", 0) 	
							GROUP BY user_id, recipe_id, menu_id, tanggal, notes							
						) x
						LEFT JOIN jp_menu y
						ON x.menu_id = y.id
						LEFT JOIN jp_diet z
						ON y.diet_id = z.id
						WHERE x.menu_id IN (0, -1)
					) b
				) a
				" . $exWhere . " 
				 ORDER BY a.tanggal, a.orders ASC
				"); 
				
		$result = $wpdb->get_results( $sql );
		
		/*Get color based on diet*/
		$allDiets = $wpdb->get_results('SELECT name, colordark, colorlight FROM jp_diet', ARRAY_A);
		$allDietsName = array_column($allDiets, 'name');		
		
		/*Get exclude list*/
		$userExcludeList = get_user_meta(wp_get_current_user()->ID, "excludeList", false); 	
		$userExcludeList = $userExcludeList[0];		
		
		$resultEl = array();
		
		if ( $result ) {			
			
			$sundayEl = $mondayEl = $tuesdayEl = $wednesdayEl = $thursdayEl = $fridayEl = $saturdayEl = '';
			$sundayNotes = $mondayNotes = $tuesdayNotes = $wednesdayNotes = $thursdayNotes = $fridayNotes = $saturdayNotes = '';
			
			$day1El = array();
			$day2El = array();
			$day3El = array();
			$day4El = array();
			$day5El = array();
			$day6El = array();
			$day7El = array();
			
			$recipeAll = array();
			$notesAll = array();

			$previousRecipe = -99;
			
			$day1 = array();
			$day2 = array();
			$day3 = array();
			$day4 = array();
			$day5 = array();
			$day6 = array();
			$day7 = array();
			
			foreach ( $result as $key ){
				$post_categories = wp_get_post_categories( $key->recipe_id );
				
				$excluded = false;
				foreach($post_categories as $c) {
					
					foreach($userExcludeList as $d) {
						
						if($d == $c)
							$excluded = true;
					}
				}
							
				if(!$excluded) {
					
					$recipe = ''; $titleRecipe = 'Notes'; $thumbnailUrl = '';
					if($key->recipe_id != 0) {
						$recipe = new TwentyDishes_Recipe($key->recipe_id);
						$titleRecipe = $recipe->title;
						$thumbnailUrl = $recipe->thumbmedium;
					} 
							
					$tanggalDateOnly = date_create( $key->tanggal );							
					$tanggalRecipe = date_format( $tanggalDateOnly, 'F j' );
					$dayRecipe = date_format( $tanggalDateOnly, 'l' );
					$dateorderRecipe = $key->date_order;
					$ordersRecipe = $key->orders;
					
					$notes = $key->notes;
					
					$recipeId = $key->recipe_id;
					$menuId = $key->menu_id;
					$tanggal = $key->tanggal;
	
					$cardColor = 'background-color:#ffffff; color:#444444; border:1px solid #cccccc;';
					$buttonColor = 'color:#555555;';			
					
					$notesEl = '';
					$recipeEl = '';
					
					if ( !empty( $notes ) ) {
						  
						$recipeEl["id"] = 0;
						$recipeEl["menuid"] = 0;
						$recipeEl["name"] = $notes ;	
					}
					else {
						if($titleRecipe == "recipe deleted")
							continue;
							
						$recipeEl = '<li class="recipe" style="' . $cardColor . '" recipeid="' . $recipeId . '" menuid="' . $menuId . '">
										<div class="recipe-header">' . (empty($key->menu_name)? 'Custom' : $key->menu_name) . '</div>
										<div class="recipe-content-wrapper">
											<div class="recipe-thumbnail" style="background-image:url(' . $thumbnailUrl . ');">
												
											</div>
											<div class="recipe-content">                                            	
												' . $titleRecipe . '
											</div>
											<div class="recipe-remove">
												<div class="glyphicon glyphicon-eye-close"></div>										
											</div>
											<div class="recipe-hover">											
												<div class="icon tdicon-notes" style="' . $buttonColor . '"></div>										
												<div class="recipe-insight">												
												</div>
											</div>
											<div class="clear"></div>
										</div>
									</li>';	
						
						$recipeEl = array();
						$recipeEl["id"] = $recipeId;
						$recipeEl["name"] = $titleRecipe;
						$recipeEl["menuid"] = $menuId;
						//$recipeEl["color"] = $buttonColor;
						$recipeEl["type"] = (empty($key->menu_name)? 'Custom' : $key->menu_name);
						$recipeEl["thumb"] = $thumbnailUrl;							
						$recipeEl["detail"] = previewrecipe( $recipeId );	
					}
					   
					if ( $dateorderRecipe == 1 ) 
					{
						$recipeEl["day"] = '1';
							
						if( in_array($recipeId, $day1) )
							continue;
						else {
							$day1[] = $recipeId;
							$day1El["recipe"][] = $recipeEl;
						}
						
						$sundayEl .= $recipeEl;
						$sundayNotes .= $notesEl;
						$sundayDate = $tanggalRecipe;
						$sundayDay = 'Day 1';
					}
					else if ( $dateorderRecipe == 2 ) 
					{
						$recipeEl["day"] = '2';						
						
						if( in_array($recipeId, $day2) )
							continue;
						else {
							$day2[] = $recipeId;
							$day2El["recipe"][] = $recipeEl;
						}
							
						$mondayEl .= $recipeEl;
						$mondayNotes .= $notesEl;
						$mondayDate = $tanggalRecipe;
						$mondayDay = 'Day 2';											
					}
					else if ( $dateorderRecipe == 3 ) 
					{
						$recipeEl["day"] = '3';
						
						if( in_array($recipeId, $day3) )
							continue;
						else {
							$day3[] = $recipeId;
							$day3El["recipe"][] = $recipeEl;
						}
							
						$tuesdayEl .= $recipeEl;
						$tuesdayNotes .= $notesEl;
						$tuesdayDate = $tanggalRecipe;
						$tuesdayDay = 'Day 3';
					}
					else if ( $dateorderRecipe == 4 ) 
					{
						$recipeEl["day"] = '4';
						
						if( in_array($recipeId, $day4) )
							continue;
						else {
							$day4[] = $recipeId;
							$day4El["recipe"][] = $recipeEl;
						}
							
						$wednesdayEl .= $recipeEl;
						$wednesdayNotes .= $notesEl;
						$wednesdayDate = $tanggalRecipe;
						$wednesdayDay = 'Day 4';
					}
					else if ( $dateorderRecipe == 5 ) 
					{						
						$recipeEl["day"] = '5';
						
						if( in_array($recipeId, $day5) )
							continue;
						else {
							$day5[] = $recipeId;
							$day5El["recipe"][] = $recipeEl;
						}
							
						$thursdayEl .= $recipeEl;
						$thursdayNotes .= $notesEl;
						$thursdayDate = $tanggalRecipe;
						$thursdayDay = 'Day 5';
					}
					else if ( $dateorderRecipe == 6 ) 
					{						
						$recipeEl["day"] = '6';
						
						if( in_array($recipeId, $day6) )
							continue;
						else {
							$day6[] = $recipeId;
							$day6El["recipe"][] = $recipeEl;
						}
							
						$fridayEl .= $recipeEl;
						$fridayNotes .= $notesEl;
						$fridayDate = $tanggalRecipe;
						$fridayDay = 'Day 6';
					}
					else if ( $dateorderRecipe == 7 ) 
					{
						$recipeEl["day"] = '7';
						
						if( in_array($recipeId, $day7) )
							continue;
						else {
							$day7[] = $recipeId;
							$day7El["recipe"][] = $recipeEl;
						}
							
						$saturdayEl .= $recipeEl;
						$saturdayNotes .= $notesEl;
						$saturdayDate = $tanggalRecipe;
						$saturdayDay = 'Day 7';
					}						
				}		
																								
			}
		}	
		
		// Untuk tombol add new recipe
		$day1AddNewRecipeEl = array();
		$day2AddNewRecipeEl = array();
		$day3AddNewRecipeEl = array();
		$day4AddNewRecipeEl = array();
		$day5AddNewRecipeEl = array();
		$day6AddNewRecipeEl = array();
		$day7AddNewRecipeEl = array();
		
		$day1AddNewRecipeEl["id"] = $day2AddNewRecipeEl["id"] = $day3AddNewRecipeEl["id"] = $day4AddNewRecipeEl["id"] = $day5AddNewRecipeEl["id"] = $day6AddNewRecipeEl["id"] = $day7AddNewRecipeEl["id"] = -1;
		
		$day1AddNewRecipeEl["name"] = $day2AddNewRecipeEl["name"] = $day3AddNewRecipeEl["name"] = $day4AddNewRecipeEl["name"] = $day5AddNewRecipeEl["name"] = $day6AddNewRecipeEl["name"] = $day7AddNewRecipeEl["name"] = "Add New Recipe";
		
		$day1AddNewRecipeEl["day"] = 1;
		$day2AddNewRecipeEl["day"] = 2;
		$day3AddNewRecipeEl["day"] = 3;
		$day4AddNewRecipeEl["day"] = 4;
		$day5AddNewRecipeEl["day"] = 5;
		$day6AddNewRecipeEl["day"] = 6;
		$day7AddNewRecipeEl["day"] = 7;
		
		$day1El["recipe"][] = $day1AddNewRecipeEl;
		$day2El["recipe"][] = $day2AddNewRecipeEl;
		$day3El["recipe"][] = $day3AddNewRecipeEl;
		$day4El["recipe"][] = $day4AddNewRecipeEl;
		$day5El["recipe"][] = $day5AddNewRecipeEl;
		$day6El["recipe"][] = $day6AddNewRecipeEl;
		$day7El["recipe"][] = $day7AddNewRecipeEl;
		
		// Con
		$resultEl["day1"] = $day1El;			
		$resultEl["day2"] = $day2El;
		$resultEl["day3"] = $day3El;
		$resultEl["day4"] = $day4El;
		$resultEl["day5"] = $day5El;
		$resultEl["day6"] = $day6El;
		$resultEl["day7"] = $day7El;	
		
		$tips4path = "<img src='" . get_template_directory_uri() . "/img/tips4.jpg'>";
		$tips5path = "<img src='" . get_template_directory_uri() . "/img/tips5.jpg'>";
			
		// Return			
		return $resultEl;
	}
?>