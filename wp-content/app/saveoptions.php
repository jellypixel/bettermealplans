<?php
	if ( isset( $_POST['servingSize'] ) && isset( $_POST['userid'] ) ) {
		define('WP_USE_THEMES', false);
		require_once('../../.wordpress/wp-load.php');
		
		// Log them in
		wp_set_current_user( $_POST['userid'] );
		
		$jsonDiet = json_decode(str_replace('\\', '', $_POST['dietPlans']));
		$jsonExclude = json_decode(str_replace('\\', '', $_POST['excludeList']));
		$ajaxResult = saveoptions($_POST['servingSize'], $jsonDiet, $jsonExclude);
		
		if($ajaxResult)
			$response['result'] = true;
		else
			$response['result'] = false;
			
		echo json_encode($response);
	}	
	
	function saveoptions($servingSize, $dietPlans, $excludeList) {		
		try {
			$current_user = wp_get_current_user();
			
			update_user_meta( $current_user->ID, "servings", $servingSize); 
			update_user_meta( $current_user->ID, "diets", $dietPlans);
			update_user_meta( $current_user->ID, "excludeList", $excludeList);

			return true;
			
		} catch (Exception $ex) {
			
			return false;
		}
	}
?>