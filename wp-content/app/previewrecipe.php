<?php
	if ( isset( $_POST['recipeid'] ) ) {
		define('WP_USE_THEMES', false);
		require_once('../../.wordpress/wp-load.php');
		$ajaxResult = previewrecipe($_POST['recipeid']);
		echo $ajaxResult;
	}	
	
	function previewrecipe($recipeid) {		
		
		// Get the Recipe Details
		$recipe = new TwentyDishes_Recipe($recipeid);
		
		$title = $recipe->title; 
		$yield = $recipe->yield;
		$readytime = $recipe->readytime; 
		$cooktime = $recipe->cooktime; 
		$difficulty = $recipe->difficulty; 
		$thumb = $recipe->thumbmedium;
		$ingredients = $recipe->ingredients;
		$instructions = $recipe->instructions;
		$cats = $recipe->cats;
		$nutritional = $recipe->nutritional;
		$star = $recipe->getFavClass();
		$summary = $recipe->summary;
		
		$de = '^%$^';			
		return 	$title . $de . 
					$yield . $de . 
					$readytime . $de . 
					$thumb . $de . 
					$recipe->printIngredients() . $de .  
					$recipe->printInstructions() . $de . 
					$cats[ count($cats) -1 ]->name . $de . 
					$cooktime . $de . 
					$difficulty . $de . 
					$star . $de . 
					$recipeid . $de . 
					"<div class='addrecipe-longcontent'>" . reverseTransform($nutritional) . "</div>" . $de . 
					$summary;
	}
	
	function reverseTransform($value)
    {
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;

        // Load contents wrapped in a temporary root node
        $dom->loadXML('<root>' . $value . '</root>');

        // Use an XPath query to get all P elements
        $xPath = new DOMXPath($dom);
        $pTags = $xPath->query('//div');

        // Loop through the P elements
        $dataStart = 0;
        $dataEnd   = 0;

		$orderTmp = 0;
        foreach ($pTags as $pTag) {
	        
	        if(( $pTag->getAttribute('class') != 'nutrition-item' ) && ($pTag->getAttribute('class') != 'nutrition-sub-item')) {
	        
	            // Get any DIV elements inside the P
	            $divs = $xPath->query('./p', $pTag);
	
	            if ($divs->length > 0) {
	                // This P element already has a div. Grab the
	                // data-start/end attributes for later
	                $div = $divs->item(0);
	                $div->setAttribute('class', $pTag->getAttribute('class') . '_p');
	            }
	            else {
	                // Create a new DIV element and set attributes
	                $div = $dom->createElement('p');
	                $div->setAttribute('class', $pTag->getAttribute('class') . '_p');
	
	                // Move all children of P into DIV
	                $child = $pTag->firstChild;
	                while ($child) {
	                    $nextChild = $child->nextSibling;
	                    $div->insertBefore($child);
	                    $child = $nextChild;
	                }
	
	                // Move the DIV inside the P element
	                $pTag->appendChild($div);
	            }
	        }
	        
	        if( $pTag->getAttribute('class') == 'nutrition-percentage' ) {
	            
	            $orderTmp++;
	            
	            if($orderTmp == 2)
	            	$pTag->setAttribute('class', 'nutrition-percentage_fullwidth');
	        }
	        
	        if( $pTag->getAttribute('class') == 'nutrition-serving' ) {
	            
	            $pTag->nodeValue = trim($pTag->nodeValue);
	        }
        }
        
        $pTags = $xPath->query('//span');

        // Loop through the P elements
        $dataStart = 0;
        $dataEnd   = 0;

		$orderTmp = 0;
        foreach ($pTags as $pTag) {
     	    if(( $pTag->getAttribute('class') != 'nutrition-item' ) && ($pTag->getAttribute('class') != 'nutrition-sub-item')) {
	            // Get any DIV elements inside the P
	            $divs = $xPath->query('./p', $pTag);
	
	            if ($divs->length > 0) {
	                // This P element already has a div. Grab the
	                // data-start/end attributes for later
	                $div = $divs->item(0); 
	                $div->setAttribute('class', $pTag->getAttribute('class') . '_p');
	            }
	            else {
	                // Create a new DIV element and set attributes
	                $div = $dom->createElement('p');
	                $div->setAttribute('class', $pTag->getAttribute('class') . '_p');
	
	                // Move all children of P into DIV
	                $child = $pTag->firstChild;
	                while ($child) {
	                    $nextChild = $child->nextSibling;
	                    $div->insertBefore($child);
	                    $child = $nextChild;
	                }
	
	                // Move the DIV inside the P element
	                $pTag->appendChild($div);
	            }
            }
            
            if( $pTag->getAttribute('class') == 'nutrition-percentage' ) {
	            
	            $orderTmp++;
	            
	            if($orderTmp == 2)
	            	$pTag->setAttribute('class', 'nutrition-percentage_fullwidth');
	        }
	        
	        if( $pTag->getAttribute('class') == 'nutrition-serving' ) {
	            
	            $pTag->nodeValue = trim($pTag->nodeValue);
	        }
        }
                
        // Get HTML, removing temporary root element
        $html = preg_replace(
            '#.*?<root>\s*(.*)\s*</root>#s', '\1',
            $dom->saveXML()
        );
        
        $html = str_replace("span", "div", $html);
        return $html;
    }
?>