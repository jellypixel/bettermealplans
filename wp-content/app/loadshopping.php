<?php
	
	if ( isset( $_POST['recipes'] ) ) {
		define('WP_USE_THEMES', false);
		require_once('../../.wordpress/wp-load.php');
		
		// Log them in
		wp_set_current_user( $_POST['userid'] );
		
		$userServingSize = get_user_meta(wp_get_current_user()->ID, "servings", true); 
		
		$ajaxResult = "";
		if($_POST['mode'] == 1) 
			$ajaxResult = loadshopping($_POST['recipes'], $userServingSize);
		else if ($_POST['mode'] == 2) 
			$ajaxResult = loadsavedshopping($_POST['userid'], $_POST['targetDate'], $_POST['recipes'], $userServingSize);

		//echo $ajaxResult;
		$response['result'] = $ajaxResult;
		echo json_encode($response);
	}	
	
	function loadshopping($recipes, $userServingSize) {		

		try {
			// Prepare Ingredients
			$arrDetail = array_filter(explode("^)^@", $recipes));
			$recipeIngredients = array();	

			foreach($arrDetail as $key){

				$recipe = new TwentyDishes_Recipe($key);
				$recipeServing = $recipe->yield;
				$recipeIngredient = $recipe->ingredients;
				
				for($i = 0; $i < count($recipeIngredient); $i++) 
					$recipeIngredient[$i]["amount_singleserve"] = $recipeIngredient[$i]["amount"] / $recipeServing;

				$recipeIngredients = isset($recipeIngredients)? $recipeIngredients : array();
				$recipeIngredients = array_merge($recipeIngredient, $recipeIngredients);		
			}
			
			/*Prepare Shopping List*/
			$finalIngredients = array();
			foreach ($recipeIngredients as $ingredient)
			{
				$term = get_term_by( "name", $ingredient["name"], "ingredient" );
				if(!$term)
					$term = get_term_by( "slug", str_replace(' ', '-', $ingredient["name"]), "ingredient" );
				
				if($term) {
					$key = $term->term_id;

					$finalIngredients[$key]["key"] = $key;
					$finalIngredients[$key]["ingredient_id"] = $key;
					$finalIngredients[$key]["amount"] += $ingredient["amount"];
					$finalIngredients[$key]["unit"] = $ingredient["unit"];
					$finalIngredients[$key]["name"] = $ingredient["name"];
					$finalIngredients[$key]["amount_singleserve"] += $ingredient["amount_singleserve"];
					$finalIngredients[$key]["amount_multiserve"] += ceil(($ingredient["amount_singleserve"] * $userServingSize) * 4) / 4;
				
					$termGroup = get_term( $key, "ingredient" );
					$finalIngredients[$key]["group"] = get_term( $termGroup->parent, "ingredient" )->name;
					
					if( empty($finalIngredients[$key]["group"]) )
						$finalIngredients[$key]["group"] = "Others";
				}
			}
			
			foreach ($finalIngredients as $key => $row) {
				$group[$key]  = $row['group'];
				$ing[$key] = $row['ingredient'];
			}							
			@array_multisort($group, SORT_ASC, $ing, SORT_ASC, $finalIngredients);			
			
			// Prepare HTML and Styling
			$currGroup = ""; $html = ""; $styleFirst = 'style="margin-top:0px;"';
			foreach ($finalIngredients as $ingredient)
			{
				// Exception
				if(($ingredient["name"] == "water") || ($ingredient["name"] == "ice"))
					continue;
					
				if($currGroup != $ingredient["group"]) {
					$logoGroup = '';
					$groupName = $ingredient["group"];
					
					if ( $ingredient["group"] == "Dairy" ) {
						$logoGroup = '<div id="dairy" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingdairy.png" /></div>';
					}
					else if ( $ingredient["group"] == "Meat" ) {
						$logoGroup = '<div id="meat" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingmeat.png" /></div>';
					}
					else if ( $ingredient["group"] == "Pantry" ) {
						$logoGroup = '<div id="pantry" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingpantry.png" /></div>';
					}
					else if ( $ingredient["group"] == "Produce" ) {
						$logoGroup = '<div id="produce" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingproduce.png" /></div>';
					}
					else if ( $ingredient["group"] == "Spices" ) {
						$logoGroup = '<div id="spices" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingspices.png" /></div>';
					}
					else {
						$logoGroup = '<div id="others" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingothers.png" /></div>';
						$groupName = "Others";
					}					
					
					$html .= "<div id='" . $groupName . "' class='ingredientTitle' ".$styleFirst."><span>" . $groupName . "</span>" . $logoGroup . "</div>";	
					$currGroup = $ingredient["group"]; $styleFirst = "";
				} 	
				
				if($ingredient["name"] == "egg(s)")
					$ingredient["amount_multiserve"] = ceil($ingredient["amount_multiserve"]);	
					
				if(
					(
						($ingredient["name"] == "avocado oil") ||
						($ingredient["name"] == "bacon grease") ||
						($ingredient["name"] == "coconut oil") ||
						($ingredient["name"] == "olive oil") ||
						($ingredient["group"] == "Oils")
					) &&
					($ingredient["amount_multiserve"] >= 4)
				  ) {
					
					$ingredient["group"] = "Oils";
					$ingredient["unit"] = "cup(s)";
					
					$cups = floor($ingredient["amount_multiserve"] / 16);
					$tbsp = $ingredient["amount_multiserve"] % 16;
					
					if($tbsp == 4) {
						$cups += 0.25;
						$tbsp = 0;
					}
					else if(($tbsp >= 5) && ($tbsp <= 6)) {
						$cups += 0.33;
						$tbsp = 0;
					}
					else if(($tbsp >= 7) && ($tbsp <= 8)) {
						$cups += 0.5;
						$tbsp = 0;
					}
					else if(($tbsp >= 9) && ($tbsp <= 10)) {
						$cups += 0.67;
						$tbsp = 0;
					}				
					else if(($tbsp >= 11) && ($tbsp <= 13)) {
						$cups += 0.75;
						$tbsp = 0;
					}
					else if(($tbsp >= 14) && ($tbsp <= 15)) {
						$cups += 1;
						$tbsp = 0;
					}																				
					
					$ingredient["amount_multiserve"] = $cups;
					$ingredient["amount_multiserve2"] = $tbsp;
				}									
				
				$html .= "<div class='ingredient_list_ingredient ".$ingredient["group"]."' id='ingredient_".$ingredient["ingredient_id"]."'>";
					$html .= "<input class='col_amount pure' value=".round($ingredient["amount_multiserve"],2)." /> ";
					$html .= "<span class='col_amount_print pure' value=".round($ingredient["amount_multiserve"],2)."></span> ";
					$html .= "<span class='col_ingredient ".$ingredient["unit"]."'>" . $ingredient["unit"] . " " . $ingredient["name"] . "</span>";
				$html .= "</div>";
			}				
			
			if($html == "")
				return "";
			else  {
				return groupArray($finalIngredients, "group");
			}
		} catch (Exception $ex) {
			
			return "Unknown Error";
		}
	}
	
	function loadsavedshopping($userid, $targetDate, $recipes, $userServingSize) {	
			
		try {
			
			// Get saved recipes
			$targetDateFormatted = date( 'm/d/Y', strtotime( $targetDate ) );
			$targetDay = date( 'w', strtotime( $targetDate ) );
			$arrTargetDate = array();
			$arrTargetDate[0] = date( 'Y-m-d', strtotime( $targetDateFormatted . '-' . $targetDay . ' days' ) );
			
			global $wpdb;
			$sql = $wpdb->prepare( 'SELECT a.*, b.note
									FROM jp_shoppinglist a, jp_shoppingnote b
									WHERE 
										a.user_id = %d AND
										a.schedule = %s AND a.user_id = b.user_id AND a.schedule = b.schedule
									', $userid, $arrTargetDate[0] ); 
		
			$result = $wpdb->get_results( $sql );		

			// Prepare Ingredients
			$arrDetail = array_filter(explode("^)^@", $recipes));
			$recipeIngredients = array();
				
			foreach($arrDetail as $key){
				
				$recipe = new TwentyDishes_Recipe($key);
				$recipeServing = $recipe->yield;
				$recipeIngredient = $recipe->ingredients;

				for($i = 0; $i < count($recipeIngredient); $i++)  {
					$recipeIngredient[$i]["amount_singleserve"] = $recipeIngredient[$i]["amount"] / $recipeServing;
				}
				
				$recipeIngredients = isset($recipeIngredients)? $recipeIngredients : array();
				$recipeIngredients = array_merge($recipeIngredient, $recipeIngredients);		
			}
			
			/*Prepare Shopping List*/
			$finalIngredients = array();
			$finalIngredients[0]["key"] = 0;
			$finalIngredients[0]["ingredient_id"] = 0;
			$finalIngredients[0]["amount"] = 0;
			$finalIngredients[0]["unit"] = 0;
			$finalIngredients[0]["name"] = $result[0]->note;
			$finalIngredients[0]["amount_singleserve"] = 0;
			$finalIngredients[0]["amount_multiserve"] = 0;
			$finalIngredients[0]["group"] = "Notes";
			
			foreach ($recipeIngredients as $ingredient)
			{
				$term = get_term_by( "name", $ingredient["name"], "ingredient" );
				if(!$term)
					$term = get_term_by( "slug", str_replace(' ', '-', $ingredient["name"]), "ingredient" );
				
				if($term) {
					$key = $term->term_id;
					
					$finalIngredients[$key]["key"] = $key;
					$finalIngredients[$key]["ingredient_id"] = $key;
					$finalIngredients[$key]["amount"] += $ingredient["amount"];
					$finalIngredients[$key]["unit"] = $ingredient["unit"];
					$finalIngredients[$key]["name"] = $ingredient["name"];
					$finalIngredients[$key]["amount_singleserve"] += $ingredient["amount_singleserve"];
					$finalIngredients[$key]["amount_multiserve"] += ceil(($ingredient["amount_singleserve"] * $userServingSize) * 4) / 4;
				
					$termGroup = get_term( $key, "ingredient" );
					$finalIngredients[$key]["group"] = get_term( $termGroup->parent, "ingredient" )->name;
					$finalIngredients[$key]["stroked"] = "no";
					
					if( empty($finalIngredients[$key]["group"]) )
						$finalIngredients[$key]["group"] = "Others";
				}
			}
			
			foreach ($finalIngredients as $key => $row) {
				$group[$key]  = $row['group'];
				$ing[$key] = $row['ingredient'];
			}							
			@array_multisort($group, SORT_ASC, $ing, SORT_ASC, $finalIngredients);			
			
			$currGroup = "";
			foreach ($finalIngredients as $ingredient)
			{
				if($currGroup == $ingredient["group"]) {
				
					array_push($finalIngredients, $ingredient);
					array_shift($finalIngredients);
					
				} else {
					
					break;
				}
			}
			
			// Prepare HTML and Styling
			$currGroup = "neverinto"; $html = ""; $styleFirst = 'style="margin-top:0px;"';
			foreach ($finalIngredients as &$ingredient)
			{
				// Exception
				if(($ingredient["name"] == "water") || ($ingredient["name"] == "ice"))
					continue;
									
				if($currGroup != $ingredient["group"]) {
					$logoGroup = '';
					$groupName = $ingredient["group"];
					
					if ( $ingredient["group"] == "Dairy" ) {
						$logoGroup = '<div id="dairy" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingdairy.png" /></div>';
					}
					else if ( $ingredient["group"] == "Meat" ) {
						$logoGroup = '<div id="meat" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingmeat.png" /></div>';
					}
					else if ( $ingredient["group"] == "Pantry" ) {
						$logoGroup = '<div id="pantry" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingpantry.png" /></div>';
					}
					else if ( $ingredient["group"] == "Produce" ) {
						$logoGroup = '<div id="produce" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingproduce.png" /></div>';
					}
					else if ( $ingredient["group"] == "Spices" ) {
						$logoGroup = '<div id="spices" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingspices.png" /></div>';
					}
					else {
						$logoGroup = '<div id="others" class="logogroup"><img src="' . get_bloginfo('template_url') . '/sys/img/ingothers.png" /></div>';
						$groupName = "Others";
					}					
					
					$html .= "<div id='" . $groupName . "' class='ingredientTitle' ".$styleFirst."><span>" . $groupName . "</span>" . $logoGroup . "</div>";	
					$currGroup = $ingredient["group"]; $styleFirst = "";
				} 			
								
				// This is gonna be slooooow... Better save it as an ID later on
				$savedQuantity = "";
				$stroked = "";
				foreach( $result as $key => $row) {

					// each column in your row will be accessible like this
					$savedIngredient = $row->ingredient;
					$quantity = $row->quantity;
					
					if(
						(
							($ingredient["name"] == "avocado oil") ||
							($ingredient["name"] == "bacon grease") ||
							($ingredient["name"] == "coconut oil") ||
							($ingredient["name"] == "olive oil") ||
							($ingredient["group"] == "Oils")
						)
						
					) {
						
						$ingredient["group"] = "Oils";
						$ingredient["unit"] = "cup(s)";
						
					}					
					
					if( trim($savedIngredient) == trim($ingredient["unit"] . " " . $ingredient["name"]) ) {
						
						$savedQuantity = $quantity;
						
						if( $row->crossed == 1 )
							$stroked = "stroked";						
					}
					
				}
				
				if($stroked == "stroked")
					$ingredient["stroked"] = "yes";
				
				if($ingredient["name"] == "egg(s)")
					$ingredient["amount_multiserve"] = ceil($ingredient["amount_multiserve"]);
				
				if(
					(
						($ingredient["name"] == "avocado oil") ||
						($ingredient["name"] == "bacon grease") ||
						($ingredient["name"] == "coconut oil") ||
						($ingredient["name"] == "olive oil") ||
						($ingredient["group"] == "Oils")
					) 
					
				  ) {
					
					$ingredient["group"] = "Oils";
					$ingredient["unit"] = "cup(s)";
					
					$cups = floor($ingredient["amount_multiserve"] / 16);
					$tbsp = $ingredient["amount_multiserve"] % 16;
					
					if($tbsp == 4) {
						$cups += 0.25;
						$tbsp = 0;
					}
					else if(($tbsp >= 5) && ($tbsp <= 6)) {
						$cups += 0.33;
						$tbsp = 0;
					}
					else if(($tbsp >= 7) && ($tbsp <= 8)) {
						$cups += 0.5;
						$tbsp = 0;
					}
					else if(($tbsp >= 9) && ($tbsp <= 10)) {
						$cups += 0.67;
						$tbsp = 0;
					}				
					else if(($tbsp >= 11) && ($tbsp <= 13)) {
						$cups += 0.75;
						$tbsp = 0;
					}
					else if(($tbsp >= 14) && ($tbsp <= 15)) {
						$cups += 1;
						$tbsp = 0;
					}																				
					
					$ingredient["amount_multiserve"] = $cups;
					$ingredient["amount_multiserve2"] = $tbsp;
				}					
				
				if($savedQuantity == "") {
									
					$html .= "<div class='ingredient_list_ingredient $stroked ".$ingredient["group"]."' id='ingredient_".$ingredient["ingredient_id"]."'>";
						$html .= "<input class='col_amount pure $stroked' value=".round($ingredient["amount_multiserve"],2)." /> ";
						$html .= "<span class='col_amount_print pure $stroked' value=".round($ingredient["amount_multiserve"],2)."></span> ";
						$html .= "<span class='col_ingredient $stroked ".$ingredient["unit"]."'>" . $ingredient["unit"] . " " . $ingredient["name"] . "</span>";
					$html .= "</div>";
				
				} else {
					$ingredient["amount_multiserve"] = $savedQuantity;
					
					$html .= "<div class='ingredient_list_ingredient $stroked ".$ingredient["group"]."' id='ingredient_'".$ingredient["ingredient_id"].">";
						$html .= "<input class='col_amount $stroked' value='".$savedQuantity."' /> ";
						$html .= "<input class='col_default $stroked' value=".round($ingredient["amount_multiserve"],2)." disabled />";
						$html .= "<span class='col_amount_print $stroked' value='".$savedQuantity."'></span> ";
						$html .= "<span class='col_ingredient $stroked ".$ingredient["unit"]."'>" . $ingredient["unit"] . " " . $ingredient["name"] . "</span>";
					$html .= "</div>";				
				}
				
			}						    	
				
			if($html == "")
				return "";
			else 
				return groupArray($finalIngredients, "group");
			
		} catch (Exception $ex) {
			
			return "Unknown error";
		}
	}	
	
	function groupArray($arr, $group, $preserveGroupKey = false, $preserveSubArrays = false) {
		$temp = array();
		foreach($arr as $key => $value) {
			$groupValue = $value[$group];
			if(!$preserveGroupKey)
			{
				unset($arr[$key][$group]);
			}
			if(!array_key_exists($groupValue, $temp)) {
				$temp[$groupValue] = array();
			}
	
			if(!$preserveSubArrays){
				$data = count($arr[$key]) == 1? array_pop($arr[$key]) : $arr[$key];
			} else {
				$data = $arr[$key];
			}
			$temp[$groupValue][] = $data;
		}
		return $temp;
	}
?>