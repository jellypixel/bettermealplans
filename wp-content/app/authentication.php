<?php 
	require_once('../../.wordpress/wp-load.php');
	 
	$response = array(
		'data'		=> array(),
		'msg'		=> 'Invalid email or password',
		'status'	=> false
	);

	$applesharedsecret = "0546f945455c4622a37a964b553ffc54";
	 
	/* Sanitize all received posts */
	foreach($_POST as $k => $value){
		$_POST[$k] = sanitize_text_field($value);
	}
	
	/**
	 * Login Method
	 *
	 */
	if(isset( $_POST['type'] )) {
		if( $_POST['type'] == 'login' )
		{
		
			/* Get user data */
			$user = get_user_by( 'email', $_POST['email'] );

			if(!$user)
				$user = get_user_by( 'login', $_POST['email'] ); 
			
			if ( $user )
			{
				$password_check = wp_check_password( $_POST['password'], $user->user_pass, $user->ID );
			
				if ( $password_check )
				{

					$memberpressCheck = false;

					wp_set_current_user($user->ID);
					if(current_user_can('memberpress_product_authorized_25456') && !current_user_can( 'update_core' ) && !current_user_can('memberpress_product_authorized_24386') && !current_user_can('memberpress_product_authorized_24567') && !current_user_can('memberpress_product_authorized_25004') && !current_user_can('memberpress_product_authorized_24312') && !current_user_can('memberpress_product_authorized_24361') && !current_user_can('memberpress_product_authorized_24314')) {
						// iOS Member.

						if(!empty($_POST['transactionReceipt'])) //iOS user first entry. Registering its token
							$memberpressCheck = true;
						else {
							//iOS Member check token
							$receiptbytes      = get_user_meta($user->ID, "transactionReceipt", true);

							$request = json_encode(array("receipt-data" => $receiptbytes,"password"=>$applesharedsecret));
							$ch = curl_init("https://buy.itunes.apple.com/verifyReceipt");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
							$jsonresult = curl_exec($ch);

							if ($jsonresult === false) {
								$info = curl_getinfo($curl);
								curl_close($curl);
								
								$response['status'] = 6;
								$response['msg'] = 'Unable to contact Apple Server. Please try again later.';
								
								echo json_encode($response);	
								exit();
							}
						
							curl_close($curl);
							$decoded = json_decode($jsonresult);
							
							if(empty($decoded->latest_receipt_info)) {

								// Try staging first
								// iOS Member check token
								$receiptbytes      = get_user_meta($user->ID, "transactionReceipt", true);

								$request = json_encode(array("receipt-data" => $receiptbytes,"password"=>$applesharedsecret));
								$ch = curl_init("https://sandbox.itunes.apple.com/verifyReceipt");
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
								curl_setopt($ch, CURLOPT_POST, true);
								curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
								$jsonresult = curl_exec($ch);

								if ($jsonresult === false) {
									$info = curl_getinfo($curl);
									curl_close($curl);
									
									$response['status'] = 6;
									$response['msg'] = 'Unable to contact Apple Server. Please try again later.';
									
									echo json_encode($response);	
									exit();
								}
							
								curl_close($curl);
								$decoded = json_decode($jsonresult);	
							} 

							if(empty($decoded->latest_receipt_info)) {
								$memberpressCheck = false;
							} else {
								$howmany = count($decoded->latest_receipt_info);
								$expirems = $decoded->latest_receipt_info[$howmany-1]->expires_date_ms;
								$nowms = round(microtime(true) * 1000);

								if($expirems > $nowms)
									$memberpressCheck = true;
								else
									$memberpressCheck = false;
							}
						}
					}
					else if(current_user_can('memberpress_authorized')) {
						// Ordinary member
						$memberpressCheck = true;
					} else {
						// User is there but not a memberpress user
						$memberpressCheck = false;
					}

					if($memberpressCheck) {
						/* Generate a unique auth token */
						$token = md5( $_POST['email'] . $_POST['password'] . rand() );
							
						/* Store / Update auth token in the database */
						if( update_user_meta( $user->ID, 'auth_token', $token ) )
						{					
							// Store iOS Token if exist
							if(!empty($_POST['transactionReceipt']))
								update_user_meta( $user->ID, 'transactionReceipt', $_POST['transactionReceipt']);

							/* Return generated token and user ID*/
							$response['status'] = 1;
							$response['data'] = array(
								'auth_token' 	=>	$token,
								'user_id'		=>	$user->ID,
								'user_login'	=>	$user->user_login,
								'user_email'	=>	$user->user_email,
								'user_firstname' => $user->user_firstname,
								'user_lastname' => $user->user_lastname
							);
							$response['msg'] = 'Successfully Authenticated';
							
							echo json_encode($response);	
						}
					} else {
						$response['status'] = 5;
						$response['msg'] = $jsonresult;
						
						echo json_encode($response);	
					}
					
				} 
				else 
				{
				
					$response['status'] = 2;
					$response['msg'] = 'Wrong Password';
					
					echo json_encode($response);	
				}
			} 
			else 
			{			
				$response['status'] = 3;
				$response['msg'] = 'Wrong User';
				
				echo json_encode($response);	
			}
		} else if( $_POST['type'] == 'check' ) {

			/* Get user data */
			$user = get_user_by( 'email', $_POST['email'] );
			if(!$user) $user = get_user_by( 'login', $_POST['email'] ); 

			if ( $user )
			{
				wp_set_current_user($user->ID);
				$memberpressCheck = false; $iosMember = false;

				if(current_user_can('memberpress_product_authorized_25456') && !current_user_can( 'update_core' ) && !current_user_can('memberpress_product_authorized_24386') && !current_user_can('memberpress_product_authorized_24567') && !current_user_can('memberpress_product_authorized_25004') && !current_user_can('memberpress_product_authorized_24312') && !current_user_can('memberpress_product_authorized_24361') && !current_user_can('memberpress_product_authorized_24314')) {
					//iOS Member check token
					$iosMember = true;
					$receiptbytes = get_user_meta($user->ID, "transactionReceipt", true);

					$request = json_encode(array("receipt-data" => $receiptbytes,"password"=>$applesharedsecret));
					$ch = curl_init("https://buy.itunes.apple.com/verifyReceipt");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
					$jsonresult = curl_exec($ch);

					if ($jsonresult === false) {
						$info = curl_getinfo($curl);
						curl_close($curl);
						
						$response['status'] = 6;
						$response['msg'] = 'Unable to contact Apple Server. Please try again later.';
						
						echo json_encode($response);	
						exit();
					}
				
					curl_close($curl);
					$decoded = json_decode($jsonresult);

					if(empty($decoded->latest_receipt_info)) {
						// Try staging first
						$receiptbytes = get_user_meta($user->ID, "transactionReceipt", true);

						$request = json_encode(array("receipt-data" => $receiptbytes,"password"=>$applesharedsecret));
						$ch = curl_init("https://sandbox.itunes.apple.com/verifyReceipt");
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
						$jsonresult = curl_exec($ch);

						if ($jsonresult === false) {
							$info = curl_getinfo($curl);
							curl_close($curl);
							
							$response['status'] = 6;
							$response['msg'] = 'Unable to contact Apple Server. Please try again later.';
							
							echo json_encode($response);	
							exit();
						}
					
						curl_close($curl);
						$decoded = json_decode($jsonresult);
					}
					
					if(empty($decoded->latest_receipt_info)) {
						$memberpressCheck = false;
					} else {
						$howmany = count($decoded->latest_receipt_info);
						$expirems = $decoded->latest_receipt_info[$howmany-1]->expires_date_ms;
						$nowms = round(microtime(true) * 1000);

						if($expirems > $nowms)
							$memberpressCheck = true;
						else
							$memberpressCheck = false;
					}
				}
				else if(current_user_can('memberpress_authorized')) {
					// Ordinary member
					$memberpressCheck = true;
				} else {
					// User is there but not a memberpress user
					$memberpressCheck = false;
				}

				if($memberpressCheck) {
					if($iosMember) {
						$howmany = count($decoded->latest_receipt_info);
						$response['data'] = $decoded->latest_receipt_info[$howmany-1];
						$response['status'] = 1;
					} else {
						$response['status'] = 2;
					}

					$response['msg'] = 'Successfully Authenticated';
					
					echo json_encode($response);	
				} else {
					$response['status'] = 5;
					$response['msg'] = 'You are not registered or your registration has expired. Please contact us if you have or register using the registration link on the app.';
					
					echo json_encode($response);	
				}
			} 
			else 
			{			
				$response['status'] = 3;
				$response['msg'] = 'Wrong User';
				
				echo json_encode($response);	
			}
		}
	} else {			
		$response['status'] = 4;
		$response['msg'] = 'Empty User / Password';
		
		echo json_encode( $response );	
	}
	
	exit();
?>