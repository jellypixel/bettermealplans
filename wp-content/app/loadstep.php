<?php
	if ( isset( $_POST['recipes'] ) ) {
		define('WP_USE_THEMES', false);
		require_once('../../.wordpress/wp-load.php');
		
		// Log them in
		wp_set_current_user( $_POST['userid'] );
				
		$userServingSize = get_user_meta(wp_get_current_user()->ID, "servings", true); 
		$ajaxResult = loadstepbyrecipewithingredients($_POST['recipes'], $userServingSize);			

		$response['result'] = $ajaxResult;
	
		echo json_encode($response);
	}	
	
	function loadstepbyrecipewithingredients($recipes, $userServingSize) {		
		
		global $wpdb;
		
		$arrDetail = array_filter(explode("^)^@", $recipes));
			
		// Get recipe step by step
		$getRecipeID = implode(",", $arrDetail);
		$menuToUpdate = $wpdb->get_results( "	
												SELECT DISTINCT GROUP_CONCAT(d.recipe) as 'recipe', e.todo FROM jp_stepbystep_recipe d, jp_stepbystep e
												WHERE d.stepbystep = e.id AND d.recipe IN ($getRecipeID) AND e.id >= 122
												GROUP BY e.todo
												ORDER BY e.priority asc
											" );
											
		$view = array();
		
		$ingredientSummary = array(); $i = 1; $allRecipe = ""; $stepIndex = 0;
		foreach ($menuToUpdate as $row ){
			
			$view["Steps"][$stepIndex]["Todo"] = $row->todo;
			
			// Search todo for any occurences of ingredients
			preg_match_all('/\[([^\]]*)\]/', $row->todo, $out);
			
			// Search all recipes with this todo
			$recipeWithThisTodo = explode(",", $row->recipe);			
			
			// Iterate through each ingredient
			$ingredientsAndRecipes = "";
			foreach($out[1] as $foundIngredient) {
				
				// Iterate through each recipes and find the number of ingredients.
				foreach($recipeWithThisTodo as $foundRecipe){
					
					$recipe = new TwentyDishes_Recipe($foundRecipe);
					$recipeName = $recipe->title;

					$recipeName = str_replace("&#038;", "&", $recipeName);
					$recipeName = str_replace("&#8217;", "'", $recipeName);

					$recipeServing = $recipe->yield;
					$recipeIngredient = $recipe->ingredients;
		
					for($i = 0; $i < count($recipeIngredient); $i++) {
						
						$foundIngredient_var1 = $foundIngredient . "s";
						$foundIngredient_var2 = $foundIngredient . "(s)";
						$foundIngredient_var3 = preg_replace('/\(([^()]*+|(?R))*\)\s*/', '', $foundIngredient);
						$foundIngredient_var4 = rtrim($foundIngredient,"s");
						$foundIngredient_var5 = $foundIngredient_var4 . "(s)";
						
						if(
						     ($recipeIngredient[$i]["name"] == $foundIngredient) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var1) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var2) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var3) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var4) ||
						     ($recipeIngredient[$i]["name"] == $foundIngredient_var5)  
						) {
							
							// Add to list of recipe that got ingredient preparation
							if(strpos($allRecipe, $recipeName) === false) {
								if($allRecipe == "")
									$allRecipe .= $recipeName;
								else
									$allRecipe .= ', ' . $recipeName;
							}
								
							//Continue our business
							$singleServe = $recipeIngredient[$i]["amount"] / $recipeServing;
							$multiServe = float2rat(ceil(($singleServe * $userServingSize) * 4) / 4);
							
							if($ingredientsAndRecipes != "")
								$ingredientsAndRecipes .= ",<br>";
							
							$ingredientsAndRecipes .= $recipeName . " (" . $multiServe . " " . $recipeIngredient[$i]["unit"] . " $foundIngredient)";
							
							if( array_key_exists($foundIngredient, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;
								
							else if( array_key_exists($foundIngredient_var1, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;
								
							else if( array_key_exists($foundIngredient_var2, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;
								
							else if( array_key_exists($foundIngredient_var3, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;
								
							else if( array_key_exists($foundIngredient_var4, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;	
								
							else if( array_key_exists($foundIngredient_var5, $ingredientSummary) ) 
								$ingredientSummary[$foundIngredient][0] += $multiServe;	
								
							else 
								$ingredientSummary[$foundIngredient][0] = $multiServe;
								
							$ingredientSummary[$foundIngredient][1] = $recipeIngredient[$i]["unit"];

							$term = get_term_by( "name", $recipeIngredient[$i]["name"], "ingredient" );
							if(!$term)
								$term = get_term_by( "slug", str_replace(' ', '-', $recipeIngredient[$i]["name"]), "ingredient" );
							
							if($term)
								$ingredientSummary[$foundIngredient][2] = get_term( $term->parent, "ingredient" )->name;	
							else
								$ingredientSummary[$foundIngredient][2] = "";
						}
					}	
				}
			}
			
			// No ingredients. Just get the recipe name.
			if($ingredientsAndRecipes == ""){
				
				// Iterate through each recipes and find the number of ingredients.
				foreach($recipeWithThisTodo as $foundRecipe){
					
					$recipe = new TwentyDishes_Recipe($foundRecipe);
					$recipeName = $recipe->title;
					
					if($ingredientsAndRecipes != "")
						$ingredientsAndRecipes .= ",<br>";
							
					$ingredientsAndRecipes .= $recipeName;
					
				}				
			}
			
			$view["Steps"][$stepIndex]["Ingredients"] = $ingredientsAndRecipes;
			$view["Steps"][$stepIndex]["Stroked"] = "no";
			
			$i++;
			$stepIndex++;
		}
	
		foreach($ingredientSummary as $ingredientname=>$value) {
			
			/*
			($ingredientname == "avocado oil") ||
			($ingredientname == "bacon grease") ||
			($ingredientname == "coconut oil") ||
			($ingredientname == "olive oil") ||
			($ingredientname == "buttermilk") ||
			($ingredientname == "heavy cream") ||
			($ingredientname == "kefir") ||
			($ingredientname == "whole milk") ||
			($ingredientname == "almond milk") ||
			($ingredientname == "apple cider juice") ||
			($ingredientname == "apple cider vinegar") ||
			($ingredientname == "apple juice") ||
			($ingredientname == "avocado oil") ||
			($ingredientname == "balsamic vinegar") ||
			($ingredientname == "barbecue sauce") ||
			($ingredientname == "beef broth") ||
			($ingredientname == "brewed coffee") ||
			($ingredientname == "champagne vinegar") ||
			($ingredientname == "chicken broth") ||
			($ingredientname == "chili oil") ||
			($ingredientname == "coconut aminos or tamari") ||
			($ingredientname == "coconut cream") ||
			($ingredientname == "coconut milk") ||
			($ingredientname == "coconut oil") ||
			($ingredientname == "coconut water") ||
			($ingredientname == "cooking sherry") ||
			($ingredientname == "cranberry juice") ||
			($ingredientname == "dry vermouth") ||
			($ingredientname == "enchilada sauce") ||
			($ingredientname == "evaporated milk") ||
			($ingredientname == "fish sauce") ||
			($ingredientname == "flax milk") ||
			($ingredientname == "honey") ||
			($ingredientname == "hot sauce") ||
			($ingredientname == "kombucha") ||
			($ingredientname == "maple syrup") ||
			($ingredientname == "marinara sauce") ||
			($ingredientname == "marsala wine") ||
			($ingredientname == "molasses") ||
			($ingredientname == "olive oil") ||
			($ingredientname == "orange juice") ||
			($ingredientname == "pickle juice") ||
			($ingredientname == "pineapple juice") ||
			($ingredientname == "red wine") ||
			($ingredientname == "red wine vinegar") ||
			($ingredientname == "rice vinegar") ||
			($ingredientname == "salsa") ||
			($ingredientname == "salsa verde") ||
			($ingredientname == "sesame oil") ||
			($ingredientname == "sherry vinegar") ||
			($ingredientname == "sweet and sour sauce") ||
			($ingredientname == "toasted sesame oil") ||
			($ingredientname == "tomato paste") ||
			($ingredientname == "tomato sauce") ||
			($ingredientname == "vegetable broth") ||
			($ingredientname == "vinegar") ||
			($ingredientname == "white balsamic vinegar") ||
			($ingredientname == "white wine") ||
			($ingredientname == "white wine vinegar") ||
			($ingredientname == "worcestershire sauce")
			*/
			
			if(
				!(
					($ingredientname == "water") 
				) &&
				($value[2] != 'Spices')
			  ) {
				  $ingredientsAndRecipes;
				  
				  $view["Summary"][] = $value[0] . ' ' . $value[1] . ' ' . $ingredientname;
			  } else {
				  $view["Summary"][] = $ingredientname;
			  }
		}
		
		$view["Recipes"] = $allRecipe;
		
		return $view;
	}		
?>