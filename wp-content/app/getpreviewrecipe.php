<?php
	// This should be the same as previewrecipe.php
	// It's for directly adding recipe to plan, instead through recipe detail lightbox

	function fraction($decimal){
		
		$big_fraction = float2rat($decimal);
		
		if($big_fraction === "33/100")
			$big_fraction = "1/3";	

		if($big_fraction === "3/10")
			$big_fraction = "1/3";		
			
		if($big_fraction === "67/100")
			$big_fraction = "2/3";				
		
		return $big_fraction;
	}
	
	function minutestohours($minutes){
		
		$minutesarr = explode(" ", $minutes);
		
		if($minutesarr[0] >= 60) {
			
			$hour = (int) $minutesarr[0] / 60;
			$minutes = $minutesarr[0] - ($hour*60);
			
			if($hour == 1)
				$hour = $hour . ' hour';
			else 
				$hour = $hour . ' hours';
			
			if($minutes == 0)
				$minutes = '';
			else if($minutes == 1)
				$minutes = $minutes . ' $minute';
			else 
				$minutes = $minutes . ' $minutes';
						
			return $hour . ' ' . $minutes;
		}
		
		return $minutes;
	}	
	
	function previewrecipe($recipeid) {		
		global $wpdb;
		$single = true;
				
		$title = get_the_title( $recipeid );
		$yield = get_post_meta( $recipeid, 'recipe_servings_normalized', $single );
		$readytime = get_post_meta( $recipeid, 'recipe_prep_time', $single );
		$readytime .= " " . get_post_meta( $recipeid, 'recipe_prep_time_text', $single );
		$readytime = minutestohours($readytime);
		$cooktime = get_post_meta( $recipeid, 'recipe_cook_time', $single );
		$cooktime .= " " . get_post_meta( $recipeid, 'recipe_cook_time_text', $single );
		$cooktime = minutestohours($cooktime);
		$difficulty = get_post_meta( $recipeid, 'recipe_difficulty', $single );
		$thumb = wp_get_attachment_url( get_post_thumbnail_id($recipeid) );
		$ingredients = get_post_meta( $recipeid, 'recipe_ingredients', $single );
		$instructions = get_post_meta( $recipeid, 'recipe_instructions', $single );
		$cats = get_the_terms($recipeid, 'category');	
		$current_user = wp_get_current_user();
		$fav = get_user_meta( $current_user->ID, 'favorite-' . $recipeid, $single );
			
		$star = '';
		
		if ( $fav == 'yes' ) {
			$star = ' fullstar';
		}
		else {
			$star = ' blankstar';
		}	
						
		foreach ( $cats as $cat ) {
			$category = $cat->name;
		}
		
		$ingCount = 1;
		$ingEl = '';
	
		foreach( $ingredients as $ingredient ) {	
		
			$key = $ingredient["ingredient_id"];
			$term = get_term( $key, "ingredient" );
			$ingredient["group"] = get_term( $term->parent, "ingredient" )->name;				
					
			$frac = fraction($ingredient["amount_normalized"]);
			$frac = ($frac == "0")? "" : $frac;
		
			if(trim($ingredient["notes"]) != "")
				$ingEl .= $frac . ' ' . $ingredient["unit"] . ' ' . $ingredient["ingredient"] . ' (' . $ingredient["notes"] . ')<br>';	
			else
				$ingEl .= $frac . ' ' . $ingredient["unit"] . ' ' . $ingredient["ingredient"] . '<br>';
				
			$ingCount++;
		}
		
		$insCount = 1;
		$insEl = '';
		foreach( $instructions as $instruction ) {
			$insEl .= $insCount . '. ' . $instruction["description"] . '<br>';			
			$insCount++;
		}

		$de = '^%$^';
		
		if($difficulty[0] == 1)
			$difficulty = 'Very Easy';
		else if($difficulty[0] == 2)
			$difficulty = 'Easy';
		else if($difficulty[0] == 3)
			$difficulty = 'Moderate';
		else if($difficulty[0] == 4)
			$difficulty = 'Difficult';
		else
			$difficulty = 'Very Difficult';				

		return $title . $de . $yield . $de . $readytime . $de . $thumb . $de . $ingEl . $de .  $insEl . $de . $category . $de . $cooktime . $de . $difficulty . $de . $star . $de . $recipeid;
	}
?>