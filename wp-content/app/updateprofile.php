<?php 
	require_once('../../.wordpress/wp-load.php');
	 
	$response = array(
		'data'		=> array(),
		'msg'		=> 'Unknown error',
		'status'	=> false
	);

	/* Sanitize all received posts */
	foreach($_POST as $k => $value){
		$_POST[$k] = sanitize_text_field($value);
	}
	
	/**
	 * Register Method
	 *
	 */
	 
	 /* Get user data */
	if( isset( $_POST['userid'] ) && isset( $_POST['firstname'] ) && isset( $_POST['email'] ))
	{
	 
		/* Get user data */
		$userid = $_POST['userid'];
		$username = $_POST['username'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$response['status'] = 5;
			$response['msg'] = 'Invalid Email';
			
			echo json_encode( $response );
		}
		
		$user = get_user_by( 'ID', $userid );

		if ( $user ) {

			$userdata = array(
				'ID' =>  $userid,
				'user_login'   =>  $username,
				'user_email'   =>  $email,
				'user_pass'  =>  md5($password), 
				'first_name'  =>  $firstname, 
				'last_name'  =>  $lastname 
		  	);

			if(empty($password))
				$userdata = array(
					'ID' =>  $userid,
					'user_login'   =>  $username,
					'user_email'   =>  $email,
					'first_name'  =>  $firstname, 
					'last_name'  =>  $lastname 
				);
		   
			$user_id = wp_insert_user( $userdata ) ;
		} else {
			$response['status'] = 3;
			$response['msg'] = 'User not exist';
			
			echo json_encode( $response );	
			exit();
		}

		if (is_wp_error($user_id)) {
			$response['status'] = 2;
			$response['msg'] = 'Unknown error';
			
			echo json_encode($response);	
		} else {
				
				try {
				$user = get_user_by( 'email', $email );
				$token = md5( $email . $password . rand() );
				
				$response['status'] = 1;
				$response['data'] = array(
					'auth_token' 	=>	$token,
					'user_id'		=>	$user->ID,
					'user_login'	=>	$user->user_login,
					'user_email'	=>	$user->user_email,
					'user_firstname' => $user->user_firstname,
					'user_lastname' => $user->user_lastname
				);
				
				$response['msg'] = 'Successfully Registered and Authenticated';
				
				echo json_encode($response);
					
				} catch (Exception $ex) {
					$response['status'] = 2;
					$response['msg'] = 'Unknown error';
				
				echo json_encode($response);
					
				}
				
		}
	} 
	else 
	{			
		$response['status'] = 4;
		$response['msg'] = 'Empty Fields';
		
		echo json_encode( $response );	
	}
	
	exit();
?>